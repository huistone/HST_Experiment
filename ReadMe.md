### 编辑器记得安装lombok插件

### HST_Library
![https://img.shields.io/badge/license-Apache%202.0-blue.svg?longCache=true&style=flat-square](https://img.shields.io/badge/license-Apache%202.0-blue.svg?longCache=true&style=flat-square)
![https://img.shields.io/badge/springboot-2.2.1-yellow.svg?style=flat-square](https://img.shields.io/badge/springboot-2.2.1-yellow.svg?style=flat-square)
![https://img.shields.io/badge/shiro-1.4.2-orange.svg?longCache=true&style=flat-square](https://img.shields.io/badge/shiro-1.4.2-orange.svg?longCache=true&style=flat-square)
![https://img.shields.io/badge/layui-2.5.5-brightgreen.svg?longCache=true&style=flat-square](https://img.shields.io/badge/layui-2.5.5-brightgreen.svg?longCache=true&style=flat-square)

HST_Library是一款简单高效的在线实验平台，使用Spring Boot，Shiro和Layui构建。权限框架基于FEBS_Shiro构建

> 实验教学平台通过校方教务处制定每个学期的教学目标，老师根据教学目标来安排教学计划和实验课程，学生根据实验课的安排进行实验课的预约，按照老师的实验安排对实验课和作业进行在线提交；老师可以在线管理学生的实验报告和实验作业并进行批改；学生可以实时查看到实验结果；教务处根据实验室及设备的占用情况和利用率进行对外开发，极大的提高是实验课的效率和利用率，同时提高了学生的动手能力。本平台适用于各类大专院校，对实验教学的管理。

### 演示地址

正在不断开发完善中，有兴趣一起开发的可以给我留言。

### 系统模块
系统功能模块组成如下所示：
```
├─系统管理
│  ├─用户管理
│  ├─角色管理
│  ├─菜单管理
│  └─部门管理
├─系统监控
│  ├─在线用户
│  ├─系统日志
│  ├─登录日志
│  ├─请求追踪
│  ├─系统信息
│  │  ├─JVM信息
│  │  ├─TOMCAT信息
│  │  └─服务器信息
├─任务调度
│  ├─定时任务
│  └─调度日志
├─代码生成
│  ├─生成配置
│  ├─代码生成
└─其他模块
   ├─FEBS组件
   │  ├─表单组件
   │  ├─表单组合
   │  ├─FEBS工具
   │  ├─系统图标
   │  └─其他组件
   ├─APEX图表
   ├─高德地图
   └─导入导出
```
### 系统特点

1. 前后端请求参数校验

2. 支持Excel导入导出

3. 前端页面布局多样化，主题多样化

4. 支持多数据源，代码生成

5. 多Tab页面，适合企业应用

6. 用户权限动态刷新

7. 浏览器兼容性好，页面支持PC，Pad和移动端。

8. 代码简单，结构清晰

### 技术选型

#### 后端
- [Spring Boot 2.2.1](http://spring.io/projects/spring-boot/)
- [Mybatis-Plus](https://mp.baomidou.com/guide/)
- [MySQL 5.7.x](https://dev.mysql.com/downloads/mysql/5.7.html#downloads),[Hikari](https://brettwooldridge.github.io/HikariCP/),[Redis](https://redis.io/)
- [Shiro 1.4.2](http://shiro.apache.org/)

#### 前端
- [Layui 2.5.5](https://www.layui.com/)
- [Nepadmin](https://gitee.com/june000/nep-admin)
- [formSelects 4.x 多选框](https://hnzzmsf.github.io/example/example_v4.html)
- [eleTree 树组件](https://layuiextend.hsianglee.cn/eletree/)
- [formSelect.js树形下拉](https://wujiawei0926.gitee.io/treeselect/docs/doc.html)
- [Apexcharts图表](https://apexcharts.com/)

### 系统截图

#### PC端
![screenshot](screenshot/pc_screenshot_1.jpg)
![screenshot](screenshot/pc_screenshot_2.jpg)
![screenshot](screenshot/pc_screenshot_3.jpg)
![screenshot](screenshot/pc_screenshot_4.jpg)
![screenshot](screenshot/pc_screenshot_5.jpg)
![screenshot](screenshot/pc_screenshot_6.jpg)

#### 手机
![screenshot](screenshot/mobile_screenshot_1.jpg)
![screenshot](screenshot/mobile_screenshot_2.jpg)
#### Pad
![screenshot](screenshot/pad_screenshot_1.jpg)
![screenshot](screenshot/pad_screenshot_2.jpg)
![screenshot](screenshot/pad_screenshot_3.jpg)
### 浏览器兼容
|[<img src="https://raw.github.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_48x48.png" alt="Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE| [<img src="https://images.gitee.com/uploads/images/2020/0324/155504_ff6701a3_1668143.png" alt="Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://images.gitee.com/uploads/images/2020/0324/155508_6fc155ce_1668143.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://images.gitee.com/uploads/images/2020/0324/155507_8d96220f_1668143.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://images.gitee.com/uploads/images/2020/0324/155505_6faaf395_1668143.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |[<img src="https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Opera
| --------- | --------- | --------- | --------- | --------- |--------- |
|IE 10+| Edge| last 15 versions| last 15 versions| last 10 versions| last 15 versions
### 参与贡献
欢迎提交PR一起完善项目，以下为提PR并合并的小伙伴（排名不分先后）：

<a href="https://github.com/everhopingandwaiting">
    <img src="https://images.gitee.com/uploads/images/2020/0324/155512_89b9505b_1668143.png" width="45px"></a>
<a href="https://github.com/mgzu">
    <img src="https://images.gitee.com/uploads/images/2020/0324/155510_2105f20d_1668143.jpeg" width="45px"></a>
<a href="https://github.com/yuuki80code">
    <img src="https://avatars0.githubusercontent.com/u/17798853?s=400&v=4" width="45px"></a>
<a href="https://github.com/cinsin">
    <img src="https://avatars1.githubusercontent.com/u/12856067?s=400&v=4" width="45px"></a>
<a href="https://github.com/Minnull">
    <img src="https://avatars2.githubusercontent.com/u/19608781?s=400&v=4" width="45px"></a>
<a href="https://github.com/Harrison0x80">
    <img src="https://avatars2.githubusercontent.com/u/8622915?s=400&v=4" width="45px"></a>
<a href="https://github.com/notlcry">
    <img src="https://avatars2.githubusercontent.com/u/1989218?s=400&v=4" width="45px"></a>
<a href="https://github.com/gelibo">
    <img src="https://images.gitee.com/uploads/images/2020/0324/155515_2b4fc52e_1668143.png" width="45px"></a>
<a href="https://github.com/FiseTch">
    <img src="https://avatars0.githubusercontent.com/u/29654322?s=400&v=4" width="45px"></a>
<a href="https://github.com/pangPython">
    <img src="https://images.gitee.com/uploads/images/2020/0324/155518_4774dd57_1668143.jpeg" width="45px"></a>
<a href="https://github.com/atsushinee">
    <img src="https://avatars2.githubusercontent.com/u/26084189?s=400&v=4" width="45px"></a>
<a href="https://github.com/liuzhuoming23">
    <img src="https://avatars1.githubusercontent.com/u/41719224?s=400&v=4" width="45px"></a>
                
### 反馈交流

有问题请留言。

