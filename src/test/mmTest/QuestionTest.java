package mmTest;

import cc.mrbird.febs.FebsApplication;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.AnalogBase;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IExperimentRemarkService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName QuestionTest
 * @Description TODO
 * @Author admin
 * @Date 2021/1/14 9:23
 * @Version 1.0
 */
@SpringBootTest(classes = FebsApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class QuestionTest {
    @Resource
    private IExperimentQuestionService experimentQuestionService;

    @Resource
    private IExperimentRemarkService experimentRemarkService;

    @Resource
    private IExperimentAnswerService experimentAnswerService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IUserCommitService userCommitService;
    
    @Test
    public void test3(){

    }

    @Test
    public void test2(){
        List<ExperimentProject> list = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getState,1));
        for (ExperimentProject experimentProject : list) {
            experimentProject.setProjectPrice(BigDecimal.valueOf(1));
            System.out.println(experimentProject.getProjectPrice());
            experimentProjectService.updateById(experimentProject);
        }
    }

    @Test
    public void test1(){
        List<ExperimentRemark> list = experimentRemarkService.list();
        for (ExperimentRemark experimentRemark : list) {
            experimentAnswerService.update(new UpdateWrapper<ExperimentAnswer>().lambda()
                .eq(ExperimentAnswer::getCreateId, experimentRemark.getRemarkId())
                .set(ExperimentAnswer::getDeptId, experimentRemark.getDeptId()));
        }
    }

}
