package mmTest;

import cc.mrbird.febs.FebsApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/17/10:27
 * @ Description:
 */
@SpringBootTest(classes = FebsApplication.class)
@RunWith(SpringRunner.class)
public class scoketTest {



    @Test
    public void client() throws IOException {
//        1,创建socket，并指定端口
        Socket socket = new Socket("192.168.2.137", 20108);
//        2，想服务端发送请求
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
        printWriter.print("AABBCC31");
        printWriter.flush();
        socket.shutdownOutput();
//        3,等待服务端的响应，获取输入流，读取客户端信息，将字节流转换为字符流
        InputStreamReader reader = new InputStreamReader(socket.getInputStream());
        BufferedReader bufferedReader = new BufferedReader(reader);
        System.out.println(bufferedReader.readLine());
        String info = null;
        while ((info = bufferedReader.readLine()) != null) {
            System.out.println("收到：" + info);
        }
//        关闭输入流
        socket.shutdownInput();
        printWriter.close();
        reader.close();
        bufferedReader.close();
        socket.close();
    }

    //发送端的代码如下：
    @Test
    public void TT() {
        try {
            Socket socket = new Socket("192.168.2.137", 20108);
            OutputStream oos = socket.getOutputStream();
//获取到输出流
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(oos));
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String line = null;
            System.out.println("dddddddddd "+socket);
            while ((line = br.readLine()) != null) {
                System.out.println("----"+line);
                bw.write(line);
                bw.flush();
//将内容写到控制台
                System.out.println(line);
            }
            System.out.println("1111111111111111  "+line);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    private static final int PORT = 8888;

    @Test
    public void server() throws IOException {
        ServerSocket serverSocket = null;
        Socket socket = null;
        try {
            //建立服务器的Socket，并设定一个监听的端口PORT
            serverSocket = new ServerSocket(PORT);
            //由于需要进行循环监听，因此获取消息的操作应放在一个while大循环中
            while (true) {
                try {
                    //建立跟客户端的连接
                    socket = serverSocket.accept();
                } catch (Exception e) {
                    System.out.println("建立与客户端的连接出现异常");
                    e.printStackTrace();
                }
                ServerThread thread = new ServerThread(socket);
                thread.start();
            }
        } catch (Exception e) {
            System.out.println("端口被占用");
            e.printStackTrace();
        } finally {
            serverSocket.close();
        }
    }
}

//服务端线程类
//继承Thread类的话，必须重写run方法，在run方法中定义需要执行的任务。
class ServerThread extends Thread {
    private Socket socket;
    InputStream inputStream;
    OutputStream outputStream;

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            while (true) {
                //接收客户端的消息并打印
                System.out.println(socket);
                inputStream = socket.getInputStream();
                byte[] bytes = new byte[1024];
                inputStream.read(bytes);
                String string = new String(bytes);
                System.out.println(string);

                //向客户端发送消息
                outputStream = socket.getOutputStream();
                outputStream.write("OK".getBytes());
                System.out.println("OK");

            }
        } catch (Exception e) {
            System.out.println("客户端主动断开连接了");
            //e.printStackTrace();
        }
        //操作结束，关闭socket
        try {
            socket.close();
        } catch (IOException e) {
            System.out.println("关闭连接出现异常");
            e.printStackTrace();
        }
    }

}
