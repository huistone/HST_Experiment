package json;
import cc.mrbird.febs.FebsApplication;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.io.*;
import java.util.*;


/**
 * @ Author 马超伟
 * @ Date 2020-10-15 08:42
 * @ Description:
 * @ Version:
 */
@SpringBootTest(classes = FebsApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class JsonTest {

    //读取json文件
    public static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static void main(String[] args) {
        String path = JsonTest.class.getClassLoader().getResource("Movie.json").getPath();
        String s = readJsonFile(path);
        JSONObject jobj = JSON.parseObject(s);
        JSONArray movies = jobj.getJSONArray("RECORDS");//构建JSONArray数组
        for (int i = 0 ; i < movies.size();i++){
            JSONObject key = (JSONObject)movies.get(i);
            String name = (String)key.get("name");
            String director = (String)key.get("director");
            String scenarist=((String)key.get("scenarist"));
            String actors=((String)key.get("actors"));
            String type=((String)key.get("type"));
            String ratingNum=((String)key.get("ratingNum"));
            String tags=((String)key.get("tags"));
            System.out.println(name);
            System.out.println(director);
            System.out.println(scenarist);
            System.out.println(actors);
            System.out.println(type);
            System.out.println(director);
            System.out.println(ratingNum);
            System.out.println(tags);
        }
    }


    private static final Gson gson = new Gson();

    /**
     * 比较两个bean是否等价
     */
    public static boolean same(Object a, Object b) {
        if (a == null) {
            return b == null;
        }
        return same(gson.toJson(a), gson.toJson(b));
    }

    /**
     * 比较两个json字符串是否等价
     */
    public static boolean same(String a, String b) {
        if (a == null) {
            return b == null;
        }
        if (a.equals(b)) {
            return true;
        }
        JsonElement aElement = JsonParser.parseString(a);
        JsonElement bElement = JsonParser.parseString(b);
        if (gson.toJson(aElement).equals(gson.toJson(bElement))) {
            return true;
        }
        return same(aElement, bElement);
    }

    private static boolean same(JsonElement a, JsonElement b) {
        if (a.isJsonObject() && b.isJsonObject()) {
            return same((JsonObject) a, (JsonObject) b);
        } else if (a.isJsonArray() && b.isJsonArray()) {
            return same((JsonArray) a, (JsonArray) b);
        } else if (a.isJsonPrimitive() && b.isJsonPrimitive()) {
            return same((JsonPrimitive) a, (JsonPrimitive) b);
        } else if (a.isJsonNull() && b.isJsonNull()) {
            return same((JsonNull) a, (JsonNull) b);
        } else {
            return Boolean.FALSE;
        }
    }

    private static boolean same(JsonObject a, JsonObject b) {
        Set<String> aSet = a.keySet();
        Set<String> bSet = b.keySet();
        if (!aSet.equals(bSet)) {
            return false;
        }
        for (String aKey : aSet) {
            if (!same(a.get(aKey), b.get(aKey))) {
                return false;
            }
        }
        return true;
    }

    private static boolean same(JsonArray a, JsonArray b) {
        if (a.size() != b.size()) {
            return false;
        }
        List<JsonElement> aList = toSortedList(a);
        List<JsonElement> bList = toSortedList(b);
        for (int i = 0; i < aList.size(); i++) {
            if (!same(aList.get(i), bList.get(i))) {
                return false;
            }
        }
        return true;
    }

    private static boolean same(JsonPrimitive a, JsonPrimitive b) {

        return a.equals(b);
    }

    private static boolean same(JsonNull a, JsonNull b) {
        return true;
    }

    private static List<JsonElement> toSortedList(JsonArray a) {
        List<JsonElement> aList = new ArrayList<>();
        a.forEach(aList::add);
        aList.sort(Comparator.comparing(gson::toJson));
        return aList;
    }

    @Test
    public  void test() {
//        List<String> obj1 = Arrays.asList("1", "2", "3");
//        List<String> obj2 = Arrays.asList("1", "3", "2");
//        System.out.println(same(obj1, obj2)); // true
//
//        String str1 = "[{\"a\":1},{\"b\":2},{\"c\":3}]";
//        String str2 = "[{\"a\":1},{\"c\":3},{\"b\":2}]";
//        System.out.println(same(str1, str2)); // true
        String json1 = "{\n" +
                " \"ID\": 100,\n" +
                " \"POT1\": 1000,\n" +
                " \"POT2\": 500,\n" +
                " \"FREQ\": 1000,\n" +
                " \"AMP\": 255,\n" +
                " \"PHA\": 120,\n" +
                " \"WAVE\": 0,\n" +
                " \"TASK\": 3,\n" +
                " \"PID\": 1\n" +
                "}";

        String  json2 = "{\n" +
                "  \"POT1\": 1000,\n" +
                "  \"POT2\": 500,\n" +
                "  \"FREQ\": 1000,\n" +
                "  \"AMP\": 255,\n" +
                "  \"PHA\": 120,\n" +
                "  \"WAVE\": 0,\n" +
                "  \"TASK\": 3,\n" +
                "  \"ID\": 100,\n" +
                "  \"PID\": 1\n" +
                "}\n";

        JSONObject jsonObject1 = JSONObject.parseObject(json1);
        JSONObject jsonObject2 = JSONObject.parseObject(json2);

        System.out.println(same(jsonObject1,jsonObject2));


    }



}
