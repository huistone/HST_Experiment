package cc.mrbird.febs.student.entity.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClassName StudentScoreVo
 * @ Description TODO
 * @Author wangke
 * @Data 2020-8-18 16:50
 * @Version 1.0
 */
@Data
public class StudentScoreVo {
    //预习题id
    private Long prepareRemarkId;

    //思考题id
    private Long thinkRemarkId;

    //项目id
    private Long projectId;

    private Long courseId;


    private String projectName;

    private Double  prepareScore;

    private Integer prepareCommit;

    private Double thinkScore;

    private Integer thinkCommit;

    private Double projectScore;

    private Integer projectCommit;

    private Double troubleScore;

    private Integer troubleCommit;

    private Long projectCommitId;

    private Double sum;

    private LocalDateTime createTime;

}
