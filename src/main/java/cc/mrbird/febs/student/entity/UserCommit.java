package cc.mrbird.febs.student.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 学生报告提交记录表
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_user_commit")
public class UserCommit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学生报告提交表
     */
    @TableId(value = "commit_id",type = IdType.AUTO)
    private Long commitId;

    /**
     * 学生ID
     */
    private Long stuId;

    /**
     * 学生姓名
     */
    @TableField(exist = false)
    private String stuName;

    /**
     * 学号
     */
    @TableField(exist = false)
    private String idNumber;

    /**
     * 学生班级ID
     */
    private Long deptId;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 学生做的项目ID
     */
    private Long projectId;

    /**
     * 项目名称
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 学生做的课程ID
     */
    private Long courseId;

    /**
     * 课程名称
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 项目负责人老师ID
     */
    private Long teacherId;

    /**
     * 老师姓名
     */
    @TableField(exist = false)
    private String teacherName;

    /**
     * 学生提交的报告URL
     */
    private String reportUrl;

    /**
     * 学生提交的报告URL
     */
    private String reportSize;

    /**
     * 学生成绩
     */
    private Double score;

    /**
     * 1提交，0，未提交，2，已批阅
     */
    private Integer isCommit;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;

    /**
     * 批阅时间
     */
    private LocalDateTime updateTime;

    /**
     * 批阅人
     */
    private String updateUser;

    /**
     * 提交时间
     */
    @TableField(exist = false)
    private String createTimes;

    /**
     * 1,正常老师开启，0关闭
     */
    private Integer useType;

    /**
     * 1正常，0删除
     */
    @TableLogic
    private Integer isDel;

    /**
     * 学生上传的次数
     */
    private Integer time;

    /**
     * 性别 0男 1女 2 保密
     */
    @TableField(exist = false)
    private String sex;

    /**
     * 1代表校园 0代表企业
     */
    private Integer userType;

}
