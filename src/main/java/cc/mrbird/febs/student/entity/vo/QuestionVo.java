package cc.mrbird.febs.student.entity.vo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QuestionVo {


    private Integer questionNumber;

    private String questionContent;

    private String questionAnswer;

    private String studentAnswer;

    private Integer status;

    private String remark;

    private Double score;

}
