package cc.mrbird.febs.student.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 实验思考题记录表
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_experiment_question")
public class ExperimentQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "question_id", type = IdType.AUTO)
    private Integer questionId;

    /**
     * 题目标题
     */
    private String questionName;

    /**
     * 题目内容
     */
    private String context;

    @TableField(exist = false)
    private List<String> contextList;

    /**
     * 参考答案
     */
    private String answer;

    /**
     * 题号
     */
    private Integer number;

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 课程ID
     */
    private Integer courseId;

    /**
     * 用于选择题的习题内容
     */

    /**
     * 1预习题，2思考题
     */
    private Integer type;

    /**
     * 项目名称  只作为传输字段 不入库
     */
    @TableField(exist = false)
    private String projectName;

    /**
     *保存选项列表  处理选择题 判断题
     */
    @TableField(exist = false)
    private List<String> optional;


    /**
     *题型
     * 2 判断
     * 3 单选
     * 4 多选
     */
    private Integer questionType;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 1正常，0删除
     */
    @TableLogic
    private Integer isDel;
}
