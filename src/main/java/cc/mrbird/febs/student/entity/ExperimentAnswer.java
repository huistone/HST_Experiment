package cc.mrbird.febs.student.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_experiment_answer")
public class ExperimentAnswer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "answer_id", type = IdType.AUTO)
    private Long answerId;

    /**
     * 题号
     */
    private Integer number;

    /**
     * 题目
     */
    @TableField(exist = false)
    private String title;

    /**
     * 答案内容
     */
    private String context;

    /**
     * 答案内容
     */
    @TableField(exist = false)
    private String contextQuestion;

    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 创建人ID
     */
    private Long createId;


    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后得分
     */
    private Double score;

    /**
     * 评分班级ID
     */
    private Long deptId;

    /**
     * 评分班级name
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 评分时间
     */
    private LocalDateTime scoreTime;

    /**
     * 问题ID
     */
    private Long questionId;

    /**
     * 1预习题，2思考题
     */
    private Integer type;

    /**
     * 创建人姓名
     */
    @TableField(exist = false)
    private String createUserName;

    /**
     * 习题所属的项目名称
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 0未提交 1已提交，2已批阅
     */
    private Integer isCommit;

    /**
     * 习题的类型，1预习题，2思考题
     */
    private Integer questionType;


    /**
     * 学生姓名
     */
    @TableField(exist = false)
    private String stuName;


    /**
     * 学号
     */
    @TableField(exist = false)
    private String idNumber;

    /**
     * 性别 0男 1女 2 保密
     */
    @TableField(exist = false)
    private String sex;

    /**
     * 区分类别, 1校园 0代表企业版
     */
    private Integer userType;


}
