package cc.mrbird.febs.student.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IMenuService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author admin
 */
@RequestMapping(FebsConstant.VIEW_PREFIX)
@Controller("studentView")
public class ViewController extends BaseController {

    @Autowired
    private IExperimentProjectService experimentProjectService;
    @Autowired
    private ITeacherProjectService teacherProjectService;

    @Autowired
    private IMenuService menuService;

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private ICourseService courseService;


    @GetMapping("/student/getOneExper/{courseId}")
    public String getOneProgect(@PathVariable Integer courseId, Model model) {
        model.addAttribute("courseId", courseId);
        return FebsUtil.view("student/myProject");
    }

    @GetMapping("/myProjectView/{projectId}")
    public String getViewHtml(@PathVariable Integer projectId, Model model) {
        ExperimentProject experimentProject = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getProjectId, projectId));
        TeacherProject teacherProject = teacherProjectService.getOne(new QueryWrapper<TeacherProject>().lambda().eq(TeacherProject::getProjectId, projectId));
        experimentProject.setTeacherId(teacherProject.getTeacherId().intValue());
        model.addAttribute("data", experimentProject);
        return FebsUtil.view("student/myProjectView");
    }

    @GetMapping("/student/index")
    public String getIndex(Model model) {
        User user = getCurrentUser();
        //则查询当前登录用户的课程列表
        List<CourseDept> courseDepts = courseDeptService.list(new QueryWrapper<CourseDept>().lambda().eq(CourseDept::getDeptId, user.getDeptId()));
        List<Course> courses = new ArrayList<>();
        for (CourseDept courseDept : courseDepts) {
            Course course = courseService.getById(courseDept.getCourseId());
            courses.add(course);
        }
        model.addAttribute("courses", courses);
        return FebsUtil.view("hst/index");
    }

}
