package cc.mrbird.febs.student.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.MenuTree;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.Menu;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IMenuService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author admin
 */
@RequestMapping(FebsConstant.VIEW_PREFIX+"/student")
@Controller
public class ProjectController extends BaseController {

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ITeacherProjectService teacherProjectService;

    @Autowired
    private IExperimentProjectService experimentProjectService;


    @RequestMapping("/project")
    public String turnMyProject(Model model){
        User user = getCurrentUser();
        try {
            MenuTree<Menu> userMenus = menuService.findUserMenus(user.getUsername());
            model.addAttribute("userMenus",userMenus);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FebsUtil.view("student/project");
    }

    @RequestMapping("/myExperiments/{courseId}")
    @ResponseBody
    public ModelAndView selectMyExperiments(@PathVariable Integer courseId, ExperimentProject experimentProject, Model model){
        //获取当前登录用户
        User currentUser = getCurrentUser();
        //获取当前登录的老师用户信息
        /*List<UserDept> list = deptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getDeptId, currentUser.getDeptId()));
        list.forEach(i->{

        });*/

        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getCourseId, courseId));
        //查询当前登录用户老师开启的全部项目
        if(one != null){
            List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                    .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                    .eq(TeacherProject::getCourseId, courseId)
                    .eq(TeacherProject::getTeacherId, one.getUserId()));

            List<ExperimentProject> experimentProjectList = this.experimentProjectService.list(new QueryWrapper<ExperimentProject>().lambda()
                    //课程号
                    .eq(ExperimentProject::getCourseId, courseId)
                    //开启的项目列表
                    .in(ExperimentProject::getProjectId, teacherProjectList.stream().map(TeacherProject::getProjectId).collect(Collectors.toList()))
                    //根据项目名的查询条件
                    .like(StringUtils.isNotBlank(experimentProject.getProjectName()), ExperimentProject::getProjectName, experimentProject.getProjectName())
                    //根据项目的状态条件查询
                    .like(experimentProject.getState() != null, ExperimentProject::getState, experimentProject.getState()));
            //封装返回结果集
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("/febs/views/student/myExperiments");
            modelAndView.addObject("data",experimentProjectList);
            return modelAndView;
        }
        return null;
    }
}
