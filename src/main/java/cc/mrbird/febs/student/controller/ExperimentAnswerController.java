package cc.mrbird.febs.student.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
@RestController
@RequestMapping("/experiment-answer")
public class ExperimentAnswerController extends BaseController {

}
