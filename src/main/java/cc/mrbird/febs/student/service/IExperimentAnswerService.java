package cc.mrbird.febs.student.service;

import cc.mrbird.febs.student.entity.ExperimentAnswer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
public interface IExperimentAnswerService extends IService<ExperimentAnswer> {
        Double getTotalScore(Long studentId,Long projectId,Long deptId);
}
