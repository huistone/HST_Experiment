package cc.mrbird.febs.student.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.entity.vo.StudentScoreVo;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 学生报告提交记录表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
public interface IUserCommitService extends IService<UserCommit> {

    /**
     * wangke
     * 获取某个学生的预习题思考题成绩
     *
     * @ Param studentId
     * @ Param projectId
     * @return
     */
    List<Object> getStudentCommit(Long studentId, Long deptId, Long projectId, Long courseId);

    /**
    * @ Description: 查询该班级下的学生实验报告提交情况
    * @ Param: [pageNum, pageSize, deptId, projectId, courseId]
    * @ return: java.util.List<cc.mrbird.febs.student.entity.UserCommit>
    * @ Author: 马超伟
    * @ Date: 2020/7/3
    */
    List<UserCommit>  listCommit(Integer pageNum,Integer pageSize,Long deptId, Long projectId, Long courseId);
    Integer  listCommitCount(Integer pageNum,Integer pageSize,Long deptId, Long projectId, Long courseId);

    /**
     * 查询实验报告总成绩
     * @ Param deptId
     * @ Param stuId
     * @ Param courseId
     * @ Param list
     * @ return
     */
    Double selectProjectScore(Long deptId, Long stuId,  Long courseId, List<Long> list);

    UserCommit  selectSingleProject( Long stuId,
                                     Long deptId,
                                     Long courseId,
                                     Long projectId);
    List<UserCommit> selectStudentScore(List<Long> projectId, QueryRequest request, Long userId, Long deptId) ;

    Integer studentScoreNum(List<Long> projectId, Long userId, Long deptId);

    IPage<StudentScoreVo> selectStudentScoreByMobile(List<Long> projectIds, QueryRequest request, Long userId, Long deptId, Integer userType);
}
