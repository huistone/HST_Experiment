package cc.mrbird.febs.student.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.mapper.ExperimentQuestionMapper;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.vo.QuestionStatisticsVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 实验思考题记录表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class ExperimentQuestionServiceImpl extends ServiceImpl<ExperimentQuestionMapper, ExperimentQuestion> implements IExperimentQuestionService {

    @Override
    public IPage<QuestionStatisticsVo> selectQuestionStatisticsList(Page<QuestionStatisticsVo> questionPage, Integer projectId, Integer type,Integer questionType) {

        return this.baseMapper.selectQuestionStatisticsList(questionPage,projectId,type,questionType);
    }
}
