package cc.mrbird.febs.student.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.entity.vo.StudentScoreVo;
import cc.mrbird.febs.student.mapper.ExperimentAnswerMapper;
import cc.mrbird.febs.student.mapper.ExperimentQuestionMapper;
import cc.mrbird.febs.student.mapper.UserCommitMapper;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IExperimentRemarkService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.units.qual.C;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 学生报告提交记录表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@Service
@Slf4j
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class UserCommitServiceImpl extends ServiceImpl<UserCommitMapper, UserCommit> implements IUserCommitService {

    @Resource
    private UserCommitMapper userCommitMapper;

    @Resource
    private IExperimentRemarkService experimentRemarkService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Override
    public  List<Object> getStudentCommit(Long studentId, Long deptId,Long projectId,Long courseId){
        Map<String, Object> map = new HashMap<>();
        List<Object> list = new ArrayList<>();
        ExperimentProject one = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getCourseId, courseId).eq(ExperimentProject::getProjectId,projectId).last("limit 1"));
        //预习题成绩
        ExperimentRemark prepareAnswer = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                .lambda()
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getCreateId, studentId)
                .eq(ExperimentRemark::getDeptId, deptId)
                .eq(ExperimentRemark::getUserType,1)
                .eq(ExperimentRemark::getType, 1)
                .eq(ExperimentRemark::getIsDel,1));
        if(prepareAnswer!=null){
            prepareAnswer.setProjectName(one.getProjectName());
        }
        //思考题成绩
        ExperimentRemark thinkAnswer = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                .lambda()
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getCreateId, studentId)
                .eq(ExperimentRemark::getDeptId, deptId)
                .eq(ExperimentRemark::getUserType,1)
                .eq(ExperimentRemark::getType, 2)
                .eq(ExperimentRemark::getIsDel,1));
        if(thinkAnswer!=null){
            thinkAnswer.setProjectName(one.getProjectName());
        }
        //查询实验报告成绩
        UserCommit userCommit = this.baseMapper.selectOne(new QueryWrapper<UserCommit>()
                .lambda()
                .eq(UserCommit::getStuId, studentId)
                .eq(UserCommit::getDeptId, deptId)
                .eq(UserCommit::getUserType,1)
                .eq(UserCommit::getCourseId, courseId)
                .eq(UserCommit::getProjectId, projectId));
        if(userCommit!=null){
            userCommit.setProjectName(one.getProjectName());
        }
        map.put("prepareAnswer",prepareAnswer);
        map.put("thinkAnswer",thinkAnswer);
        map.put("projectReport",userCommit);
        list.add(prepareAnswer);
        list.add(thinkAnswer);
        list.add(userCommit);
        return list;
    }

    @Override
    public List<UserCommit> listCommit(Integer pageNum, Integer pageSize, Long deptId, Long projectId, Long courseId) {
        pageNum=((pageNum-1)*pageSize);
        List<UserCommit> userCommits = this.baseMapper.listCommit(pageNum, pageSize, deptId, projectId, courseId);
        return userCommits;
    }

    @Override
    public Integer listCommitCount(Integer pageNum, Integer pageSize, Long deptId, Long projectId, Long courseId) {
        pageNum=((pageNum-1)*pageSize);
        return  this.baseMapper.listCommitCount(pageNum, pageSize, deptId, projectId, courseId);
    }

    @Override
    public Double selectProjectScore(Long deptId, Long stuId, Long courseId, List<Long> list) {
        return userCommitMapper.selectProjectScore(deptId,stuId,courseId,list);
    }

    @Override
    public UserCommit selectSingleProject(Long stuId, Long deptId, Long courseId, Long projectId) {
        return userCommitMapper.selectSingleProject(stuId,deptId,courseId,projectId);
    }

    @Override
    public List<UserCommit> selectStudentScore(List<Long> projectId, QueryRequest request,Long userId,Long deptId) {
        int pageNum = request.getPageNum();
        int pageSize = request.getPageSize();
        pageNum = (pageNum-1) * pageSize;
        return baseMapper.selectStudentScoreList(projectId,pageNum,pageSize,userId,deptId);
    }

    @Override
    public Integer studentScoreNum(List<Long> projectId, Long userId, Long deptId) {
        return baseMapper.studentScoreCount(projectId,userId,deptId);
    }

    @Override
    public IPage<StudentScoreVo> selectStudentScoreByMobile(List<Long> projectIds, QueryRequest request, Long userId, Long deptId, Integer userType) {
        Page<StudentScoreVo> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.selectStudentScoreByMobile(page,projectIds,userId,deptId,userType);
    }
}
