package cc.mrbird.febs.student.service.impl;

import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.mapper.ExperimentAnswerMapper;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class ExperimentAnswerServiceImpl extends ServiceImpl<ExperimentAnswerMapper, ExperimentAnswer> implements IExperimentAnswerService {

    @Resource
    private ExperimentAnswerMapper answerMapper;

    @Override
    public Double getTotalScore(Long studentId,Long projectId,Long deptId) {
        return answerMapper.getTotalScore(studentId,projectId,deptId);
    }
}
