package cc.mrbird.febs.student.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.system.entity.vo.QuestionStatisticsVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 实验思考题记录表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
public interface IExperimentQuestionService extends IService<ExperimentQuestion> {

    IPage<QuestionStatisticsVo> selectQuestionStatisticsList(Page<QuestionStatisticsVo> questionPage, Integer projectId, Integer type,Integer questionType);
}
