package cc.mrbird.febs.student.mapper;

import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.system.entity.vo.QuestionStatisticsVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 实验思考题记录表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
public interface ExperimentQuestionMapper extends BaseMapper<ExperimentQuestion> {

    IPage<QuestionStatisticsVo> selectQuestionStatisticsList(@Param("page") Page<QuestionStatisticsVo> questionPage
            ,@Param("projectId") Integer projectId
            ,@Param("type") Integer type
            ,@Param("questionType") Integer questionType);
}
