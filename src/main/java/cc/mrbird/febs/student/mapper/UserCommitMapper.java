package cc.mrbird.febs.student.mapper;

import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.entity.vo.StudentScoreVo;
import cc.mrbird.febs.system.entity.ExperimentProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;

import java.util.List;

/**
 * <p>
 * 学生报告提交记录表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
public interface UserCommitMapper extends BaseMapper<UserCommit> {


    Integer listCommitCount(@ Param("pageNum") Integer pageNum, @ Param("pageSize")Integer pageSize, @ Param("deptId")Long deptId,@ Param("projectId") Long projectId, @ Param("courseId")Long courseId);

    List<UserCommit> listCommit(@ Param("pageNum") Integer pageNum, @ Param("pageSize")Integer pageSize, @ Param("deptId")Long deptId,@ Param("projectId") Long projectId, @ Param("courseId")Long courseId);

    Double selectProjectScore(@ Param("deptId") Long deptId,@ Param("stuId") Long stuId,@ Param("courseId") Long courseId,@ Param("ids") List<Long> list);

    UserCommit  selectSingleProject(@ Param("stuId") Long stuId,
                                    @ Param("deptId") Long deptId,
                                    @ Param("courseId") Long courseId,
                                    @ Param("projectId") Long projectId);

    List<UserCommit> selectStudentScoreList(
            @ Param("projectIds") List<Long> projectId
            ,@ Param("pageNum") Integer pageNum,
            @ Param("pageSize") Integer pageSize,
            @ Param("userId") Long userId,
            @ Param("deptId") Long deptId);


    Integer studentScoreCount(@ Param("projectIds") List<Long> projectId,
                              @ Param("userId") Long userId,
                              @ Param("deptId") Long deptId);

    IPage<StudentScoreVo> selectStudentScoreByMobile(Page<StudentScoreVo> page
            , @Param("projectIds") List<Long> projectIds
            , @Param("userId") Long userId
            , @Param("deptId") Long deptId
            , @Param("userType") Integer userType);
}
