package cc.mrbird.febs.student.mapper;

import cc.mrbird.febs.student.entity.ExperimentAnswer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-28
 */
public interface ExperimentAnswerMapper extends BaseMapper<ExperimentAnswer> {
    Double getTotalScore(@ Param("studentId") Long studentId, @ Param("projectId") Long projectId,@ Param("deptId") Long deptId);
}
