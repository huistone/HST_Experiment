package cc.mrbird.febs.external.mqtt;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * 发布端
 * Title:Server
 * Description: 服务器向多个客户端推送主题，即不同客户端可向服务器订阅相同主题
 **/
@Component
@Slf4j
public class  MQTTPublish {

    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTPublish.class);

    public MqttClient client;
    public MqttTopic topic;
    public MqttMessage message;

    private static MQTTConnect mqttConnect = new MQTTConnect();

    public MQTTPublish() throws MqttException {
        connect();
    }

    public void connect()  {
        //防止重复创建MQTTClient实例
        try {
            if (client == null) {
                //就是这里的clientId，服务器用来区分用户的，不能重复
                // MemoryPersistence设置clientid的保存形式，默认为以内存保存
                client = new MqttClient(MQTTConnect.url, MQTTConnect.publishID, new MemoryPersistence());
            }
            MqttConnectOptions options = mqttConnect.getOptions();
            //判断连接状态，这里注意一下，如果没有这个判断，是非常坑的
            if (!client.isConnected()) {
                client.connect(options);
                LOGGER.info("----------------connect-----连接成功");
            } else {//这里的逻辑是如果连接成功就重新连接
                client.disconnect();
                client.connect(mqttConnect.getOptions(options));
                LOGGER.info("------------------disconnect---连接成功");
            }
        } catch (MqttException e) {
            log.info("MQTTPublish:"+e.getMessage());
        }
    }


    /**
     * MQTT发送指令
     * // @ Param page
     * // @ Param equipment
     * @ return
     * @ throws MqttException
     */
    public boolean sendMQTTMessage(String topic, String data)  {
        try {
            MQTTPublish server = new MQTTPublish();
            server.topic = server.client.getTopic(topic);
            server.message = new MqttMessage();
            server.message.setQos(2);           //消息等级//level 0：最多一次的传输  //level 1：至少一次的传输，(鸡肋)  //level 2： 只有一次的传输
            server.message.setRetained(false);  //如果重复消费，则把值改为true,然后发送一条空的消息，之前的消息就会覆盖，然后在改为false
            server.message.setPayload(data.getBytes());
            return server.publish(server.topic, server.message);
        } catch (MqttException e) {
            return false;
        }
    }

    public boolean publish(MqttTopic topic, MqttMessage message) throws MqttPersistenceException,MqttException {
        MqttDeliveryToken token = topic.publish(message);
        token.waitForCompletion();
        LOGGER.info("MQTT消息发布："
                + token.isComplete());
        return token.isComplete();
    }

    public static void main(String[] args) throws MqttException {
        MQTTPublish server = new MQTTPublish();
        String data = "Mr.Wang";
        server.topic = server.client.getTopic("topic");
        server.message = new MqttMessage();
        server.message.setQos(2);           //消息等级//level 0：最多一次的传输  //level 1：至少一次的传输，(鸡肋)  //level 2： 只有一次的传输
        server.message.setRetained(false);  //如果重复消费，则把值改为true,然后发送一条空的消息，之前的消息就会覆盖，然后在改为false
        server.message.setPayload(data.getBytes());
        server.publish(server.topic, server.message);
    }



}
