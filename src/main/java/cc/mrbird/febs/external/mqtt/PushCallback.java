package cc.mrbird.febs.external.mqtt;


import cc.mrbird.febs.common.utils.FigureUtil;
import cc.mrbird.febs.external.mqtt.callback.AetCallbackService;
import cc.mrbird.febs.external.mqtt.callback.OtherCallbackService;
import cc.mrbird.febs.external.mqtt.callback.OtsCallbackService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cc.mrbird.febs.external.mqtt.callback.OtsCallbackService.getMap;
import static cc.mrbird.febs.external.mqtt.callback.OtsCallbackService.getType;
import static cc.mrbird.febs.external.webScoket.WebSocketServer.sendInfo;
import static cc.mrbird.febs.external.webScoket.WebSocketServer.sendInfoMessage;


/**
 * 发布消息的回调类
 * <p>
 * 必须实现MqttCallback的接口并实现对应的相关接口方法CallBack 类将实现 MqttCallBack。
 * 每个客户机标识都需要一个回调实例。在此示例中，构造函数传递客户机标识以另存为实例数据。
 * 在回调中，将它用来标识已经启动了该回调的哪个实例。
 * 必须在回调类中实现三个方法：
 * <p>
 * public void messageArrived(MqttTopic topic, MqttMessage message)接收已经预订的发布。
 * public void connectionLost(Throwable cause)在断开连接时调用。
 * <p>
 * public void deliveryComplete(MqttDeliveryToken token))
 * 接收到已经发布的 QoS 1 或 QoS 2 消息的传递令牌时调用。
 * 由 MqttClient.connect 激活此回调。
 */
@Slf4j
@Service
public class PushCallback implements MqttCallback {

    @Resource
    private OtherCallbackService otherCallbackService;

    @Resource
    private AetCallbackService aetCallbackService;

    @Resource
    private OtsCallbackService otsCallbackService;

    private final MQTTSubscribe mqttSubscribe;

    @Resource
    private MQTTPublish mqttPublish;


    public PushCallback(MQTTSubscribe subsribe) throws MqttException {
        this.mqttSubscribe = subsribe;
    }

    //方法实现说明 断线重连方法，如果是持久订阅，重连是不需要再次订阅，
    // 如果是非持久订阅，重连是需要重新订阅主题 取决于options.setCleanSession(true); true为非持久订阅
    @Override
    public void connectionLost(Throwable cause) {
        // 连接丢失后，一般在这里面进行重连
        log.info("---------------------连接断开，可以做重连");
        while (true) {
            try {//如果没有发生异常说明连接成功，如果发生异常，则死循环
                Thread.sleep(1000);
                mqttSubscribe.init();
                break;
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * publish发布成功后会执行到这里
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        log.info("deliveryComplete---------" + token.isComplete());
    }

    public List<List<Double>> doubleListUs = new ArrayList<>();
    public List<List<Double>> doubleListUi = new ArrayList<>();
    public List<List<Double>> doubleListU01 = new ArrayList<>();
    public List<List<Double>> doubleListU02 = new ArrayList<>();

    public List<List<Double>> doubleListOut = new ArrayList<>();


    /**
     * subscribe订阅后得到的消息会执行到这里
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) {
        try {
            String substring = topic.substring(0, 3);
            String result = new String(message.getPayload(), StandardCharsets.UTF_8);
            if (result.contains("\n")) {
                result = result.replace("\n", "").replace("\t", "");
            }
            JSONObject jsonObject = JSONObject.parseObject(result);
            //模电回调数据
            switch (substring) {
                //模电消息
                case "AET":
                    String type = topic.substring(4, 7);
                    //如果接收的消息是OneToOne一对一，则将数据保留两位小数点后返回
                    switch (type) {
                        case "OTO":
                            if (jsonObject.getInteger("ID") == 100) {
                                aetCallbackService.savePot(jsonObject);
                                break;
                            }
                            if (jsonObject.getInteger("TASK") == 2) {
                                jsonObject.put("V1E", FigureUtil.getDouble(jsonObject.getDouble("V1E")));
                                jsonObject.put("V1B", FigureUtil.getDouble(jsonObject.getDouble("V1B")));
                                jsonObject.put("V1C", FigureUtil.getDouble(jsonObject.getDouble("V1C")));
                            } else if (jsonObject.getInteger("TASK") == 3) {
                                jsonObject.put("IB", FigureUtil.getDouble(jsonObject.getDouble("IB") * 3.0));
                            } else if (jsonObject.getInteger("TASK") == 4) {
                                jsonObject.put("IC", FigureUtil.getDouble(jsonObject.getDouble("IC")));
                            }
                            if ("AET/RXACG_BSP00001".equals(topic)) {
                                sendInfo(jsonObject.toJSONString(), jsonObject.get("ID").toString());
                            } else {
                                sendInfoMessage(jsonObject.toJSONString(), jsonObject.get("ID").toString());
                            }
                            break;
                        //如果接收的消息是OneToMore一对多
                        //则将数据封装处理为echarts期望的波形数据返回
                        case "OTM":
                            //任务1
                            if (jsonObject.get("TASK").toString().equals("1")) {
                               aetCallbackService.savePotV1E(jsonObject);
                                break;
                            } else {
                                //封装波形集合数据
                                if ("AET/RXACG_BSP00001".equals(topic)) {
                                    aetCallbackService.getDoubleList(jsonObject,1.0,1.0,1.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                                } else {
                                    aetCallbackService.dealAMP(jsonObject,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
//                                    aetCallbackService.getDoubleList(jsonObject, 1.0, 1.0, 1.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                                }
                                if (jsonObject.getInteger("SER") == 15) {
                                    //如果收到的数据都一样，则重启主板
                                    if (doubleListUi.get(2).equals(doubleListUi.get(3)) && doubleListUi.get(3).equals(doubleListUi.get(4)) && doubleListUi.get(4).equals(doubleListUi.get(5))) {
                                        String data = "{ \"ID\": 100, \"POT1\": 1000, \"POT2\": 500, \"FREQ\": 1000, \"AMP\": 255, \"PHA\": 120, \"WAVE\": 0, \"TASK\": 0, \"PID\": 1 }";
                                        JSONObject object = JSONObject.parseObject(data);
                                        object.put("ID", jsonObject.get("ID"));
                                        mqttPublish.sendMQTTMessage(topic, data);
                                        return;
                                    }
                                    //如果当前返回的数据为WAVE：0，即为正弦波的时候执行滤波
                                    if (String.valueOf(jsonObject.get("WAVE")).equals("0")) {
                                        //平均滤波
                                        aetCallbackService.recur(doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                                    }
                                    jsonObject.put("doubleListUs", doubleListUs);
                                    jsonObject.put("doubleListUi", doubleListUi);
                                    jsonObject.put("doubleListU01", doubleListU01);
                                    jsonObject.put("doubleListU02", doubleListU02);
                                    jsonObject.remove("UI");
                                    jsonObject.remove("US");
                                    jsonObject.remove("U01");
                                    jsonObject.remove("U02");

                                    //处理峰值等数据
                                    jsonObject = aetCallbackService.dealData(jsonObject,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                                    if ("AET/RXACG_BSP00001".equals(topic)) {
                                        sendInfo(jsonObject.toJSONString(), jsonObject.get("ID").toString());
                                    } else {
                                        sendInfoMessage(jsonObject.toJSONString(), jsonObject.get("ID").toString());
                                    }
                                    sendInfoMessage(jsonObject.toJSONString(), jsonObject.get("ID").toString());
                                    //最后清空数据集合
                                  otherCallbackService.clearList(doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                                }
                            }
                            break;
                        case "OTS":
                            aetCallbackService.getDoubleList(jsonObject,1.0,1.0,1.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                            if (jsonObject.getInteger("SER") == 15) {
                                if (jsonObject.getInteger("TASK")<=6) {
                                    //平均滤波
                                    aetCallbackService.recur(doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                                }
                                //处理运放json返回数据
                                OtsCallbackService.returnJsonObject(jsonObject, doubleListUi, doubleListUs, doubleListU01);
//                              处理峰值等数据
                                Map<String, String> maps = getMap();
                                List<String> types = getType(jsonObject.getInteger("TYPE"));
                                jsonObject = otsCallbackService.dealDataOut(jsonObject,doubleListUi, maps.get(types.get(0)),"UI");
                                jsonObject = otsCallbackService.dealDataOut(jsonObject,doubleListUs, maps.get(types.get(1)),"US");
                                jsonObject = otsCallbackService.dealDataOut(jsonObject,doubleListU01, maps.get(types.get(2)),"U01");
                                removeKey(jsonObject,"AMP1","AMP2","TEMP","FREQ1","FREQ2","UI_VAL","US_VAL","U01_VAL");
                                //做数据返回处理
                                returnSendInfo(jsonObject.toJSONString(),jsonObject,topic);
                                //最后清空数据集合
                                otherCallbackService.clearList(doubleListUi,doubleListUs,doubleListU01,doubleListU02);
                            }
                            break;
                        case "BUS":
                            //设置主板占用情况
                            otherCallbackService.caseBusy(jsonObject);
                            break;
                        default:
                    }
                    break;
                //数电的回调消息
                case "/D1":
                    sendInfo(result, jsonObject.get("id").toString());
                    log.info("接收主题：" + topic + "， 订阅收到的消息为：" + result.substring(0,10));
                    break;
                case "ELE":
                    //返回
                    // 当前登录用户ID
                    //电气实验回调数据
                    returnSendInfo(jsonObject.toJSONString(),jsonObject,topic);
                    break;
                default:
                    //记录异常没有执行的记录
                    otherCallbackService.saveField(topic,message,result,substring);
                    break;
            }
        } catch (Exception e) {
            log.info("回调异常：" + e.getMessage());
        }
    }

    private void  returnSendInfo(String result,JSONObject jsonObject,String topic){
        try {
            sendInfo(result, jsonObject.get("ID").toString());
            log.info("接收主题：" + topic + "， 订阅收到的消息为：" + result.substring(0,10));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * @ Author: 马超伟
     * @ Date: 2021/3/20 15:31
     * @ Params: [jsonObject, val]
     * @ return: void
     * @ Description: 移出json指定字段
     */
    private void removeKey(JSONObject jsonObject,String...val){
        for (String s : val) {
            jsonObject.remove(s);
        }
    }

}