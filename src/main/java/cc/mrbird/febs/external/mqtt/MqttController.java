package cc.mrbird.febs.external.mqtt;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.IAnalogBaseService;
import cc.mrbird.febs.system.service.IDetBaseService;
import cc.mrbird.febs.system.service.IDetRecordService;
import cc.mrbird.febs.system.service.IWaveService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static cc.mrbird.febs.external.webScoket.WebSocketServer.sendInfo;

@Slf4j
@RestController
@RequestMapping(value = "/api/insertByMq")
public class MqttController extends BaseController {

    @Resource
    private MQTTSubscribe mqttSubscribe;

    @Resource
    private MQTTPublish mqttPublish;

    @Resource
    private IDetRecordService detRecordService;

    @Resource
    private IDetBaseService detBaseService;

    @Resource
    private IAnalogBaseService analogBaseService;

    @Resource
    private RedisService redisService;

    @Resource
    private IWaveService waveService;


    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/14 9:40
     * @ Params: [topic]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description: 订阅MQ
     */
    @RequestMapping("/subscribe")
    public FebsResponse subscribe(String topic) {
        if (StringUtils.isBlank(topic)) {
            return new FebsResponse().fail().message("订阅主题为空！");
        }
        mqttSubscribe.subscribe(topic, 2);
        return new FebsResponse().success();
    }

    /**
     * @ Author: 马超伟
     * @ Date: 2020/11/4 11:56
     * @ Params: [topic]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description: 取消订阅接口
     */
    @RequestMapping("/unsubscribe")
    public FebsResponse unsubscribe(String topic) {
        if (StringUtils.isBlank(topic)) {
            return new FebsResponse().fail().message("订阅主题为空！");
        }
        mqttSubscribe.unsubscribe(topic);
        return new FebsResponse().success();
    }

    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/14 9:05
     * @ Params:  * @ Param null
     * @ return:
     * @ Description: 模电消息。MQTT发布
     */
    @RequestMapping("/mqttServer")
    public FebsResponse mqttServer(String data, String topic) {
        if (StringUtils.isBlank(topic)) {
            return new FebsResponse().fail().message("发布主题为空！");
        }
        if (StringUtils.isBlank(data)) {
            return new FebsResponse().fail().message("发布数据为空！");
        }

//        JSONObject jsonObject = JSONObject.parseObject(data);
//        Wave wave = jsonObject.toJavaObject(Wave.class);
//        System.out.println("发送的wave:"+wave);
//        LambdaQueryWrapper<Wave> eq = new QueryWrapper<Wave>().lambda()
//                .eq(IntegerUtils.isNotBlank(wave.getTask()),Wave::getTask, wave.getTask())
//                .eq(IntegerUtils.isNotBlank(wave.getPid()),Wave::getPid, wave.getPid())
//                .eq(IntegerUtils.isNotBlank(wave.getPot1()),Wave::getPot1, wave.getPot1())
//                .eq(IntegerUtils.isNotBlank(wave.getPot2()),Wave::getPot2, wave.getPot2())
//                .eq(IntegerUtils.isNotBlank(wave.getFreq()),Wave::getFreq, wave.getFreq())
//                .eq(IntegerUtils.isNotBlank(wave.getAmp()),Wave::getAmp, wave.getAmp())
//                .eq(IntegerUtils.isNotBlank(wave.getPha()),Wave::getPha, wave.getPha())
//                .eq(IntegerUtils.isNotBlank(wave.getWave()),Wave::getWave, wave.getWave());
//
//        Wave waveServiceOne = waveService.getOne(eq,false);
//        System.out.println( "AET/RXACG_BSP00001".equals(topic));
//        if (waveServiceOne!=null && !"AET/RXACG_BSP00001".equals(topic)){
//            try {
//                log.info("topic："+topic);
//                log.info("data："+data);
//                log.info("查询数据库信息并返回："+waveServiceOne);
//                sendInfo(waveServiceOne.getJsonList(), jsonObject.get("ID").toString());
//                return new FebsResponse().success().message("数据获取成功！");
//            } catch (IOException e) {
//                e.printStackTrace();
//                log.error("推送数据失败！+/api/insertByMq/mqttServer,data="+data+",topic="+topic);
//            }
//
//        }

        AnalogBase one = analogBaseService.getOne(new QueryWrapper<AnalogBase>().lambda().eq(AnalogBase::getReleaseChannel, topic),false);
        if (one == null) {
            return new FebsResponse().fail().message("主板不存在");
        }
        if (one.getAnalogStatus() == 1) {
            if (one.getAnalogUsestatus() == 1) {
                return new FebsResponse().put_code(502, "该主板正在占用中，请稍后再试！");
            }
        }
        if (redisService.get(topic) != null) {
            return new FebsResponse().put_code(502, "该主板正在占用中，请稍后再试！");
        }

        boolean b = mqttPublish.sendMQTTMessage(topic, data);
        return new FebsResponse().success().data(b);
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/8/5 18:58
     * @ Param data
     * @ Param topic
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description:数电 MQTT消息发布
     */
    @RequestMapping(value = "/receiveMsg")
    public FebsResponse receiveMsg(String data, String topic) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (StringUtils.isBlank(topic)) {
            return new FebsResponse().fail().message("订阅主题为空！");
        }
        if (StringUtils.isBlank(data)) {
            return new FebsResponse().fail().message("发布数据为空！");
        }
        List<DetRecord> recordList = detRecordService.list(new QueryWrapper<DetRecord>().lambda()
                .eq(DetRecord::getUserId, currentUser.getUserId())
                .eq(DetRecord::getIsUse, 1));
        if (recordList != null && recordList.size() > 1) {
            return new FebsResponse().put_code(501, "您有未结束的实验，请完成后重试！");
        }

        String[] split = topic.split("/");
        if (split[2].equals("Connection")) {
            DetRecord detRecord = new DetRecord();
            detRecord.setDetId(detBaseService.getOne(new QueryWrapper<DetBase>()
                    .lambda().eq(DetBase::getDetName, split[1])).getDetId());
            detRecord.setIsUse(1);
            detRecord.setUserId(currentUser.getUserId());
            detRecord.setStartTime(LocalDateTime.now());
            //设置实验次数
            DetRecord record = detRecordService.getOne(new QueryWrapper<DetRecord>()
                    .lambda()
                    .eq(DetRecord::getUserId, currentUser.getUserId())
                    .orderByDesc(DetRecord::getTime)
                    .last("limit 0,1"));
            if (record == null) {
                detRecord.setTime(1);
            } else {
                detRecord.setTime(record.getTime() + 1);
            }
            detRecordService.save(detRecord);
        }
        JSONObject jsonObject = JSONObject.parseObject(data);
        jsonObject.put("id", currentUser.getUserId());
        jsonObject.put("time", new Date().getTime());
        if (mqttPublish.sendMQTTMessage(topic, jsonObject.toString())) {
            log.info("发布的主题：" + topic + "，json数据为：" + jsonObject.toString());
            return new FebsResponse().success().message("发布成功！");
        } else {
            return new FebsResponse().fail().message("发布失败！");
        }
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/8/6 10:53
     * @ Params: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description: 查询可用主板信息
     */
    @GetMapping("/usableDet")
    public FebsResponse usableDet() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        List<DetRecord> detRecordList = detRecordService.list(new QueryWrapper<DetRecord>().lambda()
                .eq(DetRecord::getIsUse, 1));
        List<DetBase> detBaseList = detBaseService.list(new QueryWrapper<DetBase>().lambda()
                .eq(DetBase::getStatus, 1));
        List<String> detNameList = new ArrayList<>();
        if (detRecordList != null && detRecordList.size() > 0) {
            for (DetBase detBase : detBaseList) {
                boolean b = true;
                for (DetRecord detRecord : detRecordList) {
                    if (detBase.getDetId() == detRecord.getDetId()) {
                        b = false;
                    }
                }
                if (b) {
                    detNameList.add(detBase.getDetName());
                }
            }
        } else {
            detNameList = detBaseList.stream().map(DetBase::getDetName).collect(Collectors.toList());
        }
        return new FebsResponse().data(detNameList).message("查询成功！");
    }

    /**
     * @ Author: 马超伟
     * @ Date: 2020/8/7 15:04
     * @ Params: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description: 退出主板占用情况
     */
    @GetMapping("/exitDet")
    public FebsResponse exitDet() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        List<String> nameList = new ArrayList<>();
        try {
            List<DetRecord> list = detRecordService.list(new QueryWrapper<DetRecord>().lambda()
                    .eq(DetRecord::getUserId, currentUser.getUserId())
                    .eq(DetRecord::getIsUse, 1));
            for (DetRecord detRecord : list) {
                detRecordService.update(new UpdateWrapper<DetRecord>().lambda()
                        .eq(DetRecord::getDetRecordId, detRecord.getDetRecordId())
                        .set(DetRecord::getIsUse, 0)
                        .set(DetRecord::getEndTime, LocalDateTime.now()));
                nameList.add(detRecord.getDetName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("异常错误，退出失败！");
        }
        return new FebsResponse().success().message("实验退出成功！").data(nameList);
    }

}
