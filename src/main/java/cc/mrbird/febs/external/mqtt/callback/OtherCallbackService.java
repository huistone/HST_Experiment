package cc.mrbird.febs.external.mqtt.callback;

import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.others.entity.Eximport;
import cc.mrbird.febs.others.service.IEximportService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @ Author 马超伟
 * @ Date 2021-02-03 17:58
 * @ Description: 其他通用返回业务
 * @ Version:
 */
@Slf4j
@Service
public class OtherCallbackService {

    @Resource
    private IEximportService eximportService;

    @Resource
    private RedisService redisService;

    public void saveField(String topic, MqttMessage message,String result, String substring) {
        Eximport eximport = new Eximport();
        eximport.setCreateTime(new Date());
        eximport.setField2(message.getQos());
        eximport.setField1(topic);
        eximport.setField3(result);
        eximportService.save(eximport);
        log.warn("未处理的数据：Is:" + substring + " " + message);
    }

    public void caseBusy(JSONObject jsonObject){
        String subTopic = jsonObject.getString("SubTopic");
        Integer busy = jsonObject.getInteger("BUSY");

        if (busy == 0 && redisService.get(subTopic) != null) {
            redisService.del(subTopic);
            log.info("解除主板" + subTopic + "占用");
        } else if (busy == 1) {
            redisService.set(subTopic, "busy", 5L);
            log.info("设置" + subTopic + "主板占用五秒");
        }
    }

    public void clearList(List<List<Double>> doubleListUi, List<List<Double>> doubleListUs, List<List<Double>> doubleListU01,List<List<Double>> doubleListU02){
        doubleListUs.clear();
        doubleListUi.clear();
        doubleListU01.clear();
        doubleListU02.clear();
    }

}
