package cc.mrbird.febs.external.mqtt.vo;

import lombok.Data;

/**
 * @ Author 马超伟
 * @ Date 2020-10-12 09:47
 * @ Description: 波形数据实体
 * @ Version:
 */
@Data
public class WaveVo {

    /**
     * 唯一id
     */
    private Integer ID;

    /**
     * 电位器 1
     * 0 ~ 1024
     */
    private Integer POT1;

    /**
     * 电位器 2
     * 0 ~ 1024
     */
    private Integer POT2;

    /**
     * 波形的频率
     * 0 ~ 65536
     */
    private Integer FREQ;

    /**
     * 幅度
     * 0 ~ 255
     */
    private Integer AMP;

    /**
     * 相位
     * 0 ~ 180
     */
    private Integer PHA;

    /**
     * 波形
     * 正弦波 0
     * 方波   1
     * 三角波  2
      */
    private Integer WAVE;

    /**
     * 项目课程 id
     */
    private Integer PID;

    /**
     * 测试任务号
     */
    private Integer TASK;

    /**
     * 波形数据序号
     */
    private Integer SER;

    /**
     * 波形参数
     */
    private Double US;
    private Double UI;
    private Double U01;
    private Double U02;

    /**
     * 电压参数
     */
    private Double V1E;
    private Double V1B;
    private Double V1C;
    private Double V2E;
    private Double V2B;
    private Double V2C;

    /**
     * 电流参数
     */
    private Double IB;
    private Double IC;

    /**
     * 模电运放TYPE代码
     */
    private String UI1 = "1";
    private String UI2 = "2";
    private String OUT1 = "3";
    private String OUT2 = "4";
    private String OUT3 = "5";
    private String OUT3_1 = "6";
    private String OUT4 = "7";


}
