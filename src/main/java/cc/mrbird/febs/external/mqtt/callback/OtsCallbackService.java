package cc.mrbird.febs.external.mqtt.callback;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.mrbird.febs.external.mqtt.callback.AetCallbackService.getAmp;
import static cc.mrbird.febs.external.mqtt.callback.AetCallbackService.toDouble;


/**
 * @ Author 马超伟
 * @ Date 2021-02-03 17:56
 * @ Description: 模电运放实验数据处理
 * @ Version:
 */
@Slf4j
@Service
public class OtsCallbackService {


    //处理运放json返回数据
    public static JSONObject returnJsonObject(JSONObject jsonObject, List<List<Double>> doubleListUi, List<List<Double>> doubleListUs, List<List<Double>> doubleListU01){

        Map<String, String> map = getMap();
        List<String> type = getType(jsonObject.getInteger("TYPE"));
        jsonObject.put("list"+map.get(type.get(0)), doubleListUi);
        jsonObject.put("list"+map.get(type.get(1)), doubleListUs);
        jsonObject.put("list"+map.get(type.get(2)), doubleListU01);

        jsonObject.remove("UI");
        jsonObject.remove("US");
        jsonObject.remove("U01");
        jsonObject.remove("SER");
        return jsonObject;
    }

    public static List<String> getType(Integer typeInteger){
        String typeUi = String.valueOf(typeInteger/100);
        String typeUs = String.valueOf(typeInteger/10%10);
        String typeU01 = String.valueOf(typeInteger%10);
        List<String> list = new ArrayList<>();
        list.add(typeUi);
        list.add(typeUs);
        list.add(typeU01);
        return list;
    }

    public static Map<String,String> getMap(){
        Map<String,String> map = new HashMap<>();
        map.put("1","Ui1");
        map.put("2","Ui2");
        map.put("3","Out1");
        map.put("4","Out2");
        map.put("5","Out3");
        map.put("6","Out3_1");
        map.put("7","Out4");
        return map;
    }


    private List<List<Double>> getDoubleListOut(List<Object> out, Double outAmp1, Double outAmp2, List<List<Double>> doubleListOut) {
        int s;
        if (doubleListOut.size() == 0) {
            s = 0;
        } else {
            s = doubleListOut.size();
        }
        if (out != null && out.size() > 0) {
            for (int i = 0; i < out.size(); i++) {
                List<Double> listOut = new ArrayList<>();
                listOut.add((double) i + s);
                listOut.add(getValidDataOut(Double.parseDouble(out.get(i).toString()), outAmp1, outAmp2));
                doubleListOut.add(listOut);
            }
        }
        return doubleListOut;
    }


    public Double getValidDataOut(Double data, Double x, Double y) {
        if (x == null) {
            x = 1.0;
        }
        Double s = ((data / 4096.00 * 3.3 - 1.645) * (-8)) / x / y;
        return Double.parseDouble(df().format(s));
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/26 16:35
     * @ Params: [jsonObject]
     * @ return: com.alibaba.fastjson.JSONObject
     * @ Description: 对数据进行封装处理
     * 去掉第一个和最后一个异常元素
     * 计算周期、频率、峰值、峰峰值、有效值等
     */
    public JSONObject dealDataOut( JSONObject jsonObject,List<List<Double>> doubleListOut,String val,String split) {
        try {
            Map<String,Object> map = new HashMap<>();
            //去掉波形数据的第一个和最后一个元素
            if (doubleListOut != null && doubleListOut.size() > 0) {
                doubleListOut.remove(0);
                doubleListOut.remove(doubleListOut.size() - 1);
                //计算周期
                double freq = Double.parseDouble(jsonObject.get("FREQ1").toString());
                Double cycle = 1.0 / freq;
                map.put("cycle1", df().format(cycle));

                double freq2 = Double.parseDouble(jsonObject.get("FREQ2").toString());
                Double cycle2 = 1.0 / freq2;
                map.put("cycle2", df().format(cycle2));

                //计算峰值
                double d = doubleListOut.get(0).get(1);
                map.put("max", d);
                map.put("min", d);
                double[] mean = {0.0};
                List<Double> maxList = new ArrayList<>();
                List<Double> minList = new ArrayList<>();
                int bound = doubleListOut.size();
                for (int i = 0; i < bound; i++) {
                    if (doubleListOut.get(i).get(1) >= toDouble(map.get("max"))) {
                        map.put("max", doubleListOut.get(i).get(1));
                    }
                    if (doubleListOut.get(i).get(1) <= toDouble(map.get("min"))) {
                        map.put("min", doubleListOut.get(i).get(1));
                    }
                    mean[0] = mean[0] + doubleListOut.get(i).get(1);
                    //计算最大值和最小值的总和
                    if (i > 1 && i < bound - 1) {
                        if (doubleListOut.get(i).get(1) > doubleListOut.get(i - 1).get(1) && doubleListOut.get(i).get(1) > doubleListOut.get(i + 1).get(1)) {
                            maxList.add(doubleListOut.get(i).get(1));
                        }
                        if (doubleListOut.get(i).get(1) < doubleListOut.get(i - 1).get(1) && doubleListOut.get(i).get(1) < doubleListOut.get(i + 1).get(1)) {
                            minList.add(doubleListOut.get(i).get(1));
                        }
                    }
                }
                //计算最大值和最小值的平均值
                String maxValue = df().format(maxList.stream().mapToDouble(Double::doubleValue).sum() / (double) maxList.size());
                map.put("maxValue", maxValue);
                String minValue = df().format(minList.stream().mapToDouble(Double::doubleValue).sum() / (double) minList.size());
                map.put("minValue", minValue);
                //计算峰峰值(最大值-最小值)
                map.put("mm", df().format(toDouble(maxValue) - toDouble(minValue)));
//                //计算幅度（（最大值-最小值）/2.0）
                map.put("AMP1", df().format(jsonObject.getDouble("AMP1")));
                map.put("AMP2", df().format(jsonObject.getDouble("AMP2")));
                //计算有效值(幅度除以根号2)
                if (jsonObject.get(split+"_VAL")!=null){
                    map.put("valid", df().format(jsonObject.getDouble(split+"_VAL")/0.85 ));
                }
                if (jsonObject.get("TEMP")!=null){
                    map.put("TEMP", df().format(jsonObject.getDouble("TEMP") ));
                }
                map.put("FREQ1", df().format(jsonObject.getDouble("FREQ1") ));
                map.put("FREQ2", df().format(jsonObject.getDouble("FREQ2") ));
                //计算平均值（所有点的和除以点的数量）
                map.put("mean", df().format(mean[0] / Double.parseDouble(String.valueOf(doubleListOut.size()))));
            }
            jsonObject.put("map"+val,map);
        } catch (Exception e) {
            log.error("dealDataOut:计算数值出错：" + e.getMessage());
        }
        return jsonObject;
    }

    //格式化小数点
    public static DecimalFormat df(){
        return new DecimalFormat("0.0000");
    }

    private void recurOut(JSONObject jsonObject, List<List<Double>> doubleListOut) {
        //平均滤波
        double n = 8.0;

        if (doubleListOut != null && doubleListOut.size() > 0) {
            for (int i = 0; i < doubleListOut.size() - n; i++) {
                double sum = 0;
                for (int j = 0; j < n; j++) {
                    sum = sum + Double.parseDouble(doubleListOut.get(i + j).get(1).toString());
                }
                doubleListOut.get(i).set(1, Double.valueOf(df().format(sum / n)));
            }
        }
        jsonObject.put("doubleListOut", doubleListOut);

    }


}
