package cc.mrbird.febs.external.mqtt.callback;

import cc.mrbird.febs.system.entity.Pot;
import cc.mrbird.febs.system.service.IPotService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static cc.mrbird.febs.external.mqtt.callback.OtsCallbackService.df;

/**
 * @ Author 马超伟
 * @ Date 2021-02-03 17:56
 * @ Description: 模电单级放大电路实验数据处理
 * @ Version:
 */
@Slf4j
@Service
public class AetCallbackService {

    @Resource
    private IPotService potService;


    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/29 9:15
     * @ Params: [jsonObject]
     * @ return: void
     * @ Description: 封装波形集合数据
     * 模电接收到的回调消息
     * 将单条回调数据整合到一个集合里面
     */
    public void getDoubleList(JSONObject jsonObject, Double uiAmp, Double usAmp, Double U01Amp,List<List<Double>> doubleListUi, List<List<Double>> doubleListUs, List<List<Double>> doubleListU01,List<List<Double>> doubleListU02) {
        List<Object> us = jsonObject.getJSONArray("US");
        List<Object> ui = jsonObject.getJSONArray("UI");
        List<Object> u01 = jsonObject.getJSONArray("U01");
        List<Object> u02 = jsonObject.getJSONArray("U02");
        int s;
        if (doubleListUi.size() == 0) {
            s = 0;
        } else {
            s = doubleListUi.size();
        }
        for (int i = 0; i < ui.size(); i++) {
            List<Double> listUi = new ArrayList<>();
            listUi.add((double) i + s);
            listUi.add(getValidData(Double.parseDouble(ui.get(i).toString()), uiAmp));
            doubleListUi.add(listUi);
            if (us != null && us.size() > 0) {
                List<Double> listUs = new ArrayList<>();
                listUs.add((double) i + s);
                listUs.add(getValidData(Double.parseDouble(us.get(i).toString()), usAmp));
                doubleListUs.add(listUs);
            }
            List<Double> listU01 = new ArrayList<>();
            listU01.add((double) i + s);
            listU01.add(getValidData(Double.parseDouble(u01.get(i).toString()), U01Amp));
            doubleListU01.add(listU01);
            if (u02 != null && u02.size() > 0) {
                List<Double> listU02 = new ArrayList<>();
                listU02.add((double) i + s);
                listU02.add(getValidData(Double.parseDouble(u02.get(i).toString()), 1.0));
                doubleListU02.add(listU02);
            }
        }

    }

    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/29 9:12
     * @ Params: [data]
     * @ return: java.lang.Double
     * @ Description: 对数据进行波形计算处理
     */
    public Double getValidData(Double data, Double x) {
        if (x == null) {
            x = 1.0;
        }
        Double s = ((data / 4096.00 * 3.3 - 1.645) * (-8)) / x;
        return Double.parseDouble(df().format(s));
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/26 16:35
     * @ Params: [jsonObject]
     * @ return: com.alibaba.fastjson.JSONObject
     * @ Description: 对数据进行封装处理
     * 去掉第一个和最后一个异常元素
     * 计算周期、频率、峰值、峰峰值、有效值等
     */
    public JSONObject dealData(JSONObject jsonObject,List<List<Double>> doubleListUi, List<List<Double>> doubleListUs, List<List<Double>> doubleListU01,List<List<Double>> doubleListU02) {
        try {
            //去掉波形数据的第一个和最后一个元素
            doubleListUi.remove(0);
            doubleListUi.remove(doubleListUi.size() - 1);

            if (doubleListUs != null && doubleListUs.size() > 0) {
                doubleListUs.remove(0);
                doubleListUs.remove(doubleListUs.size() - 1);
            }

            doubleListU01.remove(0);
            doubleListU01.remove(doubleListU01.size() - 1);

            if (doubleListU02 != null && doubleListU02.size() > 0) {
                doubleListU02.remove(0);
                doubleListU02.remove(doubleListU02.size() - 1);
            }

            //计算周期
            double freq = Double.parseDouble(jsonObject.get("FREQ").toString());
            Double cycle = 1.0 / freq;
            jsonObject.put("cycle", df().format(cycle));

            //计算峰值
            double dUi = doubleListUi.get(0).get(1);
            double dUs = 0;
            if (doubleListUs != null && doubleListUs.size() > 0) {
                dUs = doubleListUs.get(0).get(1);
            }
            double dU01 = doubleListU01.get(0).get(1);
            double dU02 = 0;
            if (doubleListU02 != null && doubleListU02.size() > 0) {
                dU02 = doubleListU02.get(0).get(1);
            }

            jsonObject.put("maxUi", dUi);
            jsonObject.put("minUi", dUi);

            jsonObject.put("maxUs", dUs);
            jsonObject.put("minUs", dUs);

            jsonObject.put("maxU01", dU01);
            jsonObject.put("minU01", dU01);

            jsonObject.put("maxU02", dU02);
            jsonObject.put("minU02", dU02);


            double[] meanUi = {0.0};
            double[] meanUs = {0.0};
            double[] meanU01 = {0.0};
            double[] meanU02 = {0.0};

            List<Double> maxUiList = new ArrayList<>();
            List<Double> maxUsList = new ArrayList<>();
            List<Double> maxU01List = new ArrayList<>();
            List<Double> maxU02List = new ArrayList<>();

            List<Double> minUiList = new ArrayList<>();
            List<Double> minUsList = new ArrayList<>();
            List<Double> minU01List = new ArrayList<>();
            List<Double> minU02List = new ArrayList<>();

            int bound = doubleListUi.size();
            for (int i = 0; i < bound; i++) {
                if (doubleListUi.get(i).get(1) >= toDouble(jsonObject.get("maxUi"))) {
                    jsonObject.put("maxUi", doubleListUi.get(i).get(1));
                }
                if (doubleListUi.get(i).get(1) <= toDouble(jsonObject.get("minUi"))) {
                    jsonObject.put("minUi", doubleListUi.get(i).get(1));
                }

                if (doubleListUs != null && doubleListUs.size() > 0) {
                    if (doubleListUs.get(i).get(1) >= toDouble(jsonObject.get("maxUs"))) {
                        jsonObject.put("maxUs", doubleListUs.get(i).get(1));
                    }
                    if (doubleListUs.get(i).get(1) <= toDouble(jsonObject.get("minUs"))) {
                        jsonObject.put("minUs", doubleListUs.get(i).get(1));
                    }
                }

                if (doubleListU02 != null && doubleListU02.size() > 0) {
                    if (doubleListU02.get(i).get(1) >= toDouble(jsonObject.get("maxU02"))) {
                        jsonObject.put("maxU02", doubleListU02.get(i).get(1));
                    }
                    if (doubleListU02.get(i).get(1) <= toDouble(jsonObject.get("minU02"))) {
                        jsonObject.put("minU02", doubleListU02.get(i).get(1));
                    }
                }

                if (doubleListU01.get(i).get(1) >= toDouble(jsonObject.get("maxU01"))) {
                    jsonObject.put("maxU01", doubleListU01.get(i).get(1));
                }
                if (doubleListU01.get(i).get(1) <= toDouble(jsonObject.get("minU01"))) {
                    jsonObject.put("minU01", doubleListU01.get(i).get(1));
                }
                meanUi[0] = meanUi[0] + doubleListUi.get(i).get(1);
                if (doubleListUs != null && doubleListUs.size() > 0) {
                    meanUs[0] = meanUs[0] + doubleListUs.get(i).get(1);
                }
                if (doubleListU02 != null && doubleListU02.size() > 0) {
                    meanU02[0] = meanU02[0] + doubleListU02.get(i).get(1);
                }
                meanU01[0] = meanU01[0] + doubleListU01.get(i).get(1);

//          计算最大值和最小值的总和
                if (i > 1 && i < bound - 1) {
                    if (doubleListUi.get(i).get(1) > doubleListUi.get(i - 1).get(1) && doubleListUi.get(i).get(1) > doubleListUi.get(i + 1).get(1)) {
                        maxUiList.add(doubleListUi.get(i).get(1));
                    }
                    if (doubleListUi.get(i).get(1) < doubleListUi.get(i - 1).get(1) && doubleListUi.get(i).get(1) < doubleListUi.get(i + 1).get(1)) {
                        minUiList.add(doubleListUi.get(i).get(1));
                    }
                    if (doubleListUs != null && doubleListUs.size() > 0) {
                        if (doubleListUs.get(i).get(1) > doubleListUs.get(i - 1).get(1) && doubleListUs.get(i).get(1) > doubleListUs.get(i + 1).get(1)) {
                            maxUsList.add(doubleListUs.get(i).get(1));
                        }
                        if (doubleListUs.get(i).get(1) < doubleListUs.get(i - 1).get(1) && doubleListUs.get(i).get(1) < doubleListUs.get(i + 1).get(1)) {
                            minUsList.add(doubleListUs.get(i).get(1));
                        }
                    }

                    if (doubleListU02 != null && doubleListU02.size() > 0) {
                        if (doubleListU02.get(i).get(1) > doubleListU02.get(i - 1).get(1) && doubleListU02.get(i).get(1) > doubleListU02.get(i + 1).get(1)) {
                            maxU02List.add(doubleListU02.get(i).get(1));
                        }
                        if (doubleListU02.get(i).get(1) < doubleListU02.get(i - 1).get(1) && doubleListU02.get(i).get(1) < doubleListU02.get(i + 1).get(1)) {
                            minU02List.add(doubleListU02.get(i).get(1));
                        }
                    }

                    if (doubleListU01.get(i).get(1) > doubleListU01.get(i - 1).get(1) && doubleListU01.get(i).get(1) > doubleListU01.get(i + 1).get(1)) {
                        maxU01List.add(doubleListU01.get(i).get(1));
                    }
                    if (doubleListU01.get(i).get(1) < doubleListU01.get(i - 1).get(1) && doubleListU01.get(i).get(1) < doubleListU01.get(i + 1).get(1)) {
                        minU01List.add(doubleListU01.get(i).get(1));
                    }

                }

            }

            //计算最大值和最小值的平均值

            String maxUiValue = jsonObject.getString("maxUi");
            String minUiValue = jsonObject.getString("minUi");
            String maxUsValue = null;
            String minUsValue = null;
            String maxU02Value = null;
            String minU02Value = null;
            if (doubleListUs != null && doubleListUs.size() > 0) {
                maxUsValue = jsonObject.getString("maxUs");
                minUsValue = jsonObject.getString("minUs");
            }
            if (doubleListU02 != null && doubleListU02.size() > 0) {
                maxU02Value = jsonObject.getString("maxU02");
                minU02Value = jsonObject.getString("minU02");
            }
            String maxU01Value = jsonObject.getString("maxU01");
            String minU01Value = jsonObject.getString("minU01");
            if (maxUiList.size() > 0) {
                maxUiValue = df().format(maxUiList.stream().mapToDouble(Double::doubleValue).sum() / (double) maxUiList.size());
                jsonObject.put("maxUiValue", maxUiValue);
                minUiValue = df().format(minUiList.stream().mapToDouble(Double::doubleValue).sum() / (double) minUiList.size());
                jsonObject.put("minUiValue", minUiValue);


                maxU01Value = df().format(maxU01List.stream().mapToDouble(Double::doubleValue).sum() / (double) maxU01List.size());
                jsonObject.put("maxU01Value", maxU01Value);
                minU01Value = df().format(minU01List.stream().mapToDouble(Double::doubleValue).sum() / (double) minU01List.size());
                jsonObject.put("minU01Value", minU01Value);

                if (doubleListUs != null && doubleListUs.size() > 0) {
                    maxUsValue = df().format(maxUsList.stream().mapToDouble(Double::doubleValue).sum() / (double) maxUsList.size());
                    jsonObject.put("maxUsValue", maxUsValue);
                    minUsValue = df().format(minUsList.stream().mapToDouble(Double::doubleValue).sum() / (double) minUsList.size());
                    jsonObject.put("minUsValue", minUsValue);
                }
                if (doubleListU02 != null && doubleListU02.size() > 0) {
                    maxU02Value = df().format(maxU02List.stream().mapToDouble(Double::doubleValue).sum() / (double) maxU02List.size());
                    jsonObject.put("maxU02Value", maxU02Value);
                    minU02Value = df().format(minU02List.stream().mapToDouble(Double::doubleValue).sum() / (double) minU02List.size());
                    jsonObject.put("minU02Value", minU02Value);
                }


            }


            //计算峰峰值(最大值-最小值)
            jsonObject.put("mmUi", df().format(toDouble(maxUiValue) - toDouble(minUiValue)));
            if (doubleListUs != null && doubleListUs.size() > 0) {
                jsonObject.put("mmUs", df().format(toDouble(maxUsValue) - toDouble(minUsValue)));
            }
            if (doubleListU02 != null && doubleListU02.size() > 0) {
                jsonObject.put("mmU02", df().format(toDouble(maxU02Value) - toDouble(minU02Value)));
            }
            jsonObject.put("mmU01", df().format(toDouble(maxU01Value) - toDouble(minU01Value)));

            //计算幅度（（最大值-最小值）/2.0）
            jsonObject.put("ampUi", df().format(getAmp(maxUiValue, minUiValue)));
            if (doubleListUs != null && doubleListUs.size() > 0) {
                jsonObject.put("ampUs", df().format(getAmp(maxUsValue, minUsValue)));
            }
            if (doubleListU02 != null && doubleListU02.size() > 0) {
                jsonObject.put("ampU02", df().format(getAmp(maxU02Value, minU02Value)));
            }
            jsonObject.put("ampU01", df().format(getAmp(maxU01Value, minU01Value)));

            //计算有效值(幅度除以根号2)
            jsonObject.put("validUi", df().format(toDouble(jsonObject.getDouble("ampUi") / Math.sqrt(2))));
            if (doubleListUs != null && doubleListUs.size() > 0) {
                jsonObject.put("validUs", df().format(jsonObject.getDouble("ampUs") / Math.sqrt(2)));
            }
            if (doubleListU02 != null && doubleListU02.size() > 0) {
                jsonObject.put("validU02", df().format(jsonObject.getDouble("ampU02") / Math.sqrt(2)));
            }

            jsonObject.put("validU01", df().format(jsonObject.getDouble("ampU01") / Math.sqrt(2)));

            //计算平均值（所有点的和除以点的数量）
            jsonObject.put("meanUi", df().format(meanUi[0] / Double.parseDouble(String.valueOf(doubleListUi.size()))));
            if (doubleListUs != null && doubleListUs.size() > 0) {
                jsonObject.put("meanUs", df().format(meanUs[0] / Double.parseDouble(String.valueOf(doubleListUs.size()))));
            }
            if (doubleListU02 != null && doubleListU02.size() > 0) {
                jsonObject.put("meanU02", df().format(meanU02[0] / Double.parseDouble(String.valueOf(doubleListU02.size()))));
            }
            jsonObject.put("meanU01", df().format(meanU01[0] / Double.parseDouble(String.valueOf(doubleListU01.size()))));
        } catch (Exception e) {
            log.error("dealData:计算数值出错：" + e.getMessage());
        }
        return jsonObject;
    }

    /**
     * @ Author: 马超伟
     * @ Date: 2020/12/16 9:43
     * @ Params: [object]
     * @ return: java.lang.Double
     * @ Description: Object转Double
     */
    public static Double toDouble(Object object) {
        return Double.valueOf(String.valueOf(object));
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/28 17:20
     * @ Params: [jsonObject, max, min]
     * @ return: java.lang.Double
     * @ Description: 计算幅度
     */
    static Double getAmp(String max, String min) {
        return ((Double.parseDouble(max) - Double.parseDouble(min)) / 2.0);
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2021/1/5 18:54
     * @ Params: [jsonObject]
     * @ return: void
     * @ Description: 单级放大数据处理:波形数据处理方式：
     */
    public void dealAMP(JSONObject jsonObject, List<List<Double>> doubleListUi, List<List<Double>> doubleListUs, List<List<Double>> doubleListU01, List<List<Double>> doubleListU02) {
        Integer task = jsonObject.getInteger("TASK");
        Integer amp = jsonObject.getInteger("AMP");
        if (task == 7) {
            if (amp <= 60) {
                getDoubleList(jsonObject, 20.99, 20.99, 20.09,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 80) {
                getDoubleList(jsonObject, 20.99, 20.99, 10.08,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 130) {
                getDoubleList(jsonObject, 12.01, 12.01, 10.08,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 12.01, 12.01, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 7.97, 7.97, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else if (task == 8) {
            if (amp <= 80) {
                getDoubleList(jsonObject, 20.99, 20.99, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 12.01, 12.01, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 7.97, 7.97, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else if (task == 10) {
            if (amp <= 80) {
                getDoubleList(jsonObject, 20.99, 20.99, 29.18,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 12.01, 12.01, 29.18,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 7.97, 7.97, 29.18,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else if (task == 11) {
            if (amp <= 75) {
                getDoubleList(jsonObject, 20.99, 20.99, 10.08,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 80) {
                getDoubleList(jsonObject, 20.99, 20.99, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 12.01, 12.01, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 7.97, 7.97, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else if (task == 14) {
            if (amp <= 80) {
                getDoubleList(jsonObject, 20.99, 20.99, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 12.01, 12.01, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 7.97, 7.97, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else if (task == 15) {
            if (amp <= 80) {
                getDoubleList(jsonObject, 20.99, 20.99, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 12.01, 12.01, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 7.97, 7.97, 2.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else if (task == 16) {
            if (amp <= 80) {
                getDoubleList(jsonObject, 60.39, 20.99, 31.8,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 145) {
                getDoubleList(jsonObject, 60.39, 12.01, 31.8,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            } else if (amp <= 255) {
                getDoubleList(jsonObject, 60.39, 7.97, 31.8,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
            }
        } else {
            getDoubleList(jsonObject, 1.0, 1.0, 1.0,doubleListUi,doubleListUs,doubleListU01,doubleListU02);
        }
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/10/15 10:43
     * @ Params: [jsonObject]
     * @ return: com.alibaba.fastjson.JSONObject
     * @ Description: 递推平均滤波法，又称滑动平均滤波法
     */
    public void recur(List<List<Double>> doubleListUi, List<List<Double>> doubleListUs, List<List<Double>> doubleListU01,List<List<Double>> doubleListU02) {
        //平均滤波
        double n = 8.0;

        if (doubleListUs != null && doubleListUs.size() > 0) {
            for (int i = 0; i < doubleListUs.size() - n; i++) {
                double sum = 0;
                for (int j = 0; j < n; j++) {
                    sum = sum + Double.parseDouble(doubleListUs.get(i + j).get(1).toString());
                }
                doubleListUs.get(i).set(1, Double.valueOf(df().format(sum / n)));
            }
        }
        for (int i = 0; i < doubleListUi.size() - n; i++) {
            double sum = 0;
            for (int j = 0; j < n; j++) {
                sum = sum + Double.parseDouble(doubleListUi.get(i + j).get(1).toString());
            }
            doubleListUi.get(i).set(1, Double.valueOf(df().format(sum / n)));
        }
        for (int i = 0; i < doubleListU01.size() - n; i++) {
            double sum = 0;
            for (int j = 0; j < n; j++) {
                sum = sum + Double.parseDouble(doubleListU01.get(i + j).get(1).toString());
            }
            doubleListU01.get(i).set(1, Double.valueOf(df().format(sum / n)));
        }
        if (doubleListU02 != null && doubleListU02.size() > 0) {
            for (int i = 0; i < doubleListU02.size() - n; i++) {
                double sum = 0;
                for (int j = 0; j < n; j++) {
                    sum = sum + Double.parseDouble(doubleListU02.get(i + j).get(1).toString());
                }
                doubleListU02.get(i).set(1, Double.valueOf(df().format(sum / n)));
            }
        }

    }

    public void savePot(JSONObject jsonObject){
        Pot pot = new Pot();
        pot.setPotId(jsonObject.getInteger("POT1"));
        pot.setIb(jsonObject.getDouble("IB") * 3.0);
        pot.setIc(jsonObject.getDouble("IC"));
        pot.setUpdateTime(LocalDateTime.now());
        boolean b = potService.saveOrUpdate(pot);
        System.out.println("消息入库：" + b);
    }

    public void savePotV1E(JSONObject jsonObject){
        Pot pot = new Pot();
        pot.setPotId(jsonObject.getInteger("POT1"));
        pot.setV1e(jsonObject.getDouble("V1E"));
        pot.setV1b(jsonObject.getDouble("V1B"));
        pot.setV1c(jsonObject.getDouble("V1C"));

        pot.setUpdateTime(LocalDateTime.now());
        boolean b = potService.saveOrUpdate(pot);
        System.out.println("消息入库：" + b);
    }

}
