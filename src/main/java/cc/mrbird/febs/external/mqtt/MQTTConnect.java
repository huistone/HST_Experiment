package cc.mrbird.febs.external.mqtt;


import lombok.Data;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;


//https://blog.csdn.net/qq_37949192/article/details/103962848?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param
//https://blog.csdn.net/lxw983520/article/details/94348263?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-7.channel_param&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-7.channel_param
/**
 * @ Author: 马超伟
 * @ Date: 2020/10/9 18:48
 * @ Params:  * @ Param null
 * @ return:
 * @ Description: MQTT 连接工具类
 */
@Data
@PropertySource("classpath:/application.yml")
@ConfigurationProperties("mqtt")
@Component
public class MQTTConnect {

    /**
     * 连接地址
     */
    public static final String url = "tcp://39.102.57.86:1883";

    /**
     * 用户名
     */
    public static final String username = "admin";

    /**
     * 密码
     */
    public static final String password = "hst123";

    /**
     * 订阅客户端ID
     */
    public static String clientID;

    static {
        try {
            clientID = "clientId_"+InetAddress.getLocalHost().getHostName();
            if (clientID.length()>22){
                clientID = clientID.substring(0,22);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 发布客户端ID
     */
    public static String publishID;

    static {
        try {
            publishID = "publishId_"+InetAddress.getLocalHost().getHostName();
            if (clientID.length()>22){
                clientID = clientID.substring(0,22);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 超时时间
     */
    public static final Integer timeout = 3000;

    /**
     * 保持连接数
     */
    public static final Integer keepalive = 20;




    //    * 把配置里的 cleanSession 设为false，客户端掉线后 服务器端不会清除session，
//    * 当重连后可以接收之前订阅主题的消息。当客户端上线后会接受到它离线的这段时间的消息，
//    * 如果短线需要删除之前的消息则可以设置为true
    public MqttConnectOptions getOptions() {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setUserName(username);
        options.setPassword(password.toCharArray());
        options.setConnectionTimeout(timeout);
        //设置心跳
        options.setKeepAliveInterval(keepalive);
        return options;
    }

    public MqttConnectOptions getOptions(MqttConnectOptions options) {
        options.setCleanSession(false);
        options.setUserName(username);
        options.setPassword(password.toCharArray());
        options.setConnectionTimeout(timeout);
        options.setKeepAliveInterval(keepalive);
        return options;
    }


//    //	随机生成唯一client.id方法
//    public String getClientId() {
//        String nums = "";
//        String[] codeChars = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
//                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
//                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
//        for (int i = 0; i < 23; i++) {
//            int charNum = (int) Math.floor(Math.random() * codeChars.length);
//            nums = nums + codeChars[charNum];
//        }
//        return nums;
//    }

    public static void main(String[] args) throws Exception {
        InetAddress addr = InetAddress.getLocalHost();
        String ip=addr.getHostAddress().toString(); //获取本机ip
        String hostName=addr.getHostName().toString(); //获取本机计算机名称
        System.out.println(ip);
        System.out.println(hostName);
    }

}

