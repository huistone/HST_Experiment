package cc.mrbird.febs.external.netty;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/17/16:31
 * @ Description:
 */
public class TcpClient extends Thread{

        //定义一个Socket对象
        Socket socket = null;

        public TcpClient(String host, int port) {
            try {
                //需要服务器的IP地址和端口号，才能获得正确的Socket对象
                socket = new Socket(host, port);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            //客户端一连接就可以写数据个服务器了
            new sendMessThread().start();
            super.run();
            try {
                // 读Sock里面的数据
                InputStream s = socket.getInputStream();
                byte[] buf = new byte[1024];
                int len = 0;
                while ((len = s.read(buf)) != -1) {
                    System.out.println(new String(buf, 0, len));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //往Socket里面写数据，需要新开一个线程
        class sendMessThread extends Thread{
            @Override
            public void run() {
                super.run();
                //写操作
                Scanner scanner=null;
                OutputStream os= null;
                try {
                    scanner=new Scanner(System.in);
                    os= socket.getOutputStream();
                    String in="";
                    do {
                        in=scanner.next();
                        os.write((""+in).getBytes());
                        os.flush();
                    } while (!in.equals("bye"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                scanner.close();
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //函数入口
        public static void main(String[] args) {
            //需要服务器的正确的IP地址和端口号
            TcpClient clientTest=new TcpClient("192.168.2.137", 20108);
            clientTest.start();
        }

    public static void masin(String[] args)throws Exception {
        Socket socket = null;
        BufferedReader in = null;
        BufferedWriter out = null;
        BufferedReader wt = null;
        try {
            //创建Socket对象，指定服务器端的IP与端口
            socket = new Socket("192.168.2.137", 20108);
            //获取scoket的输入输出流接收和发送信息
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            wt = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                //发送信息
                String str = wt.readLine();
                out.write(str + "\n");
                out.flush();
                //如果输入的信息为“end”则终止连接
                if (str.equals("end")) {
                    break;
                }
                //否则，接收并输出服务器端信息
                System.out.println("服务器端说：" + in.readLine());
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (wt != null) {
                try {
                    wt.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
