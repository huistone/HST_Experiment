package cc.mrbird.febs.external.netty.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.util.CharsetUtil;
import org.springframework.stereotype.Component;

/**
 * server 初始化类
 */
@Component
public class NettyServerChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {

        /**
         * LengthFieldBasedFrameDecoder
         * 常用的处理大数据分包传输问题的解决类，先对构造方法LengthFieldBasedFrameDecoder中的参数做以下解释说明“
         * maxFrameLength：解码的帧的最大长度
         * lengthFieldOffset ：长度属性的起始位（偏移位），包中存放有整个大数据包长度的字节，这段字节的其实位置
         * lengthFieldLength：长度属性的长度，即存放整个大数据包长度的字节所占的长度
         * lengthAdjustmen：长度调节值，在总长被定义为包含包头长度时，修正信息长度。initialBytesToStrip：跳过的字节数，根据需要我们跳过lengthFieldLength个字节，以便接收端直接接受到不含“长度属性”的内容
         * failFast ：为true，当frame长度超过maxFrameLength时立即报TooLongFrameException异常，为false，读取完整个帧再报异常
         */

        //Netty服务端：定义一个LengthFieldBasedFrameDecoder(1024*1024*1024, 0, 4,0,4))，最大数据量是1GB,长度属性位于数据包头部，占4个字节，协议体调节值为0，跳过头部协议长度四个字节
//        ByteBuf delimiter = Unpooled.copiedBuffer("$_$".getBytes());
//        channel.pipeline().addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
        channel.pipeline().addLast(new LineBasedFrameDecoder(1024));
        channel.pipeline().addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
        channel.pipeline().addLast(new FixedLengthFrameDecoder(10));
//        channel.pipeline().addLast("httpResponseEndcoder", new HttpResponseEncoder());
//        channel.pipeline().addLast("HttpRequestDecoder", new HttpRequestDecoder());
        ChannelPipeline pipeline = channel.pipeline();
        //处理http消息的编解码
    /*    pipeline.addLast("httpServerCodec", new HttpServerCodec());
        pipeline.addLast("aggregator", new HttpObjectAggregator(65536));*/
        //添加自定义的ChannelHandler
//        pipeline.addLast("httpServerHandler", new HttpServerChannelHandler0());
        channel.pipeline().addLast("framedecoder",new LengthFieldBasedFrameDecoder(1024*1024*1024, 0, 4,0,4));
//        channel.pipeline().addLast(new DelimiterBasedFrameDecoder(65535, delimiter));;

        channel.pipeline().addLast(new NettyServerHandler());
    }
}
