package cc.mrbird.febs.external.netty;

import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.system.service.IUserService;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;


@Component
public class ServerHandler extends IoHandlerAdapter {

    @Resource
    private RedisService redisService;
    @Resource
    private IUserService userService;

    private static ServerHandler serverHandler;

    //在初始化的时候初始化静态对象和它的静态成员变量healthDataService，原理是拿到service层bean对象，静态存储下来，防止被释放。
    @PostConstruct //通过@PostConstruct实现初始化bean之前进行的操作
    public void init() {
        serverHandler = this;
        // 初使化时将已静态化的testService实例化
        serverHandler.redisService = this.redisService;
        serverHandler.userService = this.userService;
    }


    public void saveData(String msg) {
        //截取出来所有的字段
        String[] split = msg.split(",");
        //对每一个字段进行操作
        for (String item : split) {
            //截取key和value
            String[] pram_split = item.split("=");
            String key = pram_split[0];
            String value = pram_split[1];
            try {
                double parseDouble = Double.parseDouble(value);
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        }

//        mainDataList.add(mainData);
//        System.out.println("mainDataList.size==="+mainDataList.size());
//
//        if (mainDataList.size()%2000==0){
//            serverHandler.mainDataService.saveBatch(mainDataList);
//            mainDataList.clear();
//        }

//        boolean save = serverHandler.mainDataService.save(mainData);
//        System.out.println("save----： "+save);

    }

    /**
     * 发送完成之后的方法回调
     */
    public void destryList(){
//        serverHandler.mainDataService.saveBatch(mainDataList);
    }





    //测试调用
    public void test(String msg, int num) {
        String[] split = msg.split(":");
        Boolean data = serverHandler.redisService.lSet("data:" + split[0], split[1]);
        System.out.println("data--- " + data);
    }

    public String[] truncate(String[] str) {
        String[] ss = new String[str.length - 1];
        System.arraycopy(str, 1, ss, 0, ss.length);
        return ss;
    }


}
