package cc.mrbird.febs.external.netty.server;

import cc.mrbird.febs.external.netty.ServerHandler;
import cc.mrbird.febs.others.service.IEximportService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * server 处理类
 */
@Slf4j
@Component
@ChannelHandler.Sharable
public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    /**
     * Channel，表示一个连接，可以理解为每一个请求，就是一个Channel。
     * ChannelHandler，核心处理业务就在这里，用于处理业务请求。
     * ChannelHandlerContext，用于传输业务数据。
     * ChannelPipeline，用于保存处理过程需要用到的ChannelHandler和ChannelHandlerContext。
     */
    @Resource
    private IEximportService eximportService;

    /**
     * 管理一个全局map，保存连接进服务端的通道数量
     */
    public static final ConcurrentHashMap<ChannelId, ChannelHandlerContext> CHANNEL_MAP = new ConcurrentHashMap<>();

    /**
     * @ Param ctx ChannelHandlerContext
     * @author Macwx on 2020/4/02 16:10
     * @ Description: 有客户端连接服务器会触发此函数
     * @ return: void
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {

        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();

        String clientIp = insocket.getAddress().getHostAddress();
        int clientPort = insocket.getPort();

        //获取连接通道唯一标识
        ChannelId channelId = ctx.channel().id();

        System.out.println();
        //如果map中不包含此连接，就保存连接
        if (CHANNEL_MAP.containsKey(channelId)) {
            log.info("客户端【" + channelId + "】是连接状态，连接通道数量: " + CHANNEL_MAP.size());
        } else {
            //保存连接
            log.info("channelId==" + channelId);
            log.info("ctx==" + ctx);
            CHANNEL_MAP.put(channelId, ctx);

            log.info("客户端【" + channelId + "】连接netty服务器[IP:" + clientIp + "--->PORT:" + clientPort + "]");
            log.info("连接通道数量: " + CHANNEL_MAP.size());
        }
    }

    /**
     * @ Param ctx  ChannelHandlerContext
     * @author Macwx on 2020/4/02 16:10
     * @ Description: 有客户端终止连接服务器会触发此函数
     * @ return: void
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = insocket.getAddress().getHostAddress();
        ChannelId channelId = ctx.channel().id();
        //包含此客户端才去删除
        if (CHANNEL_MAP.containsKey(channelId)) {
            //删除连接
            CHANNEL_MAP.remove(channelId);
            System.out.println();
            log.info("客户端【" + channelId + "】退出netty服务器[IP:" + clientIp + "--->PORT:" + insocket.getPort() + "]");
            log.info("连接通道数量: " + CHANNEL_MAP.size());
        }
    }

    /**
     * @ Param ctx ChannelHandlerContext
     * @author Macwx on 2020/4/02 16:10
     * @ Description: 有客户端发消息会触发此函数
     * @ return: void
     */
    ExecutorService executor = Executors.newFixedThreadPool(5);
    int num = 1;
    long startTime = System.currentTimeMillis();
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println();
        // 测试运行时间
        System.out.println("开始运行时间："+startTime);
//        log.info("加载客户端报文......");
//        log.info("【" + ctx.channel().id() + "】" + " :");
//        log.info(""+msg);

        /**
         *  下面可以解析数据，保存数据，生成返回报文，将需要返回报文写入write函数
         */
        num++;
        System.out.println("-----------------------------------------------------" + num);
        System.out.println(msg);
        new ServerHandler().saveData(msg.toString());


        //引入异步业务线程池的方式，避免长时间业务耗时业务阻塞netty本身的worker工作线程
        /*executor.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                log.info("收到服务端发来的方法请求了--------------------------------------------");
                System.out.println("-------->>>>>>>>>>---"+msg.toString());

                return null;
            }
        });*/
        //响应客户端
        //  this.channelWrite(ctx.channel().id(), msg);
    }


    /**
     * 数据发送完成后的回调
     * @ Param ctx ChannelHandlerContext
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
        // 结束时间
        long endTime = System.currentTimeMillis();
        System.out.println("结束时间："+endTime);
        int time = (int) (endTime - startTime);
        System.err.println("运行时间:" + time + "ms");
        System.out.println("===================数据发送完成-----------------------------");
        new ServerHandler().destryList();
    }


    /**
     * @ Param msg       需要发送的消息内容
     * @ Param channelId 连接通道唯一id
     * @author Macwx on 2020/4/02 16:10
     * @ Description: 服务端给客户端发送消息
     * @ return: void
     */
    public void channelWrite(ChannelId channelId, Object msg) throws Exception {
        ChannelHandlerContext ctx = CHANNEL_MAP.get(channelId);
        if (ctx == null) {
            log.info("通道【" + channelId + "】不存在");
            return;
        }
        if (msg == null && msg == "") {
            log.info("服务端响应空的消息");
            return;
        }
        //将客户端的信息直接返回写入ctx
        ctx.write(msg);
        //刷新缓存区
        ctx.flush();
    }


    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

        String socketString = ctx.channel().remoteAddress().toString();

        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state() == IdleState.READER_IDLE) {
                log.info("Client: " + socketString + " READER_IDLE 读超时");
                ctx.disconnect();
            } else if (event.state() == IdleState.WRITER_IDLE) {
                log.info("Client: " + socketString + " WRITER_IDLE 写超时");
                ctx.disconnect();
            } else if (event.state() == IdleState.ALL_IDLE) {
                log.info("Client: " + socketString + " ALL_IDLE 总超时");
                ctx.disconnect();
            }
        }
    }


    /**
     * @ Param ctx ChannelHandlerContext
     * @author Macwx on 2020/4/02 16:10
     * @ Description: 发生异常会触发此函数
     * @ return: void
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {

        System.out.println();
        ctx.close();
        log.info(ctx.channel().id() + " 发生了错误,此连接被关闭" + "此时连通数量: " + CHANNEL_MAP.size());
        cause.printStackTrace();
    }


}
