package cc.mrbird.febs.external.netty.service;

import cc.mrbird.febs.external.netty.server.NettyServerHandler;
import cc.mrbird.febs.external.netty.util.ConvertCode;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;

@Slf4j
@Service
public class ServerService {

    public void sendMsg(String msg){
        Set<Map.Entry<ChannelId, ChannelHandlerContext>> entrySet = NettyServerHandler.CHANNEL_MAP.entrySet();
        System.out.println(entrySet);
        for (Map.Entry<ChannelId, ChannelHandlerContext> contextEntry : entrySet) {
            ChannelHandlerContext ctx = NettyServerHandler.CHANNEL_MAP.get(contextEntry.getKey());
             writeToClient(msg, ctx);
        }
    }


    /**
     * 公用回写数据到客户端的方法
     * @ Param receiveStr 需要回写的字符串
     * @ Param channel
     * <br>//channel.writeAndFlush(msg);//不行
     * <br>//channel.writeAndFlush(receiveStr.getBytes());//不行
     * <br>在netty里，进出的都是ByteBuf，此处应确定服务端是否有对应的编码器，将字符串转化为ByteBuf
     */
    private void writeToClient(String receiveStr, ChannelHandlerContext channel) {
        try {
            ByteBuf bufff = Unpooled.buffer();//netty需要用ByteBuf传输
            ByteBuf byteBuf = bufff.writeBytes(ConvertCode.hexString2Bytes(receiveStr));//对接需要16进制
            channel.writeAndFlush(bufff).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    StringBuilder sb = new StringBuilder("");
                    String mark = "测试：";
                    if (!StringUtils.isEmpty(mark)) {
                        sb.append("【").append(mark).append("】");
                    }
                    if (future.isSuccess()) {
                        System.out.println(sb.toString() + "回写成功" + receiveStr);
                        log.info(sb.toString() + "回写成功" + receiveStr);
                    } else {
                        System.out.println(sb.toString() + "回写失败" + receiveStr);
                        log.error(sb.toString() + "回写失败" + receiveStr);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("调用通用writeToClient()异常"+e.getMessage());
            log.error("调用通用writeToClient()异常：",e);
        }
    }

}
