package cc.mrbird.febs.external.webScoket;

import cc.mrbird.febs.im.imWebSocket.LayimChatWebSocket;
import cc.mrbird.febs.im.service.IImMsgService;
import cc.mrbird.febs.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/16/14:35
 * @ Description: 开启WebSocket支持
 * 参考地址： https://www.cnblogs.com/mrjkl/p/13089320.html
 */
@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

    @Autowired
    private void setRedisService(IDetRecordService detRecordService){
        WebSocketServer.detRecordService = detRecordService;
    }

    @Autowired
    private void setIWaveService(IWaveService waveService){
        WebSocketServer.waveService = waveService;
    }


    @Autowired
    private void setIImMsgService(IImMsgService imMsgService){
        LayimChatWebSocket.imMsgService = imMsgService;
    }

    @Autowired
    private void setIDeptService(IDeptService deptService){
        LayimChatWebSocket.deptService = deptService;
    }

    @Autowired
    private void setIUserDeptService(IUserDeptService userDeptService){
        LayimChatWebSocket.userDeptService = userDeptService;
    }

    @Autowired
    private void setIUserService(IUserService userService){
        LayimChatWebSocket.userService = userService;
    }




}