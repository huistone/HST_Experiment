package cc.mrbird.febs.external.oss;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 常量类，读取配置文件application.yml中的配置
 */
//implements InitializingBean
@Component
public class OSSConstant  {

    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint;

    @Value("${aliyun.oss.file.keyid}")
    private String keyId;

    @Value("${aliyun.oss.file.keysecret}")
    private String keySecret;

    @Value("${aliyun.oss.file.filehost}")
    private String fileHost;

    @Value("${aliyun.oss.file.bucketname}")
    private String bucketName;

    public static String END_POINT = "http://oss-cn-beijing.aliyuncs.com";
    public static String ACCESS_KEY_ID = "LTAI4FmFpCt8CVJCFuizFHSK";
    public static String ACCESS_KEY_SECRET = "i6FavZBuTy4jBEu8DUpdotm3noWB9g";
    public static String BUCKET_NAME = "huistoneoss";
    public static String FILE_HOST = "http://file.huistone.com";



//    @Override
//    public void afterPropertiesSet() throws Exception {
//        END_POINT = endpoint;
//        ACCESS_KEY_ID = keyId;
//        ACCESS_KEY_SECRET = keySecret;
//        BUCKET_NAME = bucketName;
//        FILE_HOST = fileHost;
//    }
}
