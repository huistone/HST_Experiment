package cc.mrbird.febs.app.interceptor;

import cc.mrbird.febs.app.jwt.constant.Constant;
import cc.mrbird.febs.app.jwt.util.JwtUtil;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.utils.JsonConvertUtil;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName JwtInterceptor
 * @Description TODO
 * @Author admin
 * @Date 2021/1/20 10:34
 * @Version 1.0
 */
@Component
@Slf4j
public class JwtInterceptor implements HandlerInterceptor {

    public static JwtInterceptor jwtInterceptor;

    @Resource
    private RedisService redisService;

    @PostConstruct
    public void init(){
        jwtInterceptor = this;
        jwtInterceptor.redisService = this.redisService;
    }



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Map<String, Object> map = new HashMap<>();
        String token = request.getHeader("AccessToken");
        //如果是预检请求就过滤
        if("OPTIONS".equals(request.getMethod())){
            return true;
        }

        if(token == null){
            map.put("code",401);
            map.put("msg","无token");
            responseJson(response,map);
            return false;
        }

        //获取用户名
        String username = JwtUtil.getClaim(token, Constant.ACCOUNT);

        //判断当前token是否过期，不过期刷新令牌
        Boolean expiredToken = isExpiredToken(response,token, username);
        if(expiredToken){
            //浏览器登录挤掉上一个账户
            //获取token的时间戳，与上一个token时间戳进行比对，如果不同则为异地登录
            String oldToken = (String) jwtInterceptor.redisService.get(Constant.PREFIX_SHIRO_REFRESH_TOKEN + username);
            if (!token.equals(oldToken)){
                map.put("code",405);
                map.put("message","您的账号已在别处登录！！！");
                responseJson(response,map);
                return false;
            }
            //刷新令牌
            return true;
        } else {
            //令牌已过期
            map.put("code",402);
            map.put("msg","令牌已过期，请刷新");
            responseJson(response,map);
            return false;
        }
    }

    /**
     * @Author wangke
     * @Description 判断token是否刷新
     * @Date 11:12 2020/9/21
     * @Param
     * @return
     */
    public Boolean isExpiredToken( HttpServletResponse response, String token, String username){
        try{
            if (JwtUtil.verify(token) && jwtInterceptor.redisService.get(Constant.PREFIX_SHIRO_REFRESH_TOKEN + username)!=null ) {
                return true;
            }
        } catch (Exception e){
            // 认证出现异常，传递错误信息msg
            String msg = e.getMessage();
            // 获取应用异常(该Cause是导致抛出此throwable(异常)的throwable(异常))
            Throwable throwable = e.getCause();
            if (throwable instanceof SignatureVerificationException) {
                // 该异常为JWT的AccessToken认证失败(Token或者密钥不正确)
                msg = "Token或者密钥不正确(" + throwable.getMessage() + ")";
            } else if (throwable instanceof TokenExpiredException) {
                // 该异常为JWT的AccessToken已过期
                Map<String, Object> map = new HashMap<>();
                map.put("code",402);
                map.put("msg","token已过期");
                responseJson(response,map);
                return false;
            }
        }
        return false;
    }


    /**
     * 无需转发，直接返回Response信息
     */
    private void responseJson(ServletResponse response, Map map) {
        HttpServletResponse httpServletResponse = WebUtils.toHttp(response);
        httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = httpServletResponse.getWriter()) {
            String data = JsonConvertUtil.objectToJson(new FebsResponse().data(map));
            out.append(data);
        } catch (IOException e) {
            log.error("直接返回Response信息出现IOException异常:{}", e.getMessage());
            throw new FebsException("直接返回Response信息出现IOException异常:" + e.getMessage());
        }
    }
}
