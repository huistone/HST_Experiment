package cc.mrbird.febs.app.controller;

import cc.mrbird.febs.api.question.entity.QuestionResult;
import cc.mrbird.febs.app.entity.dto.QuestionSubDTO;
import cc.mrbird.febs.app.entity.vo.QuestionDetailVo;
import cc.mrbird.febs.app.entity.vo.QuestionListVo;
import cc.mrbird.febs.app.entity.vo.QuestionSubVo;
import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentRemarkService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName QuestionApiController
 * @Description TODO
 * @Author admin
 * @Date 2021/1/21 10:26
 * @Version 1.0
 */
@RestController("appQuestionApiController")
@Slf4j
@RequestMapping("/app/question")
public class QuestionApiController extends BaseController {

    @Resource
    private IExperimentQuestionService experimentQuestionService;

    @Resource
    private IExperimentAnswerService experimentAnswerService;

    @Resource
    private IExperimentRemarkService experimentRemarkService;


    public static void main(String[] args) {
        for(int i = 65;i<=70;i++){
            char op = (char) i;
            System.out.println(op);
        }
    }

    @RequestMapping("getQuestionList")
    @ControllerEndpoint(operation = "查询H5习题列表",exceptionMessage = "查询H5习题列表失败")
    public FebsResponse getQuestionList(HttpServletRequest request,Integer type,Integer projectId){

        User currentUserByToken = getCurrentUserByToken(request);
        if (currentUserByToken == null){
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isBlank(type) || IntegerUtils.isBlank(projectId)){
            return new FebsResponse().no_param();
        }
        List<ExperimentQuestion> list = experimentQuestionService.list(new QueryWrapper<ExperimentQuestion>()
                .lambda()
                .eq(ExperimentQuestion::getProjectId, projectId)
                .eq(ExperimentQuestion::getType, type));
        List<QuestionListVo> answerList = new ArrayList<>();
        ArrayList<Integer> trueOption = null;
        QuestionListVo questionListVo = null;
        ArrayList<Character> optionList = null;
        ArrayList<QuestionDetailVo> testList= null;
        for (ExperimentQuestion experimentQuestion : list) {
            trueOption = new ArrayList<>();
            questionListVo = new QuestionListVo();
            optionList = new ArrayList<>();
            testList = new ArrayList<QuestionDetailVo>();
            questionListVo.setQuestionId(experimentQuestion.getQuestionId());
            questionListVo.setTitle(experimentQuestion.getQuestionName());
            questionListVo.setNumber(experimentQuestion.getNumber());
            questionListVo.setQuestionType(experimentQuestion.getQuestionType());
            if (experimentQuestion.getQuestionType() == 4){
                questionListVo.setIsMultiOrSingle(2);
            }else{
                questionListVo.setIsMultiOrSingle(1);
            }
            //选项内容
            String context = experimentQuestion.getContext();
            //正确答案
            String answer = experimentQuestion.getAnswer();
            String[] split = context.split("@@");
            int cIndex = 65;
            QuestionDetailVo questionDetailVo = null;
            for(int i = 0;i<split.length;i++){
                char op = (char) cIndex++;
                questionDetailVo = new QuestionDetailVo();
                questionDetailVo.setCode(false);
                questionDetailVo.setName(split[i]);
                questionDetailVo.setOption(op);
                optionList.add(op);
                //如果是判断题,则直接跟内容进行比较
                if(experimentQuestion.getQuestionType() == 2){
                    if (split[i].equals(answer)){
                        trueOption.add(i);
                        questionDetailVo.setCode(true);
                    }
                } else {
                    if(answer.contains(String.valueOf(op))){
                        trueOption.add(i);
                        questionDetailVo.setCode(true);
                    }
                }
                testList.add(questionDetailVo);
            }
            questionListVo.setTrueOption(trueOption);
            questionListVo.setTestList(testList);
            questionListVo.setOption(optionList);
            answerList.add(questionListVo);
        }
        if (answerList.size()>0){
            return new FebsResponse().success().data(answerList);
        }else{
            return new FebsResponse().fail().message("该课程下暂无习题");
        }
    }

    @RequestMapping("submitAnswer")
    public FebsResponse submitAnswer(  @RequestBody QuestionSubDTO listMap){
        System.out.println(listMap);
        return new FebsResponse();
    }


    /**
     * @ Description: ，预习题的提交接口，接收前端数据
     * @ Param: [request]
     * @ return: java.lang.String
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     * 每个项目 每个学生只能答一次题
     * 即根据 项目ID 和 学生ID 查询数据 若有 代表已提交过一次 直接return
     * 因为参数是流的形式发送 没办法直接获取 projectId 故通过转换玩的json对象获取
     */
    @RequestMapping(value = "submitPrepareQuestion",  produces = "application/json;charset=UTF-8")
    @ControllerEndpoint(operation = "h5预习题提交成功",exceptionMessage = "h5预习题提交失败")
    public FebsResponse submitPrepareQuestion(HttpServletRequest request, String str) {
        User currentUser = getCurrentUserByToken(request);
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        List<QuestionSubVo> questionSubVos = JSONObject.parseArray(str, QuestionSubVo.class);
        QuestionSubVo questionSubVo = questionSubVos.get(0);
        Long projectId = Long.parseLong(questionSubVo.getProjectId());
        ExperimentRemark answers = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType,currentUser.getUserType())
                .eq(ExperimentRemark::getIsDel,1)
                .eq(currentUser.getUserType() == 1,ExperimentRemark::getDeptId, currentUser.getDeptId())
                .eq(ExperimentRemark::getType, 1));
        //判断用户是否已经提交过。
        if (answers!= null && answers.getScore() >= 60.0) {
            return new FebsResponse().put_code(500, "成绩合格,不可再次提交习题");
        }
        //查找出之前所有的预习题提交记录,将他们逻辑删除
        List<ExperimentRemark> list = experimentRemarkService.list(new QueryWrapper<ExperimentRemark>()
                .lambda()
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType, 1)
                .eq(ExperimentRemark::getDeptId, currentUser.getDeptId())
                .eq(ExperimentRemark::getType, 1));
        boolean flag = false;
        if(list!=null&&list.size()>0){
            for (ExperimentRemark experimentRemark : list) {
                if (experimentRemark.getIsCommit() == 2){
                    flag = true;
                    break;
                }
            }
        }
        if (flag){
            return new FebsResponse().fail().message("该实验已批阅,不可重复做题");
        }
        for (ExperimentRemark experimentRemark : list) {
            experimentRemarkService.removeById(experimentRemark.getRemarkId());
        }
        ExperimentRemark experimentRemark = new ExperimentRemark();
        experimentRemark.setType(1);
        experimentRemark.setCreateId(currentUser.getUserId());
        experimentRemark.setIsCommit(1);
        experimentRemark.setUserType(currentUser.getUserType());
        experimentRemark.setDeptId(currentUser.getDeptId());
        experimentRemark.setProjectId(projectId);
        experimentRemark.setCreateTime(LocalDateTime.now());
        experimentRemarkService.save(experimentRemark);

        Double score = 0.0;
        QuestionResult questionResult = new QuestionResult();
        List<Long> questionResultList = new ArrayList<>();
        List<ExperimentAnswer> experimentAnswerList = new ArrayList<>();
        for (int i = 0; i < questionSubVos.size(); i++) {
            //json对象转Java实体类
            ExperimentAnswer answer = new  ExperimentAnswer();
            ExperimentQuestion experimentQuestion = experimentQuestionService.getOne(new QueryWrapper<ExperimentQuestion>().lambda()
                    .eq(ExperimentQuestion::getQuestionId, questionSubVo.getQuestionId())
                    .select(ExperimentQuestion::getAnswer,
                            ExperimentQuestion::getQuestionType,
                            ExperimentQuestion::getType));

            double s = 0.0;
            answer.setCreateId(experimentRemark.getRemarkId());
            answer.setUserType(currentUser.getUserType());
            answer.setScore(s);
            answer.setProjectId(projectId);
            answer.setType(experimentQuestion.getType());
            answer.setQuestionType(questionSubVos.get(i).getQuestionType());
            answer.setContext(questionSubVos.get(i).getContext());
            answer.setNumber(questionSubVos.get(i).getNumber());
            answer.setQuestionId(questionSubVos.get(i).getQuestionId().longValue());
            if (getScore(answer)) {
                score += 10.0;
                s += 10.0;
                answer.setScore(10.0);
            } else {
                questionResultList.add(answer.getQuestionId());
            }

            experimentAnswerList.add(answer);
        }
        experimentAnswerService.saveBatch(experimentAnswerList);
        questionResult.setErroQuestionList(questionResultList);
        questionResult.setScore(score);
        experimentRemarkService.update(new UpdateWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getRemarkId, experimentRemark.getRemarkId())
                .set(ExperimentRemark::getScore, score));
        return new FebsResponse().success().message("提交成功！").data(questionResult);
    }

    /**
     * @ Description: 思考题的提交接口
     * @ Param: [request]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/30
     */
    @RequestMapping("/submitThinkQuestion")
    @ControllerEndpoint(operation = "h5思考题提交成功",exceptionMessage = "h5思考题提交失败")
    public FebsResponse shortAnswerSub(HttpServletRequest request, String str) {
        User currentUser = getCurrentUserByToken(request);
        if (currentUser == null){
            return new FebsResponse().no_login();
        }
//        System.out.println(questionSubDTO);
        List<QuestionSubVo> questionSubVos = JSONObject.parseArray(str, QuestionSubVo.class);
        QuestionSubVo questionSubVo = questionSubVos.get(0);

        Long projectId = Long.parseLong(questionSubVo.getProjectId());
        List<ExperimentRemark> answers = experimentRemarkService.list(new QueryWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType,currentUser.getUserType())
                .eq(currentUser.getUserType() == 1,ExperimentRemark::getDeptId, currentUser.getDeptId())
                .eq(ExperimentRemark::getType, 2));
        if (answers.size() > 0) {
            return new FebsResponse().fail().message("您已提交，请勿重复提交");
        }
        ExperimentRemark experimentRemark = new ExperimentRemark();
        experimentRemark.setCreateId(currentUser.getUserId());
        experimentRemark.setIsCommit(1);
        experimentRemark.setDeptId(currentUser.getDeptId());
        experimentRemark.setProjectId(projectId);
        experimentRemark.setType(2);
        experimentRemark.setUserType(currentUser.getUserType());
        experimentRemark.setCreateTime(LocalDateTime.now());
        experimentRemarkService.save(experimentRemark);
        /**
         * 业务处理代码
         */
        QuestionResult questionResult = new QuestionResult();
        if (questionSubVos.size()>0) {
            List<ExperimentAnswer> experimentAnswerList = new ArrayList<>();
            double scoreOne = 100/questionSubVos.size();
            questionResult.setScore(0.0);
            List<Long> longList = new ArrayList<>();
            for (int i = 0; i < questionSubVos.size(); i++) {
                //json对象转Java实体类
                ExperimentAnswer answer = new ExperimentAnswer();
                answer.setCreateId(experimentRemark.getRemarkId());
                answer.setContext(questionSubVos.get(i).getContext());
                answer.setNumber(questionSubVos.get(i).getNumber());
                answer.setQuestionId(questionSubVos.get(i).getQuestionId().longValue());
                answer.setUserType(currentUser.getUserType());
                answer.setProjectId(projectId);
                answer.setDeptId(experimentRemark.getDeptId());
                answer.setQuestionType(questionSubVos.get(i).getQuestionType());
                ExperimentQuestion experimentQuestion = experimentQuestionService.getOne(new QueryWrapper<ExperimentQuestion>().lambda()
                        .eq(ExperimentQuestion::getQuestionId, questionSubVo.getQuestionId())
                        .select(ExperimentQuestion::getAnswer,
                                ExperimentQuestion::getQuestionType,
                                ExperimentQuestion::getType));
                answer.setType(experimentQuestion.getType());
                //如果是判断题，判断对错并设置分数
                if (experimentQuestion.getQuestionType()==2){
                    if (getScore(answer)) {
                        answer.setScore(scoreOne);
                        questionResult.setScore(questionResult.getScore()+scoreOne);
                    } else {
                        answer.setScore(0.0);
                        longList.add(answer.getQuestionId());
                    }
                    //其他题型设置
                }else {
                    if (answer.getContext().equals(experimentQuestion.getAnswer())){
                        answer.setScore(scoreOne);
                        questionResult.setScore(questionResult.getScore()+scoreOne);
                    }else {
                        answer.setScore(0.0);
                        longList.add(answer.getQuestionId());
                    }
                }
                experimentAnswerList.add(answer);

            }

            experimentAnswerService.saveBatch(experimentAnswerList);
            experimentRemarkService.update(new UpdateWrapper<ExperimentRemark>().lambda()
                    .set(ExperimentRemark::getScore,questionResult.getScore())
                    .eq(ExperimentRemark::getRemarkId,experimentRemark.getRemarkId()));
            questionResult.setErroQuestionList(longList);
        }
        return new FebsResponse().success().data(questionResult).message("提交成功！");
    }


    /**
     * 判断评分的接口
     * GuiZi_cheng
     * @ Param experimentAnswer
     * @return
     */
    public boolean getScore(ExperimentAnswer experimentAnswer) {
        ExperimentQuestion question = experimentQuestionService.getById(experimentAnswer.getQuestionId());
        //参考答案
        String answer = question.getAnswer().toUpperCase();

        //提交的答案
        String subAnswer = experimentAnswer.getContext().toUpperCase();
        String strAnswer = "";
        if (subAnswer.equals("A")) {
            strAnswer = "对";
        }
        if (subAnswer.equals("B")) {
            strAnswer = "错";
        }
        if (answer.equals(subAnswer) || answer.equals(strAnswer)) {
            return true;
        } else {
            return false;
        }
    }
}
