package cc.mrbird.febs.app.controller;

import cc.mrbird.febs.app.entity.vo.CourseAndProjectVo;
import cc.mrbird.febs.app.entity.vo.ProjectListVo;
import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import cn.hutool.http.server.HttpServerRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ ClassName HomeApi
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/1/20 10:31
 * @ Version 1.0
 */
@RestController("appHomeApiController")
@RequestMapping("/app/home")
@Slf4j
public class HomeApiController extends BaseController {

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private ICourseService courseService;

    @Resource
    private ICourseDeptService courseDeptService;

    /**
     * @Author wangke
     * @Description 查询热门实验接口
     * @Date 9:14 2021/1/27
     * @Param
     * @return
     */
    @RequestMapping("getHostProject")
    @ControllerEndpoint(operation = "查询首页热门实验",exceptionMessage = "查询首页热门实验失败")
    public FebsResponse getHostProject(HttpServletRequest request){
        try{
            User currentUserByToken = getCurrentUserByToken(request);

            if (currentUserByToken!=null && currentUserByToken.getDeptId()!=null){
                Long deptId = currentUserByToken.getDeptId();
                userDeptService.list(new QueryWrapper<UserDept>()
                        .lambda()
                        .eq(UserDept::getDeptId,deptId)
                        .eq(UserDept::getType,1));
                List<TeacherProject> list = teacherProjectService.list(new QueryWrapper<TeacherProject>()
                        .lambda()
                        .eq(TeacherProject::getDeptId, deptId)
                        .eq(TeacherProject::getIsDel, 1)
                        .select(TeacherProject::getProjectId));
                //查出开放的项目
                List<Long> collect = list.stream().map(TeacherProject::getProjectId).sorted(Comparator.naturalOrder()).distinct().collect(Collectors.toList());
                List<ExperimentProject> list1 = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .in(collect.size() > 0, ExperimentProject::getProjectId, collect)
                        .last("limit 0,5")
                        .select(ExperimentProject::getProjectId
                                , ExperimentProject::getProjectName
                                , ExperimentProject::getProjectPictureUrl
                                , ExperimentProject::getSubTitle
                        )
                        .orderByAsc(ExperimentProject::getProjectSort));
                return new FebsResponse().success().data(list1);
            } else  {
                List<ExperimentProject> list1 = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .last("limit 0,5")
                        .select(ExperimentProject::getProjectId
                                , ExperimentProject::getProjectName
                                , ExperimentProject::getProjectPictureUrl
                                , ExperimentProject::getSubTitle
                        )
                        .orderByAsc(ExperimentProject::getProjectSort));
                return new FebsResponse().success().data(list1);
            }
        }catch (Exception e){
            log.error("getHostProject:异常"+e.getMessage());
        }
        return new FebsResponse().success();
    }

    @RequestMapping("getOpenCourse")
    @ControllerEndpoint(operation = "查询已开放的课程",exceptionMessage = "查询已开放的课程失败")
    public FebsResponse getOpenCourse(HttpServletRequest request){
        User currentUserByToken = getCurrentUserByToken(request);
        List<Course> courseList = new ArrayList<>();
        //校园用户要判断后台是否开放
        if (currentUserByToken!=null && currentUserByToken.getUserType() == 1){
            Long deptId = currentUserByToken.getDeptId();
            List<CourseDept> list = courseDeptService.list(new QueryWrapper<CourseDept>()
                    .lambda()
                    .eq(CourseDept::getDeptId, deptId));

            for (CourseDept courseDept : list) {
                Course one = courseService.getOne(new QueryWrapper<Course>()
                        .lambda()
                        .eq(Course::getCourseId, courseDept.getCourseId())
                        .eq(Course::getState, 1)
                        .orderByAsc(Course::getCourseSort)
                        .select(Course::getCourseId,
                                Course::getCourseName,
                                Course::getCoursePictureUrl,
                                Course::getCourseSort));
                courseList.add(one);
            }
        } else {
          courseList = courseService.list(new QueryWrapper<Course>().lambda()
                  .eq(Course::getState,1));
        }
        List<Course> collect = courseList.stream().sorted(Comparator.comparing(Course::getCourseSort)).collect(Collectors.toList());
        return new FebsResponse().success().data(collect);
    }

    @RequestMapping("getCourseBar")
    @ControllerEndpoint(operation = "查询课程列表",exceptionMessage = "查询课程列表失败")
    public FebsResponse getCourseBar(){
        List<Course> list = courseService.list(new QueryWrapper<Course>()
                .lambda()
                .eq(Course::getState, 1)
                .select(Course::getCourseId, Course::getCourseName, Course::getCoursePictureUrl));

        return new FebsResponse().success().data(list);
    }

    @RequestMapping("getThreeProject")
    @ControllerEndpoint(operation = "查询三个实验项目",exceptionMessage = "查询三个实验项目失败")
    public FebsResponse getThreeProject(){
        List<ExperimentProject> list = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getState,1)
                .select(ExperimentProject::getProjectPictureUrl
                        ,ExperimentProject::getProjectId
                        ,ExperimentProject::getProjectName
                        ,ExperimentProject::getCourseId
                        ,ExperimentProject::getProjectContentUrl)
                .last("limit 0,3"));
        return new FebsResponse().success().data(list);
    }

    @RequestMapping("getCourseAndProject")
    @ControllerEndpoint(operation = "查询课程分类下的实验",exceptionMessage = "查询课程分类下的实验失败")
    public FebsResponse getCourseAndProject(HttpServletRequest request){
        User currentUserByToken = getCurrentUserByToken(request);
        List<CourseAndProjectVo> courseAndProjectVoArrayList = new ArrayList<>();
        CourseAndProjectVo courseAndProjectVo = null;
        if (currentUserByToken!=null && currentUserByToken.getUserType()==1){
            Long deptId = currentUserByToken.getDeptId();
            List<CourseDept> list = courseDeptService.list(new QueryWrapper<CourseDept>()
                    .lambda()
                    .eq(CourseDept::getDeptId, deptId));
            List<Integer> courseId = list.stream().distinct().map(CourseDept::getCourseId).sorted((x, y)->x-y).collect(Collectors.toList());
            List<Course> list1 = courseService.list(new QueryWrapper<Course>()
                    .lambda()
                    .in(courseId.size() > 0, Course::getCourseId, courseId)
                    .select(Course::getCourseName,Course::getCourseId));
            for (Course course : list1) {
                courseAndProjectVo = new CourseAndProjectVo();
                courseAndProjectVo.setCourseId(course.getCourseId());
                courseAndProjectVo.setCourseName(course.getCourseName());
                List<ProjectListVo> projectList = new ArrayList<>();
                ProjectListVo projectListVo = null;
                List<ExperimentProject> list2 = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq(ExperimentProject::getCourseId, course.getCourseId())
                        .eq(ExperimentProject::getState, 1)
                        .select(ExperimentProject::getProjectName
                                , ExperimentProject::getProjectId
                                , ExperimentProject::getProjectPictureUrl));
                for (ExperimentProject experimentProject : list2) {
                    projectListVo = new ProjectListVo();
                    projectListVo.setProjectId(experimentProject.getProjectId());
                    projectListVo.setProjectName(experimentProject.getProjectName());
                    projectListVo.setProjectPictureUrl(experimentProject.getProjectPictureUrl());
                    projectList.add(projectListVo);
                }
                courseAndProjectVo.setChildren(projectList);
                courseAndProjectVoArrayList.add(courseAndProjectVo);
            }
        }else{
            List<Course> list1 = courseService.list(new QueryWrapper<Course>()
                    .lambda()
                    .eq(Course::getState,1)
                    .select(Course::getCourseName,Course::getCourseId));
            for (Course course : list1) {
                courseAndProjectVo = new CourseAndProjectVo();
                courseAndProjectVo.setCourseId(course.getCourseId());
                courseAndProjectVo.setCourseName(course.getCourseName());
                List<ProjectListVo> projectList = new ArrayList<>();
                ProjectListVo projectListVo = null;
                List<ExperimentProject> list2 = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq(ExperimentProject::getCourseId, course.getCourseId())
                        .eq(ExperimentProject::getState, 1)
                        .select(ExperimentProject::getProjectName
                                , ExperimentProject::getProjectId
                                , ExperimentProject::getProjectPictureUrl));
                for (ExperimentProject experimentProject : list2) {
                    projectListVo = new ProjectListVo();
                    projectListVo.setProjectId(experimentProject.getProjectId());
                    projectListVo.setProjectName(experimentProject.getProjectName());
                    projectListVo.setProjectPictureUrl(experimentProject.getProjectPictureUrl());
                    projectList.add(projectListVo);
                }
                courseAndProjectVo.setChildren(projectList);
                courseAndProjectVoArrayList.add(courseAndProjectVo);
            }
        }
        return new FebsResponse().success().data(courseAndProjectVoArrayList);
    }

}
