package cc.mrbird.febs.app.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.Feedback;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IFeedbackService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static cc.mrbird.febs.external.mqtt.MQTTConnect.url;

/**
 * @ Author 马超伟
 * @ Date 2021-01-27 10:13
 * @ Description:
 * @ Version:
 */
@CrossOrigin
@RequestMapping("/app/back")
@RestController
public class FeedBackAppController extends BaseController {

    @Resource
    private IFeedbackService feedbackService;

    /**
     * @ Author: 马超伟
     * @ Date: 2021/1/27 16:54
     * @ Params: [req：用户信息, feedback：发布参数, request：图片信息, num：图片数量]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description: 提交反馈接口
     */
    @RequestMapping("/addBack")
    public FebsResponse addBack(HttpServletRequest req,Feedback feedback, MultipartRequest request, Integer num) {
        feedback.setCreateTime(LocalDateTime.now());
        feedback.setUseInfo(1);
        String url= "";
        //获取上传的图片
        List<MultipartFile> files = new ArrayList<>();
        if (num > 0) {
            for (int i = 0; i < num; i++) {
                files.add(request.getFile("images" + i));
            }
            for (MultipartFile file : files) {
                if (file != null && file.getSize() > 0) {
                    String basePath = "feedback";
                    try {
                        url += aliOSSUpload(file, basePath)+"@";
                        System.out.println("url：" + url);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return new FebsResponse().fail().message("图片上传失败！");
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(url)){
            url = url.substring(0, url.length() -1);
            feedback.setImageUrl(url);
        }
        User currentUserByToken = getCurrentUserByToken(req);
        if (currentUserByToken!=null){
            feedback.setCreateName(currentUserByToken.getNickname());
            feedback.setCreateId(currentUserByToken.getUserId().intValue());
        }
        return new FebsResponse().complete(feedbackService.save(feedback));
    }

}
