package cc.mrbird.febs.app.controller;

import cc.mrbird.febs.app.entity.vo.CourseSelectVo;
import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.CourseSelect;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.FileUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.entity.vo.StudentScoreVo;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName MobileProjectApiController
 * @Description TODO
 * @Author admin
 * @Date 2021/1/25 9:11
 * @Version 1.0
 */
@RequestMapping("/app/project")
@RestController
@Slf4j
public class MobileProjectApiController extends BaseController {

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IVideoService iVideoService;

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentRemarkService experimentRemarkService;

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private ICourseService courseService;

    @RequestMapping("getSingleProjectInfo")
    @ControllerEndpoint(operation = "查询实验详情",exceptionMessage = "查询实验详情失败")
    public FebsResponse getProjectDetail(Integer projectId,HttpServletRequest request){
        if(IntegerUtils.isBlank(projectId)){
            return new FebsResponse().no_param();
        }
        ExperimentProject one = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, projectId)
                .select(ExperimentProject::getProjectName
                        , ExperimentProject::getProjectId
                        , ExperimentProject::getProjectContent
                        , ExperimentProject::getProjectPictureUrl
                        , ExperimentProject::getSubTitle));
        List<Video> list = iVideoService.list(new QueryWrapper<Video>()
                .lambda()
                .eq(Video::getProjectId, projectId));
//        for (Video video : list) {
//            String fileUrl = video.getVideoUrl();
//            String videoLength = FileUtil.ReadVideoTime(fileUrl,request);//视频时长
//            System.out.println("===========================视频时长：" + videoLength + "===========================");
//            video.setVideoSize(videoLength);
//        }


        Map<Object, Object> map = new HashMap<>();
        map.put("projectInfo",one);
        map.put("videoList",list);
        return new FebsResponse().success().data(map);
    }

    /**
     * 查询学生实验成绩接口
     * @ Param request
     * @ return
     */
    @GetMapping("selectStudentProjectScore")
    @ControllerEndpoint(operation = "查询学生实验成绩",exceptionMessage = "查询学生实验成绩失败")
    public FebsResponse selectStudentProjectScore(QueryRequest request, HttpServletRequest request1,Integer courseId){
        try {
            User currentUser = getCurrentUserByToken(request1);
            if (currentUser==null){
                return new FebsResponse().no_login();
            }

            List<StudentScoreVo> studentScoreList = new ArrayList<>();
            List<Long> projectIds = new ArrayList<>();
            //如果是校园版用户,需要从开启的实验项目中查询
            if (currentUser.getUserType() == 1){
                //通过课程查找出所有实验项目
                List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq( ExperimentProject::getCourseId, courseId)
                        .orderByAsc(ExperimentProject::getProjectSort)
                        .select(ExperimentProject::getProjectId));
                List<Long> projectId = projectList.stream().map(s -> s.getProjectId()).collect(Collectors.toList());
                for (Long pId : projectId) {
                    //通过班级id和实验项目id查开启实验项目
                    TeacherProject one = teacherProjectService.getOne(new QueryWrapper<TeacherProject>()
                            .lambda()
                            .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                            .eq(TeacherProject::getProjectId, pId));
                    if(one!=null){
                        projectIds.add(pId);
                    }
                }
                if (projectIds.size() == 0){
                    return new FebsResponse().data(null).success().message("暂无开启的实验项目");
                }
            }else{
                //通过课程查找出所有实验项目
                List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq(ExperimentProject::getCourseId,courseId)
                        .orderByAsc(ExperimentProject::getProjectSort)
                        .select(ExperimentProject::getProjectId));
                projectIds = projectList.stream().map(s -> s.getProjectId()).collect(Collectors.toList());
            }

            IPage<StudentScoreVo> userCommitPage = userCommitService.selectStudentScoreByMobile(projectIds, request, currentUser.getUserId(), currentUser.getDeptId(),currentUser.getUserType());
            List<StudentScoreVo> records = userCommitPage.getRecords();
            for (StudentScoreVo userCommit : records) {
                if( userCommit.getProjectCommit() == null){
                    userCommit.setProjectCommit(0);
                }
                ExperimentRemark prepareRemark = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                        .lambda()
                        .eq(ExperimentRemark::getProjectId, userCommit.getProjectId())
                        .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                        .eq(ExperimentRemark::getType, 1)
                        .eq(ExperimentRemark::getUserType,currentUser.getUserType())
                        .eq(ExperimentRemark::getIsDel,1)
                        .select(ExperimentRemark::getIsCommit, ExperimentRemark::getScore,ExperimentRemark::getRemarkId));
                ExperimentRemark thinkRemark = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                        .lambda()
                        .eq(ExperimentRemark::getProjectId, userCommit.getProjectId())
                        .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                        .eq(ExperimentRemark::getType, 2)
                        .eq(ExperimentRemark::getUserType,currentUser.getUserType())
                        .eq(ExperimentRemark::getIsDel,1)
                        .select(ExperimentRemark::getIsCommit, ExperimentRemark::getScore,ExperimentRemark::getRemarkId));
                //预习题
                if(prepareRemark == null){
                    prepareRemark = new ExperimentRemark();
                    prepareRemark.setIsCommit(0);
                }
                if (thinkRemark == null){
                    thinkRemark = new ExperimentRemark();
                    thinkRemark.setIsCommit(0);
                }
                //以下几步是为了计算总分,不然没必要
                if (prepareRemark.getScore() == null){
                    prepareRemark.setScore(0.0);
                }
                if (thinkRemark.getScore() == null){
                    thinkRemark.setScore(0.0);
                }
                if (userCommit.getProjectScore() ==null){
                    userCommit.setProjectScore(0.0);
                }
                userCommit.setPrepareRemarkId(prepareRemark.getRemarkId());
                userCommit.setThinkRemarkId(thinkRemark.getRemarkId());
                userCommit.setPrepareCommit(prepareRemark.getIsCommit());
                userCommit.setThinkCommit(thinkRemark.getIsCommit());
                userCommit.setPrepareScore(prepareRemark.getScore());
                userCommit.setThinkScore(thinkRemark.getScore());
                userCommit.setSum(userCommit.getProjectScore()*0.6+prepareRemark.getScore()*0.2+prepareRemark.getScore()*0.2);
                userCommit.setTroubleCommit(0);
                userCommit.setTroubleScore(0.0);
            }
            return new FebsResponse().data(getDataTable(userCommitPage)).success();
        } catch (Exception e) {
            log.error("api/center/selectStudentProjectScore"+e.getMessage());
            return new FebsResponse().fail().message("查询失败");
        }

    }

    /**
     * @ Author wangke
     * @ Description 获取查询成绩页面的课程下拉框 
     * @ Date 11:10 2021/1/29
     * @ Param 
     * @ return 
     */
    @RequestMapping("getCourseSelect")
    @ControllerEndpoint(operation = "获取课程下拉框",exceptionMessage = "获取课程下拉框失败")
    public FebsResponse getCourseSelect(HttpServletRequest request){
        User currentUserByToken = getCurrentUserByToken(request);

        if (currentUserByToken == null){
            return new FebsResponse().no_login();
        }
        //校园版用户
        if (currentUserByToken.getUserType() == 1){
            //查询班级所分配的课程
            List<CourseDept> list = courseDeptService.list(new QueryWrapper<CourseDept>()
                    .lambda()
                    .eq(CourseDept::getDeptId, currentUserByToken.getDeptId())
                    .select(CourseDept::getCourseId));
            List<Integer> collect = list.stream().map(CourseDept::getCourseId).collect(Collectors.toList());
            List<Course> list1 = courseService.list(new QueryWrapper<Course>().lambda()
                    .in(collect.size() > 0, Course::getCourseId, collect)
                    .select(Course::getCourseId, Course::getCourseName));
            List<CourseSelectVo> collect1 = list1.stream().map(s -> {
                CourseSelectVo courseSelectVo = new CourseSelectVo();
                BeanUtils.copyProperties(s,courseSelectVo);
                return courseSelectVo;
            }).collect(Collectors.toList());
            return new FebsResponse().success().data(collect1);
        }else {
            //企业版用户
            List<Course> list = courseService.list();
            List<CourseSelectVo> collect = list.stream().map(s -> {
                CourseSelectVo courseSelectVo = new CourseSelectVo();
                BeanUtils.copyProperties(s,courseSelectVo );
                return courseSelectVo;
            }).collect(Collectors.toList());

            return new FebsResponse().success().data(collect);
        }

    }





}
