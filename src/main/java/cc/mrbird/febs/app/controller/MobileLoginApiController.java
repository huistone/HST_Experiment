package cc.mrbird.febs.app.controller;

import cc.mrbird.febs.app.entity.vo.MobileUserInfoVo;
import cc.mrbird.febs.app.entity.vo.UserLoginVo;
import cc.mrbird.febs.app.jwt.constant.Constant;
import cc.mrbird.febs.app.jwt.util.JwtUtil;
import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.annotation.Limit;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.service.ValidateCodeService;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.monitor.entity.LoginLog;
import cc.mrbird.febs.monitor.service.ILoginLogService;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.Role;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.IOException;

/**
 * @ClassName LoginApiController
 * @Description TODO
 * @Author admin
 * @Date 2021/1/20 17:02
 * @Version 1.0
 */
@Slf4j
@RequestMapping("/app/mobile")
@RestController
public class MobileLoginApiController extends BaseController {

    @Resource
    private IUserService userService;

    @Autowired
    private ValidateCodeService validateCodeService;

    @Autowired
    private ILoginLogService loginLogService;

    @Resource
    private RedisService redisService;

    @Resource
    private IDeptService deptService;



    /**
     * RefreshToken过期时间
     */
    @Value("${refreshTokenExpireTime}")
    private String refreshTokenExpireTime;



    /**
     * RefreshToken过期时间
     */
    @Value("${accessTokenExpireTime}")
    private String accessTokenExpireTime;

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * @Author wangke
     * @Description 手机端登录
     * @Date 8:49 2021/1/21
     * @Param 
     * @return 
     */
    @RequestMapping("login")
    @Limit(key = "mLogin", period = 60, count = 20, name = "登录接口", prefix = "limit")
    public FebsResponse login(
             String username,
             String password,
            HttpServletResponse response
            ){
//        HttpServletRequest request = getRequest();
//        HttpSession session = request.getSession();
//        validateCodeService.check( session.getId(), verifyCode);
        //1、如果传的是用户名
        User serviceOne = userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getUsername, username), false);
        if (serviceOne == null) {
            //查询手机号
            serviceOne =  userService.getOne(new QueryWrapper<User>().lambda()
                    .eq(User::getMobile, username), false);
            if (serviceOne == null){
                return new FebsResponse().fail().message("用户名错误！");
            }
        }
        Role currentRole = getCurrentRole(serviceOne);

        if (currentRole.getRoleId() == 82L||currentRole.getRoleId() == 86L){
            password = MD5Util.encrypt(serviceOne.getUsername().toLowerCase(), password);
            //如果都不是，那就说明用户名或者密码错误，登录失败！
            if (!password.equals(serviceOne.getPassword())){
                return new FebsResponse().fail().message("密码错误");
            }

            String currentTimeMillis = String.valueOf(System.currentTimeMillis());
            logger.info("登录时获取的currentTimeMillis"+currentTimeMillis);
            //2.创建token
            String token = JwtUtil.sign(serviceOne.getUsername(), currentTimeMillis);
            //1.将token存入redis,进行登录判断
            redisService.set(Constant.PREFIX_SHIRO_REFRESH_TOKEN + serviceOne.getUsername(), token, Long.parseLong(accessTokenExpireTime));
            //存储俩小时用户信息
            UserLoginVo userLoginVo = new UserLoginVo();
            BeanUtils.copyProperties(serviceOne,userLoginVo);
            redisService.set(Constant.PREFIX_LOGIN_CUSTOMER+serviceOne.getUsername(),userLoginVo,Constant.PREFIX_LOGIN_EXRP_HOUR);
            response.setHeader("AccessToken", token);
            response.setHeader("Access-Control-Expose-Headers", "AccessToken");
            // 保存登录日志
            LoginLog loginLog = new LoginLog();
            loginLog.setUsername(username);
            loginLog.setSystemBrowserInfo();
            this.loginLogService.saveLoginLog(loginLog);
            serviceOne.setPassword(null);
            serviceOne.setIsTab(null);
            serviceOne.setIsDel(null);
            serviceOne.setTheme(null);
            this.userService.updateLoginTime(username);
            return new FebsResponse().success().data(userLoginVo);
        }else{
            return new FebsResponse().fail().message("管理员不得登录该系统！");
        }

    }

    /**
     * @Author wangke
     * @Description 验证码 
     * @Date 8:48 2021/1/21
     * @Param 
     * @return 
     */
    @GetMapping("/images/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws IOException, FebsException {
        validateCodeService.create(request, response);
    }

    /**
     * @ Description: 手机号+验证码进行登录
     * @ Param: [mobile, code]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @PostMapping("/MobileLogin")
    public FebsResponse MobileLogin(String mobile, String code,HttpServletResponse response){
        String smsCode = (String) redisService.get(mobile);

        if (code==null){
            return new FebsResponse().fail().message("验证码输入错误！");
        }
        if (code.equals(smsCode)){
            //验证码校验正确，执行登录操作
            User serviceOne = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile)
                    .eq(User::getIsDel,1));
            if (serviceOne==null){
                return new FebsResponse().fail().message("用户信息不存在！");
            }
            Role currentRole = getCurrentRole(serviceOne);
            if (currentRole.getRoleId() == 82 || currentRole.getRoleId() == 86){
                serviceOne.setPassword(null);
                String currentTimeMillis = String.valueOf(System.currentTimeMillis());
                logger.info("登录时获取的currentTimeMillis"+currentTimeMillis);
                //2.创建token
                String token = JwtUtil.sign(serviceOne.getUsername(), currentTimeMillis);
                //1.将token存入redis,进行登录判断
                redisService.set(Constant.PREFIX_SHIRO_REFRESH_TOKEN + serviceOne.getUsername(), token, Long.parseLong(accessTokenExpireTime));
                //存储俩小时用户信息
                UserLoginVo userLoginVo = new UserLoginVo();
                BeanUtils.copyProperties(serviceOne,userLoginVo);
                redisService.set(Constant.PREFIX_LOGIN_CUSTOMER+serviceOne.getUsername(),userLoginVo,Constant.PREFIX_LOGIN_EXRP_HOUR);
                response.setHeader("accessToken", token);
                response.setHeader("Access-Control-Expose-Headers", "accessToken");
//            UsernamePasswordToken token = new UsernamePasswordToken(serviceOne.getUsername(), serviceOne.getPassword(), true);
//            super.login(token);
                // 保存登录日志
                LoginLog loginLog = new LoginLog();
                loginLog.setUsername(serviceOne.getUsername());
                loginLog.setSystemBrowserInfo();
                this.loginLogService.saveLoginLog(loginLog);
                return new FebsResponse().success().message("登录成功！");
            }else{
                return new FebsResponse().fail().message("管理员不得登录该系统！");
            }
        }else {
            return new FebsResponse().fail().message("验证码输入错误！");
        }
    }

    /**
     * @ Description: 发送短信验证码
     * @ Param: [mobile, templateCode]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     */
    @RequestMapping("sendSmsMsg")
    @ControllerEndpoint(exceptionMessage = "手机端发送短信验证码失败",operation = "手机端发送短信验证码")
    public FebsResponse sendSmsMsg(String mobile,String templateCode){
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile).eq(User::getStatus, 1));
        if (one!=null){
            return new FebsResponse().fail().message("手机号已被使用!");
        }
        if (sendSms(mobile, templateCode)) {
            return new FebsResponse().success().message("发送成功！");
        }
        return new FebsResponse().fail().message("发送失败！！！");
    }

    /**
     * @ Author wangke
     * @ Description 找回密码发送短信功能
     * @ Date 18:30 2021/2/1
     * @ Param
     * @ return
     */
    @RequestMapping("sendSmsMsgByForgetPassword")
    @ControllerEndpoint(exceptionMessage = "找回密码的发送短信失败",operation = "找回密码的发送短信")
    public FebsResponse sendSmsMsgByForgetPassword(String mobile,String templateCode){
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile).eq(User::getStatus, 1));

        if (one == null){
            return new FebsResponse().fail().message("该手机号暂无绑定账户");
        }

        if (sendSms(mobile, templateCode)) {
            return new FebsResponse().success().message("发送成功！");
        }else{
            return new FebsResponse().fail().message("短信发送频繁,请稍后再试！！！");
        }

    }

    /**
     * @ Author wangke
     * @ Description 手机号登录
     * @ Date 18:30 2021/2/1
     * @ Param
     * @ return
     */
    @RequestMapping("sendSmsMsgByMobileLogin")
    @ControllerEndpoint(exceptionMessage = "H5手机号登录失败",operation = "H5手机号登录")
    public FebsResponse sendSmsMsgByMobileLogin(String mobile,String templateCode){
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile).eq(User::getStatus, 1));

        if (one == null){
            return new FebsResponse().fail().message("该手机号暂无绑定账户");
        }

        Role currentRole = getCurrentRole(one);

        if (currentRole.getRoleId() == 82L || currentRole.getRoleId() == 86L){
            if (sendSms(mobile, templateCode)) {
                return new FebsResponse().success().message("发送成功！");
            }else{
                return new FebsResponse().fail().message("短信发送频繁,请稍后再试！！！");
            }
        } else {
            return new FebsResponse().fail().message("管理员不得登录该系统！");
        }
    }

    /**
     * @ Author wangke
     * @ Description 找回密码接口
     * @ Date 9:50 2021/1/29
     * @ Param 
     * @ return 
     */
    @RequestMapping("resetPassword")
    @ControllerEndpoint(operation = "H5重置用户密码",exceptionMessage = "H5重置密码失败")
    public FebsResponse resetPassword(String mobile,String password){
        User one = userService.getOne(new QueryWrapper<User>()
                .lambda()
                .eq(User::getMobile, mobile)
                .select(User::getUserId,User::getUsername));
        if (one ==null){
            return new FebsResponse().fail().message("该手机号暂无匹配账户");
        }
        one.setPassword(MD5Util.encrypt(one.getUsername(), password));
        userService.updateById(one);
        redisService.del(Constant.PREFIX_LOGIN_CUSTOMER+one.getUsername(),Constant.PREFIX_SHIRO_REFRESH_TOKEN + one.getUsername());
        return new FebsResponse().success().message("重置密码成功");
    }



    /**
     * @ Description: 验证验证码是否正确
     * @ Param: [mobile, code]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     */
    @GetMapping("validSms")
    public FebsResponse validSms(String mobile,String code){
        String smsCode = (String) redisService.get(mobile);
        if (code==null){
            return new FebsResponse().fail().message("验证码输入错误！");
        }
        if (code.equals(smsCode)){
            return new FebsResponse().success().message("验证码输入正确！");
        }else {
            return new FebsResponse().fail().message("验证码输入错误！");
        }
    }

    /**
     * @Author wangke
     * @Description 手机端注册用户 
     * @Date 9:42 2021/1/27
     * @Param 
     * @return 
     */
    @PostMapping("regist")
    public FebsResponse regist(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password,
            @NotBlank(message = "{required}") String telephone) throws FebsException {
        User user = userService.findByName(username);
        if (user != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if(userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile,telephone))!=null){
            return new FebsResponse().fail().message("该手机号已存在");
        }
        this.userService.registMobileUser(username, password,telephone);
        return new FebsResponse().success();
    }

    /**
     * @Author wangke
     * @Description 判断是否登录接口
     * @Date 16:07 2021/1/28
     * @Param 
     * @return 
     */
    @GetMapping("isLogin")
    @ControllerEndpoint(operation = "判断是否登录",exceptionMessage = "H5判断是否登录异常")
    public FebsResponse isLogin(HttpServletRequest request){
        User currentUserByToken = getCurrentUserByToken(request);

        if (currentUserByToken == null){
            return new FebsResponse().no_login();
        }
        if (redisService.get(Constant.PREFIX_LOGIN_CUSTOMER + currentUserByToken.getUsername())==null){
            return new FebsResponse().no_login();
        }

        return new FebsResponse().success();
        
    }
    
    /**
     * @ Description: 更新密码
     * @ Param: [password：原密码, passwordNew：新密码]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: wk
     * @ Date: 2020/5/28
     */
    @PostMapping("/updatePwd")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "更新用户密码失败",operation = "更新用户密码成功")
    public FebsResponse updatePwd(String password, String passwordNew,HttpServletRequest request) {
         User currentUser = getCurrentUserByToken(request);
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login().message("用户未登录，无法获取用户信息");
        }
        if (currentUser.getPassword().equals(MD5Util.encrypt(currentUser.getUsername(), password))) {
            currentUser.setPassword(MD5Util.encrypt(currentUser.getUsername(), passwordNew));
            userService.updateById(currentUser);
            redisService.del(Constant.PREFIX_LOGIN_CUSTOMER+currentUser.getUsername(),Constant.PREFIX_SHIRO_REFRESH_TOKEN + currentUser.getUsername());
            return new FebsResponse().success().message("密码修改成功！");
        } else {
            return new FebsResponse().fail().message("原密码错误，修改失败！");
        }
    }

    /**
     * @Author wangke
     * @Description 退出登录
     * @Date 16:52 2021/1/28
     * @Param 
     * @return 
     */
    @RequestMapping("logout")
    @ControllerEndpoint(operation = "H5用户退出登录")
    public FebsResponse logout(HttpServletRequest request){
        try {
            User currentUserByToken = getCurrentUserByToken(request);
            if (currentUserByToken == null){
                return new FebsResponse().success();
            }
            redisService.del(Constant.PREFIX_LOGIN_CUSTOMER+currentUserByToken.getUsername(),Constant.PREFIX_SHIRO_REFRESH_TOKEN + currentUserByToken.getUsername());

            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("账号退出异常");
        }
    }

    /**
     * @Author wangke
     * @Description 获取手机用户信息 
     * @Date 9:41 2021/1/27
     * @Param 
     * @return 
     */
    @RequestMapping("getMobileUserInfo")
    @ControllerEndpoint(exceptionMessage = "获取手机用户信息失败")
    public FebsResponse getMobileUserInfo(HttpServletRequest request){

        try {
            User currentUserByToken = getCurrentUserByToken(request);

            if (currentUserByToken == null){
                return new FebsResponse().no_login();
            }
            currentUserByToken = userService.getOne(new QueryWrapper<User>()
                    .lambda()
                    .eq(User::getUserId,currentUserByToken.getUserId()));

            if ( currentUserByToken.getUserType()==1 ){
                Dept one = deptService.getOne(new QueryWrapper<Dept>()
                        .lambda()
                        .eq(Dept::getDeptId, currentUserByToken.getDeptId()));
                currentUserByToken.setDeptName(one.getDeptName());
            }

            MobileUserInfoVo userLoginVo = new MobileUserInfoVo();

            BeanUtils.copyProperties(currentUserByToken,userLoginVo);

            return new FebsResponse().success().data(userLoginVo);

        } catch (BeansException e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }

    }

    @RequestMapping("updateMobileUserInfo")
    @ControllerEndpoint(operation = "H5修改用户信息",exceptionMessage = "H5修改用户信息失败")
    public FebsResponse updateMobileUserInfo( User user,HttpServletRequest request){
        User currentUserByToken = getCurrentUserByToken(request);

        if (currentUserByToken == null){
            return new FebsResponse().no_login();
        }

        userService.updateById(user);
        return new FebsResponse().success();
    }

    /**
     * @ Description: 更新用户头像信息
     * @ Param: [imgUrl]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/1
     */
    @PostMapping("/updateProfile")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "更新用户头像失败",operation = "更新用户头像成功")
    public FebsResponse updateProfile( Integer userId, MultipartFile imgUrl,HttpServletRequest request) {
        User currentUser = userService.getById(userId);
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }

//        MultipartFile file = BASE64DecodedMultipartFileUtil.base64ToMultipart(imgUrl);
        //更新头像地址
        String url = null;
        if (imgUrl != null && imgUrl.getSize() > 0) {
            String basePath = "avatar";
            try {
                url = aliOSSUpload(imgUrl, basePath);
                currentUser.setAvatar(url);
            } catch (IOException e) {
                e.printStackTrace();
                return new FebsResponse().fail().message("图片上传失败！");
            }
        }
        if (userService.updateById(currentUser)) {
            return new FebsResponse().success().message("用户头像更新成功").put("imgUrl", url);
        } else {
            return new FebsResponse().fail().message("更新失败！");
        }
    }

}
