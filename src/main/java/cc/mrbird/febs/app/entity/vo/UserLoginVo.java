package cc.mrbird.febs.app.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName UserLoginInfo
 * @Description TODO
 * @Author admin
 * @Date 2020/12/29 10:25
 * @Version 1.0
 */
@Data
public class UserLoginVo {
    /**
     * 用户 ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String username;

    private String idNumber;

    /**
     * 密码
     */
    private String password;

    /**
     * 课程 ID
     */
    private Long courseId;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    @TableField("MOBILE")
    private String mobile;

    /**
     * 状态 0锁定 1有效
     */
    private String status;

    @TableField("DEPT_ID")
    private Long deptId;

    private Integer userType;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 最近访问时间
     */
    private Date lastLoginTime;

    /**
     * 性别 0男 1女 2 保密
     */
    private String sex;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 主题
     */
    private String theme;

    /**
     * 是否开启 tab 0开启，1关闭
     */
    private String isTab;

    /**
     * 描述
     */
    private String description;
    /**
     * 普通用户的标识，对当前开发者帐号唯一
     */
    private String openid;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 国家
     */
    private String country;

    /**
     * 用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。
     */
    private String unionid;

    /**
     * 1正常，0删除
     */
//    @TableLogic
    private Integer isDel;


}
