package cc.mrbird.febs.app.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName UserInfoVo
 * @Description TODO
 * @Author admin
 * @Date 2021/1/27 10:15
 * @Version 1.0
 */
@Data
public class MobileUserInfoVo {

    /**
     * 用户 ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 课程 ID
     */
    private Long courseId;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    @TableField("MOBILE")
    private String mobile;

    private String idNumber;

    /**
     * 状态 0锁定 1有效
     */
    private String status;

    @TableField("DEPT_ID")
    private Long deptId;

    private Integer userType;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 最近访问时间
     */
    private Date lastLoginTime;

    /**
     * 性别 0男 1女 2 保密
     */
    private String sex;

    /**
     * 头像
     */
    private String avatar;


    /**
     * 描述
     */
    private String description;




}
