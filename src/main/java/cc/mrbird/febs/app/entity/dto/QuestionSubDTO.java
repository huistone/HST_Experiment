package cc.mrbird.febs.app.entity.dto;

import cc.mrbird.febs.app.entity.vo.QuestionSubVo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ ClassName QuestionSubDTO
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/1/29 15:23
 * @ Version 1.0
 */
@Data
public class QuestionSubDTO  implements Serializable {

    private List<QuestionSubVo> questionSubVos;

}
