package cc.mrbird.febs.app.entity.vo;

import lombok.Data;

/**
 * @ClassName QuestionDetail
 * @Description TODO
 * @Author admin
 * @Date 2021/1/21 14:43
 * @Version 1.0
 */
@Data
public class QuestionDetailVo {

    /**
     * 选项
     */
    private String name;

    /**
     * 是否为正确答案
     */
    private Boolean code;

    private Character option;



    /**
     * 是否选中
     */
    private Boolean click_index = false;

}
