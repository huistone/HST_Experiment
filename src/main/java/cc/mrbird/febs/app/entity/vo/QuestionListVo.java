package cc.mrbird.febs.app.entity.vo;

import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;

/**
 * @ClassName QuestionListVo
 * @Description TODO
 * @Author admin
 * @Date 2021/1/21 11:29
 * @Version 1.0
 */
@Data
public class QuestionListVo {

    /**
     * 正确答案下标
     */
    private ArrayList<Integer> trueOption;

    private Boolean code2 = false;

    private ArrayList<Integer> current = new ArrayList<>();

    private Integer questionId;

    /**
     * 题号
     */
    private Integer number;

    /**
     *题型
     * 2 判断
     * 3 单选
     * 4 多选
     */
    private Integer questionType;

    /**
     * 题目名称
     */
    private String title;

    /**
     * 是单选还是多选 1单 2多
     */
    private Integer isMultiOrSingle;

    /**
     * 选项集合
      */
    private ArrayList<Character> option;

    /**
     * 选项详情
     */
    private ArrayList<QuestionDetailVo> testList;

}
