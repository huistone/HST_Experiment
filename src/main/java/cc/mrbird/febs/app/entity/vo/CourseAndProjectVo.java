package cc.mrbird.febs.app.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @ClassName CourseAndProjectVo
 * @Description TODO
 * @Author admin
 * @Date 2021/1/20 11:48
 * @Version 1.0
 */
@Data
public class CourseAndProjectVo {
    private Long courseId;

    private String courseName;

    private List<ProjectListVo> children;

}
