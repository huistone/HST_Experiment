package cc.mrbird.febs.app.entity.vo;

import lombok.Data;

/**
 * @ ClassName QuestionSubVo
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/1/29 14:29
 * @ Version 1.0
 */
@Data
public class QuestionSubVo {

    private String projectId;

    private Integer number;

    private String context;

    private Integer questionId;

    private Integer questionType;

}
