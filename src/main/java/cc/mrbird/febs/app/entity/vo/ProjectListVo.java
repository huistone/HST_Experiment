package cc.mrbird.febs.app.entity.vo;

import lombok.Data;

/**
 * @ClassName ProjectListVo
 * @Description TODO
 * @Author admin
 * @Date 2021/1/21 16:46
 * @Version 1.0
 */
@Data
public class ProjectListVo {

    private Long projectId;

    private String projectName;

    private String projectPictureUrl;


}
