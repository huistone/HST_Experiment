package cc.mrbird.febs.app.entity.vo;

import lombok.Data;

/**
 * @ ClassName CourseSelectVo
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/1/29 9:11
 * @ Version 1.0
 */
@Data
public class CourseSelectVo {


    private Long courseId;
    private String courseName;


}
