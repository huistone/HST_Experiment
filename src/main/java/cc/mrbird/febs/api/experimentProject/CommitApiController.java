package cc.mrbird.febs.api.experimentProject;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.annotation.Limit;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TableCols;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/commit")
public class CommitApiController extends BaseController {

    @Resource
    private ITeacherProjectService teacherProjectService;
    @Resource
    private IExperimentProjectService projectService;
    @Resource
    private IUserCommitService userCommitService;
    @Resource
    private IUserService userService;


    @RequestMapping("/getCols")
    public FebsResponse getCols(Integer deptId) {
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        Collection<ExperimentProject> experimentProjects = getProjectsID(deptId, currentUser);
        List<TableCols> colsList = new ArrayList<>();
        colsList.add(new TableCols("idNumber", "学号", "center", true));
        colsList.add(new TableCols("name", "姓名", "center", false));
        for (ExperimentProject experimentProject : experimentProjects) {
            TableCols tableCols = new TableCols();
            tableCols.setTitle(experimentProject.getProjectName());
            tableCols.setField(experimentProject.getProjectId().toString());
            tableCols.setAlign("center");
            tableCols.setSort(false);
            colsList.add(tableCols);
        }
        return new FebsResponse().success().data(colsList);
    }

    /**
     * @ Author: 王珂
     * @ Description:
     * @ Date: 11:23 2020/7/3
     * @ params:  * @ Param null
     */
    private Collection<ExperimentProject> getProjectsID(Integer deptId, User user) {
        List<TeacherProject> teacherProjects = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                .eq(TeacherProject::getDeptId, deptId)
                .eq(TeacherProject::getTeacherId, user.getUserId()));
        List<Long> collect = teacherProjects.stream().map(TeacherProject::getProjectId).collect(Collectors.toList());
        return projectService.listByIds(collect);
    }

    @GetMapping("/commitList")
    //@RequiresPermissions("teacher:commit")
    @ControllerEndpoint(operation = "查看提交情况", exceptionMessage = "学生提交情况查询失败")
    public FebsResponse commitList(Integer deptId, QueryRequest request) {
        User currentUser = getCurrentUser();
        if (currentUser==null){
            return new FebsResponse().no_login();
        }

        Page<UserCommit> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "commit_id", FebsConstant.ORDER_ASC, false);
        IPage<Map<String, Object>> mapIPage = this.userCommitService.pageMaps(page, new QueryWrapper<UserCommit>().lambda()
                .eq(IntegerUtils.isNotBlank(deptId), UserCommit::getDeptId, deptId)
        );
        List<Map<String, Object>> mapList = mapIPage.getRecords();
        for (Map<String, Object> map : mapList) {
            map.put("idNumber", "211111");
            Collection<ExperimentProject> experimentProjects = getProjectsID(deptId, currentUser);
            for (ExperimentProject experimentProject : experimentProjects) {
                UserCommit commit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getProjectId, experimentProject.getProjectId()));
                String status = "<div style=\"color: red;font-style:oblique\">未提交</div>";
                if (commit != null) {
                    if (IntegerUtils.isNotBlank(commit.getIsCommit())) {
                        status = "<div style=\"color: orange;\">未批阅</div>";
                    }
                    if (StringUtils.isNotBlank(commit.getScore().toString())) {
                        status = commit.getScore().toString();
                    }
                }

                User stu = userService.getById((Serializable) map.get("stu_id"));

                map.put(experimentProject.getProjectId().toString(), status);
                map.put("idNumber", stu.getIdNumber());
                map.put("name", stu.getUsername());
            }
        }
        Map<String, Object> dataTable = getDataTable(mapIPage);
        return new FebsResponse().success().data(dataTable);
    }


}
