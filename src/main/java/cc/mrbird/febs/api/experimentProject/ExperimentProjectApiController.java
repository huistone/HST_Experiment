package cc.mrbird.febs.api.experimentProject;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/api/experimentProject")
public class ExperimentProjectApiController extends BaseController {
    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService projectService;

    /**
    * @ Description: 获取实验项目接口
    * @ Param: [projectId, courseId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/6
    */
    @GetMapping("/project/{projectId}/{courseId}")
    @ResponseBody
    @ControllerEndpoint(operation = "获取实验项目成功",exceptionMessage = "获取实验项目失败")
    public FebsResponse systemExperimentProjectView(@PathVariable Long projectId, @PathVariable Integer courseId) {
        User student = getCurrentUser();
        if(projectId==null || courseId==null){
            return new FebsResponse().no_param();
        }
        if(student==null){
            return new FebsResponse().no_login();
        }
        List<TeacherProject> list = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                .eq(TeacherProject::getDeptId, student.getDeptId())
                .eq(TeacherProject::getProjectId, projectId)
                .eq(TeacherProject::getCourseId, courseId)
        );
        if(list.size()==0){
            return new FebsResponse().fail().message("暂无数据");
        }
        ExperimentProject project = experimentProjectService.getById(projectId);
        project.setTeacherId(list.get(0).getTeacherId().intValue());
        return new FebsResponse().success().data(project);
    }

    @GetMapping("list")
    @RequiresPermissions("experimentProject:view")
    public FebsResponse courseList(ExperimentProject experimentProject, QueryRequest request) {
        Page<ExperimentProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "project_id", FebsConstant.ORDER_ASC, false);
        IPage<ExperimentProject> iPage = this.experimentProjectService.page(page, new QueryWrapper<ExperimentProject>().lambda()
                .like(StringUtils.isNotBlank(experimentProject.getProjectName()), ExperimentProject::getProjectName, experimentProject.getProjectName())
                .like(experimentProject.getState() != null,ExperimentProject::getState,experimentProject.getState()));
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
     *GuiYongkang
     * 2020/4/02 10:36am
     * 教师查询班级
     */
    @GetMapping("/select/depts")
    @ControllerEndpoint(operation = "查询老师所属班级",exceptionMessage = "查询老师所属班级失败")
    @RequiresPermissions("teacherProject:view")
    public FebsResponse selectTeacherProject(){
        Collection<Dept> depts;
        try {
            depts = getDepts();
        } catch (Exception e) {
            log.error("/api/experimentProject/select/depts:"+e.getMessage());
            e.printStackTrace();
            return new FebsResponse().fail().message("查询班级失败");
        }
        return new FebsResponse().success().data(depts);
    }
    /**
     * 查询当前登录老师所拥有的班级列表
     */
    private  Collection<Dept> getDepts(){
        User currentUser = getCurrentUser();
        if (currentUser==null){
            throw new FebsException(FebsConstant.USER_UNLOGIN_MESSAGE) ;
        }
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, currentUser.getUserId()));
        List<Long> collect = userDeptList.stream().map(UserDept::getDeptId).collect(Collectors.toList());
        return deptService.listByIds(collect);
    }

    /**
     * @ Author: 王珂
     * @ Description: 查看教师开启实验项目记录
     * @ Date: 9:33 2020/7/3
     * @ params:  * @ Param null
     */
    @RequestMapping("/deptList")
    @RequiresPermissions("teacherProject:view")
    @ControllerEndpoint(operation = "查询开启的实验项目成功",exceptionMessage = "查询开启的实验项目成功")
    public FebsResponse getTeacherProjectList(QueryRequest request, TeacherProject teacherProject){
        User currentUser = getCurrentUser();
        if (currentUser==null){
            return new FebsResponse().no_login();
        }
        //获取当前登录用户信息
        Page<TeacherProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "project_id", FebsConstant.ORDER_ASC, false);
        IPage<TeacherProject> iPage = this.teacherProjectService.page(page,new QueryWrapper<TeacherProject>().lambda()
                .like(StringUtils.isNotBlank(teacherProject.getClassName()),TeacherProject::getClassName,teacherProject.getClassName())
                .like(StringUtils.isNotBlank(teacherProject.getProjectName()),TeacherProject::getProjectName,teacherProject.getProjectName())
                .like(TeacherProject::getTeacherId,currentUser.getUserId())
                .eq(IntegerUtils.isLongNotBlank(teacherProject.getDeptId()),TeacherProject::getDeptId,teacherProject.getDeptId())
        );
        List<TeacherProject> records = iPage.getRecords();
        for (TeacherProject r:records ) {
            Dept dept = deptService.getById(r.getDeptId());
            r.setClassName(dept.getDeptName());
            ExperimentProject eProject = experimentProjectService.getById(r.getProjectId());
            r.setProjectName(eProject.getProjectName());
        }
        iPage.setRecords(records);
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * @ Author: 王珂
     * @ Description: 教师新增实验项目
     * @ Date: 9:00 2020/7/4
     * @ params:  * @ Param null
     */
    @RequestMapping("/add")
    @RequiresPermissions("teacherProject:add")
    @ControllerEndpoint(operation = "新增实验项目", exceptionMessage = "新增实验项目失败")
    public FebsResponse teacherProjectAdd(String deptId,String projectId){
        String[] projectIds = projectId.split(",");
        User currentUser = getCurrentUser();
        if (currentUser==null){
            return new FebsResponse().no_login();
        }
        for (String id : projectIds) {
            TeacherProject teacherProject = new TeacherProject();
            teacherProject.setTeacherId(currentUser.getUserId());
            teacherProject.setProjectId(Long.parseLong(id));
            teacherProject.setDeptId(Long.parseLong(deptId));
            teacherProject.setCourseId(projectService.getById(Long.parseLong(id)).getCourseId());
            teacherProjectService.save(teacherProject);
        }
        return new FebsResponse().success();
    }


    @GetMapping("/getAllTeacherProject")
    @ControllerEndpoint(operation = "获取老师所带的所有项目", exceptionMessage = "获取老师所带的所有项目失败")
    public FebsResponse getAllTeacherProjects() {
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        List<ExperimentProject> projectList = projectService.list(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getCourseId,currentUser.getCourseId()));
        return new FebsResponse().success().data(projectList);
    }

}
