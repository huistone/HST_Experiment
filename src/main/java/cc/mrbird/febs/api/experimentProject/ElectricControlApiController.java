package cc.mrbird.febs.api.experimentProject;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.EcScore;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IEcScoreService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName ElectricControlApiContoller
 * @ Description TODO
 * @Author admin
 * @Date 2020/11/25 15:20
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/electricControl")
public class ElectricControlApiController extends BaseController {

    @Resource
    private IEcScoreService ecScoreService;

    
    @RequestMapping("saveScore")
    @ControllerEndpoint(exceptionMessage = "保存电气成绩失败",operation = "保存电气成绩成功")
    public FebsResponse saveScore(Integer type,Double score,Integer projectId){
        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }
        try {
            EcScore ecScore = new EcScore();
            ecScore.setType(type);
            ecScore.setScore(score);
            ecScore.setUserId(currentUser.getUserId());
            ecScore.setProjectId(projectId);
            ecScoreService.save(ecScore);
        } catch (Exception e) {
            return new FebsResponse().fail().message("保存成绩失败");
        }
        return new FebsResponse().success().message("保存成绩成功");
    }

}
