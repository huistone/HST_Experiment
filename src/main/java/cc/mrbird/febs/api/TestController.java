package cc.mrbird.febs.api;

import cn.shuibo.annotation.Decrypt;
import cn.shuibo.annotation.Encrypt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * Author:Bobby
 * DateTime:2019/4/17 15:22
 **/
@RestController
@RequestMapping("/api/rsa")
public class TestController {

    /**
     * 对返回值加密
     *
     * @return
     */
    @Encrypt
    @GetMapping("/test01")
    public TestBean test01() {
        TestBean testBean = new TestBean();
        testBean.setName("maChaoWei");
        testBean.setAge(18);
        return testBean;
    }

    /**
     * 对传过来的加密参数进行解密
     *
     * @return
     * @ Param testBean
     */
    @Decrypt
    @PostMapping("/test02")
    public String test02(@RequestBody TestBean testBean) {
        return testBean.toString();
    }




}
