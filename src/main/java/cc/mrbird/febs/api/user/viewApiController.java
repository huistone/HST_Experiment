package cc.mrbird.febs.api.user;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/07/07/18:59
 * @ Description:
 */
@Controller
@RequestMapping("/api/skip")
public class viewApiController {

    @GetMapping("/toSkip")
    @ControllerEndpoint(operation = "重定向教师首页成功",exceptionMessage = "重定向教师首页失败")
    public String skip(){
        return "redirect:http://teacher.huistone.com/";
    }
}
