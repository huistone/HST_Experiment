package cc.mrbird.febs.api.user;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import cc.mrbird.febs.system.mapper.DeptMapper;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/05/18/10:16
 * @ Description: 班级查询接口
 */
@RestController
@RequestMapping("/api")
public class deptApiController extends BaseController {

    @Resource
    private IDeptService deptService;

    @Resource
    private DeptMapper deptMapper;

    /**
    * @ Description: 查询部门列表
    * @ Param: [deptId：0，一级部门，]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/18
    */
    @GetMapping("getDept")
    @ControllerEndpoint(operation = "查询部门列表成功",exceptionMessage = "查询部门列表失败")
    public FebsResponse getDept(Integer deptId){
        List<Dept> deptList = deptService.list(new QueryWrapper<Dept>().lambda()
                .eq(Dept::getParentId, deptId)
                .orderByAsc(Dept::getOrderNum));
        return new FebsResponse().success().data(deptList);
    }

    /**
    * @ Description: 获取element的树形下拉组件
    * @ Param: [deptId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/18
    */
    @GetMapping("getTreeDept")
    @ControllerEndpoint(exceptionMessage = "获取部门树形下拉组件失败",operation = "获取部门树形下拉组件成功")
    public FebsResponse getTreeDept(){
        List<ElementCascader> cascader = deptMapper.getCascader();
        return new FebsResponse().success().data(cascader);
    }



}
