package cc.mrbird.febs.api.user;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.BASE64DecodedMultipartFileUtil;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/api")
public class UserApiController extends BaseController {


    @Resource
    private IRoleService roleService;

    @Resource
    private IUserService userService;

    @Resource
    private IUserRoleService userRoleService;

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ICourseService courseService;

    @Resource
    private ICourseDeptService courseDeptService;

    @GetMapping("/islogin")
    @ResponseBody
    public FebsResponse isLogin() {
        User user = getCurrentUser();
        if (user != null && user.getUserId() != null) {
            user.setPassword(null);
            user.setIsTab(null);
            user.setIsDel(null);
            user.setTheme(null);
            return new FebsResponse().success().data(user).message("true");
        } else {
            return new FebsResponse().fail().message("false");
        }
    }

    /**
     * @ Description: 退出登录
     * @ Param: []
     * @ return: java.lang.String
     * @ Author: 马超伟
     * @ Date: 2020/5/14
     */
    @GetMapping("/logOut")
    @ResponseBody
    public FebsResponse logOut() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return new FebsResponse().success();
    }


    /**
     * @ Description: 前台学生注册
     * @ Param: [user：用户对象]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     */
    @PostMapping("/student_register")
    @ResponseBody
    public FebsResponse student_register(User user) {
        if (user.getUsername() == null ||
                user.getIdNumber() == null ||
                user.getPassword() == null ||
                user.getDeptId() == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("注册失败！请检查输入信息是否正确！");
        }
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda().
                eq(User::getIdNumber, user.getIdNumber())) != null) {
            return new FebsResponse().fail().message("该学号已存在");
        }
        //设置密码加密
        user.setPassword(MD5Util.encrypt(user.getUsername(), user.getPassword()));
        //设置默认值
        user.setCreateTime(new Date());
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setStatus(User.STATUS_VALID);
        user.setSex(User.SEX_UNKNOW);
        user.setDescription("请更新您的用户签名");
        //进行用户信息添加注册
        if (!userService.save(user)) {
            return new FebsResponse().fail().message("注册失败！");
        }
        //添加注册的用户角色
        UserRole ur = new UserRole();
        ur.setUserId(user.getUserId());
        ur.setRoleId(FebsConstant.STUDENT_ID);
        userRoleService.save(ur);

        return new FebsResponse().success().message("注册成功！");
    }

    /**
     * @ Description: 注册参数校验接口
     * @ Param: [user]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/19
     */
    @GetMapping("/validParam")
    @ResponseBody
    public FebsResponse validParam(User user) {
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda().
                eq(User::getIdNumber, user.getIdNumber())) != null) {
            return new FebsResponse().fail().message("该学号已存在");
        }
        return new FebsResponse().success().message("参数可用!");
    }

    /**
     * @ Description: 获取用户 个人中心接口
     * @ Param: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/27
     */
    @GetMapping("/getUserInfo")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "获取用户个人信息失败",operation = "获取用户个人信息成功")
    public FebsResponse getUserInfo() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }

       currentUser = userService.getById(currentUser.getUserId());
        //确保用户已经登录
        Map map = new HashMap<>(16);
        //获取用户个人信息,并去掉没必要返回的字段
        currentUser.setPassword(null);
        currentUser.setIsTab(null);
        currentUser.setIsDel(null);
        currentUser.setTheme(null);
        if (IntegerUtils.isLongNotBlank(currentUser.getDeptId())) {
            currentUser.setDeptName(deptService.getOne(new QueryWrapper<Dept>()
                    .lambda()
                    .eq(Dept::getDeptId, currentUser.getDeptId())
                    .select(Dept::getDeptName))
                    .getDeptName());
        }
        UserRole one = userRoleService.getOne(new QueryWrapper<UserRole>()
                .lambda()
                .eq(UserRole::getUserId, currentUser.getUserId()));
        //设置角色名
        currentUser.setRoleName(roleService.getOne(new QueryWrapper<Role>()
                .lambda()
                .eq(Role::getRoleId, one.getRoleId())).getRoleName());
        //设置角色id
        currentUser.setRoleId(one.getRoleId());
        map.put("user_info", currentUser);

        //查询学生的课程信息
        if (currentUser.getDeptId() != null) {
            List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>()
                    .lambda()
                    .eq(CourseDept::getDeptId, currentUser.getDeptId()));
            List courseList = new ArrayList<>();
            for (CourseDept courseDept : courseDeptList) {
                Course course = courseService.getOne(new QueryWrapper<Course>()
                        .lambda()
                        .eq(Course::getCourseId,courseDept.getCourseId())
                        .eq(Course::getState,1));
                courseList.add(course);
            }
            map.put("courseList", courseList);
        }
        //查询用户的实验报告提交情况
        List<UserCommit> userCommitList = userCommitService.list(new QueryWrapper<UserCommit>()
                .lambda()
                .eq(UserCommit::getStuId, currentUser.getUserId()));
        if (userCommitList != null && userCommitList.size() > 0) {
            List listCommit = new ArrayList();
            for (UserCommit userCommit : userCommitList) {
                userCommit.setDeptName(deptService.getById(userCommit.getDeptId()).getDeptName());
                userCommit.setCourseName(courseService.getById(userCommit.getCourseId()).getCourseName());
                userCommit.setProjectName(experimentProjectService.getById(userCommit.getProjectId()).getProjectName());
                listCommit.add(userCommit);
            }
            map.put("user_project_commit", listCommit);
        }
        //查询学生提交的习题
        //待完善
        return new FebsResponse().success().data(map).message("查询成功");
    }
    
    /**
     * @ Description: 执行用户信息的更新方法
     * @ Param: [trueName, nickname, avatar, description, sex, email, mobile]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/27
     */
    @PostMapping("/updateUserInfo")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "更新用户个人信息失败",operation = "更新用户个人信息成功")
    public FebsResponse updateUserInfo(User user) {
        User currentUser = getCurrentUser();
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login().message("用户未登录，无法获取用户信息");
        }
        //判断手机号是否存在
        if (user.getMobile() != null && userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        //判断邮箱是否存在
        //先判断不为空，再判断不等于他本身
        if (user.getEmail() != null) {
            User one = userService.getOne(new QueryWrapper<User>().lambda().
                    eq(User::getEmail, user.getEmail()));
            if (one != null && one.getUserId() == currentUser.getUserId()) {
                return new FebsResponse().fail().message("该邮箱已存在");
            }
        }
        currentUser.setTrueName(user.getTrueName());
        currentUser.setNickname(user.getNickname());
        currentUser.setDescription(user.getDescription());
        currentUser.setSex(user.getSex());
        String email =  user.getEmail();
        if(email!=null && email.length()>0){
           currentUser.setEmail(email);
        }
        currentUser.setMobile(user.getMobile());
        if (userService.updateById(currentUser)) {
            return new FebsResponse().success().message("用户信息更新成功");
        } else {
            return new FebsResponse().fail().message("更新失败！");
        }
    }

    /**
     * @ Description: 更新用户头像信息
     * @ Param: [imgUrl]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/1
     */
    @PostMapping("/updateProfile")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "更新用户头像失败",operation = "更新用户头像成功")
    public FebsResponse updateProfile(String imgUrl) {
        User currentUser = getCurrentUser();
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }

        MultipartFile file = BASE64DecodedMultipartFileUtil.base64ToMultipart(imgUrl);
        //更新头像地址
        String url = null;
        if (file != null && file.getSize() > 0) {
            String basePath = "avatar";
            try {
                url = aliOSSUpload(file, basePath);
                currentUser.setAvatar(url);
            } catch (IOException e) {
                e.printStackTrace();
                return new FebsResponse().fail().message("图片上传失败！");
            }
        }
        if (userService.updateById(currentUser)) {
            return new FebsResponse().success().message("用户头像更新成功").data(url);
        } else {
            return new FebsResponse().fail().message("更新失败！");
        }
    }


    /**
     * @ Description: 更新密码
     * @ Param: [password：原密码, passwordNew：新密码]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/28
     */
    @PostMapping("/updatePwd")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "更新用户密码失败",operation = "更新用户密码成功")
    public FebsResponse updatePwd(String password, String passwordNew) {
        User currentUser = getCurrentUser();
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login().message("用户未登录，无法获取用户信息");
        }
        if (currentUser.getPassword().equals(MD5Util.encrypt(currentUser.getUsername(), password))) {
            currentUser.setPassword(MD5Util.encrypt(currentUser.getUsername(), passwordNew));
            userService.updateById(currentUser);
            return new FebsResponse().success().message("密码修改成功！");
        } else {
            return new FebsResponse().fail().message("原密码错误，修改失败！");
        }
    }

    /**
     * @ Description: 找回密码。又叫重置密码
     * @ Param: [mobile, password]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/28
     */
    @PostMapping("/resetPwd")
    @ResponseBody
    @ControllerEndpoint(operation = "用户找回密码成功",exceptionMessage = "用户找回密码失败")
    public FebsResponse resetPwd(String mobile, String password) {
        User user = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile));
        if (user != null) {
            user.setPassword(MD5Util.encrypt(user.getUsername(), password));
            userService.updateById(user);
            return new FebsResponse().success().message("密码重置成功！");
        } else {
            return new FebsResponse().fail().message("用户信息不存在！");
        }
    }


}
