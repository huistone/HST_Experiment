package cc.mrbird.febs.api.user;


import cc.mrbird.febs.common.annotation.Limit;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.service.ValidateCodeService;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.monitor.entity.LoginLog;
import cc.mrbird.febs.monitor.service.ILoginLogService;
import cc.mrbird.febs.system.entity.Role;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class LoginApiController extends BaseController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ValidateCodeService validateCodeService;
    @Autowired
    private ILoginLogService loginLogService;
    
    @Resource
    private RedisService redisService;

    /**
     * @ Description: 前台用户登录接口，可通过用户名、手机号、学号进行登录
     * @ Param: [username, idNumber, mobile, password, verifyCode, rememberMe, request]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/1
     */
    @RequestMapping("/login")
    @Limit(key = "login", period = 60, count = 20, name = "登录接口", prefix = "limit")
    public FebsResponse login(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password,
            @NotBlank(message = "{required}") String verifyCode,
            boolean rememberMe) throws FebsException {
        HttpServletRequest request = getRequest();
        HttpSession session = request.getSession();
        validateCodeService.check( session.getId(), verifyCode);
        //1、如果传的是用户名
        User serviceOne = userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getUsername, username), false);
        if (serviceOne == null) {
            //2,如果传的是学号
            serviceOne = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getIdNumber, username));
            if (serviceOne == null) {
                //3，如果传的是手机号
                serviceOne = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, username));
            }
        }
        //如果都不是，那就说明用户名或者密码错误，登录失败！
        if (serviceOne == null) {
            return new FebsResponse().fail().message("用户名或密码错误！");
        }

        //只有角色Id为82或81的才可以登录学院系统
        Role currentRole = getCurrentRole(serviceOne);
        if (currentRole.getRoleId() == 81L || currentRole.getRoleId() == 82L ){
            //执行登录成功之后的业务
            username = serviceOne.getUsername();
            password = MD5Util.encrypt(serviceOne.getUsername().toLowerCase(), password);
            UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
            super.login(token);
            // 保存登录日志
            LoginLog loginLog = new LoginLog();
            loginLog.setUsername(username);
            loginLog.setSystemBrowserInfo();
            this.loginLogService.saveLoginLog(loginLog);

            User user = (User) SecurityUtils.getSubject().getPrincipal();

            user.setPassword(null);
            user.setIsTab(null);
            user.setIsDel(null);
            user.setTheme(null);
            this.userService.updateLoginTime(username);
            String sessionId = getRequest().getSession().getId();
            return new FebsResponse().success().data(user).message("true").put("session",sessionId);
        }else{
            return new FebsResponse().fail().message("只允许教师学生登录");
        }

    }

    /**
     * 不用输验证码的登录接口
     * @ Description: 前台用户登录接口，可通过用户名、手机号、学号进行登录
     * @ Param: [username, idNumber, mobile, password, verifyCode, rememberMe, request]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/1
     */
    @RequestMapping("/loginNo")
    @Limit(key = "loginNo", period = 60, count = 20, name = "登录接口", prefix = "limit")
    public FebsResponse loginNo(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password,
            boolean rememberMe) throws FebsException {
        //1、如果传的是用户名
        User serviceOne = userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getUsername, username)
                .eq(User::getIsDel,1), false);
        if (serviceOne != null) {
            password = MD5Util.encrypt(serviceOne.getUsername().toLowerCase(), password);
        } else {
            //2,如果传的是学号
            serviceOne = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getIdNumber, username));
            if (serviceOne != null) {
                password = MD5Util.encrypt(serviceOne.getUsername().toLowerCase(), password);
            } else {
                //3，如果传的是手机号
                serviceOne = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, username));
                if (serviceOne != null) {
                    password = MD5Util.encrypt(serviceOne.getUsername().toLowerCase(), password);
                }
            }
        }
        //如果都不是，那就说明用户名或者密码错误，登录失败！
        if (serviceOne == null) {
            return new FebsResponse().fail().message("用户名或密码错误！");
        }
        //执行登录成功之后的业务
        username = serviceOne.getUsername();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        super.login(token);
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        user.setPassword(null);
        user.setIsTab(null);
        user.setIsDel(null);
        user.setTheme(null);
        String sessionId = getRequest().getSession().getId();
        return new FebsResponse().success().data(user).message("true").put("session",sessionId);

    }
    
    /**
    * @ Description: 手机号+验证码进行登录
    * @ Param: [mobile, code]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @PostMapping("/MobileLogin")
    public FebsResponse MobileLogin(String mobile, String code){
        String smsCode = (String) redisService.get(mobile);
        if (code==null){
            return new FebsResponse().fail().message("验证码输入错误！");
        }
        if (code.equals(smsCode)){
            //验证码校验正确，执行登录操作
            User serviceOne = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile)
            .eq(User::getIsDel,1));
            if (serviceOne==null){
                return new FebsResponse().fail().message("用户信息不存在！");
            }
            UsernamePasswordToken token = new UsernamePasswordToken(serviceOne.getUsername(), serviceOne.getPassword(), true);
            super.login(token);
            // 保存登录日志
            LoginLog loginLog = new LoginLog();
            loginLog.setUsername(serviceOne.getUsername());
            loginLog.setSystemBrowserInfo();
            this.loginLogService.saveLoginLog(loginLog);
            return new FebsResponse().success().message("登录成功！");
        }else {
            return new FebsResponse().fail().message("验证码输入错误！");
        }
    }

    @PostMapping("regist")
    public FebsResponse regist(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password) throws FebsException {
        User user = userService.findByName(username);
        if (user != null) {
            throw new FebsException("该用户名已存在");
        }
        this.userService.regist(username, password);
        return new FebsResponse().success();
    }

    @GetMapping("index/{username}")
    public FebsResponse index(@NotBlank(message = "{required}") @PathVariable String username) {
        // 更新登录时间
        this.userService.updateLoginTime(username);
        Map<String, Object> data = new HashMap<>();
        // 获取系统访问记录
        Long totalVisitCount = this.loginLogService.findTotalVisitCount();
        data.put("totalVisitCount", totalVisitCount);
        Long todayVisitCount = this.loginLogService.findTodayVisitCount();
        data.put("todayVisitCount", todayVisitCount);
        Long todayIp = this.loginLogService.findTodayIp();
        data.put("todayIp", todayIp);
        // 获取近期系统访问记录
        List<Map<String, Object>> lastSevenVisitCount = this.loginLogService.findLastSevenDaysVisitCount(null);
        data.put("lastSevenVisitCount", lastSevenVisitCount);
        User param = new User();
        param.setUsername(username);
        List<Map<String, Object>> lastSevenUserVisitCount = this.loginLogService.findLastSevenDaysVisitCount(param);
        data.put("lastSevenUserVisitCount", lastSevenUserVisitCount);
        return new FebsResponse().success().data(data);
    }

    @GetMapping("/images/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws IOException, FebsException {
        validateCodeService.create(request, response);
    }


}
