package cc.mrbird.febs.api.user;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.service.RedisService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/05/15/16:08
 * @ Description:
 */
@RestController
@RequestMapping("/api")
public class SendSmsApiController extends BaseController {

    @Resource
    private RedisService redisService;

    /**
    * @ Description: 发送短信验证码
    * @ Param: [mobile, templateCode]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/15
    */
    @PostMapping("sendSmsMsg")
    public FebsResponse sendSmsMsg(String mobile,String templateCode){
        if (sendSms(mobile, templateCode)) {
            return new FebsResponse().success().message("发送成功！");
        }
        return new FebsResponse().fail().message("发送失败！！！");
    }

    /**
    * @ Description: 验证验证码是否正确
    * @ Param: [mobile, code]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/15
    */
    @GetMapping("/validSms")
    public FebsResponse validSms(String mobile,String code){
        String smsCode = (String) redisService.get(mobile);
        if (code==null){
            return new FebsResponse().fail().message("验证码输入错误！");
        }
        if (code.equals(smsCode)){
            return new FebsResponse().success().message("验证码输入正确！");
        }else {
            return new FebsResponse().fail().message("验证码输入错误！");
        }
    }

}
