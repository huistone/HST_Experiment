package cc.mrbird.febs.api.mainData;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ElectricRecordApiController
 * @ Description TODO
 * @Author admin
 * @Date 2020/12/5 14:28
 * @Version 1.0
 */
@RequestMapping("api/electricRecord")
@RestController
@Slf4j
public class ElectricRecordApiController {
}
