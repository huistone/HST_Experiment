package cc.mrbird.febs.api.mainData;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.AnalogBase;
import cc.mrbird.febs.system.entity.AnalogRecord;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IAnalogBaseService;
import cc.mrbird.febs.system.service.IAnalogRecordService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @ ClassName AnalogBaseApiController
 * @ Description
 * @ Author admin
 * @ Date 2020/10/14 8:59
 * @ Version 1.0
 */
@RequestMapping("/api/analogBase")
@RestController
public class AnalogBaseApiController extends BaseController {

    @Resource
    private IAnalogBaseService analogBaseService;

    @Resource
    private IAnalogRecordService analogRecordService;


    /**
     * @ Author wangke
     * @ Description 获取所有主板列表
     * @ Date 9:05 2020/10/14
     * @ Param
     * @return
     */
    @RequestMapping("getAnalogBaseList")
    @ControllerEndpoint(operation = "获取模电主板列表",exceptionMessage = "获取模电主板列表失败")
    public FebsResponse getAnalogBaseList(QueryRequest queryRequest,AnalogBase analogBase){

        User currentUser = getCurrentUser();
        if(currentUser == null){
            return new FebsResponse().no_login();
        }

        Page<AnalogBase> projectPage = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
        SortUtil.handlePageSort(queryRequest, projectPage, "analog_id", FebsConstant.ORDER_ASC, false);
        IPage<AnalogBase> page = analogBaseService.page(projectPage, new QueryWrapper<AnalogBase>()
                .lambda()
                .eq(AnalogBase::getAnalogStatus,1)
                .like(AnalogBase::getProjectId,analogBase.getProjectId())
                .select(AnalogBase::getAnalogName
                        , AnalogBase::getAnalogId
                        , AnalogBase::getAnalogStatus
                        , AnalogBase::getAnalogUsestatus
                        , AnalogBase::getOperator
                        , AnalogBase::getAnalogNumber
                        , AnalogBase::getReleaseChannel
                        , AnalogBase::getDescription
                        , AnalogBase::getProjectId)
        );
        List<AnalogBase> records = page.getRecords();
        records.forEach(s->{
            Long analogId = s.getAnalogId();
            int count = analogRecordService.count(new QueryWrapper<AnalogRecord>()
                    .lambda()
                    .eq(AnalogRecord::getAnalogId, analogId)
                    .eq(AnalogRecord::getIsUse,1));
            s.setUseNumber(count);
        });
        return new FebsResponse().data(getDataTable(page));
    }

    /**
     * @ Author wangke
     * @ Description 绑定主板修改主板状态
     * @ Date 9:57 2020/10/14
     * @ Param
     * @return 
     */
    @RequestMapping("bindAnalogStatus")
    @ControllerEndpoint(operation = "绑定主板和操作人",exceptionMessage = "绑定主板和操作人失败")
    public FebsResponse bindAnalogStatus(Long analogId){
        User currentUser = getCurrentUser();
        if(currentUser == null){
            return new FebsResponse().no_login();
        }
        try {
            AnalogBase base = analogBaseService.getOne(new QueryWrapper<AnalogBase>()
                    .lambda()
                    .eq(AnalogBase::getAnalogId, analogId)
                    .eq(AnalogBase::getAnalogStatus,1)
                    .select(AnalogBase::getAnalogId
                    ,AnalogBase::getAnalogUsestatus
                    ,AnalogBase::getOperator));

            base.setOperator(currentUser.getTrueName());
//            base.setAnalogUsestatus(1);
            analogBaseService.updateById(base);
            //保存记录
            AnalogRecord analogRecord = new AnalogRecord();
            analogRecord.setUserId(currentUser.getUserId());
            analogRecord.setAnalogId(analogId);
            analogRecord.setBeginTime(LocalDateTime.now());
            analogRecord.setTrueName(currentUser.getTrueName());
            analogRecord.setIsUse(1);
            analogRecordService.save(analogRecord);
            Long analogRecordId = analogRecord.getAnalogRecordId();
            return new FebsResponse().success().put("analogRecordId",analogRecordId);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("selectAnalogPublicTopic")
    @ControllerEndpoint(exceptionMessage = "查询模电主板发布主题失败",operation = "查询模电主板发布主题")
    public FebsResponse selectAnalogPublicTopic(Integer analogId){
        User currentUser = getCurrentUser();

        if (currentUser == null){
            return new FebsResponse().no_login();
        }

        AnalogBase one = null;
        try {
            one = analogBaseService.getOne(new QueryWrapper<AnalogBase>().lambda()
                    .eq(AnalogBase::getAnalogId, analogId)
                    .select(AnalogBase::getReleaseChannel));
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("查询发步主题失败");
        }

        return new FebsResponse().success().data(one);
    }

    @RequestMapping("freedAnalogBase")
    @ControllerEndpoint(operation = "释放主板",exceptionMessage = "释放主板失败")
    public FebsResponse freedAnalogBase(Long analogId,Long analogRecordId){
        User currentUser = getCurrentUser();
        if(currentUser == null ){
            return new FebsResponse().no_login();
        }
        if(analogId == null){
            return new FebsResponse().no_param();
        }
        try {
            AnalogBase base = analogBaseService.getOne(new QueryWrapper<AnalogBase>()
                    .lambda()
                    .eq(AnalogBase::getAnalogId, analogId)
                    .eq(AnalogBase::getAnalogStatus,1));
//            base.setAnalogUsestatus(0);
            base.setOperator("暂无使用");
            analogBaseService.updateById(base);
            AnalogRecord analogRecord = analogRecordService.getById(analogRecordId);
            analogRecord.setEndTime(LocalDateTime.now());
            analogRecord.setIsUse(0);
            analogRecordService.updateById(analogRecord);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }


}
