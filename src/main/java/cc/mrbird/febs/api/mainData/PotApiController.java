package cc.mrbird.febs.api.mainData;


import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.Pot;
import cc.mrbird.febs.system.service.IPotService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * POT1 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/api/pot")
public class PotApiController extends BaseController {

    @Resource
    private IPotService potService;

    @RequestMapping("/getUce")
    public FebsResponse getUce(){
        List<Pot> list = potService.list();
        List<ArrayList<Object>> mapList = new ArrayList<>();

        for (Pot pot : list) {
            ArrayList<Object> arrayList = new ArrayList<>();
            arrayList.add(pot.getV1c()-pot.getV1e());
            arrayList.add(pot.getIc());
            mapList.add(arrayList);
        }
        return new FebsResponse().success().data(mapList);

    }

    @RequestMapping("/getUbe")
    public FebsResponse getUbe(){
        List<Pot> list = potService.list();
        List<ArrayList<Object>> mapList = new ArrayList<>();

        for (Pot pot : list) {
            ArrayList<Object> arrayList = new ArrayList<>();
            arrayList.add(pot.getV1b()-pot.getV1e());
            arrayList.add(pot.getIb());
            mapList.add(arrayList);
        }
        return new FebsResponse().success().data(mapList);
    }

    @RequestMapping("/getIbIc")
    public FebsResponse getIbIc(){
        List<Pot> list = potService.list(new QueryWrapper<Pot>().lambda().ge(Pot::getPotId,450));
        List<ArrayList<Object>> mapList = new ArrayList<>();
        for (Pot pot : list) {
            ArrayList<Object> arrayList = new ArrayList<>();
            arrayList.add(pot.getIb());
            arrayList.add(pot.getIc());
            mapList.add(arrayList);
        }
        return new FebsResponse().success().data(mapList);
    }

}
