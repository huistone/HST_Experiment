package cc.mrbird.febs.api;

import lombok.Data;

/**
 * @ Author: 马超伟
 * @ Date: 2020/12/16 16:11
 * @ Params:  * @ Param null
 * @ return: 加解密工具类
 * @ Description: 
 */
@Data
public class TestBean {

    public String name;
    public Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "TestBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
