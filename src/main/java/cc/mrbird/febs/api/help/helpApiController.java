package cc.mrbird.febs.api.help;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.Help;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IHelpService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @ Author: 王珂
 * @ Description: 帮助文档API
 * @ Date: 10:48 2020/7/2
 * @ params:  * @ Param null
 */
@RestController
@RequestMapping("/api/help")
@Slf4j
public class helpApiController extends BaseController {

        @Resource
        private IHelpService helpService;
        /**
         * @ Author: 王珂
         * @ Description: 查询帮助文档
         * @ Date: 11:06 2020/7/2
         * @ params:  * @ Param null
         */
        @GetMapping("/list")
        @ControllerEndpoint(operation = "查询帮助文档" ,exceptionMessage = "查询帮助文档失败")
        public FebsResponse getHelpList(Integer courseId){
            try {
                List<Help> helpList = helpService.list(new QueryWrapper<Help>().lambda()
                        .eq(Help::getCourseId, courseId)
                        .select(Help::getHelpName, Help::getNumber,Help::getHelpId)
                        .orderByAsc(Help::getNumber));
                return new FebsResponse().success().data(helpList);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("/api/help/list"+e.getMessage());
                return new FebsResponse().fail().message("查询失败");
            }
        }
        /**
         * @ Author: 王珂
         * @ Description: 根据id查询帮助文档
         * @ Date: 11:07 2020/7/2
         * @ params:  * @ Param null
         */
        @GetMapping("/getOne/{helpId}")
        @ControllerEndpoint(operation = "查询单个帮助文档" ,exceptionMessage = "查询单个帮助文档失败")
        public FebsResponse getOne(@PathVariable Integer helpId){
            if (helpId==null){
                return new FebsResponse().fail().message("查询单个帮助文档失败");
            }
            Help help = helpService.getById(helpId);
            return new FebsResponse().success().data(help);
        }
        
        /**
         * @ Author: 王珂
         * @ Description: 查询一个类型的帮助文档
         * @ Date: 11:07 2020/7/2
         * @ params:  * @ Param null
         */
         @RequestMapping("/getListByKind")
         @ControllerEndpoint(operation = "查询类型帮助文档" ,exceptionMessage = "查询类型帮助文档失败")
         public FebsResponse getListByKind(Integer kindId,String helpName){
            if (kindId==null){
                return new FebsResponse().fail().message("类型id不能为空");
            }
             List<Help> helpList = helpService.list(new QueryWrapper<Help>()
                     .lambda()
                     .eq(Help::getParentId,kindId)
                     .like(StringUtils.isNotBlank(helpName),Help::getHelpName,helpName));
            return new FebsResponse().success().data(helpList);
    }
        /**
         * @ Author: 王珂
         * @ Description: 添加帮助文档
         * @ Date: 11:09 2020/7/2
         * @ params:  * @ Param null
         */
        @RequestMapping("/addOne")
        @ControllerEndpoint(operation = "添加帮助文档" ,exceptionMessage = "添加帮助文档失败")
        public FebsResponse addOne(Help help){
            User user = getCurrentUser();
            if(user==null){
                return new FebsResponse().fail().message(FebsConstant.USER_UNLOGIN_MESSAGE);
            }
            help.setCreateId(user.getUserId().intValue());
            help.setCreateName(user.getUsername());
            Date curDate = new Date(System.currentTimeMillis());
            help.setCreateTime(curDate);
            boolean save ;
            try {
                save = helpService.save(help);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("/api/help/addHelp:"+e.getMessage());
                return new FebsResponse().fail().message("添加失败");
            }
            if(save){
                return new FebsResponse().success().message("添加成功");
            }
            return new FebsResponse().fail().message("添加失败");
        }

        @RequestMapping("/updateOne")
        @ControllerEndpoint(operation = "修改帮助文档" ,exceptionMessage = "修改帮助文档失败")
        public FebsResponse updateOne(Help help){
            User user = getCurrentUser();
            if(user == null){
                return new FebsResponse().fail().message(FebsConstant.USER_UNLOGIN_MESSAGE);
            }
            help.setUpdateId(user.getUserId().intValue());
            help.setUpdateName(user.getUsername());
            Date curDate = new Date(System.currentTimeMillis());
            help.setUpdateTime(curDate);

            boolean update ;
            try {
                update = helpService.updateById(help);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("/api/help/updateOne:"+e.getMessage());
                return new FebsResponse().fail().message("修改失败");
            }
            if(update){
                return new FebsResponse().success().message("修改成功");
            }
            return new FebsResponse().fail().message("修改失败");
        }

        @RequestMapping("/deleteByIds")
        @ControllerEndpoint(operation = "删除帮助文档" ,exceptionMessage = "删除帮助文档失败")
        public FebsResponse deleteByIds(String ids){
            if(ids == null){
                return new FebsResponse().fail().message("删除失败");
            }
            String[] array = ids.split(",");
            List<String> idList = Arrays.asList(array);
            boolean delete ;
            try {
                delete = helpService.removeByIds(idList);
            } catch (Exception e) {
                log.error("/api/help/deleteByIds:"+e.getMessage());
                e.printStackTrace();
                return new FebsResponse().fail().message("删除失败");
            }
            if(delete){
                return new FebsResponse().success().message("删除成功");
            }
            return new FebsResponse().fail().message("删除失败");
        }

}
