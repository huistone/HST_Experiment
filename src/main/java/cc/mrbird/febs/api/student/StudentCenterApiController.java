package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.entity.vo.QuestionVo;
import cc.mrbird.febs.student.entity.vo.StudentScoreVo;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName StudentCenterAPIController
 * @ Description
 * @Author wangke
 * @Data 2020-8-18 17:22
 * @Version 1.0
 */
@Controller
@RequestMapping("/api/center")
@ResponseBody
@Slf4j
public class StudentCenterApiController extends BaseController {

    @Resource
    private IUserRoleService userRoleService;

    @Resource
    private IRoleService roleService;

    @Resource
    private IDeptService deptService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IExperimentAnswerService experimentAnswerService;

    @Resource
    private IExperimentRemarkService experimentRemarkService;

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private IExperimentQuestionService experimentQuestionService;

    @Resource
    private IUserService userService;


    @GetMapping("getPrepareDetailInfo")
    @ControllerEndpoint(operation = "查询预习题成绩详情",exceptionMessage = "查询预习题成绩详情失败")
    public FebsResponse getPrepareDetailInfo(Long remarkId){
        User student = getCurrentUser();
        if (student == null){
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isBlank(remarkId.intValue())){
            return new FebsResponse().no_param();
        }
        try{
            ExperimentRemark remark = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>().lambda()
                    .eq(ExperimentRemark::getRemarkId, remarkId)
                    .eq(ExperimentRemark::getUserType,1)
                    .eq(ExperimentRemark::getIsDel,1)
                    .select(ExperimentRemark::getIsCommit, ExperimentRemark::getScore,ExperimentRemark::getRemark));
            QuestionVo questionVo = null;
            List<QuestionVo> questionList = new ArrayList<>();
            List<ExperimentAnswer> answerList = experimentAnswerService.list(new QueryWrapper<ExperimentAnswer>()
                    .lambda()
                    .eq(ExperimentAnswer::getCreateId, remarkId)
                    .eq(ExperimentAnswer::getUserType,1)
                    .select(ExperimentAnswer::getProjectId,
                            ExperimentAnswer::getNumber,
                            ExperimentAnswer::getScore,
                            ExperimentAnswer::getContext,
                            ExperimentAnswer::getQuestionId));
            for (ExperimentAnswer experimentAnswer : answerList) {
                questionVo = new QuestionVo();
                ExperimentQuestion question = experimentQuestionService.getById(experimentAnswer.getQuestionId());
                questionVo.setQuestionContent(question.getQuestionName());
                questionVo.setQuestionNumber(question.getNumber());
                questionVo.setQuestionAnswer(question.getAnswer());
                questionVo.setStudentAnswer(experimentAnswer.getContext());
                questionVo.setScore(experimentAnswer.getScore());
                questionList.add(questionVo);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("prepareInfo",questionList);
            map.put("score",remark.getScore());
            map.put("remark",remark.getRemark());
            map.put("scoreTeacher",remark.getScoreUser());
            map.put("scoreTime",remark.getScoreTime());
            return new FebsResponse().data(map).success();
        }catch (Exception e){
            log.error("/api/center/getPrepareDetailInfo"+e.getMessage());
            return new FebsResponse().fail();
        }

    }


    @GetMapping("getStudentInfo")
    @ControllerEndpoint(operation = "查询个人信息",exceptionMessage = "查询个人信息失败")
    public FebsResponse getStudentInfo(){
        try {
            User currentUser = getCurrentUser();

            if (currentUser == null) {
                return new FebsResponse().no_login();
            }

            currentUser =  userService.getById(currentUser.getUserId());


            UserRole one = userRoleService.getOne(new QueryWrapper<UserRole>()
                    .lambda()
                    .eq(UserRole::getUserId, currentUser.getUserId()));
            //设置角色名
            currentUser.setRoleName(roleService.getOne(new QueryWrapper<Role>()
                    .lambda()
                    .eq(Role::getRoleId, one.getRoleId())).getRoleName());
            //设置角色id
            currentUser.setRoleId(one.getRoleId());
            Dept thirdDept = deptService.getOne(new QueryWrapper<Dept>()
                    .lambda()
                    .eq(Dept::getDeptId, currentUser.getDeptId())
                    .select(Dept::getDeptName,Dept::getParentId));
            Dept secondDept = deptService.getOne(new QueryWrapper<Dept>()
                    .lambda()
                    .eq(Dept::getDeptId, thirdDept.getParentId())
                    .select(Dept::getDeptName,Dept::getParentId));
            Dept FirstDept = deptService.getOne(new QueryWrapper<Dept>()
                    .lambda()
                    .eq(Dept::getDeptId, secondDept.getParentId())
                    .select(Dept::getDeptName, Dept::getParentId));
            currentUser.setFirstDeptName(FirstDept.getDeptName());
            currentUser.setThirdDeptName(thirdDept.getDeptName());
            return  new FebsResponse().data(currentUser).success();
        } catch (Exception e) {
            log.error("api/center/getStudentInfo"+e.getMessage());
            return new FebsResponse().fail();
        }
    }

    @GetMapping("getStudentCourseList")
    @ControllerEndpoint(operation = "获取学生课程列表",exceptionMessage = "获取学生课程列表失败")
    public FebsResponse getStudentCourseList(){
        try {
            User currentUser = getCurrentUser();
            if (currentUser == null){
                return new FebsResponse().no_login();
            }
            List<CourseDept> list = courseDeptService.list(new QueryWrapper<CourseDept>()
                    .lambda()
                    .eq(CourseDept::getDeptId, currentUser.getDeptId()));
            List courseList = new ArrayList<>();
            for (CourseDept courseDept : list) {
                Course course = courseService.getOne(new QueryWrapper<Course>()
                        .lambda()
                        .eq(Course::getCourseId,courseDept.getCourseId())
                        .eq(Course::getState,1)
                        .select(Course::getCourseId,Course::getCourseName,Course::getCoursePictureUrl,Course::getEnglishName));
                if(course!=null){
                    courseList.add(course);
                }
            }
            return new FebsResponse().data(courseList).success();
        } catch (Exception e) {
            log.error("api/center/getStudentCourseList"+e.getMessage());
            return new FebsResponse().fail();
        }
    }

    /**
     * 查询学生实验成绩接口
     * @ Param request
     * @return
     */
    @GetMapping("selectStudentProjectScore")
    @ControllerEndpoint(operation = "查询学生实验成绩",exceptionMessage = "查询学生实验成绩失败")
    public FebsResponse selectStudentProjectScore(QueryRequest request){
        try {
            User currentUser = getCurrentUser();
            if (currentUser==null){
                return new FebsResponse().no_login();
            }
            //查找课程id
            List<CourseDept> courseDepts = courseDeptService.list(new QueryWrapper<CourseDept>()
                    .lambda()
                    .eq(CourseDept::getDeptId, currentUser.getDeptId())
                    .select(CourseDept::getCourseId));
            List<Integer> courseIds = courseDepts.stream().map(s -> s.getCourseId()).collect(Collectors.toList());
            //通过课程查找出所有实验项目
            List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                    .lambda()
                    .in(courseIds.size() > 0, ExperimentProject::getCourseId, courseIds)
                    .select(ExperimentProject::getProjectId));
            List<Long> projectIds = projectList.stream().map(s -> s.getProjectId()).collect(Collectors.toList());
            List<StudentScoreVo> studentScoreList = new ArrayList<>();
            List<Long> experimentProjectList = new ArrayList<>();
            for (Long projectId : projectIds) {
                //通过班级id和实验项目id查开启实验项目
                TeacherProject one = teacherProjectService.getOne(new QueryWrapper<TeacherProject>()
                        .lambda()
                        .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                        .eq(TeacherProject::getProjectId, projectId)
                        .eq(TeacherProject::getIsDel,1));
                if(one!=null){
                    experimentProjectList.add(projectId);
                }
            }
            if (experimentProjectList.size() == 0){
                return new FebsResponse().data(null).success().message("暂无开启的实验项目");
            }
            StudentScoreVo studentScoreVo = null;
            List<UserCommit> list= userCommitService.selectStudentScore(experimentProjectList, request, currentUser.getUserId(), currentUser.getDeptId());
            for (UserCommit userCommit : list) {
                studentScoreVo = new StudentScoreVo();
                Double  projectScore =  userCommit.getScore();
                if(projectScore ==null){
                    projectScore = 0.0;
                }
                studentScoreVo.setProjectId(userCommit.getProjectId());
                studentScoreVo.setProjectScore(projectScore);
                Integer projectCommit = userCommit.getIsCommit();
                if(projectCommit == null){
                    projectCommit = 0;
                }
                studentScoreVo.setProjectCommitId(userCommit.getCommitId());
                LocalDateTime createTime = userCommit.getCreateTime();
                if (createTime != null){
                    studentScoreVo.setCreateTime(createTime);
                }
                studentScoreVo.setProjectCommit(projectCommit);
                ExperimentRemark prepareRemark = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                        .lambda()
                        .eq(ExperimentRemark::getProjectId, userCommit.getProjectId())
                        .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                        .eq(ExperimentRemark::getUserType,1)
                        .eq(ExperimentRemark::getType, 1)
                        .eq(ExperimentRemark::getIsDel,1)
                        .select(ExperimentRemark::getIsCommit, ExperimentRemark::getScore,ExperimentRemark::getRemarkId));
                ExperimentRemark thinkRemark = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                        .lambda()
                        .eq(ExperimentRemark::getProjectId, userCommit.getProjectId())
                        .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                        .eq(ExperimentRemark::getUserType,1)
                        .eq(ExperimentRemark::getType, 2)
                        .eq(ExperimentRemark::getIsDel,1)
                        .select(ExperimentRemark::getIsCommit, ExperimentRemark::getScore,ExperimentRemark::getRemarkId));
                //预习题
                if(prepareRemark == null){
                    prepareRemark = new ExperimentRemark();
                }
                Integer prepareCommit = prepareRemark.getIsCommit();
                Double prepareScore = prepareRemark.getScore();
                if (prepareScore == null || prepareCommit!=2){
                    prepareScore = 0.0;
                }
                if (thinkRemark == null){
                    thinkRemark = new ExperimentRemark();
                }
                Double thinkScore = thinkRemark.getScore();
                Integer thinkCommit = thinkRemark.getIsCommit();
                if (thinkScore == null||thinkCommit!=2){
                    thinkScore = 0.0;
                }
                studentScoreVo.setPrepareScore(prepareScore);
                studentScoreVo.setThinkScore(thinkScore);
                if(prepareCommit ==null){
                    prepareCommit = 0;
                }
                if (thinkCommit == null){
                    thinkCommit = 0;
                }
                studentScoreVo.setPrepareRemarkId(prepareRemark.getRemarkId());
                studentScoreVo.setThinkRemarkId(thinkRemark.getRemarkId());
                studentScoreVo.setPrepareCommit(prepareCommit);
                studentScoreVo.setThinkCommit(thinkCommit);
                studentScoreVo.setSum(projectScore*0.6+prepareScore*0.1+thinkScore*0.1);
                studentScoreVo.setProjectName(userCommit.getProjectName());
                studentScoreVo.setCourseId(userCommit.getCourseId());
                studentScoreVo.setTroubleCommit(0);
                studentScoreVo.setTroubleScore(0.0);
                studentScoreList.add(studentScoreVo);
            }
            Integer integer = userCommitService.studentScoreNum(experimentProjectList, currentUser.getUserId(), currentUser.getDeptId());
            HashMap<String, Object> map = new HashMap<>();
            map.put("rows",studentScoreList);
            map.put("total",integer);
            return new FebsResponse().data(map).success();
        } catch (Exception e) {
            log.error("api/center/selectStudentProjectScore"+e.getMessage());
            return new FebsResponse().fail().message("查询失败");
        }

    }
}
