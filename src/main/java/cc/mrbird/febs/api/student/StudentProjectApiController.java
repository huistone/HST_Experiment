package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/studentProject")
public class StudentProjectApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IUserService userService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IUserCommitService userCommitService;


    @RequestMapping("/list")
    @ControllerEndpoint(operation = "学生查询实验项目", exceptionMessage = "学生查询实验项目失败")
    public FebsResponse studentProjectList(Integer courseId, ExperimentProject experimentProject, QueryRequest request){
        //获取当前登录用户
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().fail().message(FebsConstant.USER_UNLOGIN_MESSAGE);
        }
        if (courseId==null){
            return new FebsResponse().fail().message("课程ID不能为空");
        }
        //获取当前登录学生用户的老师用户信息
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getCourseId, courseId));
        //查询当前登录用户老师开启的全部项目
        Map<String, Object> dataTable = null;
        try {
            if(one != null){
                List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                        .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                        .eq(TeacherProject::getCourseId, courseId)
                        .eq(TeacherProject::getTeacherId, one.getUserId()));
                //创建分页对象
                Page<ExperimentProject> page = new Page<>(request.getPageNum(), request.getPageSize());
                //创建排序处理器
                SortUtil.handlePageSort(request, page, "project_id", FebsConstant.ORDER_ASC, false);
                //创建分页条件
                IPage<ExperimentProject> iPage = this.experimentProjectService.page(page, new QueryWrapper<ExperimentProject>().lambda()
                        //课程号
                        .eq(ExperimentProject::getCourseId,courseId)
                        //开启的项目列表
                        .in(ExperimentProject::getProjectId,teacherProjectList.stream().map(TeacherProject::getProjectId).collect(Collectors.toList()))
                        //根据项目名的查询条件
                        .like(StringUtils.isNotBlank(experimentProject.getProjectName()), ExperimentProject::getProjectName, experimentProject.getProjectName())
                        //根据项目的状态条件查询
                        .like(experimentProject.getState() != null,ExperimentProject::getState,experimentProject.getState()));
                //封装返回结果集
                dataTable  = getDataTable(iPage);
            }
        } catch (Exception e){
            e.printStackTrace();
            logger.error("/api/studentProject/list:"+e.getMessage());
            return new FebsResponse().fail().message("系统异常！");
        }
        return new FebsResponse().success().data(dataTable);
    }

    @RequestMapping("/downloadReport")
    @ControllerEndpoint(operation = "下载实验报告", exceptionMessage = "下载实验报告失败")
    public FebsResponse downloadReport(Integer studentId,Integer projectId){
        if(studentId==null||projectId==null){
            return new FebsResponse().fail().message("学生ID和项目ID不能为空");
        }
        try {
            UserCommit userCommit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getProjectId,projectId).eq(UserCommit::getStuId,studentId).orderByDesc(UserCommit::getCreateTime).last("limit 1"));
            String url = userCommit.getReportUrl();
            downloadOss(url);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/studentProject/downloadReport"+e.getMessage());
            return new FebsResponse().fail().message("下载实验报告失败");
        }
        return new FebsResponse().success();
    }

    @RequestMapping("/downLoadTemplateReport/{projectId}")
    @ControllerEndpoint(operation = "下载实验报告模板成功",exceptionMessage = "下载实验报告模板失败")
    public FebsResponse downLoadTemplateReport(@PathVariable Integer projectId){
        if(projectId==null){
            return new FebsResponse().fail().message("项目ID不能为空");
        }
        ExperimentProject project = experimentProjectService.getById(projectId);
        String url = project.getReportUrl();
        if(url == null || url ==""){
            return new FebsResponse().fail().message("该实验项目还没有模板");
        }else {
            try {
                downloadOss(url);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("/api/studentProject/downLoadTemplateReport/:"+e.getMessage());
            }
            return new FebsResponse().success();
        }
    }
}
