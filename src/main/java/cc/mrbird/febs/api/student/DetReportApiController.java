package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.FigureUtil;
import cc.mrbird.febs.system.entity.DetReport;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDetreport;
import cc.mrbird.febs.system.service.IDetRecordService;
import cc.mrbird.febs.system.service.IDetReportService;
import cc.mrbird.febs.system.service.IUserDetreportService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.google.gson.JsonArray;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName DetReportApiController
 * @ Description 数电实验报告记录
 * @Author wangke
 * @Data 2020-8-5 11:40
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/api/detReport")
public class DetReportApiController extends BaseController {
    @Resource
    private IDetReportService detReportService;

    @Resource
    private IUserDetreportService userDetReportService;

    @RequestMapping(value = "receiveJson")
    @ControllerEndpoint(operation = "获取receiveJson成功",exceptionMessage ="获取receiveJson失败" )
    public FebsResponse receiveJson(String data){
//        JSONArray jsonParam = this.getJSONParam(request);
//        JSONObject jsonObject = jsonParam.getJSONObject(0);
        JSONObject jsonObject = JSONObject.parseObject(data);
        String time = (String) jsonObject.get("time");
        String name = jsonObject.get("name").toString();
        return new FebsResponse().success().data(jsonObject);
    }

    @GetMapping("/getOne/{detReportId}")
    public FebsResponse getOne(@PathVariable Integer detReportId){
        DetReport report = detReportService.getById(detReportId);
        String detReportJson = report.getDetReportJson();
        Map map = JSON.parseObject(detReportJson, Map.class);
        String detReportVoltage = map.get("detReportVoltage").toString();
        String detReportExpress = map.get("detReportExpress").toString();
        return new FebsResponse().success();
    }

    @RequestMapping(value="getMap")
    public FebsResponse getMap(@RequestBody Map map){
        String name = (String) map.get("name");
        String time = (String) map.get("time");
        return new FebsResponse().data(map);
    }

    @RequestMapping("saveReportInfo")
    @ControllerEndpoint(exceptionMessage = "批阅实验报告失败",operation = "批阅实验报告成功")
    public FebsResponse saveDetReport(@RequestBody Map detReportMap){
        try {
            User student = getCurrentUser();
            if(student==null){
                return new FebsResponse().no_login();
            }
            int rightAnswer = 0;
            double sum = 100.00;
            String[] logics = new String[]{"1","0","0","1","1","1","1","1","1","0","1","0"};
            String [] tests = new String[]{"0.3","0.5","0.8","1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","2.0"};
            String[] voltages = {"2", "2"};
            String [] monitors = {"绿","红"};
            String detReportVoltage = detReportMap.get("detReportVoltage").toString();
            String detReportExpress = detReportMap.get("detReportExpress").toString();
            String detReportTest = detReportMap.get("detReportTest").toString();
            String detReportLogic = detReportMap.get("detReportLogic").toString();
            String detReportMonitor = detReportMap.get("detReportMonitor").toString();
            //各电压中以,分割
            String[] voltage = detReportVoltage.split(StringPool.COMMA);
            if (voltages[0].equals(voltage[0])){
                rightAnswer++;
            }
            if(voltages[1].equals(voltage[1])){
                rightAnswer++;
            }
            String[] monitor = detReportMonitor.split(StringPool.COMMA);
            if(monitor[0].contains(monitors[0])){
                rightAnswer++;
            }
            if(monitor[1].contains(monitors[1])){
                rightAnswer++;
            }
            String[] logic = detReportLogic.split(StringPool.COMMA);
            for (int i = 0;i<logic.length;i++){
                if(logics[i].equals(logic[i])){
                    rightAnswer++;
                }
            }

            String[] test = detReportTest.split(StringPool.COMMA);
            for (int i = 0;i < test.length;i++){
                if(tests[i].equals(test[i])){
                    rightAnswer++;
                }
            }
            sum = sum*(rightAnswer/28.00);
            sum = FigureUtil.getDouble(sum);
            Map<String, Object> map = new HashMap<>(4);
            map.put("score",sum);
            DetReport detReport = new DetReport();
            detReport.setDetReportSum(sum);
            //转成json串
            String detReportMapString = JSON.toJSONString(detReportMap);
            detReport.setDetReportJson(detReportMapString);
            detReportService.save(detReport);
            UserDetreport userDetreport = new UserDetreport();
            userDetreport.setUserId(student.getUserId());
            userDetreport.setDetReportId(detReport.getDetReportId());
            userDetreport.setReportCreateTime(LocalDateTime.now());
            userDetReportService.save(userDetreport);
            return new FebsResponse().data(map).success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/detRecord/saveDeportInfo"+e.getMessage());
            return new FebsResponse().fail();
        }
    }

//    @RequestMapping("saveReportInfo")
//    public FebsResponse saveDetReport(DetReport detReport){
//        try {
//            User student = getCurrentUser();
//            if(student==null){
//                return new FebsResponse().no_login();
//            }
//            int rightAnswer = 0;
//            double sum = 100.00;
//            String[] logics = new String[]{"1","0","0","1","1","1","1","1","1","0","1","0"};
//            String [] tests = new String[]{"0.3","0.5","0.8","1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","2.0"};
//            String[] voltages = {"2", "2"};
//            String [] monitors = {"绿","红"};
//            String detReportVoltage = detReport.getDetReportVoltage();
//            String detReportExpress = detReport.getDetReportExpress();
//            String detReportTest = detReport.getDetReportTest();
//            String detReportLogic = detReport.getDetReportLogic();
//            String detReportMonitor = detReport.getDetReportMonitor();
//            //各电压中以,分割
//            String[] voltage = detReportVoltage.split(StringPool.COMMA);
//            if (voltages[0].equals(voltage[0])){
//                rightAnswer++;
//            }
//            if(voltages[1].equals(voltage[1])){
//                rightAnswer++;
//            }
//            String[] monitor = detReportMonitor.split(StringPool.COMMA);
//            if(monitor[0].contains(monitors[0])){
//                rightAnswer++;
//            }
//            if(monitor[1].contains(monitors[1])){
//                rightAnswer++;
//            }
//            String[] logic = detReportLogic.split(StringPool.COMMA);
//            for (int i = 0;i<logic.length;i++){
//                if(logic[i].equals(logics[i])){
//                    rightAnswer++;
//                }
//            }
//
//            String[] test = detReportTest.split(StringPool.COMMA);
//            for (int i = 0;i < test.length;i++){
//                if(tests[i].equals(test[i])){
//                    rightAnswer++;
//                }
//            }
//            sum = sum*(rightAnswer/28.00);
//            sum = FigureUtil.getDouble(sum);
//            Map<String, Object> map = new HashMap<>(4);
//            map.put("detReportVoltage", Arrays.asList(voltages));
//            map.put("detReportMonitor",Arrays.asList(monitors));
//            map.put("detReportLogic",Arrays.asList(logics));
//            map.put("detReportTest",Arrays.asList(tests));
//            map.put("score",sum);
//            detReport.setDetReportSum(sum);
//            detReportService.save(detReport);
//            UserDetreport userDetreport = new UserDetreport();
//            userDetreport.setUserId(student.getUserId());
//            userDetreport.setDetReportId(detReport.getDetReportId());
//            userDetreport.setReportCreateTime(LocalDateTime.now());
//            userDetReportService.save(userDetreport);
//            return new FebsResponse().data(map).success();
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error("api/detRecord/saveDeportInfo"+e.getMessage());
//            return new FebsResponse().fail();
//        }
//    }

}
