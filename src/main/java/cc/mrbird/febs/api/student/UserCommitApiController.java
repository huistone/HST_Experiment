package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/userCommit")
public class UserCommitApiController extends BaseController {
    @Autowired
    private IUserCommitService userCommitService;

    @Resource
    private IExperimentProjectService projectService;


    /**
     * 学生，填写实验报告
     *
     * @ param projectId
     * @ return
     */
    @GetMapping("/getCommitByUser")
    @ControllerEndpoint(operation = "查询实验报告",exceptionMessage = "实验报告模板查询失败")
    public FebsResponse getCommitByUser(@RequestParam("projectId") Integer projectId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        /**
         * 1、先查询当前登录用户有没有提交过
         * 未提交，返回老师给定的模板。
         * 已提交，返回用户之前提交的记录
         * TODO 若学生已提交报告，老师再修改模板，则学生再次修改报告模板不会更新，
         * 解决办法：老师更新模板时同步删除已提交的所有报告
         */
        Map<String, Object> map = new HashMap<>(16);
        UserCommit userCommit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda()
                .eq(UserCommit::getStuId, currentUser.getUserId())
                .eq(UserCommit::getProjectId, projectId));
        if (userCommit == null) {
            ExperimentProject experimentProject = projectService.getOne(
                    new QueryWrapper<ExperimentProject>().lambda()
                            .eq(ExperimentProject::getProjectId, projectId)
            .select(ExperimentProject::getProjectId,ExperimentProject::getReportSize));

            map.put("isCommit", 0);
            map.put("reportSize", experimentProject.getReportSize());
        } else {
            map.put("isCommit", userCommit.getIsCommit());
            map.put("reportSize", userCommit.getReportSize());
            map.put("score", userCommit.getScore());
        }
        return new FebsResponse().success().data(map);
    }

    /**
     * @ Author: 马超伟
     * @ Date: 2021/4/1 14:19
     * @ Params: [projectId, reportSize]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Description:提交或者更新实验报告
     */
    @PostMapping("/updateReport")
    public FebsResponse updateReport(@RequestParam("projectId") Long projectId,
                                     @RequestParam("reportSize") String reportSize,
                                     @RequestParam("teacherId")Long teacherId,
                                     @RequestParam("courseId")Long courseId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        UserCommit userCommit = new UserCommit();
        UserCommit bool = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda()
                .eq(UserCommit::getStuId, currentUser.getUserId())
                .eq(UserCommit::getProjectId, projectId));
        //数据库里面查提交记录 若没有记录则新增
        if (bool == null) {
            userCommit.setCreateTime(LocalDateTime.now());
            userCommit.setDeptId(currentUser.getDeptId());
            userCommit.setIsCommit(BaseController.USE_INFO);
            userCommit.setStuId(currentUser.getUserId());

            userCommit.setTeacherId(teacherId);
            userCommit.setProjectId(projectId);
            userCommit.setCourseId(courseId);
            userCommit.setReportSize(reportSize);
            userCommit.setUserType(BaseController.USE_INFO);
            userCommitService.save(userCommit);
        } else {
            //存在，就是更新
            bool.setUpdateTime(LocalDateTime.now());
            bool.setReportSize(reportSize);
            userCommitService.updateById(bool);
        }
        return new FebsResponse().success();
    }

    @RequestMapping("/upload")
    @ControllerEndpoint(operation = "上传文件成功", exceptionMessage = "上传文件失败")
    public FebsResponse upload(@RequestParam("file") MultipartFile file, TeacherProject teacherProject) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        UserCommit userCommit = new UserCommit();
        UserCommit bool = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getStuId, currentUser.getUserId()).eq(UserCommit::getProjectId, teacherProject.getProjectId()));
        //数据库里面查上传记录 若没有记录则新增
        if (bool == null) {
            try {
                userCommit.setCreateTime(LocalDateTime.now());
                userCommit.setDeptId(currentUser.getDeptId());
                userCommit.setIsCommit(BaseController.USE_INFO);
                userCommit.setStuId(currentUser.getUserId());

                userCommit.setTeacherId(teacherProject.getTeacherId());
                userCommit.setProjectId(teacherProject.getProjectId());
                userCommit.setCourseId(teacherProject.getCourseId());

                userCommit.setUserType(1);
                userCommit.setTime(1);
                userCommit.setReportUrl(getUrl(file));
                userCommitService.save(userCommit);
            } catch (Exception e) {
                e.printStackTrace();
                return new FebsResponse().fail().message("文件上传失败,请重新上传");
            }
        } else {
            if (bool.getTime() < 3) {
                try {
                    bool.setCreateTime(LocalDateTime.now());
                    bool.setTime(bool.getTime() + 1);
                    bool.setReportUrl(getUrl(file));
                    userCommitService.updateById(bool);
                } catch (Exception e) {
                    e.printStackTrace();
                    return new FebsResponse().fail().message("文件上传失败,请重新上传");
                }
            } else {
                return new FebsResponse().fail().message("每位学生只能上传三次实验报告");
            }
        }
        return new FebsResponse().success().message("实验报告上传成功");
    }


}
