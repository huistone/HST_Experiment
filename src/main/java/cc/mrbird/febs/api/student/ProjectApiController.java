package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.AddCache;
import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest2;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.utils.FileUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import cc.mrbird.febs.system.service.*;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/04/21/9:58
 * @ Description:
 */
@Slf4j
@RestController
@RequestMapping("/api/student")
public class ProjectApiController extends BaseController {

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IUserService userService;

    @Resource
    private IDeptService deptService;

    @Resource
    private RedisService redisService;

    @Resource
    private IExperimentRemarkService remarkService;

    /**
     * 判断学生能否进入实验接口
     * @param projectId
     * @return
     */
    @PostMapping("/accessProject")
    @ControllerEndpoint(exceptionMessage = "判断能否进入实验接口失败" ,operation = "判断能否进入实验接口")
    public FebsResponse accessProject(@RequestParam(value = "projectId",required = true) Long projectId){
        User currentUser = getCurrentUser();
        if (currentUser ==null){
            return new FebsResponse().no_login();
        }

        ExperimentRemark one = remarkService.getOne(new QueryWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType, 1)
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getType, 1)
                .eq(ExperimentRemark::getIsDel,1)
                .select(ExperimentRemark::getScore)
        );
        if (one == null){
            return new FebsResponse().fail().message("该用户暂未填写与习题");
        }
        if (one.getScore() <60.0){
            return new FebsResponse().fail().message("预习题成绩不合格");
        }

        return new FebsResponse().success();
    }

    /**
     * @ Description: 获取首页用户登录过之后的课程信息
     * @ Param: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/7/1
     */
    @PostMapping("/getAllCourse")
    @AddCache(key = "getAllCourse")
    @ControllerEndpoint(exceptionMessage = "获取登录课程信息失败" ,operation = "获取登录课程信息成功")
    public FebsResponse getAllProjects() {
        User user = getCurrentUser();
        //如果当前用户没有登录，或者是管理员登录 只返回前五条课程信息
        if (user == null || user.getDeptId() == null) {
            List<Course> courseList = courseService.list(new QueryWrapper<Course>().lambda()
                    .select(Course::getCourseName, Course::getCourseId, Course::getCreateTime, Course::getCoursePictureUrl,Course::getEnglishName)
                    .orderByAsc(Course::getCourseSort)
                    .last("limit 0,5"));
//
            return new FebsResponse().success().data(courseList);
        }
        List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>().lambda()
                .eq(CourseDept::getDeptId, user.getDeptId()));
        List<Course> courseList = new ArrayList<>();
        courseDeptList.forEach(i -> {
            Course course = courseService.getOne(new QueryWrapper<Course>().lambda()
                    .select(Course::getCourseName, Course::getCourseId, Course::getCreateTime, Course::getCoursePictureUrl, Course::getCourseSort,Course::getEnglishName)
                    .eq(Course::getCourseId, i.getCourseId()));
            courseList.add(course);
        });
        List<Course> newList = courseList.stream().sorted(Comparator.comparing(Course::getCourseSort)).collect(Collectors.toList());
        return new FebsResponse().data(newList).success().message("查询数据成功");
    }

    /**
     * @ Description: 获取首页用户登录过之后的课程信息
     * @ Param: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/7/1
     */
    @RequestMapping("/getAllCourseProject")
    @AddCache(key = "getAllCourseProject")
    @ControllerEndpoint(exceptionMessage = "获取登录课程信息失败" ,operation = "获取登录课程信息成功")
    public FebsResponse getAllCourseProject() {
        User user = getCurrentUser();
        //如果当前用户没有登录，只返回前五条课程信息
        try {
            if (user == null) {
                List<Course> courseList = courseService.list(new QueryWrapper<Course>().lambda()
                        .eq(Course::getState,1)
                        .eq(Course::getIsDel,1)
                        .select(Course::getCourseName, Course::getCourseId, Course::getCreateTime, Course::getCoursePictureUrl,Course::getCourseContent,Course::getEnglishName)
                        .orderByAsc(Course::getCourseSort)
                        .last("limit 0,5"));
                courseList.forEach(i->{
                    List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                            .lambda()
                            .eq(ExperimentProject::getCourseId, i.getCourseId())
                            .eq(ExperimentProject::getState,1)
                            .eq(ExperimentProject::getIsDel,1)
                            .select(ExperimentProject::getProjectName,ExperimentProject::getCourseId,ExperimentProject::getProjectPictureUrl,ExperimentProject::getProjectId)
                            .orderByAsc(ExperimentProject::getProjectId)
                            .last("limit 0,6"));
                    i.setChildren(projectList);
                });
                return new FebsResponse().success().data(courseList);
            }
            if (user.getDeptId() == null) {
                return new FebsResponse().fail().message("该学生还没有加入班级，无法进行实验！");
            }
            List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>().lambda()
                    .eq(CourseDept::getDeptId, user.getDeptId()));
            List<Course> courseList = new ArrayList<>();
            courseDeptList.forEach(i -> {
                Course course = courseService.getOne(new QueryWrapper<Course>().lambda()
                        .select(Course::getCourseName, Course::getCourseId, Course::getCreateTime, Course::getCoursePictureUrl, Course::getCourseSort,Course::getCourseContent,Course::getEnglishName)
                        .eq(Course::getCourseId, i.getCourseId())
                        .eq(Course::getState,1)
                        .eq(Course::getIsDel,1));
                if(course!=null){
                    List<ExperimentProject> children = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                            .lambda()
                            .eq(ExperimentProject::getCourseId, course.getCourseId())
                            .eq(ExperimentProject::getState, 1)
                            .eq(ExperimentProject::getIsDel, 1)
                            .select(ExperimentProject::getProjectName,ExperimentProject::getProjectPictureUrl,ExperimentProject::getCourseId,ExperimentProject::getProjectId)
                            .orderByAsc(ExperimentProject::getProjectSort)
                            .last("limit 0,6"));
                    course.setChildren(children);
                    courseList.add(course);
                }
            });
            List<Course> newList = courseList.stream().sorted(Comparator.comparing(Course::getCourseSort)).collect(Collectors.toList());
            return new FebsResponse().data(newList).success().message("查询数据成功");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/student/getAllCourseProject"+e.getMessage());
            return new FebsResponse().fail().message("查询失败");
        }
    }


    @GetMapping("getCourseDetailInfo")
    @ControllerEndpoint(operation = "查询课程详情",exceptionMessage = "查询课程详情失败")
    public FebsResponse getCourseDetailInfo(Integer courseId){
        try{
            if (IntegerUtils.isBlank(courseId)){
                return new FebsResponse().no_param();
            }
            Course course = courseService.getOne(new QueryWrapper<Course>()
                    .lambda()
                    .eq(Course::getCourseId, courseId)
                    .select(Course::getCourseId
                            , Course::getCourseName
                            , Course::getCourseContent, Course::getEnglishName
                            , Course::getCoursePictureUrl));
            return new FebsResponse().data(course).success();
        }catch (Exception e){
            log.error("api/student/getCourseDetailInfo:"+e.getMessage());
            return new FebsResponse().fail();
        }
    }



    @GetMapping("getCourseBar")
    @ControllerEndpoint(exceptionMessage = "获取课程导航栏失败" ,operation = "获取课程导航栏成功")
    public FebsResponse getCourseBar(){
        try {
            User currentUser = getCurrentUser();
            if (currentUser == null || currentUser.getDeptId()==null){
                List<Course> list = courseService.list(new QueryWrapper<Course>()
                        .lambda()
                        .eq(Course::getState,1)
                        .select(Course::getCourseId,Course::getCourseName,Course::getCoursePictureUrl)
                        .orderByAsc(Course::getCourseSort));
                return new FebsResponse().success().data(list);
            }else{
                List<CourseDept> list = courseDeptService.list(new QueryWrapper<CourseDept>().lambda()
                        .eq(CourseDept::getDeptId, currentUser.getDeptId())
                );
                List<Integer> collect = list.stream().map(CourseDept::getCourseId
                ).distinct().collect(Collectors.toList());
                List<Course> list1 = courseService.list(new QueryWrapper<Course>()
                        .lambda()
                        .in(collect.size() > 0, Course::getCourseId, collect)
                        .select(Course::getCourseId, Course::getCourseName, Course::getCoursePictureUrl)
                        .eq(Course::getState,1)
                        .orderByAsc(Course::getCourseSort));
                return new FebsResponse().success().data(list1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/student/getCourseBar:获取课程导航栏失败"+e.getMessage());
            return new FebsResponse().fail().message("查询失败");
        }
    }

    @GetMapping("getProjects")
    @ControllerEndpoint(exceptionMessage = "获取实验项目失败" ,operation = "获取实验项目成功")
    public FebsResponse getProjects(Long courseId, QueryRequest2 request){
        try {
            User currentUser = getCurrentUser();
            Page<ExperimentProject> page = new Page<ExperimentProject>(request.getPageNum(), request.getPageSize());
            SortUtil.handlePageSort(request, page, "project_sort", FebsConstant.ORDER_ASC, false);
            if (currentUser==null || currentUser.getDeptId() == null ){
                IPage<ExperimentProject> page1 = experimentProjectService.page(page, new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq(courseId != null, ExperimentProject::getCourseId, courseId)
                        .eq(ExperimentProject::getState, 1)
                        .eq(ExperimentProject::getIsDel, 1)
                        .select(ExperimentProject::getProjectSort, ExperimentProject::getProjectId, ExperimentProject::getProjectName, ExperimentProject::getProjectPictureUrl,ExperimentProject::getCourseId));
                return new FebsResponse().success().data(getDataTable(page1));
            } else {
                List<TeacherProject> list = teacherProjectService.list(new QueryWrapper<TeacherProject>()
                        .lambda()
                        .eq(TeacherProject::getStatus, 1)
                        .eq(TeacherProject::getIsDel, 1)
                        .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                        .eq(TeacherProject::getCourseId, courseId)
                        .select(TeacherProject::getProjectId));
                List<Long> projectIds = list.stream().map(TeacherProject::getProjectId
                ).collect(Collectors.toList());
                if(projectIds.size()>0){
                    IPage<ExperimentProject> page1 = experimentProjectService.page(page, new QueryWrapper<ExperimentProject>()
                            .lambda()
                            .in(ExperimentProject::getProjectId,projectIds)
                            .orderByAsc(ExperimentProject::getProjectSort)
                            .eq(ExperimentProject::getCourseId,courseId)
                            .select(ExperimentProject::getProjectSort, ExperimentProject::getProjectId, ExperimentProject::getProjectName, ExperimentProject::getProjectPictureUrl,ExperimentProject::getCourseId));
                    return new FebsResponse().success().data(getDataTable(page1));
                } else {
                    return new FebsResponse().code(403).message("该课程暂未开启项目");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("查询失败");
        }
    }

    @GetMapping("getSingleProject")
    @ControllerEndpoint(exceptionMessage = "获取实验项目详情失败" ,operation = "获取实验项目详情成功")
    public FebsResponse getSingleProject(Long projectId,Long courseId){
        if(projectId == null || courseId == null){
            return new FebsResponse().no_param();
        }

//        Object data = redisService.get("getSingleProject" + projectId + "" + courseId + "");
//        //缓存中有数据
//        if (data != null){
//            log.info("getSingleProject" + projectId + "" + courseId + "缓存命中");
//            return new FebsResponse().success().data(data);
//        }
//        log.info("getSingleProject" + projectId + "" + courseId + "缓存不命中,查询数据库");
        try {
            Map<String, Object> map = new HashMap<>();
            ExperimentProject project = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                    .lambda().eq(ExperimentProject::getProjectId, projectId)
                    .eq(ExperimentProject::getCourseId, courseId)
                    .select(ExperimentProject::getProjectId,
                            ExperimentProject::getProjectName,
                            ExperimentProject::getProjectContent,
                            ExperimentProject::getProjectPictureUrl,
                            ExperimentProject::getCourseId,
                            ExperimentProject::getProjectContentUrl));
            Course course = courseService.getOne(new QueryWrapper<Course>()
                    .lambda()
                    .eq(Course::getCourseId,project.getCourseId())
                    .select(Course::getCourseName));
            project.setCourseName(course.getCourseName());
            map.put("projectInfo",project);
            //缓存一天
//            redisService.set("getSingleProject"+projectId+""+courseId+"",map,86400L);
            return new FebsResponse().success().data(map);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("查询失败");
        }
    }
    
    @RequestMapping("getFiveProject")
    @ControllerEndpoint(operation = "获取随机实验项目列表",exceptionMessage = "获取随机实验项目列表失败")
    public FebsResponse getFiveProject(Integer courseId){
        HashMap<String, Object> map = new HashMap<>();
        if(courseId == null  ){
            return  new FebsResponse().no_param();
        }

        try {
            List<ExperimentProject> list = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                    .lambda()
                    .eq(ExperimentProject::getCourseId, courseId)
                    .select(ExperimentProject::getProjectPictureUrl
                            ,ExperimentProject::getProjectId
                            ,ExperimentProject::getProjectName
                            ,ExperimentProject::getCourseId
                            ,ExperimentProject::getProjectContentUrl)
                    .last("limit 0,5"));
            map.put("projectList",list);
            return new FebsResponse().data(map).success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    /**
     * @ Author wangke
     * @ Description 判断该实验是否已经发布接口
     * @ Date 10:35 2021/1/27
     * @ Param
     * @ return
     */
    @RequestMapping("projectIsPublish")
    @ControllerEndpoint(exceptionMessage = "判断实验是否发布异常",operation = "判断实验是否发布")
    public FebsResponse projectIsPublish(Long projectId,Long courseId){

        if (courseId == null || projectId == null){
            return  new FebsResponse().no_param();
        }

        User currentUser = getCurrentUser();
        if (currentUser != null && currentUser.getDeptId()!= null){
            int count = teacherProjectService.count(new QueryWrapper<TeacherProject>()
                   .lambda()
                    .eq(TeacherProject::getProjectId, projectId)
                    .eq(TeacherProject::getCourseId, courseId)
                    .eq(currentUser.getDeptId()!=null,TeacherProject::getDeptId,currentUser.getDeptId())
                    .eq(TeacherProject::getStatus, 1)
                    .eq(TeacherProject::getIsDel, 1));
            if (count == 0){
                return new FebsResponse().code(403).message("该实验暂未发布");
            }
        }else if (currentUser==null){
            return new FebsResponse().no_login();
        }

        return new FebsResponse().success();
    }

    /**
     * @ Author wangke
     * @ Description 获取实验项目里的班级任课老师等信息
     * @ Date 8:47 2020/9/22
     * @ Param
     * @ return
     */
    @RequestMapping("getSingleProjectInfo")
    @ControllerEndpoint(operation = "获取实验基本信息",exceptionMessage = "获取实验基本信息失败")
    public FebsResponse getSingleProjectInfo(Long projectId,Long courseId){
        try {
            Map<String, Object> map = new HashMap<>();
            if(courseId == null  || projectId == null){
                return  new FebsResponse().no_param();
            }
//            Object data = redisService.get("getSingleProjectInfo" + projectId + "" + courseId);
//            if (data!=null){
//                return new FebsResponse().data(map).success();
//            }
            User student = getCurrentUser();
            ExperimentProject project = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                    .lambda()
                    .eq(ExperimentProject::getProjectId, projectId)
                    .select(ExperimentProject::getProjectName
                            , ExperimentProject::getProjectPictureUrl));
            map.put("projectName",project.getProjectName());
            map.put("projectPictureUrl",project.getProjectPictureUrl());
            if(student!=null && student.getDeptId() != null ){
                Dept thirdDept = deptService.getById(student.getDeptId());
                Dept secondDept = deptService.getById(thirdDept.getParentId());
                Dept firstDept = deptService.getById(secondDept.getParentId());
                //任课老师
                UserDept one = userDeptService.getOne(new QueryWrapper<UserDept>()
                        .lambda()
                        .eq(UserDept::getDeptId, student.getDeptId())
                        .eq(UserDept::getCourseId, courseId)
                        .eq(UserDept::getType, 1)
                        .select(UserDept::getUserId));
                map.put("firstDept",firstDept.getDeptName());
                map.put("secondDept",secondDept.getDeptName());
                map.put("thirdDept",thirdDept.getDeptName());
                if(one==null){
                    map.put("teacherName","暂无老师");

                }else{
                    User teacher = userService.getById(one.getUserId());
                    map.put("teacherName",teacher.getTrueName());
                }
                redisService.set("getSingleProjectInfo" + projectId + "" + courseId,map,86400L);
                return new FebsResponse().data(map).success();
            } else {
                map.put("firstDept","暂无");
                map.put("secondDept","暂无");
                map.put("thirdDept","暂无");
                map.put("teacherName","暂无老师");
//                redisService.set("getSingleProjectInfo" + projectId + "" + courseId,map,86400L);
                return new FebsResponse().data(map).success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }



    /**
     * @ Author wangke
     * @ Description 预览pdf
     * @ Date 10:00 2020/9/22
     * @ Param
     * @ return
     */
    @RequestMapping("previewProjectPdf")
    public FebsResponse previewProjectPdf(HttpServletResponse response,Long projectId){
//        if(getCurrentUser()==null){
//            return new FebsResponse().no_login();
//        }
        //根据实验项目id查找
        ExperimentProject project = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, projectId)
                .select(ExperimentProject::getProjectName, ExperimentProject::getProjectContentUrl));
        if (StringUtils.isBlank(project.getProjectContentUrl())){
            return new FebsResponse().fail().data("暂无文档");
        }
        try{
            FileUtil.previewPdf(response,project.getProjectName(),project.getProjectContentUrl());
        }catch(Exception e){
            return new FebsResponse().fail().data("预览失败");
        }
        return new FebsResponse().success();
    }

   


    /**
     * @return
     * @ Description: 获取该同学对应的所有实验项目
     * @ Author: 王珂
     * @ Date: 2020/6/29
     */
    @GetMapping("getProjectList")
    @ControllerEndpoint(operation = "获取课程项目下拉菜单", exceptionMessage = "获取课程项目下拉菜单失败")
    public FebsResponse getProjectList(Long deptId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (deptId == null) {
            return new FebsResponse().no_param();
        }
        try {
            List<Object> list = new ArrayList<>();
            List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>()
                    .lambda()
                    .eq(IntegerUtils.isNotBlank(deptId.intValue()), UserDept::getDeptId, deptId)
                    .eq(UserDept::getUserId, currentUser.getUserId()));
            for (UserDept userDept : userDeptList) {
                ElementCascader cascader = new ElementCascader();
                cascader.setValue(userDept.getCourseId());
                cascader.setLabel(courseService.getById(userDept.getCourseId()).getCourseName());
                //查询二级菜单的，实验项目
                List<ExperimentProject> experimentProjectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>().lambda()
                        .eq(ExperimentProject::getCourseId, userDept.getCourseId())
                        .eq(ExperimentProject::getState, 1)
                        .orderByAsc(ExperimentProject::getProjectId));
                if (experimentProjectList != null && experimentProjectList.size() > 0) {
                    List<ElementCascader> elementCascaders = new ArrayList<>();
                    for (ExperimentProject experimentProject : experimentProjectList) {
                        ElementCascader cascader1 = new ElementCascader();
                        cascader1.setValue(experimentProject.getProjectId());
                        cascader1.setLabel(experimentProject.getProjectName());
                        elementCascaders.add(cascader1);
                    }
                    cascader.setChildren(elementCascaders);
                }
                list.add(cascader);
            }
            return new FebsResponse().success().data(list);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("/api/student/getProjectList:" + e.getMessage());
            return new FebsResponse().fail().message("查询数据失败");
        }


    }

    /**
     * @ Description: 获取实验课程下面的所有开启的实验项目
     * @ Param: [courseId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/6
     */
    @GetMapping("/myExperiments/{courseId}")
    @ControllerEndpoint(exceptionMessage = "获取课程对应开启的实验项目失败",operation = "获取课程对应开启的实验项目成功")
    public FebsResponse selectMyExperiments(@PathVariable Integer courseId) {
        //获取当前登录用户
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        //获取当前登录的老师用户信息
        UserDept userDept = userDeptService.getOne(new QueryWrapper<UserDept>().lambda()
                .eq(UserDept::getDeptId, currentUser.getDeptId())
                .eq(UserDept::getCourseId, courseId)
                .eq(UserDept::getType, 1), false);
        if (userDept == null) {
            return new FebsResponse().put_code(501, "该课程暂未分配教师！");
        }
        //查询当前登录用户老师开启的全部项目
        List<ExperimentProject> projects = new ArrayList<>();
        List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                .eq(TeacherProject::getCourseId, courseId)
                .eq(TeacherProject::getTeacherId, userDept.getUserId())
                .eq(TeacherProject::getIsDel, 1)
                .eq(TeacherProject::getStatus, 1)
                .orderByAsc(TeacherProject::getProjectId));
        //根据已经开启的项目的ID查找对应的项目信息
        teacherProjectList.forEach(i -> {
            ExperimentProject project = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                    .lambda()
                    .eq(ExperimentProject::getProjectId, i.getProjectId())
                    .select(ExperimentProject::getProjectName, ExperimentProject::getProjectId));
            projects.add(project);
        });
        Map<String,Object> map = new HashMap<>();
        //查询课程信息
        Course course = courseService.getById(courseId);
        if (projects.size() > 0) {
            map.put("projectList", projects);
            map.put("course", course);
            return new FebsResponse().success().message("查询数据成功").data(map);
        } else {
            return new FebsResponse().fail().message("失败");
        }
    }
}
