package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.FigureUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.ReportUtils;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.entity.vo.QuestionStatisticsVo;
import cc.mrbird.febs.system.service.*;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/29/18:04
 * @ Description: 学生提交习题记录接口
 */
@RestController
@RequestMapping("/api/studentAnswer")
public class StudentAnswerApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IExperimentAnswerService experimentAnswerService;

    @Resource
    private IExperimentQuestionService experimentQuestionService;

    @RequestMapping("getQuestionStatisticsList")
    @ControllerEndpoint(operation = "查询习题正确率接口",exceptionMessage = "查询习题正确率异常")
    public FebsResponse getQuestionStatisticsList(Integer deptId,Integer projectId,Integer type,Integer questionType,QueryRequest queryRequest){

        if (IntegerUtils.isBlank(projectId) || IntegerUtils.isBlank(type) || IntegerUtils.isBlank(questionType)){
            return new FebsResponse().no_param();
        }

        Page<QuestionStatisticsVo> questionPage = new Page<>(queryRequest.getPageNum(),queryRequest.getPageSize());
        IPage<QuestionStatisticsVo> page = experimentQuestionService.selectQuestionStatisticsList(questionPage,projectId,type,questionType);
        List<QuestionStatisticsVo> questionList = page.getRecords();
        ArrayList<QuestionStatisticsVo> questionStatisticsVoList = new ArrayList<>();
        for (QuestionStatisticsVo question : questionList) {
            double checkCount = experimentAnswerService.count(new QueryWrapper<ExperimentAnswer>()
                    .lambda()
                    .eq(ExperimentAnswer::getProjectId, projectId)
                    .eq(deptId != null, ExperimentAnswer::getDeptId, deptId)
                    .eq(ExperimentAnswer::getQuestionId, question.getQuestionId())
                    .eq(ExperimentAnswer::getUserType,1)
                    .gt(ExperimentAnswer::getScore,0.00)
                    .groupBy(ExperimentAnswer::getQuestionId, ExperimentAnswer::getNumber));
            double count = experimentAnswerService.count(new QueryWrapper<ExperimentAnswer>()
                    .lambda()
                    .eq(ExperimentAnswer::getProjectId, projectId)
                    .eq(ExperimentAnswer::getUserType,1)
                    .eq(deptId != null, ExperimentAnswer::getDeptId, deptId)
                    .eq(ExperimentAnswer::getQuestionId, question.getQuestionId())
                    .groupBy(ExperimentAnswer::getQuestionId, ExperimentAnswer::getNumber));
            question.setAnswerCount((long) count);
            question.setCorrectCount((long)checkCount);
            if (count>0){
                question.setCorrectRate(new BigDecimal(checkCount / count).setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue());
            }else{
                question.setCorrectRate(0.00);
            }
            questionStatisticsVoList.add(question);
        }
        List<QuestionStatisticsVo> collect = questionStatisticsVoList.stream().sorted(Comparator.comparing(QuestionStatisticsVo::getNumber)).collect(Collectors.toList());
        return new FebsResponse().success().data(collect);
    }


    /**
    * @ Description: 学生，预习题提交接口
    * @ Param: [request]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/29
    */
    @PostMapping(produces = "application/json;charset=UTF-8")
    @ControllerEndpoint(operation = "学生预习题提交成功",exceptionMessage = "学生预习题提交失败")
    public FebsResponse answerCommit(HttpServletRequest request){
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        JSONArray jsonParam = this.getJSONParam(request);
        JSONObject json = jsonParam.getJSONObject(0);
        ExperimentAnswer awe = json.toJavaObject(json, ExperimentAnswer.class);
        Long projectId = awe.getProjectId();
        List<ExperimentAnswer> answers = experimentAnswerService.list(new QueryWrapper<ExperimentAnswer>().lambda()
                .eq(ExperimentAnswer::getCreateId, currentUser.getUserId())
                .eq(ExperimentAnswer::getProjectId, projectId)
                .eq(ExperimentAnswer::getUserType,1)
                .eq(ExperimentAnswer::getQuestionType,1));
        if(answers.size()>0){
            return new FebsResponse().fail().message("您已提交，请勿重复提交");
        }
        return null;
    }


    @GetMapping("cloneFile")
    public FebsResponse cloneFile(HttpServletRequest request) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("f:/");
        FileOutputStream fileOutputStream = new FileOutputStream("F:/excel/4.xlsx");
        byte [] by = new byte[1024];
        int len;
        while ((len = fileInputStream.read(by)) > 0) {
            fileOutputStream.write(by,0,len);
        }
        return new FebsResponse();
    }

}
