package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.ReportUtils;
import cc.mrbird.febs.others.entity.StudentExport;
import cc.mrbird.febs.others.entity.UserImport;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/api/studentInformation")
@ResponseBody
@Slf4j
public class StudentInfoApiController extends BaseController {

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IUserService userService;

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IExperimentRemarkService experimentRemarkService;





    /**
     * @ Description: 查询学生信息分页查询
     * @ Param: [requst，deptId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 王珂
     * @ Date: 2020/6/29
     */
    @GetMapping("studentInfo")
    @ControllerEndpoint(operation = "查询学生信息", exceptionMessage = "查询学生信息失败")
    public FebsResponse selectStudentInfo(QueryRequest request, Long deptId){
        User teacher = getCurrentUser();
        if(teacher==null){
            return new FebsResponse().no_login();
        }
        IPage iPage = null;
        try {
            iPage = userService.selectStudentInfo(request, deptId,teacher);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("/api/studentInformation/studentInfo,查询学生信息失败：");
            return new FebsResponse().fail().message("查询学生信息失败");
        }
        return new FebsResponse().success().data(getDataTable(iPage));
    }

    /**
     * @author: 王珂
     * @ Description: 学生成绩信息条件分页查询
     * @Date: 19:12 2020/7/14
     * @ Params:  * @ Param null
     */
    @GetMapping("studentScoreInfo")
    @ControllerEndpoint(operation = "查询学生成绩信息",exceptionMessage = "查询学生成绩信息失败")
    public FebsResponse selectStudentScoreInfo(QueryRequest request, Long deptId,Long projectId){
        User teacher = getCurrentUser();
        if(teacher==null){
            return new FebsResponse().no_login();
        }
        if(deptId == null || projectId == null){
            return new FebsResponse().no_param();
        }
        Map<String,Object> map = new HashMap<>();
        try {
            //创建分页对象
            List<User> users = userService.selectStudentScoreInfoByPage(deptId, projectId,request.getPageNum(), request.getPageSize());
            List<User> list = new ArrayList<>();
            for (User user : users) {
                double scoreSum = 0.0;
                if(user.getPreCommit()==null){
                    user.setPreCommit(0);
                }
                if(user.getThCommit()==null){
                    user.setThCommit(0);
                }
                if(user.getProCommit()==null){
                    user.setProCommit(0);
                }
                if(user.getPreCommit()==2){
                    scoreSum+=user.getPreScore()*0.1;
                }
                if(user.getThCommit()==2){
                    scoreSum+=user.getThScore()*0.1;
                }
                if(user.getProCommit()==2){
                    scoreSum+=user.getProScore()*0.6;
                }
                user.setScoreSum(new BigDecimal(scoreSum).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                list.add(user);
            }
            Integer count = userService.selectStudentScoreCount(deptId, projectId);
            map.put("rows",list);
            map.put("total",count);
            return new FebsResponse().success().data(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new FebsResponse().fail().message("查询失败");

    }

    /**
     * @author: 王珂
     * @ Description: 学生信息下的个人成绩信息
     * @Date: 17:59 2020/7/14
     * @ Params:  * @ Param null
     */
    @GetMapping("studentSingleScore")
    @ControllerEndpoint(operation = "查询学生个人成绩信息", exceptionMessage = "查询学生个人成绩信息失败")
    public FebsResponse selectStudentSingleScore(Long studentId,Long courseId){
        User teacher = getCurrentUser();
        if(teacher==null){
            return new FebsResponse().no_login();
        }
        if(studentId==null){
            return new FebsResponse().no_param();
        }
        try {
            Map<String, Object> map = new HashMap<>();
            User student = userService.getOne(new QueryWrapper<User>()
                    .lambda()
                    .eq(User::getIsDel, 1)
                    .eq(User::getStatus, 1)
                    .eq(User::getUserId,studentId)
                    .select(User::getTrueName,User::getIdNumber,User::getDeptId));
            //查班级所含课程的所有
            List<UserDept> userDepts = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                    .eq(UserDept::getDeptId, student.getDeptId())
                    .eq(UserDept::getCourseId, courseId));

            for (UserDept userDept : userDepts) {
                if(userDept.getType()==1){
                    student.setTeacherName(userService.getById(userDept.getUserId()).getTrueName());
                }
            }
            Course course = courseService.getById(courseId);
            student.setCourseName(course.getCourseName());
            student.setCourseId(courseId);
            //班级
            Dept thirdDept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptId, student.getDeptId()));
            //年级
            Dept secondDept = deptService.getById(thirdDept.getParentId());
            //院系
            Dept firstDept = deptService.getById(secondDept.getParentId());
            student.setThirdDeptName(thirdDept.getDeptName());
            student.setFirstDeptName(firstDept.getDeptName());
            List<Object> list = new ArrayList<>();
            Double prepareColumn = 0.00;
            Double thinkColumn = 0.00;
            Double projectColumn = 0.00;
            Double scoreSum = 0.00;
            //根据课程id查询所有实验项目
            List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                    .lambda()
                    .eq(ExperimentProject::getCourseId,courseId)
                    .eq(ExperimentProject::getIsDel, 1)
                    .select(ExperimentProject::getProjectId,ExperimentProject::getProjectName));
           Integer size =  projectList.size();
            for (ExperimentProject experimentProject : projectList) {
                UserScore user = new UserScore();
                //查询实验报告
                UserCommit userCommit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda()
                        .eq(UserCommit::getDeptId,student.getDeptId())
                        .eq(UserCommit::getCourseId, courseId)
                        .eq(UserCommit::getProjectId,experimentProject.getProjectId())
                        .eq(UserCommit::getStuId,studentId)
                        .eq(UserCommit::getUserType,1)
                        .select(UserCommit::getScore,UserCommit::getIsCommit,UserCommit::getCommitId));
                if(userCommit==null){
                    userCommit = new UserCommit();
                    userCommit.setIsCommit(0);
                }
                Double projectScore = 0.00;
                if(userCommit.getScore() != null && userCommit.getIsCommit() == 2){
                    projectScore = userCommit.getScore();
                }
                //查询预习题成绩
                ExperimentRemark prepare = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                        .lambda()
                        .eq(ExperimentRemark::getCreateId, studentId)
                        .eq(ExperimentRemark::getType, 1)
                        .eq(ExperimentRemark::getUserType,1)
                        .eq(ExperimentRemark::getIsDel,1)
                        .eq(ExperimentRemark::getProjectId,experimentProject.getProjectId())
                        .select(ExperimentRemark::getScore,ExperimentRemark::getIsCommit,ExperimentRemark::getRemarkId));

                if(prepare==null){
                    prepare = new ExperimentRemark();
                    prepare.setIsCommit(0);
                }
                Double prepareScore = 0.00;
                if(prepare.getScore() != null && prepare.getIsCommit()==2){
                    prepareScore = prepare.getScore();
                }
                //查询思考题成绩
                ExperimentRemark think =  experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>()
                        .lambda()
                        .eq(ExperimentRemark::getCreateId, studentId)
                        .eq(ExperimentRemark::getType, 2)
                        .eq(ExperimentRemark::getUserType,1)
                        .eq(ExperimentRemark::getIsDel,1)
                        .eq(ExperimentRemark::getProjectId,experimentProject.getProjectId())
                        .select(ExperimentRemark::getScore,ExperimentRemark::getIsCommit,ExperimentRemark::getRemarkId));
                if(think==null){
                    think = new ExperimentRemark();
                    think.setIsCommit(0);
                }
                Double thinkScore = 0.00;
                if(think.getScore() != null && think.getIsCommit() == 2){
                    thinkScore = think.getScore();
                }
                Double rowScore = thinkScore*0.1+prepareScore*0.1+projectScore*0.6;
                user.setProjectScore(userCommit);
                user.setThinkScore(think);
                user.setPrepareScore(prepare);
                user.setProjectName(experimentProject.getProjectName());
                //每个实验的总成绩
                rowScore = new BigDecimal(rowScore).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                user.setScoreRow(rowScore);
                list.add(user);
                //总成绩
                scoreSum += rowScore;
                prepareColumn += prepareScore;
                thinkColumn += thinkScore;
                projectColumn += projectScore;
            }
            //预习题总分
            prepareColumn = new BigDecimal(prepareColumn/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("prepareScore",prepareColumn);
            //思考题总分
            thinkColumn = new BigDecimal(thinkColumn/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("thinkScore",thinkColumn);
            //实验项目总分
            projectColumn = new BigDecimal(projectColumn/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("projectScore",projectColumn);
            scoreSum = new BigDecimal(scoreSum/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("scoreSum",scoreSum);
            map.put("student",student);
            map.put("list",list);
            return new FebsResponse().success().data(map);
        } catch (Exception e) {
            log.error("/api/studentInformation/studentSingleScore"+e.getMessage());
            e.printStackTrace();
            return new FebsResponse().fail().message("查询失败");
        }

    }


    @GetMapping("exportExcelWithStudentInfo")
    @ControllerEndpoint(operation = "导出学生信息",exceptionMessage = "导出学生信息失败")
    public FebsResponse exportExcelWithStudentInfo(HttpServletResponse response,Long deptId){
        try {
            List<User> list = userService.list(new QueryWrapper<User>()
                    .lambda().isNotNull(User::getDeptId).eq(User::getIsDel,1).eq(User::getDeptId,deptId));
            list.stream().forEach(s->{
                s.setDeptName(deptService.getById(s.getDeptId()).getDeptName());
            });

            ExcelKit.$Export(User.class, response).downXlsx(list, false);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success().message("导出学生信息成功");
    }


    /**
     * @author: 王珂
     * @ Description: 导出班级学生成绩汇总
     * @Date: 11:15 2020/7/16
     * @ Params:  * @ Param null
     */
    @GetMapping("exportExcelWithDeptScore")
    @ControllerEndpoint(operation = "导出班级学生成绩成功",exceptionMessage = "导出班级学生成绩失败")
    public FebsResponse exportExcelWithDeptScore(Long deptId,Long courseId,HttpServletResponse response,HttpServletRequest request){
        User teacher = getCurrentUser();
        if(teacher==null){
            return new FebsResponse().no_login();
        }
        if(deptId == null){
            return new FebsResponse().no_param();
        }
        File tempFile = null;
        try{
            URL url = new URL("http://file.huistone.com/picture/2021/01/13/ccf4d7a544b74c1b959424631c081d2f/courseScoreInfo.xlsx");
            InputStream inputStream = new BufferedInputStream(url.openStream());
            // 模板临时目录
            String rootPath = request.getServletContext().getRealPath("template_temp2/");
            //临时文件名
            String filePath = rootPath +  "_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "thinkScore";
            tempFile = new File(filePath);
            //保存临时文件
            ReportUtils.saveTempFile(inputStream,tempFile);
            TemplateExportParams params = new TemplateExportParams(filePath);
            params.setSheetName(new String[]{"班级成绩信息"});
            Dept thirdDept = deptService.getById(deptId);
            Dept secondDept = deptService.getById(thirdDept.getParentId());
            Dept firstDept = deptService.getById(secondDept.getParentId());
            Map<String, Object> map = new HashMap<>();
            map.put("firstDept",firstDept.getDeptName());
            //查出当前课程
            //TODO 班级成绩信息课程id暂时自己定义
//            UserDept userDept = userDeptService.getOne(new QueryWrapper<UserDept>()
//                    .lambda()
//                    .eq(UserDept::getDeptId, deptId)
//                    .eq(UserDept::getUserId, teacher.getUserId())
//                    .eq(UserDept::getIsDel, 1)
//                    .last("limit 1"));
            Course course = courseService.getById(courseId);
            List<UserDept> list1 = userDeptService.list(new QueryWrapper<UserDept>()
                    .lambda()
                    .eq(UserDept::getDeptId, deptId)
                    .eq(UserDept::getCourseId, courseId)
                    .eq(UserDept::getIsDel,1));
            map.put("courseName",course.getCourseName());
            for (UserDept userDept : list1) {
                if(userDept.getType()==1){
                    User byId = userService.getById(userDept.getUserId());
                    map.put("teacherName",byId.getTrueName());
                }
            }
            //上课班级
            map.put("deptName",thirdDept.getDeptName());
            //上课人数
            int studentCount = userService.count(new QueryWrapper<User>().lambda().eq(User::getDeptId, deptId).eq(User::getIsDel, 1).eq(User::getStatus, 1));
            map.put("studentCount",studentCount);

            List<Map<String, Object>> maps = new ArrayList<>();

            //查出该班级下面的所有学生
            List<User> userList = userService.list(new QueryWrapper<User>().lambda().eq(User::getDeptId, deptId).eq(User::getIsDel, 1).eq(User::getStatus, 1).select(User::getIdNumber,User::getTrueName,User::getSex,User::getUserId));
            //先查出该课程下所有的项目
            List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getCourseId, courseId).eq(ExperimentProject::getState, 1)
            .select(ExperimentProject::getProjectId));
            List<Long> list = new ArrayList<>();
            for (ExperimentProject experimentProject : projectList) {
                list.add(experimentProject.getProjectId());
            }
            int size = list.size();
            Double scoreSum = 0.00;
            Double passCount = 0.00;
            Double maxScore = 0.00;
            Double minScore = 100.00;
            List<Double> scoreList = new ArrayList<>();
            //根据项目id,学生id 查出所有实验报告成绩/size
            int count=1;
            for (User user : userList) {
                Double preScore = 0.00;
                Double thkScore = 0.00;
                Double proScore = 0.00;
                Map<String, Object> map1 = new HashMap<>();
                //每一行的预习题思考题实验报告成绩
                Double proScore2 = userCommitService.selectProjectScore(deptId, user.getUserId(), courseId, list);
                if(proScore2!=null){
                    proScore = proScore2;
                }
                List<Double> doubles = experimentRemarkService.selectRemarkScore(user.getUserId(), deptId, list);
                if (doubles!=null&&doubles.size()==1){
                    preScore = doubles.get(0);
                }else if(doubles!=null&&doubles.size()==2){
                    preScore = doubles.get(0);
                    thkScore = doubles.get(1);
                }
                preScore = preScore /size;
                thkScore = thkScore /size;
                proScore = proScore /size;
                Double rowScore = (preScore*0.1) + (thkScore*0.1) +(proScore*0.6);
                BigDecimal bigDecimal = new BigDecimal(rowScore).setScale(2, BigDecimal.ROUND_HALF_UP);
                map1.put("idNumber",user.getIdNumber());
                map1.put("trueName",user.getTrueName());
                if("0".equals(user.getSex())){
                    map1.put("sex","男");
                }else if("1".equals(user.getSex())){
                    map1.put("sex","女");
                }else{
                    map1.put("sex","保密");
                }
                BigDecimal preScore1 = new BigDecimal(preScore).setScale(2, BigDecimal.ROUND_HALF_UP);
                map1.put("prepareScore",preScore1);
                BigDecimal thkScore1 = new BigDecimal(thkScore).setScale(2, BigDecimal.ROUND_HALF_UP);
                map1.put("thinkScore",thkScore1);
                BigDecimal proScore1 = new BigDecimal(proScore).setScale(2, BigDecimal.ROUND_HALF_UP);
                map1.put("projectScore",proScore1);
                map1.put("rowScore",bigDecimal);
                map1.put("number",count++);
                if(rowScore >= 60){
                    passCount++;
                }
                if(rowScore >= maxScore){
                    maxScore = rowScore;
                }

                if(rowScore <= minScore){
                    minScore = rowScore;
                }
                scoreList.add(rowScore);
                scoreSum += rowScore;
                maps.add(map1);
            }
            //平均值
            Double averageScore = scoreSum / studentCount;
            BigDecimal bigDecimal = new BigDecimal(averageScore).setScale(2, BigDecimal.ROUND_HALF_UP);
            //方差
            Double variance = 0.00;
            for (Double aDouble : scoreList) {
                variance += (aDouble-averageScore)*(aDouble-averageScore);
            }
            //平均方差
            Double aveVariance = 0.00;
            aveVariance = variance/studentCount;
            //标准差
            Double standard = Math.sqrt(aveVariance);
            BigDecimal standard1 = new BigDecimal(standard).setScale(2, BigDecimal.ROUND_HALF_UP);
            Double passRate = passCount / studentCount;
            BigDecimal passRate1 =  new BigDecimal(passRate).setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put("averageScore",bigDecimal);
            map.put("passRate",passRate1);
            BigDecimal maxScore1 = new BigDecimal(maxScore).setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put("maxScore",maxScore1);
            BigDecimal minScore1 = new BigDecimal(minScore).setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put("minScore",minScore1);
            map.put("standard",standard1);
            map.put("maplist",maps);
            String dateFormat = DateUtil.getDateFormat( new Date(), DateUtil.FULL_TIME_PATTERN_);
            map.put("localDate",dateFormat);
            Workbook workBook = ExcelExportUtil.exportExcel(params,map);
            String fileName = thirdDept.getDeptName()+"班级成绩详情.xlsx";
            //浏览器下载方法
            ReportUtils.export(response,workBook,fileName,request);
            return new FebsResponse().success().message("导出成功");
        }catch (Exception e){
            return new FebsResponse().fail().message("导出失败");
        }finally{
            // 删除临时文件
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }

    }

    /**
     * @author: 王珂
     * @ Description: 导出学生个人成绩信息
     * @Date: 17:10 2020/7/14
     * @ Params:  * @ Param null
     */
    @GetMapping("exportExcelWithStudentScore")
    @ControllerEndpoint(operation = "导出个人信息成绩excel",exceptionMessage = "导出个人信息成绩excel失败")
    public FebsResponse exportStudentScoreExcel(Long studentId,Long courseId,HttpServletResponse response, HttpServletRequest request){
        User teacher1  = getCurrentUser();
        if(teacher1 == null){
            return new FebsResponse().no_login();
        }
        if(studentId==null || courseId==null){
            return new FebsResponse().no_param();
        }
        File tempFile = null;
        try{
            URL url = new URL("http://file.huistone.com/view/2020/07/18/e4b378ba8b014db2bc5f4dd37cedec81/个人实验信息成绩统计.xlsx");
            InputStream inputStream = new BufferedInputStream(url.openStream());
            // 模板临时目录
            String rootPath = request.getServletContext().getRealPath("template_temp1/");
            //临时文件名
            String filePath = rootPath +  "_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "thinkScore";
            tempFile = new File(filePath);
            //保存临时文件
            ReportUtils.saveTempFile(inputStream,tempFile);
            TemplateExportParams params = new TemplateExportParams(filePath);
            params.setSheetName(new String[]{"个人成绩信息"});
            //采集数据
            Map<String, Object> map = new HashMap<>();
            User student = userService.getOne(new QueryWrapper<User>()
                    .lambda()
                    .eq(User::getIsDel, 1)
                    .eq(User::getStatus, 1)
                    .eq(User::getUserId,studentId));
            Dept byId = deptService.getById(student.getDeptId());

            Course byId1 = courseService.getById(courseId);
            map.put("trueName",student.getTrueName());
            map.put("idNumber",student.getIdNumber());
            map.put("deptName",byId.getDeptName());
            map.put("courseName",byId1.getCourseName());
            Dept thirdDept = deptService.getById(student.getDeptId());
            Dept secondDept = deptService.getById(thirdDept.getParentId());
            deptService.getById(secondDept.getParentId());
            map.put("firstName",deptService.getById(secondDept.getParentId()).getDeptName());
            UserDept one = userDeptService.getOne(new QueryWrapper<UserDept>()
                    .lambda()
                    .eq(UserDept::getCourseId, courseId)
                    .eq(UserDept::getType, 1)
                    .eq(UserDept::getDeptId, student.getDeptId())
                    .eq(UserDept::getIsDel, 1));
            User teacher = userService.getById(one.getUserId());
            map.put("teacherName",teacher.getTrueName());

            List<Map<String, Object>> maps = new ArrayList<>();
            Double prepareColumn = 0.00;
            Double thinkColumn = 0.00;
            Double projectColumn = 0.00;
            Double scoreSum = 0.00;
            //查询课程下的实验的思考题预习题实验报告成绩
            List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                    .lambda()
                    .eq(ExperimentProject::getCourseId, courseId)
                    .eq(ExperimentProject::getIsDel, 1));
            int size = projectList.size();
            int count = 1;
            for (ExperimentProject experimentProject : projectList) {
                Map<String, Object> map1 = new HashMap<>();
                map1.put("count",count++);
                map1.put("projectName",experimentProject.getProjectName());
                ExperimentRemark prepare = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>().lambda()
                    .eq(ExperimentRemark::getIsCommit,2)
                    .eq(ExperimentRemark::getCreateId,studentId)
                    .eq(ExperimentRemark::getUserType,1)
                    .eq(ExperimentRemark::getIsDel,1)
                    .eq(ExperimentRemark::getType,1)
                    .eq(ExperimentRemark::getDeptId,student.getDeptId())
                    .eq(ExperimentRemark::getProjectId,experimentProject.getProjectId())
                    .select(ExperimentRemark::getIsCommit,ExperimentRemark::getScore,ExperimentRemark::getRemarkId));
                Double prepareScore = 0.00;
                if(prepare != null && prepare.getScore()!=null){
                    prepareScore = prepare.getScore();
                    DecimalFormat df = new DecimalFormat("#.00");
                    map1.put("prepareScore", df.format(prepareScore));
                }
                else{
                    map1.put("prepareScore","0.00");
                }
                ExperimentRemark think = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>().lambda()
                        .eq(ExperimentRemark::getIsCommit,2)
                        .eq(ExperimentRemark::getCreateId,studentId)
                        .eq(ExperimentRemark::getType,2)
                        .eq(ExperimentRemark::getUserType,1)
                        .eq(ExperimentRemark::getIsDel,1)
                        .eq(ExperimentRemark::getDeptId,student.getDeptId())
                        .eq(ExperimentRemark::getProjectId,experimentProject.getProjectId())
                        .select(ExperimentRemark::getIsCommit,ExperimentRemark::getScore,ExperimentRemark::getRemarkId));
                Double thinkScore = 0.00;
                if(think != null && think.getScore()!=null){
                    thinkScore = think.getScore();
                    map1.put("thinkScore",decimalFormat(thinkScore));
                } else {
                    map1.put("thinkScore","0.00");
                }
                UserCommit userCommit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda()
                        .eq(UserCommit::getDeptId,student.getDeptId())
                        .eq(UserCommit::getCourseId,one.getCourseId())
                        .eq(UserCommit::getProjectId,experimentProject.getProjectId())
                        .eq(UserCommit::getStuId,studentId)
                        .eq(UserCommit::getUserType,1)
                        .eq(UserCommit::getIsCommit,2)
                        .select(UserCommit::getScore,UserCommit::getIsCommit,UserCommit::getCommitId));
                Double projectScore = 0.00;
                if (userCommit != null && userCommit.getScore()!=null){
                    projectScore = userCommit.getScore();
                    map1.put("projectScore",decimalFormat(projectScore));
                }else{
                    map1.put("projectScore","0.00");
                }

                Double rowScore = thinkScore*0.1+prepareScore*0.1+projectScore*0.6;
                BigDecimal rowScore1 = new BigDecimal(rowScore).setScale(2, BigDecimal.ROUND_HALF_UP);
                map1.put("rowScore",rowScore1);
                maps.add(map1);
                //总成绩
                scoreSum += rowScore;
                prepareColumn += prepareScore;
                thinkColumn += thinkScore;
                projectColumn += projectScore;
            }
            //每个实验的成绩
            map.put("maplist",maps);
            //预习题总成绩
            prepareColumn = new BigDecimal(prepareColumn/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("prepareScoreSum",prepareColumn);
            //实验报告成绩
            projectColumn = new BigDecimal(projectColumn/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("projectScoreSum",projectColumn);
            //思考题成绩
            thinkColumn = new BigDecimal(thinkColumn/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            map.put("thinkScoreSum",thinkColumn);
            //总分
            scoreSum = prepareColumn*0.1 + projectColumn*0.6+thinkColumn*0.1;
            BigDecimal scoreSum1 = new BigDecimal(scoreSum).setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put("ScoreSum",scoreSum1);
            Workbook workBook = ExcelExportUtil.exportExcel(params,map);
            String fileName = student.getTrueName()+"个人成绩详情.xlsx";
            //浏览器下载方法
            ReportUtils.export(response,workBook,fileName,request);
        }catch (Exception e){
            log.error("/api/studentInformation/exportExcelWithStudentScore,导出个人成绩信息失败："+e.getMessage());
            return new FebsResponse().fail().message("导出个人成绩信息成功");
        }finally{
            // 删除临时文件
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }
        return new FebsResponse().success().message("导出个人成绩信息成功");
    }

    public Double bigDecimal(Double param,Double size){
        return  new BigDecimal(param/size).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public String decimalFormat(Double param){
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(param);
    }

    /**
     * @ Description: 查询单个学生信息
     * @ Param: [requst，deptId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 王珂
     * @ Date: 2020/6/29
     */
    @GetMapping("singleStudentInfo")
    @ControllerEndpoint(operation = "查询单个学生信息", exceptionMessage = "查询单个学生信息失败")
    public FebsResponse selectSingleStudentInfo(Long userId){
        try {
            if(getCurrentUser() == null){
                return new FebsResponse().no_login();
            }
            User user = userService.selectSingleStudentInfo(userId);
            return new FebsResponse().success().data(user);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("/api/studentInformation/singleStudentInfo,查询单个学生信息失败："+e.getMessage());
            return new FebsResponse().fail().message("查询单个学生信息失败");
        }
    }

    /**
     * @ Description: 查询学生预习题,思考题,实验报告成绩信息,
     * @ Param: [requst，deptId,projectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 王珂
     * @ Date: 2020/6/29
     */
    @RequestMapping("/studentScore")
    @ControllerEndpoint(operation = "查询学生成绩", exceptionMessage = "查询学生成绩失败")
    public FebsResponse selectStudentScore( Long userId, Long deptId, Long projectId,Long courseId ){
        if(userId==null||deptId==null||projectId==null||courseId==null){
            return new FebsResponse().fail().message("参数不足无法获取数据");
        }
        if (getCurrentUser() == null){
            return  new FebsResponse().no_login();
        }
        try {
            List<Object> studentCommit = userCommitService.getStudentCommit(userId, deptId, projectId, courseId);
            return new FebsResponse().success().data(studentCommit);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("/api/studentInformation/studentScore,查询单个学生信息失败："+e.getMessage());
            return new FebsResponse().fail().message("查询学生成绩信息失败");
        }
    }


    @GetMapping("/studentList")
    @RequiresPermissions("deptStudent:view")
    @ControllerEndpoint(operation = "查询学生信息成功",exceptionMessage = "查询学生信息失败")
    public FebsResponse selectUserStudent(QueryRequest request){
        //查询，用户表的学生角色用户，状态用有效，且学生所在班级为传入的班级ID
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda().eq(TeacherProject::getCourseId, currentUser.getCourseId()).eq(TeacherProject::getTeacherId,currentUser.getUserId()));
        if(teacherProjectList.size() !=0 &&teacherProjectList!=null){
            List<Long> deptsList = new ArrayList<>();
            teacherProjectList.forEach(
                    i ->
                            deptsList.add(i.getDeptId())
            );
            IPage<User> page = new Page(request.getPageNum(),request.getPageSize());
            IPage<User> users = userService.page(page,new QueryWrapper<User>().in("DEPT_ID",deptsList));
            Map<String, Object> dataTable = getDataTable(users);
            return new FebsResponse().success().data(dataTable);
        }
        return new FebsResponse().success().message("数据为空");
    }


}
