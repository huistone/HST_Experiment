package cc.mrbird.febs.api.student;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.entity.UserRole;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserRoleService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/studentInformations")
@ResponseBody
@Slf4j
public class StudentInformation extends BaseController {

    @Autowired
    private ITeacherProjectService teacherProjectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserDeptService userDeptService;

    @Autowired
    private IUserRoleService userRoleService;

    @PostMapping("/studentInfo")
    @ControllerEndpoint(operation = "查询学生信息", exceptionMessage = "查询学生信息失败")
    public FebsResponse selectStudentInfo(QueryRequest request,String deptId){
        //通过班级Id查出用户ID并且获取所有角色是学生的信息
        List<UserDept> list = userDeptService.list(new QueryWrapper<UserDept>()
                .lambda()
                .eq(UserDept::getDeptId, deptId)
                .select(UserDept::getUserId));
        List<Long> userIds = list.stream().map(UserDept::getUserId).collect(Collectors.toList());
        List<User> userList = new ArrayList<>();
        //获取所有学生ID
        List<Object> studentIds = new ArrayList<>();
        for (Long userId : userIds) {
            UserRole one = userRoleService.getOne(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId, userId));
            if(one.getRoleId()==82){
                studentIds.add(userId);
            }
        }
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<User> page1 = this.userService.page(page, new QueryWrapper<User>()
                .lambda()
                .in(User::getUserId, studentIds)
                .eq(User::getIsDel, 1)
                .eq(User::getStatus, 1));

        return new FebsResponse().success().data(getDataTable(page1));
    }
    


    @GetMapping("/studentList")
    @RequiresPermissions("deptStudent:view")
    public FebsResponse selectUserStudent(QueryRequest request){
        //查询，用户表的学生角色用户，状态用有效，且学生所在班级为传入的班级ID
        User currentUser = getCurrentUser();
        List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda().eq(TeacherProject::getCourseId, currentUser.getCourseId()).eq(TeacherProject::getTeacherId,currentUser.getUserId()));
        if(teacherProjectList.size() !=0 &&teacherProjectList!=null){
            List<Long> deptsList = new ArrayList<>();
            teacherProjectList.forEach(
                    i ->
                            deptsList.add(i.getDeptId())
            );
            Page page = new Page(request.getPageNum(),request.getPageSize());
            IPage<User> users = userService.page(page,new QueryWrapper<User>().in("DEPT_ID",deptsList));
            Map<String, Object> dataTable = getDataTable(users);
            return new FebsResponse().success().data(dataTable);
        }
        return new FebsResponse().success().message("数据为空");
    }


}
