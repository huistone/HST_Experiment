package cc.mrbird.febs.api.teacher;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.common.utils.imageByFontsUtil;
import cc.mrbird.febs.others.entity.UserImport;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserRole;
import cc.mrbird.febs.system.mapper.UserMapper;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserRoleService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ ClassName TeacherImportApiController
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/3/20 9:21
 * @ Version 1.0
 */
@RestController
@RequestMapping("/api/teacher")
public class TeacherImportApiController extends BaseController {

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserService userService;

    @Resource
    private IUserRoleService userRoleService;

    @Resource
    private UserMapper userMapper;

    /**
     * 去空格
     * @ Param str
     * @return
     */
    private String StringTrim(String str){
        return str.replaceAll("[\\s\\u00A0]+","").trim();
    }

    /**
     * @ Author wangke
     * @ Description 教师端导入学生信息接口 
     * @ Date 10:06 2021/3/20
     * @ Param 
     * @ return 
     */
    @RequestMapping("importStudentInfoWithExcel")
    public FebsResponse ImportStudentInfo(MultipartFile file) throws IOException {

        User currentUser = getCurrentUser();
        if(currentUser == null){
            return new FebsResponse().no_login();
        }
        if (file.isEmpty()) {
            throw new FebsException("导入数据为空");
        }
        String filename = file.getOriginalFilename();
        if (!StringUtils.endsWith(filename, ".xlsx")) {
            throw new FebsException("只支持.xlsx类型文件导入");
        }
        // 开始导入操作
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<User> data = new ArrayList<>();
        final List<Map<String, Object>> error = Lists.newArrayList();
        ExcelKit.$Import(UserImport.class).readXlsx(file.getInputStream(), new ExcelReadHandler<UserImport>() {
            @Override
            public void onSuccess(int sheet, int row, UserImport userImport) {
                // 数据校验成功时，加入集合
                User user = new User();
                user.setCreateTime(new Date());
                String idnumber = userImport.getIdNumber();
                StringBuilder iNumber = new StringBuilder();
                for(int i = 0;i<idnumber.length();i++){
                    if(idnumber.charAt(i)>=48&&idnumber.charAt(i)<=57){
                        iNumber.append(idnumber.charAt(i));
                    }
                }
                String idNumber = StringTrim(iNumber.toString());
                idNumber = idNumber.replaceAll(" ","");
                String username = "stu"+idNumber;
                String str = StringTrim(username);
                user.setUsername(str);
                user.setPassword(MD5Util.encrypt(str, User.DEFAULT_PASSWORD));
                if(userImport.getSex()==null){
                    userImport.setSex("2");
                }
                user.setSex(userImport.getSex());
                user.setTrueName(userImport.getTrueName());
                //这下面就是设置基础信息
                user.setStatus(User.STATUS_VALID);
                user.setCreateTime(new Date());
                String image = imageByFontsUtil.generateImg(userImport.getTrueName());
                user.setAvatar(image);
                user.setTheme(User.THEME_BLACK);
                user.setIsTab(User.TAB_OPEN);
                user.setMobile(userImport.getMobile());
                user.setIdNumber(idNumber);
                user.setDeptName(userImport.getCourse());
                //设置描述内容
                if(userImport.getDescription()==null){
                    user.setDescription("我是"+user.getTrueName());
                }else{
                    user.setDescription(userImport.getDescription());
                }
                //设置昵称
                user.setNickname(userImport.getTrueName());
                user.setIsDel(1);
                data.add(user);
            }

            @Override
            public void onError(int sheet, int row, List<ExcelErrorField> errorFields) {
                // 数据校验失败时，记录到 error集合
                error.add(ImmutableMap.of("row", row, "errorFields", errorFields));
            }
        });

        if (CollectionUtils.isNotEmpty(data)) {
            for (User user : data) {
                Dept dept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptName, user.getDeptName()));
                if (dept!=null){
                    user.setDeptId(dept.getDeptId());
                }
                userMapper.insert(user);
                UserRole ur = new UserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(FebsConstant.STUDENT_ID);
                userRoleService.save(ur);
            }

        }
        ImmutableMap<String, Object> result = ImmutableMap.of(
                "time", stopwatch.stop().toString(),
                "success", data,
                "successCount",data.size(),
                "error", error,
                "errorCount",error.size()
        );
        return new FebsResponse().success().data(result);
    }

    /**
     * @ Author wangke
     * @ Description 教师端导入学生信息所需模板 
     * @ Date 10:33 2021/3/20
     * @ Param 
     * @ return 
     */
    @GetMapping("studentExcelTemplate")
    @ControllerEndpoint(operation = "下载学生导入模板成功",exceptionMessage = "下载学生导入模板失败")
    public void generateImportStudentTemplate(){
        try {
//            downloadTemplate("学生信息导入模板","http://file.huistone.com/excel/2020/07/14/a27758c09f404b7ca1bc2a10688e8350/学生Excel导入模板1.xlsx",response);
            downloadOss("http://file.huistone.com/excel/2020/07/29/91ed232119584cc7b1cbc1f7dd048aec/学生信息导入模板.xlsx");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @ Author wangke
     * @ Description 添加学生接口
     * @ Date 14:42 2021/3/20
     * @ Param
     * @ return
     */
    @RequestMapping("addStudentForTeacher")
    @ControllerEndpoint(operation = "教师端添加学生",exceptionMessage = "教师端添加学生失败")
    public FebsResponse addStudentForTeacher(User user){
        if (user.getUsername() == null ||
                user.getIdNumber() == null ||
                user.getDeptId() == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("添加失败！请检查输入信息是否完善！");
        }
        try{
//            userService.addStudentForTeacher(user);
            if (userService.findByName(user.getUsername()) != null) {
                return new FebsResponse().fail().message("该用户名已存在");
            }
            if (userService.getOne(new QueryWrapper<User>().lambda()
                    .eq(User::getMobile, user.getMobile())) != null) {
                return new FebsResponse().fail().message("手机号已存在");
            }
            if (userService.getOne(new QueryWrapper<User>().lambda().
                    eq(User::getIdNumber, user.getIdNumber())) != null) {
                return new FebsResponse().fail().message("该学号已存在");
            }
//            user.setDeptId(deptId.longValue());
            user.setCreateTime(new Date());
            user.setStatus(User.STATUS_VALID);
            user.setAvatar(User.DEFAULT_AVATAR);
            user.setTheme(User.THEME_BLACK);
            user.setIsTab(User.TAB_OPEN);
            user.setUserType(FebsConstant.SCHOOL_TYPE);
            user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
            userService.save(user);
            // 保存用户角色
            setUserRoles(user, 82L);
        }catch (FebsException e){
            return new FebsResponse().fail().message("添加学生信息失败");
        }
        return new FebsResponse().success();
    }

    private void setUserRoles(User user, Long roleId) {
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getUserId());
        userRole.setRoleId(Long.valueOf(roleId));
        userRoleService.save(userRole);
    }

    /**
     * @ Author wangke
     * @ Description 导出学生信息接口
     * @ Date 15:14 2021/3/20
     * @ Param
     * @ return
     */
    @GetMapping("exportStudentInfo")
    @ControllerEndpoint(exceptionMessage = "导出学生Excel失败")
    public void exportStudent(HttpServletResponse response,@RequestParam(value = "deptId",required = true) Long deptId) {
        List<User> list = userService.list(new QueryWrapper<User>()
                .lambda()
                .eq(User::getDeptId,deptId)
                .eq(User::getIsDel, 1));
        list.stream().forEach(s -> {
            s.setDeptName(deptService.getById(s.getDeptId()).getDeptName());
        });

        ExcelKit.$Export(User.class, response).downXlsx(list, false);
    }

    /**
     * @ Author wangke
     * @ Description 查询学生信息接口
     * @ Date 15:59 2021/3/20
     * @ Param
     * @ return
     */
    @RequestMapping("selectStudentInfo")
    @ControllerEndpoint(operation = "查询单个学生信息",exceptionMessage = "查询单个学生信息失败")
    public FebsResponse selectStudentInfo(@RequestParam(value = "userId",required = true) Long userId){
        if (getCurrentUser()==null){
            return new FebsResponse().no_login();
        }
        return new FebsResponse().success().data(userService.getById(userId));
    }

    /**
     * @ Author wangke
     * @ Description 教师端修改学生信息接口
     * @ Date 16:32 2021/3/20
     * @ Param 
     * @ return 
     */
    @RequestMapping("updateStudent")
    @ControllerEndpoint(exceptionMessage = "修改学生信息失败",operation = "修改学生信息")
    public FebsResponse updateStudent( User user){
        try {
            int i = userService.updateStudentInfo(user);
            if(i==501){
                return new FebsResponse().fail().message("手机号重复");
            }else if(i==200){
                return new FebsResponse().success();
            }else if(i == 502){
                return new FebsResponse().fail().message("学号重复");
            }else{
                return new FebsResponse().fail();
            }
        } catch (Exception e) {
            return new FebsResponse().fail().message("修改学生信息失败");
        }

    }


}
