package cc.mrbird.febs.api.teacher;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.entity.vo.CourseVo;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ Description: 老师，实验项目的开启接口
 * @ Author: 马超伟
 * @ Date: 2020/6/24
 */
@RestController
@RequestMapping("/api/teacherProject")
public class TeacherPorjectApiController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ITeacherProjectService teacherProjectService;
    @Resource
    private IUserDeptService userDeptService;
    @Resource
    private ICourseService courseService;
    @Resource
    private IExperimentProjectService experimentProjectService;


    @GetMapping("courseList")
    @ControllerEndpoint(operation = "查询老师所带班级下拉框成功",exceptionMessage = "查询老师所带班级下拉框失败")
    public FebsResponse courseList(Integer deptId){
        User teacher = getCurrentUser();
        if (teacher==null){
            return new FebsResponse().no_login();
        }
        List<UserDept> list = userDeptService.list(new QueryWrapper<UserDept>()
                .lambda()
                .eq(UserDept::getDeptId, deptId)
                .eq(UserDept::getUserId, teacher.getUserId())
                .eq(UserDept::getIsDel,1));
        List<CourseVo> courseList = new ArrayList<>();
        CourseVo courseVo = null;
        for (UserDept userDept : list) {
            courseVo =  new CourseVo();
            courseVo.setCourseId(userDept.getCourseId().intValue());
            courseVo.setCourseName(courseService.getById(userDept.getCourseId()).getCourseName());
            courseList.add(courseVo);
        }
        return new FebsResponse().data(courseList).success();
    }

    /**
     * @ Description: 查询，老师开启实验页面，老师的班级列表查询
     * @ Param: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/24
     */
    @GetMapping("/listDept")
    @ControllerEndpoint(operation = "查询老师开启实验页面，老师的班级列表查询", exceptionMessage = "查询老师开启实验页面，老师的班级列表查询失败")
    public FebsResponse listDept() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                        .eq(UserDept::getUserId, currentUser.getUserId())
        );
        FebsResponse response = new FebsResponse();
        List<Map<String, Object>> list = new ArrayList<>();
        //去重班级id
        List<Long> deptIds = userDeptList.stream().map(UserDept::getDeptId).distinct().collect(Collectors.toList());
        try {
            Map<String, Object> map = null;
            for (Long deptId : deptIds) {
                map = new HashMap<>();
                map.put("deptId",deptId);
                map.put("deptName",getDeptName(deptId));
                list.add(map);
            }
            response.data(list);
            return response.success();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherProject/listDept,查询失败：" + e.getMessage());
            return response.fail().message("查询失败");
        }
    }

    /**
     * @ Description: 查询老师开启项目页面，老师拥有的课程和项目列表
     * @ Param: []
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/24
     */
    @GetMapping("/listProject")
    @ControllerEndpoint(operation = "查询老师开启项目页面，老师拥有的课程和项目列表", exceptionMessage = "查询老师开启项目页面，老师拥有的课程和项目列表失败")
    public FebsResponse listProject(Long deptId) {
        User currentUser = getCurrentUser();

        if (currentUser == null) {
            return new FebsResponse().no_login();
        }

        if (IntegerUtils.isLongBlank(deptId)) {
            return new FebsResponse().no_param();
        }

        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                .eq(UserDept::getUserId, currentUser.getUserId())
                .eq(UserDept::getDeptId, deptId)
        );
        List<ElementCascader> list = new ArrayList<>();
        try {
            for (UserDept userDept : userDeptList) {
                ElementCascader cascader = new ElementCascader();
                cascader.setValue(userDept.getCourseId());
                cascader.setLabel(courseService.getById(userDept.getCourseId()).getCourseName());
                //查询二级菜单的，实验项目
                List<ExperimentProject> experimentProjectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>().lambda()
                        .eq(ExperimentProject::getCourseId, userDept.getCourseId())
                        .eq(ExperimentProject::getState, 1)
                        .orderByAsc(ExperimentProject::getProjectId));
                if (experimentProjectList != null && experimentProjectList.size() > 0) {
                    List<ElementCascader> elementCascaders = new ArrayList<>();
                    for (ExperimentProject experimentProject : experimentProjectList) {
                        ElementCascader cascader1 = new ElementCascader();
                        cascader1.setValue(experimentProject.getProjectId());
                        cascader1.setLabel(experimentProject.getProjectName());
                        elementCascaders.add(cascader1);
                    }
                    cascader.setChildren(elementCascaders);
                }
                list.add(cascader);
            }
            return new FebsResponse().success().data(list);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherProject/listProject,查询失败：" + e.getMessage());
            return new FebsResponse().fail().message("数据查询异常！");
        }
    }

    /**
     * @ Description: 开启项目
     * @ Param: [deptId, projectId, courseId, startTime, endTime]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/24
     */
    @GetMapping("/openProject")
    @ControllerEndpoint(operation = "开启项目", exceptionMessage = "开启项目失败")
    public FebsResponse openProject(Long deptId, Long projectId, Long courseId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(deptId) || IntegerUtils.isLongBlank(projectId) || IntegerUtils.isLongBlank(courseId)) {
            return new FebsResponse().no_param();
        }
        UserDept userDept = validTeacherByCourse(deptId, courseId, currentUser.getUserId());
        if (userDept == null) {
            return Objects.requireNonNull(new FebsResponse().put("code", 501)).message("您非该课程的主课老师，无法进行项目开启操作！");
        }
        TeacherProject serviceOne = teacherProjectService.getOne(new QueryWrapper<TeacherProject>().lambda()
                        .eq(TeacherProject::getTeacherId, currentUser.getUserId())
                        .eq(TeacherProject::getDeptId, deptId)
                        .eq(TeacherProject::getProjectId, projectId)
                        .eq(TeacherProject::getCourseId, courseId)
                        .eq(TeacherProject::getIsDel, 1)
//                .eq(TeacherProject::getStatus, 1)
        );
        if (serviceOne != null) {
            return Objects.requireNonNull(new FebsResponse().put("code", 502)).message("该课程您已发布过，请勿重复发布！");
        }
        TeacherProject teacherProject = new TeacherProject();
        teacherProject.setTeacherId(currentUser.getUserId());
        teacherProject.setDeptId(deptId);
        teacherProject.setProjectId(projectId);
        teacherProject.setCourseId(courseId);
        teacherProject.setStartTime(LocalDateTime.now());
        teacherProject.setStatus(1);
        if (teacherProjectService.save(teacherProject)) {
            return new FebsResponse().success().message("操作成功！");
        } else {
            logger.error("/api/teacherProject/openProject,开启失败：");
            return new FebsResponse().fail().message("操作失败！");
        }
    }

    /**
     * @ Description: 开启项目
     * @ Param: [deptId, projectId, courseId, startTime, endTime]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/24
     */
    @GetMapping("/openMultiProject")
    @ControllerEndpoint(operation = "开启项目", exceptionMessage = "开启项目失败")
    public FebsResponse openMultiProject(Long deptId, String projectIds, Long courseId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(deptId) || projectIds==null|| IntegerUtils.isLongBlank(courseId)){
            return new FebsResponse().no_param();
        }
        String[] split = projectIds.split(",");
        if(split.length<=0||split==null)
            return new FebsResponse().no_param();
        List<Long> collect = Arrays.asList(split).stream().map(s -> Long.parseLong(s)).collect(Collectors.toList());
        UserDept userDept = validTeacherByCourse(deptId, courseId, currentUser.getUserId());
        if (userDept == null) {
            return Objects.requireNonNull(new FebsResponse().put("code", 501)).message("您非该课程的主课老师，无法进行项目开启操作！");
        }
        List<TeacherProject> serviceOne = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                        .eq(TeacherProject::getTeacherId, currentUser.getUserId())
                        .eq(TeacherProject::getDeptId, deptId)
                        .in(collect.size()>0, TeacherProject::getProjectId, collect)
                        .eq(TeacherProject::getCourseId, courseId)
                        .eq(TeacherProject::getIsDel, 1)
//                .eq(TeacherProject::getStatus, 1)
        );
        if (serviceOne != null) {
            return Objects.requireNonNull(new FebsResponse().put("code", 502)).message("该课程您已发布过，请勿重复发布！");
        }
        List<TeacherProject> teacherProjects = new ArrayList<>();
        for (Long aLong : collect) {
            TeacherProject teacherProject = new TeacherProject();
            teacherProject.setTeacherId(currentUser.getUserId());
            teacherProject.setDeptId(deptId);
            teacherProject.setProjectId(aLong);
            teacherProject.setCourseId(courseId);
            teacherProject.setStartTime(LocalDateTime.now());
            teacherProject.setStatus(1);
            teacherProjects.add(teacherProject);
        }
        if (teacherProjectService.saveBatch(teacherProjects)) {
            return new FebsResponse().success().message("操作成功！");
        } else {
            logger.error("/api/teacherProject/openProject,开启失败：");
            return new FebsResponse().fail().message("操作失败！");
        }
    }


    /**
     * @ Description: 查询已经开启的项目
     * @ Param: [queryRequest, teacherProject]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/28
     */
    @GetMapping("/listTeacherProject")
    @ControllerEndpoint(operation = "查询已经开启的项目", exceptionMessage = "查询已经开启的项目失败")
    public FebsResponse listTeacherProject(QueryRequest queryRequest, TeacherProject teacherProject) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        try {
            //创建分页条件查询
            Page<TeacherProject> projectPage = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
            SortUtil.handlePageSort(queryRequest, projectPage, "project_id", FebsConstant.ORDER_ASC, false);
            LambdaQueryWrapper<TeacherProject> queryWrapper = new QueryWrapper<TeacherProject>().lambda()
                    .eq(IntegerUtils.isLongNotBlank(teacherProject.getDeptId()), TeacherProject::getDeptId, teacherProject.getDeptId())
                    .eq(IntegerUtils.isLongNotBlank(teacherProject.getProjectId()), TeacherProject::getProjectId, teacherProject.getProjectId());
            //如果课程ID不传，
            if (IntegerUtils.isLongBlank(teacherProject.getDeptId())) {
                List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                        .eq(UserDept::getUserId, currentUser.getUserId())
                );
                List<TeacherProject> teacherProjects = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                        .eq(TeacherProject::getDeptId, userDeptList.get(0).getDeptId())
                        .eq(TeacherProject::getCourseId, userDeptList.get(0).getCourseId())
                        .select(TeacherProject::getTeacherProjectId));
                List<Integer> collect = teacherProjects.stream().map(TeacherProject::getTeacherProjectId).collect(Collectors.toList());
                queryWrapper.in(collect.size()>0, TeacherProject::getTeacherProjectId,collect);
                //两个以上课程班级的话拼接 or
                for (int i = 1; i < userDeptList.size(); i++) {
                    List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                            .eq(TeacherProject::getDeptId, userDeptList.get(i).getDeptId())
                            .eq(TeacherProject::getCourseId, userDeptList.get(i).getCourseId())
                            .select(TeacherProject::getTeacherProjectId));
                    List<Integer> collect1 = teacherProjectList.stream().map(TeacherProject::getTeacherProjectId).collect(Collectors.toList());
                    queryWrapper.or().in(collect1.size()>0, TeacherProject::getTeacherProjectId,collect1);
                }
            }

            IPage<TeacherProject> teacherProjectIPage = teacherProjectService.page(projectPage, queryWrapper);
            List<TeacherProject> teacherProjectList = teacherProjectIPage.getRecords();
            teacherProjectList.forEach(project -> {
                project.setDeptName(getDeptName(project.getDeptId()));
                project.setProjectName(experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda()
                        .eq(ExperimentProject::getProjectId, project.getProjectId())
                        .select(ExperimentProject::getProjectName))
                        .getProjectName());
                project.setCourseName(courseService.getOne(new QueryWrapper<Course>().lambda()
                        .eq(Course::getCourseId, project.getCourseId())
                        .select(Course::getCourseName))
                        .getCourseName());
                //再判断传入的班级，课程，项目对于该老师是否是主课老师
                if (project.getDeptId() != null && project.getCourseId() != null) {
                    UserDept userDept = validTeacherByCourse(project.getDeptId(), project.getCourseId(), currentUser.getUserId());
                    if (userDept == null) {
                        //辅导老师
                        project.setType(0);
                    } else {
                        //主课老师
                        project.setType(1);
                    }
                }
            });
            return new FebsResponse().success().message("查询成功！").data(getDataTable(teacherProjectIPage));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherProject/listTeacherProject,查询已经开启的项目失败：" + e.getMessage());
            return new FebsResponse().fail().message("查询失败！");
        }
    }

    /**
     * @ Description: 查询已经开启的项目
     * @ Param: [queryRequest, teacherProject]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/28
     */
    @GetMapping("/listTeacherMultiProject")
    @ControllerEndpoint(operation = "查询已经开启的项目", exceptionMessage = "查询已经开启的项目失败")
    public FebsResponse listTeacherMultiProject(QueryRequest queryRequest, TeacherProject teacherProject) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        try {
            String projectIds = teacherProject.getProjectIds();
            List<Long> collect2 = new ArrayList<>();
            if(projectIds!=null){
                String [] split = projectIds.split(",");
                collect2 = Arrays.asList(split).stream().map(s -> Long.parseLong(s)).collect(Collectors.toList());
            }
            //创建分页条件查询
            Page<TeacherProject> projectPage = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
            SortUtil.handlePageSort(queryRequest, projectPage, "project_id", FebsConstant.ORDER_ASC, false);
            LambdaQueryWrapper<TeacherProject> queryWrapper = new QueryWrapper<TeacherProject>().lambda()
                    .eq(IntegerUtils.isLongNotBlank(teacherProject.getDeptId()), TeacherProject::getDeptId, teacherProject.getDeptId())
                    .in(collect2.size()>0, TeacherProject::getProjectId,collect2);
//                    .eq(IntegerUtils.isLongNotBlank(teacherProject.getProjectId()), TeacherProject::getProjectIds, teacherProject.getProjectId());
            //如果课程ID不传，
            if (IntegerUtils.isLongBlank(teacherProject.getDeptId())) {
                List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                        .eq(UserDept::getUserId, currentUser.getUserId())
                );
                List<TeacherProject> teacherProjects = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                        .eq(TeacherProject::getDeptId, userDeptList.get(0).getDeptId())
                        .eq(TeacherProject::getCourseId, userDeptList.get(0).getCourseId())
                        .select(TeacherProject::getTeacherProjectId));
                List<Integer> collect = teacherProjects.stream().map(TeacherProject::getTeacherProjectId).collect(Collectors.toList());
                queryWrapper.in(collect.size()>0, TeacherProject::getTeacherProjectId,collect);
                //两个以上课程班级的话拼接 or
                for (int i = 1; i < userDeptList.size(); i++) {
                    List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                            .eq(TeacherProject::getDeptId, userDeptList.get(i).getDeptId())
                            .eq(TeacherProject::getCourseId, userDeptList.get(i).getCourseId())
                            .select(TeacherProject::getTeacherProjectId));
                    List<Integer> collect1 = teacherProjectList.stream().map(TeacherProject::getTeacherProjectId).collect(Collectors.toList());
                    queryWrapper.or().in(collect1.size()>0, TeacherProject::getTeacherProjectId,collect1);
                }
            }

            IPage<TeacherProject> teacherProjectIPage = teacherProjectService.page(projectPage, queryWrapper);
            List<TeacherProject> teacherProjectList = teacherProjectIPage.getRecords();
            teacherProjectList.forEach(project -> {
                project.setDeptName(getDeptName(project.getDeptId()));
                project.setProjectName(experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda()
                        .eq(ExperimentProject::getProjectId, project.getProjectId())
                        .select(ExperimentProject::getProjectName))
                        .getProjectName());
                project.setCourseName(courseService.getOne(new QueryWrapper<Course>().lambda()
                        .eq(Course::getCourseId, project.getCourseId())
                        .select(Course::getCourseName))
                        .getCourseName());
                //再判断传入的班级，课程，项目对于该老师是否是主课老师
                if (project.getDeptId() != null && project.getCourseId() != null) {
                    UserDept userDept = validTeacherByCourse(project.getDeptId(), project.getCourseId(), currentUser.getUserId());
                    if (userDept == null) {
                        //辅导老师
                        project.setType(0);
                    } else {
                        //主课老师
                        project.setType(1);
                    }
                }
            });
            return new FebsResponse().success().message("查询成功！").data(getDataTable(teacherProjectIPage));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherProject/listTeacherProject,查询已经开启的项目失败：" + e.getMessage());
            return new FebsResponse().fail().message("查询失败！");
        }
    }



    /**
     * @ Description: 关闭项目，单个或者多个
     * @ Param: [teacherProjectId：id集合数组]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/28
     */
    @GetMapping("closeProject")
    @ControllerEndpoint(operation = "关闭项目，单个或者多个", exceptionMessage = "关闭项目，单个或者多个失败")
    public FebsResponse closeProject(Integer teacherProjectId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (teacherProjectId == null) {
            return new FebsResponse().no_param();
        }
        TeacherProject teacherProject = teacherProjectService.getById(teacherProjectId);
        UserDept userDept = validTeacherByCourse(teacherProject.getDeptId(), teacherProject.getCourseId(), currentUser.getUserId());
        if (userDept == null) {
            return new FebsResponse().put_code(501, "您非该课程的主课老师，无法进行此项操作！");
        }
        boolean update = teacherProjectService.update(new UpdateWrapper<TeacherProject>().lambda()
                .set(TeacherProject::getStatus, 0)
                .set(TeacherProject::getEndTime, LocalDateTime.now())
                .set(TeacherProject::getIsDel, 0)
                .eq(TeacherProject::getTeacherProjectId, teacherProjectId));
        if (update) {
            return new FebsResponse().success().message("实验项目关闭成功！");
        } else {
            logger.error("/api/teacherProject/closeProject,关闭项目失败：");
            return new FebsResponse().fail().message("操作失败！");
        }
    }

    /**
     * @ Description: 删除一个，或者多个项目记录
     * @ Param: [teacherProjectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/28
     */
    @GetMapping("/deleteProject")
    @ControllerEndpoint(operation = "删除一个，或者多个项目记录", exceptionMessage = "删除一个，或者多个项目记录")
    public FebsResponse deleteProject(Integer teacherProjectId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (teacherProjectId == null) {
            return new FebsResponse().no_param();
        }
        TeacherProject teacherProject = teacherProjectService.getById(teacherProjectId);
        UserDept userDept = validTeacherByCourse(teacherProject.getDeptId(), teacherProject.getCourseId(), currentUser.getUserId());
        if (userDept == null) {
            return new FebsResponse().put_code(501, "您非该课程的主课老师，无法进行此项操作！");
        }
        if (teacherProjectService.removeById(teacherProjectId)) {
            return new FebsResponse().success().message("删除成功！");
        } else {
            logger.error("/api/teacherProject/deleteProject,删除项目失败：");
            return new FebsResponse().fail().message("删除失败！");
        }
    }

}
