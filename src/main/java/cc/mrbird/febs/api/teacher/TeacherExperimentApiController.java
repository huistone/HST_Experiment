package cc.mrbird.febs.api.teacher;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.FileUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.ReportUtils;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/30/15:16
 * @ Description:
 */
@RestController
@RequestMapping("/api/teacherExperiment")
public class TeacherExperimentApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IExperimentAnswerService experimentAnswerService;
    @Resource
    private IUserService userService;
    @Resource
    private IExperimentProjectService experimentProjectService;
    @Resource
    private IExperimentQuestionService experimentQuestionService;
    @Resource
    private IExperimentRemarkService experimentRemarkService;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserCommitService userCommitService;

    @ResponseBody
    @RequestMapping("previewPdf")
    public FebsResponse previewPdf(HttpServletResponse response, Long projectId, Long studentId){
        if(getCurrentUser()==null){
            return new FebsResponse().no_login();
        }
        //根据学生id以及实验项目id查找
        UserCommit one = userCommitService.getOne(new QueryWrapper<UserCommit>()
                .lambda()
                .eq(UserCommit::getProjectId, projectId)
                .eq(UserCommit::getStuId, studentId)
                .select(UserCommit::getReportUrl));
        String reportUrl = one.getReportUrl();

        String projectName = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, projectId)
                .select(ExperimentProject::getProjectName)).getProjectName();
        try{
            FileUtil.previewPdf(response,projectName,reportUrl);
        }catch(Exception e){
            return new FebsResponse().fail().data("预览失败");
        }
        return new FebsResponse().success();
    }

    @ResponseBody
    @RequestMapping("previewPdf1")
    public FebsResponse previewPdf1(HttpServletResponse response, String url){
        URL fileUrl = null;
        InputStream inputStream =  null;
        try {
            fileUrl = new URL(url);
            inputStream = new BufferedInputStream(fileUrl.openStream());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedInputStream br = new BufferedInputStream(inputStream);
            byte[] bs = new byte[1024];
            int len = 0;
            response.reset();
                URL u = new URL("file:///" + url);
                String contentType = u.openConnection().getContentType();
                response.setContentType(contentType);
                response.setHeader("Content-Disposition", "inline;filename="
                        + "2019年上半年英语四级笔试准考证(戴林峰).pdf");
                // 文件名应该编码成utf-8，注意：使用时，我们可忽略这句
            OutputStream out = response.getOutputStream();
            while ((len = br.read(bs)) > 0) {
                out.write(bs, 0, len);
            }
            out.flush();
            out.close();
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FebsResponse();
    }


    /**
     * @ Description: 预习题的查询统计接口
     * @ Param: [dept, projectId,type:1预习题，2思考题]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/30
     */
    @GetMapping("/listExperimentPrep")
    @ControllerEndpoint(operation = "预习题提交情况查询", exceptionMessage = "预习题提交情况查询失败")
    public FebsResponse listExperimentPrep(QueryRequest queryRequest, Long deptId, Long projectId, Integer type) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(deptId) || IntegerUtils.isLongBlank(projectId) || IntegerUtils.isBlank(type)) {
            return new FebsResponse().no_param();
        }
        try {
            List<ExperimentRemark> experimentRemarks = experimentRemarkService.listCommit(queryRequest.getPageNum(), queryRequest.getPageSize(), projectId, deptId, type);
            for (ExperimentRemark experimentRemark : experimentRemarks) {
                if (experimentRemark.getIsCommit()==null){
                    experimentRemark.setIsCommit(0);
                }
                if (experimentRemark.getProjectName()==null){
                    experimentRemark.setProjectName(experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda()
                    .eq(ExperimentProject::getProjectId,projectId)).getProjectName());
                }
            }
            int count = experimentRemarkService.listCommitCount(queryRequest.getPageNum(), queryRequest.getPageSize(), projectId, deptId, type);
            FebsResponse response = new FebsResponse().success().message("查询成功！").data(experimentRemarks).put("total", count);
            return response;
        } catch (
                Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherExperiment/listExperimentPrep,预习题提交情况查询失败：" + e.getMessage());
            return new FebsResponse().fail().message("查询失败！");
        }
    }

    /**
     * @ Description: 查询单个学生的预习题提交信息
     * @ Param: [createId, deptId, projectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/30
     */
    @GetMapping("/listRemarkByCreateId")
    @ControllerEndpoint(operation = "查询单个学生的预习题提交信息", exceptionMessage = "查询单个学生的预习题提交信息查询失败")
    public FebsResponse listAnswerByCreateId(Long remarkId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(remarkId)) {
            return new FebsResponse().no_param();
        }
        try {
            //查询所有提交主表
            ExperimentRemark experimentRemark = experimentRemarkService.getById(remarkId);
            //查询所有提交详情表
            List<ExperimentAnswer> experimentAnswerList = experimentAnswerService.list(new QueryWrapper<ExperimentAnswer>().lambda()
                    .eq(ExperimentAnswer::getCreateId, remarkId).eq(ExperimentAnswer::getUserType,1));
            experimentAnswerList.forEach(experimentAnswer -> {
                ExperimentQuestion experimentQuestion = experimentQuestionService.getOne(new QueryWrapper<ExperimentQuestion>().lambda()
                        .eq(ExperimentQuestion::getQuestionId, experimentAnswer.getQuestionId())
                        .select(ExperimentQuestion::getQuestionName, ExperimentQuestion::getAnswer, ExperimentQuestion::getContext));
                String title = experimentQuestion.getQuestionName();
                if (experimentRemark.getType()==2){
                    List<String> list = Arrays.asList(experimentQuestion.getContext().replace("&nbsp;", "").split("@@"));
                    for (int i = 0; i < list.size(); i++) {
                        list.set(i, Character.toUpperCase((char)(65+i))+"、" + list.get(i)+"</br>");
                    }
                    String replace = list.toString()
                            .replace("[", "")
                            .replace("]", "")
                            .replace(",", "</br>")
                            .replace(" ", "");
                    title+="</br>"+ replace;
                }
                experimentAnswer.setTitle(title);

                experimentAnswer.setContextQuestion(experimentQuestion.getAnswer());
            });
            FebsResponse response = new FebsResponse().success().data(experimentAnswerList);
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("score", experimentAnswerList.stream().mapToDouble(m -> m.getScore()).sum());
            map.put("scoreUser", experimentRemark.getScoreUser());
            map.put("scoreTime", experimentRemark.getScoreTime());
            map.put("isCommit", experimentRemark.getIsCommit());
            map.put("remark", experimentRemark.getRemark());
            response.put("map", map);
            response.message("查询成功！");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherExperiment/listAnswerByCreateId,查询单个学生的预习题提交信息查询失败：" + e.getMessage());
            return new FebsResponse().fail().message("查询失败！");
        }
    }

    /**
     * @ Description: 预习题评阅
     * @ Param: [remark, deptId, createId, projectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/7/1
     */
    @GetMapping("/getRemark")
    @ControllerEndpoint(operation = "预习题批阅成功",exceptionMessage = "预习题批阅失败")
    public FebsResponse getRemark(Long remarkId, String remark) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }

        if (IntegerUtils.isLongBlank(remarkId)) {
            return new FebsResponse().no_param();
        }

        try {
            if (experimentRemarkService.getById(remarkId).getIsCommit() == 2) {
                return new FebsResponse().put_code(501, "您已批阅过，请勿重复批阅！");
            }
            experimentRemarkService.update(new UpdateWrapper<ExperimentRemark>().lambda()
                    .eq(ExperimentRemark::getRemarkId, remarkId)
                    .set(StringUtils.isNotBlank(remark), ExperimentRemark::getRemark, remark)
                    .set(ExperimentRemark::getIsCommit, 2)
                    .set(ExperimentRemark::getScoreTime, LocalDateTime.now())
                    .set(ExperimentRemark::getScoreUser, currentUser.getTrueName()));
            return new FebsResponse().success().message("评阅成功！");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherExperiment/getRemark,批阅失败：" + e.getMessage());
            return new FebsResponse().fail().message("批阅失败！");
        }
    }

    /**
     * @ Author: 王珂
     * @ Description: 预习题导出
     * @ Date: 17:15 2020/7/3
     * @ params:  * @ Param null
     */
    @GetMapping("exportExcelWithPrepareAnswer")
    @ControllerEndpoint(operation = "导出预习题成绩",exceptionMessage = "导出预习题成绩失败")
    public FebsResponse exportPrepareAnswer(Long remarkId, HttpServletResponse response, HttpServletRequest request)  {
        if(getCurrentUser()==null){
            return new FebsResponse().no_login();
        }
        if(remarkId==null){
            return new FebsResponse().no_param();
        }
        ExperimentRemark remark = experimentRemarkService.getById(remarkId);
        if(remark.getIsCommit()!=2){
            return new FebsResponse().put_code(501,"未审批，无法导出");
        }
        File tempFile = null;
        try {
            URL url = new URL("http://file.huistone.com/excel/2020/07/09/a97fbcbabd1848f3bc13b0ed11cee05e/prepareAnswer.xlsx");
            InputStream inputStream = new BufferedInputStream(url.openStream());
            // 模板临时目录
            String rootPath = request.getServletContext().getRealPath("template_temp/");
            //临时文件名
            String filePath = rootPath +  "_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "prepareScore";
            tempFile = new File(filePath);
            //保存临时文件
            ReportUtils.saveTempFile(inputStream,tempFile);
            TemplateExportParams params = new TemplateExportParams(filePath);
            params.setHeadingRows(1);
            params.setSheetName(new String[]{"预习题成绩信息"});
            params.setColForEach(true);
            params.setHeadingStartRow(0);
            params.setScanAllsheet(true);
            Map<String, Object> map = getExcelInfo(remarkId, 1);
            Workbook workBook = ExcelExportUtil.exportExcel(params,map);
            User student = userService.getOne(new QueryWrapper<User>().lambda()
                    .eq(User::getUserId, remark.getCreateId()));
            String fileName = student.getTrueName()+"预习题成绩详情.xlsx";
            //浏览器下载方法
            ReportUtils.export(response,workBook,fileName,request);
        } catch (IOException e) {
            logger.error("api/teacherExperiment/exportExcelWithPrepareAnswer:导出预习题Excel失败"+e.getMessage());
            return new FebsResponse().fail().message("导出失败");
        } finally {
            // 删除临时文件
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }
        return new FebsResponse().success().message("导出成功");
    }

    /**
     * @ Author: 王珂
     * @ Description: 抽取思考题和预习题导出公共代码
     * @ Date: 16:20 2020/7/6
     * @ params:  * @ Param remarkId,type
     */
    public Map<String,Object> getExcelInfo(Long remarkId,Integer type){
        ExperimentRemark remark = experimentRemarkService.getById(remarkId);
        //学生信息
        User student = userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getUserId, remark.getCreateId()));

        ExperimentProject project = experimentProjectService.getById(remark.getProjectId());

        //查出带该门课的老师
        List<UserDept> userDept = userDeptService.list(new QueryWrapper<UserDept>()
                .lambda()
                .eq(UserDept::getDeptId, remark.getDeptId())
                .eq(UserDept::getCourseId, project.getCourseId()));
        Long userId = null;
        //获取主课老师id
        for (UserDept dept : userDept) {
            if (dept.getType()==1){
                userId = dept.getUserId();
                break;
            }
        }
        User teacher =  userService.getById(userId);
        String studentTrueName = teacher.getTrueName();
        if(studentTrueName==null){
            studentTrueName = "暂无主课老师";
        }

        Dept dept = deptService.getById(student.getDeptId());
        //创建日期
        LocalDateTime createTime = remark.getCreateTime();
        //审批时间
        LocalDateTime scoreTime =  remark.getScoreTime();

        Map<String, Object> map = new HashMap<>();
        map.put("deptName",dept.getDeptName());
        String projectName = project.getProjectName();
        map.put("projectName",projectName);
        map.put("scoreTeacher",remark.getScoreUser());
        map.put("score",remark.getScore());
        map.put("commitTime", DateUtil.getDateFormat(DateUtil.localDateTimeToDate(createTime),"yyyy-MM-dd HH:mm:ss"));
        map.put("scoreTime", DateUtil.getDateFormat(DateUtil.localDateTimeToDate(scoreTime),"yyyy-MM-dd HH:mm:ss"));
        String remarkMsg = remark.getRemark();
        if(remarkMsg==null){
            remarkMsg =  "暂无评语";
        }
        map.put("remark",remarkMsg);
        map.put("mainTeacher",studentTrueName);
        map.put("studentName",student.getTrueName());
        map.put("idNumber",student.getIdNumber());
        List<Map<String, Object>> maps = new ArrayList<>();
        List<ExperimentAnswer> list = experimentAnswerService.list(new QueryWrapper<ExperimentAnswer>()
                .lambda()
                .eq(ExperimentAnswer::getCreateId, remarkId)
                .eq(ExperimentAnswer::getUserType,1));
        //查所有的答案
        for (ExperimentAnswer experimentAnswer : list) {
            ExperimentQuestion question = experimentQuestionService.getOne(new QueryWrapper<ExperimentQuestion>()
                    .lambda()
                    .eq(ExperimentQuestion::getNumber, experimentAnswer.getNumber())
                    .eq(ExperimentQuestion::getProjectId,remark.getProjectId())
                    .eq(ExperimentQuestion::getType,type));
            Map<String, Object> map1 = new HashMap<>();
            String title = question.getQuestionName();
            String str = title.replaceAll("<[.[^<]]*>", "");
            map1.put("number",experimentAnswer.getNumber());
            map1.put("context",experimentAnswer.getContext());
            map1.put("title",str );
            maps.add(map1);
        }
        map.put("maplist",maps);
        return map;
    }

    /**
     * @ Author: 王珂
     * @ Description: 思考题excel导出
     * @ Date: 16:17 2020/7/6
     * @ params:  * @ Param remarkId
     */
    @GetMapping("exportExcelWithThink")
    @ControllerEndpoint(operation = "思考题导出" ,exceptionMessage = "思考题导出失败")
    public FebsResponse exportExcelWithThink(Long remarkId, HttpServletResponse response, HttpServletRequest request){
        if(getCurrentUser()==null){
            return new FebsResponse().no_login();
        }
        if(remarkId==null){
            return new FebsResponse().no_param();
        }
        ExperimentRemark remark = experimentRemarkService.getById(remarkId);
        if(remark.getIsCommit()!=2){
            return new FebsResponse().put_code(501,"未审批，无法导出");
        }
        File tempFile = null;
        try {
            URL url = new URL("http://file.huistone.com/excel/2020/07/09/63392222dca540e39a101a98454cd5f8/thinkAnswer.xlsx");
            InputStream inputStream = new BufferedInputStream(url.openStream());
            // 模板临时目录
            String rootPath = request.getServletContext().getRealPath("template_temp/");
            //临时文件名
            String filePath = rootPath +  "_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "thinkScore";
            tempFile = new File(filePath);
            //保存临时文件
            ReportUtils.saveTempFile(inputStream,tempFile);
            TemplateExportParams params = new TemplateExportParams(filePath);
            params.setHeadingRows(1);
            params.setSheetName(new String[]{"思考题成绩信息"});
            params.setColForEach(true);
            params.setHeadingStartRow(0);
            params.setScanAllsheet(true);
            Map<String, Object> map = getExcelInfo(remarkId, 2);
            Workbook workBook = ExcelExportUtil.exportExcel(params,map);
            User student = userService.getOne(new QueryWrapper<User>().lambda()
                    .eq(User::getUserId, remark.getCreateId()));
            String fileName = student.getTrueName()+"思考题成绩详情.xlsx";
            //浏览器下载方法
            ReportUtils.export(response,workBook,fileName,request);
        } catch (IOException e) {
            logger.error("api/teacherExperiment/exportExcelWithPrepareAnswer:导出思考题Excel失败"+e.getMessage());
            return new FebsResponse().fail().message("导出思考题Excel失败");
        } finally{
            // 删除临时文件
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }
        return new FebsResponse().success().message("导出思考题Excel成功");
    }

    /**
     * @ Description: 预习题的批量批阅
     * @ Param: [remarkIds]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/7/2
     */
    @GetMapping("batchRead/{remarkIds}")
    @ControllerEndpoint(operation = "批量批阅预习题", exceptionMessage = "批量批阅预习题失败")
    public FebsResponse batchRead(@NotBlank(message = "{required}") @PathVariable String remarkIds) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (StringUtils.isBlank(remarkIds)) {
            return new FebsResponse().no_param();
        }
        try {
            String[] ids = remarkIds.split(StringPool.COMMA);
            List<String> list = Arrays.asList(ids);
            for (String s : list) {
                //如果已经批阅了，则跳过批量设置
                if (experimentRemarkService.getById(s).getIsCommit() == 2) {
                    continue;
                }
                boolean update = experimentRemarkService.update(new UpdateWrapper<ExperimentRemark>().lambda()
                        .set(ExperimentRemark::getIsCommit, 2)
                        .set(ExperimentRemark::getScoreUser, currentUser.getTrueName())
                        .set(ExperimentRemark::getScoreTime, LocalDateTime.now())
                        .eq(ExperimentRemark::getRemarkId, s));
                if (!update) {
                    return new FebsResponse().put_code(501, "发生异常错误，更细失败！");
                }
            }
            return new FebsResponse().success().message("批量批阅成功!");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherExperiment/batchRead,批量批阅失败：" + e.getMessage());
            return new FebsResponse().fail().message("批阅失败！");
        }
    }


}
