package cc.mrbird.febs.api.teacher;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.BASE64DecodedMultipartFileUtil;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/24/11:18
 * @ Description: 老师页面，个人中心，老师数据等接口
 */
@RestController
@RequestMapping("/api/teacherInfo")
@Slf4j
public class TeacherInfoApiController extends BaseController {

    @Resource
    private IUserService userService;

    /**
     * @ Description: 获取老师详细信息
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 王珂
     * @ Date: 2020/6/29
     */
    @GetMapping
    @ControllerEndpoint(operation = "获取老师详细信息", exceptionMessage = "获取老师详细信息失败")
    public FebsResponse getTeacherInfo(){
        User user = null;
        try {
            User currentUser = getCurrentUser();
            if(currentUser==null){
                return new FebsResponse().no_login();
            }
            user = userService.selectTeacherInfo(currentUser);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/teacherInfo/：获取老师信息失败"+e.getMessage());
            return new FebsResponse().fail().message("获取老师信息失败");
        }
        return new FebsResponse().success().data(user);
    }

    /**
     * @ Author 王珂
     * @ Param trueName 真实名字
     * @ Param description 描述
     * @ Param sex 性别
     * @ Param email 邮箱
     * @ Param nickName 昵称
     * @ Description 修改教师个人信息
     * @return
     */
    @PostMapping("updateTeacherInfo")
    @ControllerEndpoint(operation = "修改老师个人信息", exceptionMessage = "修改老师个人信息失败")
    public FebsResponse updateTeacherInfo(String trueName, String description, String sex, String email, String nickName){
        User currentUser = getCurrentUser();
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if(nickName==null){
            return new FebsResponse().fail().message("昵称不能为空");
        }
        //判断邮箱是否存在
        if(email==null){
            return new FebsResponse().fail().message("邮箱不能为空");
        }
        //先判断不为空，再判断不等于他本身
        if (email != null) {
            User one = userService.getOne(new QueryWrapper<User>().lambda().
                    eq(User::getEmail, email));
            if (one != null && !one.getUserId().equals(currentUser.getUserId())) {
                return new FebsResponse().put("code",502).message("该邮箱已存在");
            }
        }
        currentUser.setTrueName(trueName);
        currentUser.setDescription(description);
        currentUser.setSex(sex);
        currentUser.setEmail(email);
        currentUser.setNickname(nickName);
        if (userService.updateById(currentUser)) {
            return new FebsResponse().success().message("用户信息更新成功");
        } else {
            return new FebsResponse().fail().message("更新失败！");
        }
    }

    /**
     * @ author: 王珂
     * @ Description: 修改教师手机号API
     * @ Date: 16:47 2020/7/8
     * @ params:  mobile 手机号
     */
    @GetMapping("updateTeacherMobile")
    @ControllerEndpoint(operation = "修改老师手机号", exceptionMessage = "修改老师手机号失败")
    public FebsResponse updateMobile(String mobile){
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        if (StringUtils.isBlank(mobile)){
            return new FebsResponse().no_param();
        }
        currentUser.setMobile(mobile);
        if (userService.updateById(currentUser)) {
            return new FebsResponse().success().message("手机号更新成功");
        } else {
            return new FebsResponse().fail().message("更新失败！");
        }
    }

    /**
     * @author: 王珂
     * @ Description: 修改教师头像
     * @Date: 16:49 2020/7/8
     * @ Params:  imgUrl 图片地址
     */
    @PostMapping("updateTeacherProfile")
    @ControllerEndpoint(operation = "修改老师头像", exceptionMessage = "修改老师头像失败")
    public FebsResponse updateTeahcerProfile(String imgUrl){
        User currentUser = getCurrentUser();
        //判断用户是否登录
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        MultipartFile file = BASE64DecodedMultipartFileUtil.base64ToMultipart(imgUrl);
        //更新头像地址
        String url = null;
        if (file != null && file.getSize() > 0) {
            String basePath = "avatar";
            try {
                url = aliOSSUpload(file, basePath);
                currentUser.setAvatar(url);
            } catch (IOException e) {
                e.printStackTrace();
                return new FebsResponse().put("code",501).message("图片上传失败！");
            }
        }
        if (userService.updateById(currentUser)) {
            return new FebsResponse().success().message("用户头像更新成功").data(url);
        } else {
            return new FebsResponse().fail().message("更新失败！");
        }
    }

    /**
     * @ Description: 更新密码
     * @ Param: [password：原密码, passwordNew：新密码]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 王珂
     * @ Date: 2020/6/29
     */
    @PostMapping("updatePwd")
    @ControllerEndpoint(operation = "修改老师密码", exceptionMessage = "修改老师密码失败")
    public FebsResponse updatePwd(String password,String passwordNew){
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        if (currentUser.getPassword().equals(MD5Util.encrypt(currentUser.getUsername(), password))) {
            currentUser.setPassword(MD5Util.encrypt(currentUser.getUsername(), passwordNew));
            userService.updateById(currentUser);
            return new FebsResponse().success().message("密码修改成功！");
        } else {
            return new FebsResponse().fail().message("原密码错误，修改失败！");
        }
    }

    @GetMapping(value = "/preview")
    @ControllerEndpoint(operation = "处理pdf成功",exceptionMessage = "处理pdf失败")
    public void pdfStreamHandler(HttpServletRequest request, HttpServletResponse response) {
        //PDF文件地址
        File file = new File(System.getProperty("user.dir")+"/src/main/resources/static/api/aaa.pdf");
        if (file.exists()) {
            byte[] data = null;
            FileInputStream input=null;
            try {
                input= new FileInputStream(file);
                data = new byte[input.available()];
                input.read(data);
                response.getOutputStream().write(data);
            } catch (Exception e) {
                System.out.println("pdf文件处理异常：" + e);
            }finally{
                try {
                    if(input!=null){
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }





}
