package cc.mrbird.febs.api.teacher;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/28/15:47
 * @ Description: 实验统计页面接口
 */
@RestController
@RequestMapping("/api/teacherStatistics")
public class TeacherStatisticsApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IUserService userService;
    @Resource
    private IUserCommitService userCommitService;
    @Resource
    private IExperimentProjectService experimentProjectService;


    /**
     * @ Description: 实验统计数据接口
     * @ Param: [deptId, projectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/28
     */
    @GetMapping("/getStatistics")
    @ControllerEndpoint(operation = "查询实验统计数据", exceptionMessage = "查询实验统计数据失败")
    public FebsResponse getStatistics(Long deptId, Long projectId) {
        Map<String,Object> map = new HashMap<>();
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login().message("您还没有进行登录，无法进行查询操作！");
        }
        try {
            //查询班级总人数
            List<User> studentByDeptIdList = userService.getStudentByDeptId(deptId);
            map.put("total_dept", studentByDeptIdList.size());
            //查询已提交人数
            LambdaQueryWrapper<UserCommit> queryWrapper = new QueryWrapper<UserCommit>().lambda()
                    .eq(UserCommit::getDeptId, deptId)
                    .eq(UserCommit::getProjectId, projectId)
                    .eq(UserCommit::getUserType,1)
                    .eq(UserCommit::getTeacherId, currentUser.getUserId())
                    .eq(UserCommit::getIsDel, 1)
                    .eq(UserCommit::getUseType, 1);
            List<UserCommit> commitList = userCommitService.list(queryWrapper);
            map.put("has_submit", commitList.size());
            queryWrapper.isNotNull(UserCommit::getScore).eq(UserCommit::getIsCommit, 2);
            //查询已批阅数量
            List<UserCommit> userCommitList = userCommitService.list(queryWrapper);
            map.put("has_read", userCommitList.size());
            return new FebsResponse().success().data(map);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherStatistics/getStatistics,实验统计数据接口查询失败：" + e.getMessage());
            return new FebsResponse().fail().message("查询失败！");
        }
    }

    /**
     * @ Description: 查询实验提交统计数据
     * @ Param: [deptId, projectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/29
     */
    @GetMapping("/listCommit")
    @ControllerEndpoint(operation = "查询实验提交数据", exceptionMessage = "查询实验统计提交数据失败")
    public FebsResponse listCommit(QueryRequest queryRequest, Long deptId, Long projectId, Long courseId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(deptId) || IntegerUtils.isLongBlank(projectId) || IntegerUtils.isLongBlank(courseId)) {
            return new FebsResponse().no_param();
        }
        try {
            List<UserCommit> userCommits = userCommitService.listCommit(queryRequest.getPageNum(), queryRequest.getPageSize(), deptId, projectId, courseId);
            for (UserCommit userCommit : userCommits) {
                if (userCommit.getIsCommit() == null) {
                    userCommit.setIsCommit(0);
                }
                if (userCommit.getProjectName() == null) {
                    userCommit.setProjectName(experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda()
                            .eq(ExperimentProject::getProjectId, projectId)).getProjectName());
                }
            }
            Integer count = userCommitService.listCommitCount(queryRequest.getPageNum(), queryRequest.getPageSize(), deptId, projectId, courseId);
            return new FebsResponse().success().message("查询成功！").data(userCommits).put("total", count);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherStatistics/listCommit,查询实验提交统计数据查询失败：" + e.getMessage());
            return new FebsResponse().fail().message("查询失败！");
        }
    }

    /**
     * @ Description: 下载实验报告接口
     * @ Param: [commitId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/29
     */
    @GetMapping("/downloadReport")
    @ControllerEndpoint(operation = "下载实验报告", exceptionMessage = "实验报告下载失败")
    public FebsResponse downloadReport(Long commitId) {
        if (IntegerUtils.isLongBlank(commitId)) {
            return new FebsResponse().no_param();
        }
        if (getCurrentUser() == null){
            return new FebsResponse().no_login();
        }
        try {
            UserCommit userCommit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getCommitId, commitId).select(UserCommit::getReportUrl));
            if (userCommit == null && userCommit.getReportUrl() == null) {
                return Objects.requireNonNull(new FebsResponse().put("code", 501)).message("参数错误或者数据不存在，下载失败！");
            }
            String url = userCommit.getReportUrl();
            downloadOss(url);
            return new FebsResponse().success().message("下载成功！");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherStatistics/downloadReport,实验报告下载失败：" + e.getMessage());
            return new FebsResponse().fail().message("下载失败！");
        }
    }

    /**
     * @ Description: 批阅报告接口
     * @ Param: [commitId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/29
     */
    @GetMapping("/readCommit")
    @ControllerEndpoint(operation = "批阅实验报告", exceptionMessage = "实验报告批阅失败")
    public FebsResponse readCommit(Long commitId, Double score) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(commitId) || score == null) {
            return new FebsResponse().no_param();
        }
        try {
            if (userCommitService.getById(commitId).getIsCommit() == 2) {
                return Objects.requireNonNull(new FebsResponse().put("code", 501)).message("已评阅，无法重复批阅");
            }
            if (userCommitService.update(new UpdateWrapper<UserCommit>().lambda()
                    .set(UserCommit::getScore, score)
                    .set(UserCommit::getIsCommit, 2)
                    .set(UserCommit::getUpdateUser, currentUser.getTrueName())
                    .set(UserCommit::getUpdateTime, LocalDateTime.now())
                    .eq(UserCommit::getCommitId, commitId))) {
                return new FebsResponse().success().message("批阅成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherStatistics/readCommit,实验报告批阅失败：" + e.getMessage());
        }
        return new FebsResponse().fail().message("批阅失败！");
    }


    /**
     * @ Description: 查询实验报告批阅信息
     * @ Param: [commitId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/7/2
     */
    @GetMapping("/getRead")
    @ControllerEndpoint(operation = "查询实验报告批阅信息成功",exceptionMessage = "查询实验报告批阅信息失败")
    public FebsResponse getRead(Long commitId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isLongBlank(commitId)) {
            return new FebsResponse().no_param();
        }
        UserCommit userCommit = userCommitService.getById(commitId);
        return new FebsResponse().success().data(userCommit).message("查询成功！");
    }

}

