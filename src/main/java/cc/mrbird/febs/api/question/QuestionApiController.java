package cc.mrbird.febs.api.question;

import cc.mrbird.febs.api.question.entity.QuestionResult;
import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author : GuiZi_cheng
 * date : 2020/4/28/11:03
 * 思考题的接口
 */
@RestController
@RequestMapping("/api/question")
public class QuestionApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IExperimentQuestionService experimentQuestionService;
    @Resource
    private IExperimentProjectService experimentProjectService;
    @Resource
    private ICourseDeptService courseDeptService;
    @Resource
    private IDeptService deptService;
    @Resource
    private IExperimentAnswerService experimentAnswerService;
    @Resource
    private IUserService userService;
    @Resource
    private IExperimentRemarkService experimentRemarkService;

    /**
     *
     * @param projectId
     * @return
     */
    @RequestMapping("randomPublishQuestion")
    @ControllerEndpoint(operation = "随机发布十道题",exceptionMessage = "随机发布十道题失败")
    public FebsResponse randomPublishQuestion(@RequestParam(value = "projectId") Long projectId
            ,@RequestParam(value="type") Integer type){
        User currentUser = getCurrentUser();

        if (currentUser==null){
            return new FebsResponse().no_login();
        }

        List<ExperimentQuestion> list = experimentQuestionService.list(new QueryWrapper<ExperimentQuestion>()
                .lambda()
                .eq(ExperimentQuestion::getProjectId, projectId)
                .eq(ExperimentQuestion::getType, type)
                .last("ORDER BY RAND() LIMIT 10"));
        List<ExperimentQuestion> typeJudge = new ArrayList<>();
        List<ExperimentQuestion> typeChoose = new ArrayList<>();
        List<ExperimentQuestion> typeMoreChoose = new ArrayList<>();

        list.stream().forEach(question->{
            List<String> list1 = Arrays.asList(question.getContext().replace("&nbsp;", "").split("@@"));
            for (int i = 0; i < list1.size(); i++) {
                list1.set(i, Character.toUpperCase((char)(65+i))+"、" + list1.get(i));
            }
            question.setOptional(list1);
            if (question.getQuestionType()==2){
                typeJudge.add(question);
            }else if(question.getQuestionType()==3){
                typeChoose.add(question);
            }else if (question.getQuestionType()==4){
                typeMoreChoose.add(question);
            }
        });
        Map<String,Object> map = new HashMap<>();
        map.put("typeJudge",typeJudge);
        map.put("typeChoose",typeChoose);
        map.put("typeMoreChoose",typeMoreChoose);
        return new FebsResponse().success().data(map);
    }

    /**
     * @ Param projectId 项目id
     * @ Param queryRequest 分页参数
     * @ Param type 类型
     * @ Param questionName 题目名称
     * @return 获取全部的习题列表
     */
    @RequestMapping("/questionList")
    @ControllerEndpoint(operation = "获取题目列表",exceptionMessage = "获取题目列表失败")
    public FebsResponse questionList(Integer projectId, QueryRequest queryRequest, Integer type, String questionName) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (currentUser != null) {
            Page<ExperimentQuestion> page = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
            SortUtil.handlePageSort(queryRequest, page, "question_id", FebsConstant.ORDER_ASC, false);
            IPage<ExperimentQuestion> iPage = this.experimentQuestionService.page(page, new QueryWrapper<ExperimentQuestion>().lambda()
                    .eq(IntegerUtils.isNotBlank(type), ExperimentQuestion::getType, type)
                    .eq(IntegerUtils.isNotBlank(projectId), ExperimentQuestion::getProjectId, projectId)
                    .like(StringUtils.isNotBlank(questionName), ExperimentQuestion::getQuestionName, questionName)
            );
            List<ExperimentQuestion> questionList = iPage.getRecords();
            if (questionList != null) {
                questionList.forEach(i -> {
                    i.setOptional(Arrays.asList(i.getContext().replace("&nbsp;", "").split("@@")));
                });
            }
            return new FebsResponse().success().data(getDataTable(iPage)).message("查询数据成功");
        }
        return new FebsResponse().fail().message("数据参数异常");
    }


    /**
     * @ Description: 思考题的查询接口
     * @ Param: [projectId, queryRequest, type, questionName]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/7/4
     */
    @GetMapping("/questionThinkList")
    @ControllerEndpoint(exceptionMessage = "获取思考题失败",operation = "获取思考题成功")
    public FebsResponse questionThinkList(Integer projectId, QueryRequest queryRequest) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isBlank(projectId)) {
            return new FebsResponse().no_param();
        }
        Page<ExperimentQuestion> page = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
        SortUtil.handlePageSort(queryRequest, page, "question_id", FebsConstant.ORDER_ASC, false);
        IPage<ExperimentQuestion> iPage = this.experimentQuestionService.page(page, new QueryWrapper<ExperimentQuestion>().lambda()
                .eq(ExperimentQuestion::getType, 2)
                .eq(ExperimentQuestion::getProjectId, projectId));
        List<ExperimentQuestion> questionList = iPage.getRecords();
        List<ExperimentQuestion> typeJudge = new ArrayList<>();
        List<ExperimentQuestion> typeChoose = new ArrayList<>();
        List<ExperimentQuestion> typeMoreChoose = new ArrayList<>();
        for (ExperimentQuestion experimentQuestion : questionList) {
            List<String> list = Arrays.asList(experimentQuestion.getContext().replace("&nbsp;", "").split("@@"));
            for (int i = 0; i < list.size(); i++) {
                list.set(i, Character.toUpperCase((char)(65+i))+"、" + list.get(i));
            }
            experimentQuestion.setOptional(list);
            if (experimentQuestion.getQuestionType()==2){
                typeJudge.add(experimentQuestion);
            }else if(experimentQuestion.getQuestionType()==3){
                typeChoose.add(experimentQuestion);
            }else if (experimentQuestion.getQuestionType()==4){
                typeMoreChoose.add(experimentQuestion);
            }
        }
        Map<String,Object> map = new HashMap<>();
        map.put("typeJudge",typeJudge);
        map.put("typeChoose",typeChoose);
        map.put("typeMoreChoose",typeMoreChoose);
        return new FebsResponse().success().data(map).message("查询数据成功");
    }


    /**
     * @ Description: ，预习题的提交接口，接收前端数据
     * @ Param: [request]
     * @ return: java.lang.String
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     * 每个项目 每个学生只能答一次题
     * 即根据 项目ID 和 学生ID 查询数据 若有 代表已提交过一次 直接return
     * 因为参数是流的形式发送 没办法直接获取 projectId 故通过转换玩的json对象获取
     */
    @RequestMapping(value = "/questionCommit", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ControllerEndpoint(operation = "预习题提交成功",exceptionMessage = "预习题提交失败")
    public FebsResponse getByRequest(HttpServletRequest request) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        JSONArray jsonParam = this.getJSONParam(request);
        //判断用户是否已经提交过。
        JSONObject json = jsonParam.getJSONObject(0);
        ExperimentAnswer awe = JSON.toJavaObject(json, ExperimentAnswer.class);
        Long projectId = awe.getProjectId();
        ExperimentRemark answers = experimentRemarkService.getOne(new QueryWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType,1)
                .eq(ExperimentRemark::getDeptId, currentUser.getDeptId())
                .eq(ExperimentRemark::getType, 1)
                .eq(ExperimentRemark::getIsDel,1));
        if (answers!= null && answers.getScore() >= 60.0) {
            return new FebsResponse().put_code(501, "成绩合格,不可再次提交习题");
        }
        //查找出之前所有的预习题提交记录,将他们逻辑删除
        List<ExperimentRemark> list = experimentRemarkService.list(new QueryWrapper<ExperimentRemark>()
                .lambda()
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType, 1)
                .eq(ExperimentRemark::getDeptId, currentUser.getDeptId())
                .eq(ExperimentRemark::getType, 1));
        boolean flag = false;
        if(list!=null&&list.size()>0){
            for (ExperimentRemark experimentRemark : list) {
                if (experimentRemark.getIsCommit() == 2){
                    flag = true;
                    break;
                }
            }
        }
        if (flag){
            return new FebsResponse().fail().message("该实验已批阅,不可重复做题");
        }
        for (ExperimentRemark experimentRemark : list) {
            experimentRemarkService.removeById(experimentRemark.getRemarkId());
        }
        ExperimentRemark experimentRemark = new ExperimentRemark();
        experimentRemark.setType(1);
        experimentRemark.setCreateId(currentUser.getUserId());
        experimentRemark.setIsCommit(1);
        experimentRemark.setUserType(1);
        experimentRemark.setDeptId(currentUser.getDeptId());
        experimentRemark.setProjectId(awe.getProjectId());
        experimentRemark.setCreateTime(LocalDateTime.now());
        experimentRemark.setIsDel(1);
        experimentRemarkService.save(experimentRemark);

        Double score = 0.0;
        QuestionResult questionResult = new QuestionResult();
        List<Long> questionResultList = new ArrayList<>();
        List<ExperimentAnswer> experimentAnswerList = new ArrayList<>();
        for (int i = 0; i < jsonParam.size(); i++) {
            JSONObject jsonObject = jsonParam.getJSONObject(i);
            //json对象转Java实体类
            ExperimentAnswer answer = jsonObject.toJavaObject(jsonObject, ExperimentAnswer.class);
            double s = 0.0;
            answer.setCreateId(experimentRemark.getRemarkId());
            if (getScore(answer)) {
                score += 10.0;
                s += 10.0;
            } else {
                questionResultList.add(answer.getQuestionId());
            }
            answer.setUserType(1);
            answer.setScore(s);
            experimentAnswerList.add(answer);
        }
        experimentAnswerService.saveBatch(experimentAnswerList);
        questionResult.setErroQuestionList(questionResultList);
        questionResult.setScore(score);
        experimentRemarkService.update(new UpdateWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getRemarkId, experimentRemark.getRemarkId())
                .set(ExperimentRemark::getScore, score));
        return new FebsResponse().success().message("提交成功！").data(questionResult);
    }


    /**
     * 判断评分的接口
     * GuiZi_cheng
     * @ Param experimentAnswer
     * @return
     */
    public boolean getScore(ExperimentAnswer experimentAnswer) {
        ExperimentQuestion question = experimentQuestionService.getById(experimentAnswer.getQuestionId());
        //参考答案
        String answer = question.getAnswer().toUpperCase();

        //提交的答案
        String subAnswer = experimentAnswer.getContext().toUpperCase();
        String strAnswer = "";
        if (subAnswer.equals("A")) {
            strAnswer = "对";
        }
        if (subAnswer.equals("B")) {
            strAnswer = "错";
        }
        if (answer.equals(subAnswer) || answer.equals(strAnswer)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @ Description: 思考题的提交接口
     * @ Param: [request]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/30
     */
    @RequestMapping("/shortAnswerSub")
    @ControllerEndpoint(operation = "思考题提交成功",exceptionMessage = "思考题提交失败")
    public FebsResponse shortAnswerSub(HttpServletRequest request) {
        User currentUser = getCurrentUser();
        JSONArray jsonParam = this.getJSONParam(request);
        //判断如果思考题提交过之后不让再次提交
        JSONObject json = jsonParam.getJSONObject(0);
        ExperimentAnswer awe = json.toJavaObject(json, ExperimentAnswer.class);
        Long projectId = awe.getProjectId();
        List<ExperimentRemark> answers = experimentRemarkService.list(new QueryWrapper<ExperimentRemark>().lambda()
                .eq(ExperimentRemark::getCreateId, currentUser.getUserId())
                .eq(ExperimentRemark::getProjectId, projectId)
                .eq(ExperimentRemark::getUserType,1)
                .eq(ExperimentRemark::getDeptId, currentUser.getDeptId())
                .eq(ExperimentRemark::getIsDel,1)
                .eq(ExperimentRemark::getType, 2));
        if (answers.size() > 0) {
            return new FebsResponse().fail().message("您已提交，请勿重复提交");
        }
        ExperimentRemark experimentRemark = new ExperimentRemark();
        experimentRemark.setCreateId(currentUser.getUserId());
        experimentRemark.setIsCommit(1);
        experimentRemark.setDeptId(currentUser.getDeptId());
        experimentRemark.setProjectId(awe.getProjectId());
        experimentRemark.setType(2);
        experimentRemark.setUserType(1);
        experimentRemark.setCreateTime(LocalDateTime.now());
        experimentRemarkService.save(experimentRemark);
        /**
         * 业务处理代码
         */
        QuestionResult questionResult = new QuestionResult();
        if (jsonParam != null) {
            List<ExperimentAnswer> experimentAnswerList = new ArrayList<>();
            double scoreOne = 100/jsonParam.size();
            questionResult.setScore(0.0);
            List<Long> longList = new ArrayList<>();
            for (int i = 0; i < jsonParam.size(); i++) {
                JSONObject jsonObject = jsonParam.getJSONObject(i);
                //json对象转Java实体类
                ExperimentAnswer answer = jsonObject.toJavaObject(jsonObject, ExperimentAnswer.class);
                answer.setCreateId(experimentRemark.getRemarkId());
                answer.setUserType(1);
                answer.setDeptId(experimentRemark.getDeptId());
                ExperimentQuestion experimentQuestion = experimentQuestionService.getOne(new QueryWrapper<ExperimentQuestion>().lambda()
                        .eq(ExperimentQuestion::getQuestionId, answer.getQuestionId())
                        .select(ExperimentQuestion::getAnswer));
                //如果是判断题，判断对错并设置分数
                if (answer.getQuestionType()==2){
                    if (getScore(answer)) {
                        answer.setScore(scoreOne);
                        questionResult.setScore(questionResult.getScore()+scoreOne);
                    } else {
                        answer.setScore(0.0);
                        longList.add(answer.getQuestionId());
                    }
                    //其他题型设置
                }else {
                    if (answer.getContext().equals(experimentQuestion.getAnswer())){
                        answer.setScore(scoreOne);
                        questionResult.setScore(questionResult.getScore()+scoreOne);
                    }else {
                        answer.setScore(0.0);
                        longList.add(answer.getQuestionId());
                    }
                }
                experimentAnswerList.add(answer);

            }
            experimentAnswerService.saveBatch(experimentAnswerList);
            experimentRemarkService.update(new UpdateWrapper<ExperimentRemark>().lambda()
                    .set(ExperimentRemark::getScore,questionResult.getScore())
                    .eq(ExperimentRemark::getRemarkId,experimentRemark.getRemarkId()));
            questionResult.setErroQuestionList(longList);
        }
        return new FebsResponse().success().data(questionResult).message("提交成功！");
    }



    /**
     * 获取简答题列表
     */
    @RequestMapping("/getShortAnswerList")
    @ControllerEndpoint(operation = "获取简答题成功",exceptionMessage = "获取简答题失败")
    public FebsResponse getShortAnswerList(QueryRequest request, Integer deptId) {
        User user = getCurrentUser();
        if (user==null){
            return new FebsResponse().no_login();
        }
        Page page = new Page(request.getPageNum(), request.getPageSize());
        IPage answerPage = experimentAnswerService.page(page, new QueryWrapper<ExperimentAnswer>().select("DISTINCT create_id,dept_id,project_id").eq("question_type", 2).eq(IntegerUtils.isNotBlank(deptId), "dept_id", deptId).eq("is_del", 1));
        List<ExperimentAnswer> list = answerPage.getRecords();
        if (list != null) {
            List<ExperimentAnswer> newList = new ArrayList<>();
            list.forEach(i -> {
                User userById = userService.getById(i.getCreateId());
                if (userById != null) {
                    i.setCreateUserName(userById.getUsername());
                    i.setDeptName(deptService.getById(i.getDeptId()).getDeptName());//创建人的ID
                    i.setProjectName(experimentProjectService.getById(i.getProjectId()).getProjectName());
                    i.setCreateTime(LocalDateTime.now());
                    Double totalScore = experimentAnswerService.getTotalScore(i.getCreateId(), i.getProjectId(), i.getDeptId());
                    i.setScore(totalScore);
                    newList.add(i);
                }
            });
            answerPage.setRecords(newList);
        }
        return new FebsResponse().success().data(getDataTable(answerPage));
    }


}
