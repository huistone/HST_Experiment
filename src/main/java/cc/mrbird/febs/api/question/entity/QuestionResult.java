package cc.mrbird.febs.api.question.entity;

import lombok.Data;

import java.util.List;

@Data
public class QuestionResult {
    private List<Long> erroQuestionList;
    private Double score;
}
