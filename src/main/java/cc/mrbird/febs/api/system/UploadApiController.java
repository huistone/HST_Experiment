package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @ ClassName UploadApiController
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/3/17 15:35
 * @ Version 1.0
 */
@RestController
@RequestMapping("/api/upload")
public class UploadApiController extends BaseController {


    /**
     * 上传图片接口
     * @param file
     * @return
     */
    @PostMapping("/getPictureUrl")
    public FebsResponse getPicUrl(MultipartFile file) {
        String basePath = "picture";
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(url);
        return new FebsResponse().success().data(url);
    }

}
