package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * @ClassName PdfApiController
 * @ Description TODO
 * @Author admin
 * @Date 2020/10/20 16:11
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/pdf")
public class PdfApiController extends BaseController {

    @Resource
    private IExperimentProjectService experimentProjectService;

    @GetMapping("toPdf")
    public String toPdf(){
        return "api/web/viewer";
    }

//    @GetMapping("/prePdfResp")
//    @ResponseBody
//    public FebsResponse previewPdf(Long projectId, HttpServletResponse response){
//        ExperimentProject byId = experimentProjectService.getById(projectId);
//        String projectContentUrl = byId.getProjectContentUrl();
//        URL url = null;
//        try {
//            byte[] data = null;
//            url = new URL(projectContentUrl);
//            InputStream inputStream = new BufferedInputStream(url.openStream());
//            response.setHeader("Content-Disposition", "attachment;fileName="+byId.getProjectName());
//            response.setHeader("Access-Control-Allow-Origin", "*");
//            response.setContentType("application/octet-stream");
//            data = new byte[inputStream.available()];
//            inputStream.read(data);
//            ServletOutputStream outputStream = response.getOutputStream();
//            IOUtils.write(IOUtils.toByteArray(inputStream), outputStream);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String fileStr = getFileStr(projectContentUrl);
//        return new FebsResponse().put("base64",fileStr);
//    }
    @GetMapping("/prePdfResp")
    @ResponseBody
    @CrossOrigin
    public void previewPdf(Long projectId, HttpServletResponse response){
        ExperimentProject byId = experimentProjectService.getById(projectId);
        String projectContentUrl = byId.getProjectContentUrl();
        URL url = null;
        InputStream fis = null;
        try {
            //打开请求连接
            url = new URL(projectContentUrl);
            URLConnection connection = url.openConnection();
            HttpURLConnection conn=(HttpURLConnection) connection;
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            conn.connect();
            response.setHeader( "Content-Disposition", "attachment;filename=" + URLEncoder.encode(byId.getProjectName(), "UTF-8"));// 设置在下载框默认显示的文件名
            response.setContentType("application/octet-stream");// 指明response的返回对象是文件流
//            response.setContentType("application/pdf;charset=UTF-8");
//            response.setHeader("Access-Control-Allow-Origin", "*");
            ServletOutputStream sos=response.getOutputStream();
            fis=conn.getInputStream();
            int b;
            while((b=fis.read())!=-1){
                sos.write(b);
            }
            sos.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("prePdf")
    public FebsResponse getProjectContentUrl(Long projectId){
        User currentUser = getCurrentUser();

        if (currentUser == null){
            return new FebsResponse().no_login();
        }

        if(IntegerUtils.isBlank(projectId.intValue())){
            return new FebsResponse().no_param();
        }

        ExperimentProject one = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, projectId)
                .select(ExperimentProject::getProjectContentUrl));

        if (StringUtils.isBlank(one.getProjectContentUrl())){
            return new FebsResponse().fail().message("该实验暂无文档");
        }
        return new FebsResponse().success().data(one.getProjectContentUrl());
    }


    /**
     * 文件转化成base64字符串
     * 将文件转化为字节数组字符串，并对其进行Base64编码处理
     */
    public static String getFileStr(String filePath) {
        byte[] data = null;
        try {
            URL url = new URL(filePath);
            InputStream inputStream = new BufferedInputStream(url.openStream());
            data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        // 返回 Base64 编码过的字节数组字符串
        return encoder.encode(data);
    }
}
