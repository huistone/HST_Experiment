package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.Feedback;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IFeedbackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@ResponseBody
@Slf4j
@RequestMapping("/api/feedback")
public class FeedBackApiController extends BaseController {

    @Resource
    private IFeedbackService feedbackService;

    /**
     * wangke 老师端意见反馈接口
     * @ Param feedback
     * @return
     */
    @PostMapping("/addFeeBackWithTeacher")
    @ControllerEndpoint(operation = "添加老师意见反馈",exceptionMessage = "添加老师意见反馈失败")
    public FebsResponse addFeeBackWithTeacher(Feedback feedback){
        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }
        Feedback feedback1 = saveInfo(currentUser, feedback);
        try {
            feedbackService.saveOrUpdate(feedback1);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/feedback/addFeeBackWithTeacher:"+e.getMessage());
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("/addFeedback")
    @ControllerEndpoint(operation = "添加意见反馈",exceptionMessage = "添加意见反馈失败")
    public FebsResponse addFeedBack(Feedback feedback) {
        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }
        Feedback feedback1 = saveInfo(currentUser, feedback);
        try {
            feedbackService.saveOrUpdate(feedback1);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/feedback/addFeeback:"+e.getMessage());
            return new FebsResponse().fail();
        }
    }

    public Feedback saveInfo(User currentUser,Feedback feedback){
        feedback.setCreateId(currentUser.getUserId().intValue());
        feedback.setCreateName(currentUser.getUsername());
        feedback.setCreateTime(LocalDateTime.now());
        feedback.setCreateName(currentUser.getUsername());
        feedback.setState(BaseController.FAIL);
        return feedback;
    }
}
