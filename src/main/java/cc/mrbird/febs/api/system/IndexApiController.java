package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.monitor.service.ILoginLogService;
import cc.mrbird.febs.system.service.IUserService;
import com.sun.corba.se.spi.activation.BadServerDefinition;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/23/17:15
 * @ Description: 查询系统首页接口
 */
@RestController
@RequestMapping("/api/index")
public class IndexApiController extends BaseController {

    @Resource
    private IUserService userService;

    @Resource
    private ILoginLogService loginLogService;


    /**
    * @ Description: 查询系统访问记录
    * @ Param: [username]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/23
    */
    @GetMapping("index/{username}")
    @ControllerEndpoint(operation = "查询系统访问记录成功",exceptionMessage = "查询系统访问记录失败")
    public FebsResponse index(@NotBlank(message = "{required}") @PathVariable String username) {
        if(getCurrentUser()==null){
            return new FebsResponse().no_login();
        }


        // 更新登录时间
        this.userService.updateLoginTime(username);
        Map<String, Object> data = new HashMap<>();
        // 获取系统访问记录
        Long totalVisitCount = this.loginLogService.findTotalVisitCount();
        data.put("totalVisitCount", totalVisitCount);
        Long todayVisitCount = this.loginLogService.findTodayVisitCount();
        data.put("todayVisitCount", todayVisitCount);
        Long todayIp = this.loginLogService.findTodayIp();
        data.put("todayIp", todayIp);
        return new FebsResponse().success().data(data);
    }


}
