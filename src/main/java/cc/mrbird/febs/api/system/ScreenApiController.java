package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.BASE64DecodedMultipartFileUtil;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.Screen;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.vo.CourseScreenVo;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IScreenService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName ScreenApiController
 * @Description TODO
 * @Author admin
 * @Date 2020/12/30 17:15
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/screen")
public class ScreenApiController extends BaseController {

    @Resource
    private IScreenService screenService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ICourseService courseService;

    /**
     * @Author wangke
     * @Description 上传实验截图
     * @Date 15:29 2020/12/31
     * @Param
     * @return
     */
    @RequestMapping("uploadScreen")
    @ControllerEndpoint(operation = "上传实验截图",exceptionMessage = "上传实验截图失败")
    public FebsResponse uploadScreen(Integer projectId,String pictureUrl){

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }
        Screen screen = new Screen();
        screen.setUserId(currentUser.getUserId().intValue());
        screen.setProjectId(projectId);
        Long courseId = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, projectId)
                .select(ExperimentProject::getCourseId)).getCourseId();

        screen.setCourseId(courseId.intValue());

        MultipartFile file = BASE64DecodedMultipartFileUtil.base64ToMultipart(pictureUrl);
        //更新头像地址
        String url = null;
        if (file != null && file.getSize() > 0) {
            String basePath = "screen";
            try {
                url = aliOSSUpload(file, basePath);
                screen.setPictureUrl(url);
            } catch (IOException e) {
                e.printStackTrace();
                return new FebsResponse().fail().message("图片上传失败！");
            }
        }
        screen.setCreateTime(LocalDateTime.now());
        screenService.save(screen);
        return new FebsResponse().success();
    }

    /**
     * @Author wangke
     * @Description 获取课程bar
     * @Date 16:30 2020/12/31
     * @Param
     * @return
     */
    @RequestMapping("getCourseList")
    @ControllerEndpoint(operation = "获取实验截图的课程",exceptionMessage = "获取实验截图的课程失败")
    public FebsResponse getCourseList(){
        User currentUser = getCurrentUser();

        if (currentUser == null){
            return new FebsResponse().no_login();
        }

        List<Screen> list = screenService.list(new QueryWrapper<Screen>()
                .lambda()
                .eq(Screen::getUserId, currentUser.getUserId())
                .select( Screen::getCourseId));
        if (list.size() == 0){
            return new FebsResponse().fail().message("该用户暂无上传截图");
        }


        List<Integer> courseIds = list.stream().distinct().map(screen -> {
            return screen.getCourseId();
        }).collect(Collectors.toList());
        List<Course> list1 = courseService.list(new QueryWrapper<Course>()
                .lambda()
                .in(courseIds.size() > 0, Course::getCourseId, courseIds)
                .select(Course::getCourseName, Course::getCourseId));

//        List<CourseScreenVo> collect = list1.stream().map(course -> {
//            CourseScreenVo courseScreenVo = new CourseScreenVo();
//            BeanUtils.copyProperties(course, courseScreenVo);
//            return courseScreenVo;
//        }).collect(Collectors.toList());
//        for (CourseScreenVo courseScreenVo : collect) {
//            List<Screen> list2 = screenService.list(new QueryWrapper<Screen>()
//                    .lambda()
//                    .eq(Screen::getCourseId, courseScreenVo.getCourseId())
//                    .eq(Screen::getUserId,currentUser.getUserId())
//                    .select(Screen::getProjectId));
//            List<Integer> projectIds = list2.stream().map(screen -> {
//                return screen.getProjectId();
//            }).collect(Collectors.toList());
//            List<ExperimentProject> list3 = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
//                    .lambda()
//                    .in(projectIds.size() > 0, ExperimentProject::getProjectId, projectIds)
//                    .select(ExperimentProject::getCourseId, ExperimentProject::getProjectName, ExperimentProject::getProjectPictureUrl));
//            courseScreenVo.setChildren(list3);
//        }
        return new FebsResponse().success().data(list1);
    }

    /**
     * @Author wangke
     * @Description 获取课程下的实验
     * @Date 16:30 2020/12/31
     * @Param
     * @return
     */
    @RequestMapping("getProjectList")
    @ControllerEndpoint
    public FebsResponse getProjectList(Integer courseId){

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }

        if (IntegerUtils.isBlank(courseId)){
            return new FebsResponse().no_param();
        }

        List<Screen> projectIds = screenService.list(new QueryWrapper<Screen>()
                .lambda()
                .eq(Screen::getCourseId, courseId)
                .eq(Screen::getUserId,currentUser.getUserId())
                .select(Screen::getProjectId));
        if (projectIds.size() == 0){
            return new FebsResponse().fail().message("该用户该实验暂无截图");
        }

        List<Integer> collect = projectIds.stream().distinct().map(s->{
            return s.getProjectId();
        }).collect(Collectors.toList());

        List<ExperimentProject> list = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .in(collect.size() > 0, ExperimentProject::getProjectId, collect)
                .select(ExperimentProject::getProjectId, ExperimentProject::getProjectName, ExperimentProject::getProjectPictureUrl));

        return new FebsResponse().success().data(list);
    }


    @RequestMapping("getProjectScreenList")
    @ControllerEndpoint(operation = "获取当前用户的实验截图列表",exceptionMessage = "获取当前用户的实验截图列表失败")
    public FebsResponse getProjectScreenList(Integer projectId){

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }

        if (IntegerUtils.isBlank(projectId)){
            return new FebsResponse().no_param();
        }

        List<Screen> list = screenService.list(new QueryWrapper<Screen>()
                .lambda()
                .eq(Screen::getUserId, currentUser.getUserId())
                .eq(Screen::getProjectId, projectId)
                .orderByDesc(Screen::getCreateTime));
        for (Screen screen : list) {
            ArrayList<String> objects = new ArrayList<>();
            objects.add(screen.getPictureUrl());
            screen.setImgList(objects);
        }
        HashMap<String, List> map = new LinkedHashMap<>();

        Set<String> timeList = new LinkedHashSet<>();
        for (Screen screen : list) {
            LocalDateTime createTime = screen.getCreateTime();
            String s = DateUtil.formatFullTime(createTime, DateUtil.FULL_TIME_PATTERN_);
            timeList.add(s);
            map.put(s,new ArrayList<Screen>());
        }

        for (String time : timeList) {
            for (Screen screen : list) {
                LocalDateTime createTime = screen.getCreateTime();
                String s = DateUtil.formatFullTime(createTime, DateUtil.FULL_TIME_PATTERN_);
                if (time.equals(s)){
                    List list2 = map.get(time);
                    list2.add(screen);
                    map.put(time,list2);
                }
            }
        }
        ArrayList<Object> timeArrays = new ArrayList<>();
        HashMap<String, Object> lastMap = null;
        Set<String> strings = map.keySet();
         for (String string : strings) {
            lastMap = new HashMap<>();
            List list1 = map.get(string);
            lastMap.put("date",string);
            lastMap.put("imgList",list1);
            timeArrays.add(lastMap);
        }

        return new FebsResponse().success().data(timeArrays);
    }

}
