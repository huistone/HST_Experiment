/*
package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.utils.MemberRulesUtil;
import cc.mrbird.febs.rabbit.pojo.Mail;
import cc.mrbird.febs.rabbit.service.SendEmailService;
import cc.mrbird.febs.rabbit.util.MailUtil;
import cc.mrbird.febs.system.entity.User;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

import static cc.mrbird.febs.common.utils.FebsUtil.getCurrentUser;

*/
/**
 * @ClassName EmailApiController
 * @ Description 发送邮件接口
 * @Author 王珂
 * @Data 2020/7/13 17:38
 * @Version 1.0
 *//*


//@RequestMapping("/api/email")
@RestController
public class EmailApiController extends BaseController {

    @Resource
    private SendEmailService sendEmailService;

    @Resource
    private MailUtil mailUtil;

    @Resource
    private RedisService redisService;

    @RequestMapping("/send")
    @ControllerEndpoint(operation = "发送邮件成功",exceptionMessage = "发送邮件失败")
    public FebsResponse sendEmail(Mail mail){
        try {
            sendEmailService.send(mail);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("邮件发送失败");
        }
        return new FebsResponse().success().message("邮件发送成功");
    }



    @RequestMapping("sendEmailByTemplate1")
    public FebsResponse sendEmailByTemplate1(String emailAddr){
        User student = getCurrentUser();
        if (student == null ){
            return new FebsResponse().no_login();
        }
        String emailCode = MemberRulesUtil.createRandom(true, 4);
        Mail mail = new Mail();
        mail.setContent(emailCode);
        mail.setTo(emailAddr);
        mail.setTitle("修改邮箱验证");
        try {
            mailUtil.thymeleafEmail(mail,"febs/views/email/emailTemplate.html");
            redisService.set(emailAddr,emailCode,300L);
            return new FebsResponse().success();
        } catch (MessagingException e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("sendEmailByTemplate")
    public FebsResponse sendEmailByTemplate(String emailAddr){
        User student = getCurrentUser();
        if (student == null ){
            return new FebsResponse().no_login();
        }
        String emailCode = MemberRulesUtil.createRandom(true, 4);
        Mail mail = new Mail();
        mail.setContent(emailCode);
        mail.setTo(emailAddr);
        mail.setTitle("修改邮箱验证");
        try {
            sendEmailService.sendHtmlMail(mail);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @GetMapping("validEmail")
    public FebsResponse validEmail(String emailAddr,String code){
        if(StringUtils.isNotBlank(emailAddr) || StringUtils.isNotBlank(code) ){
            return new FebsResponse().no_login();
        }
        if(emailAddr == null || code ==null || emailAddr == ""|| code ==""){
            return new FebsResponse().no_login();
        }

        String smsCode = (String) redisService.get(emailAddr);
        if (code==null){
            return new FebsResponse().fail().message("验证码输入错误！");
        }
        if (code.equals(smsCode)){
            return new FebsResponse().success().message("验证码输入正确！");
        }else {
            return new FebsResponse().fail().message("验证码输入错误！");
        }
    }


    @GetMapping("sendMultiEmail")
    public FebsResponse sendMultiEmail(){
        List<String> list = new ArrayList<>();
        list.add("2115163490@qq.com");
        list.add("1127689762@qq.com");
        list.add("3215534141@qq.com");
        list.add("2226164204@qq.com");
        list.add("1093577159@qq.com");
        list.add("952088702@qq.com");
        list.add("2803224774@qq.com");
        list.add("2772673016@qq.com");
        list.add("1262467270@qq.com");
        list.add("1127689762@qq.com");
        list.add("862777011@qq.com");
        list.add("814375158@qq.com");
        list.add("985055395@qq.com");
        list.add("2115163490@qq.com");
        Mail mail = new Mail();
        long startTime = System.currentTimeMillis();
        for (String s : list) {
            mail.setTo(s);
            mail.setContent("非诚勿扰");
            sendEmailService.send(mail);
        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);
        return new FebsResponse().success().data("发送成功");
    }


}
*/
