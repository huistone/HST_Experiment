package cc.mrbird.febs.api.system;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.SysConfig;
import cc.mrbird.febs.system.service.ISysConfigService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/05/17:06
 * @ Description:
 */
@RestController
@RequestMapping("/api/sysConfig")
public class SysConfigApiController extends BaseController {

    @Resource
    private ISysConfigService configService;


    @RequestMapping("getViewData")
    @ControllerEndpoint(operation = "查询系统配置成功",exceptionMessage = "查询系统配置失败")
    public FebsResponse getViewData(String configKey){
        Map<String, List> map = new HashMap<>();
        /**
         * 以此类推 把前端需要的东西封装成为一个Map
         */
        String[] configKeys = configKey.split(",");
        List<SysConfig> sysConfigList = configService.list();
        for (int i = 0;i<configKeys.length;i++){
            List<SysConfig> satisfy = new ArrayList<>();
            for (SysConfig s:sysConfigList) {
                if(s.getConfigKey().equals(configKeys[i])){
                    satisfy.add(s);
                }
            }
            map.put(configKeys[i],satisfy);
        }
        return new FebsResponse().success().data(map);
    }


}
