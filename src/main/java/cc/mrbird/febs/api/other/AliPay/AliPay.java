//package cc.mrbird.febs.api.other.AliPay;
//
//import cc.mrbird.febs.common.configure.AlipayConfig;
//import cc.mrbird.febs.common.utils.FebsUtil;
//import com.alipay.api.AlipayApiException;
//import com.alipay.api.AlipayClient;
//import com.alipay.api.DefaultAlipayClient;
//import com.alipay.api.internal.util.AlipaySignature;
//import com.alipay.api.request.AlipayTradePagePayRequest;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.time.LocalDateTime;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//
///**
// * Created with IntelliJ IDEA.
// *
// * @ Auther: 马超伟
// * @ Date: 2020/07/17/14:16
// * @ Description: 阿里支付工具类
// */
//@Controller
//@RequestMapping("/api/macw/aliPay")
//public class AliPay {
//
//    private Logger logger = LoggerFactory.getLogger(getClass());
//
//    /**
//     * 去支付
//     *
//     * @ Param orderNo   订单编号
//     * @ Param money     订单金额
//     * @ Param orderName 订单名称
//     * @ Param orderDesp 订单描述
//     * @return
//     * @throws IOException
//     * @throws AlipayApiException
//     */
//    @RequestMapping("toPay")
//    public String toAlipay(HttpServletResponse response,String orderNo, String money, String orderName, String orderDesp, Model model) throws IOException, AlipayApiException {
//
//        //获得初始化的AlipayClient
//        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);
//
//        //设置请求参数
//        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
//        alipayRequest.setReturnUrl(AlipayConfig.return_url);
//        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);
//
//        //商户订单号，商户网站订单系统中唯一订单号，必填
//        String out_trade_no = orderNo;
//        //付款金额，必填
//        String total_amount = money;
//        //订单名称，必填
//        String subject = orderName;
//        //商品描述，可空
//        String body = orderDesp;
//
//        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
//                + "\"total_amount\":\"" + total_amount + "\","
//                + "\"subject\":\"" + subject + "\","
//                + "\"body\":\"" + body + "\","
//                + "\"timeout_express\":\"10m\","    //设置十分钟内支付有效
//                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
//
//        //若想给BizContent增加其他可选请求参数，以增加自定义超时时间参数timeout_express来举例说明
//        //alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
//        //		+ "\"total_amount\":\""+ total_amount +"\","
//        //		+ "\"subject\":\""+ subject +"\","
//        //		+ "\"body\":\""+ body +"\","
//        //		+ "\"timeout_express\":\"10m\","
//        //		+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
//        //请求参数可查阅【电脑网站支付的API文档-alipay.trade.page.pay-请求参数】章节
//
//        //请求
//        String result = alipayClient.pageExecute(alipayRequest).getBody();
//
//        //输出
////        out.println(result);
//        logger.debug("result=====-------------" + result);
//
//        response.setContentType("text/html;charset=UTF-8");
//        PrintWriter out = response.getWriter();
//        out.print(result);
//        return null;
//
////        model.addAttribute("pay", result);
////        return FebsUtil.view("others/aliPay/toPay");
//////        return mv;
//    }
//
//
//    /**
//     * 支付成功后的 异步回调
//     *
//     * @ Param request
//     * @throws AlipayApiException
//     * @throws UnsupportedEncodingException
//     */
//    //注意异步返回结果通知是以post请求形式返回的
//    @RequestMapping("notifyUrl")
//    public void notify_url(HttpServletRequest request) throws AlipayApiException, UnsupportedEncodingException {
//        //获取支付宝POST过来反馈信息
//        Map<String, String> params = convertRequestParamsToMap(request);
//        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名
//
//        //——请在这里编写您的程序（以下代码仅作参考）——
//	/* 实际验证过程建议商户务必添加以下校验：
//	1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
//	2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
//	3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
//	4、验证app_id是否为该商户本身。
//	*/
//        if (signVerified) {//验证成功
//            //商户订单号
//            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
//            logger.debug("商户订单号==============================" + out_trade_no);
//            //支付宝交易号
//            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");
//            logger.debug("支付宝交易号==============================" + trade_no);
//
//            //交易状态
//            String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");
//
//            /**
//             * 收到TRADE_FINISHED请求后，这笔订单就结束了，支付宝不会再主动请求商户网站了；
//             * 收到TRADE_SUCCESS请求后，后续一定还有至少一条通知记录，即TRADE_FINISHED。
//             * 所以，在做通知接口时，切记使用判断订单状态用或的关系。
//             */
//            if (trade_status.equals("TRADE_FINISHED")) {
//                //判断该笔订单是否在商户网站中已经做过处理
//                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
//                //如果有做过处理，不执行商户的业务程序
//                /**
//                 * TRADE_SUCCESS状态代表了充值成功，也就是说钱已经进了支付宝（担保交易）或卖家（即时到账）；
//                 * 这时候，这笔交易应该还可以进行后续的操作（比如三个月后交易状态自动变成TRADE_FINISHED），
//                 * 因为整笔交易还没有关闭掉，也就是说一定还有主动通知过来。
//                 */
//
//                //注意：
//                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
//            } else if (trade_status.equals("TRADE_SUCCESS")) {
//                //判断该笔订单是否在商户网站中已经做过处理
//                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
//                //如果有做过处理，不执行商户的业务程序
//                /**
//                 * 而TRADE_FINISHED代表了这笔订单彻底完成了，不会再有任何主动通知过来了。
//                 */
//                //注意：
//                //付款完成后，支付宝系统发送该交易状态通知
//
//            }
////成功
//            //更新订单状态
////            OrderBase orderBase = iOrderBaseService.getOne(new QueryWrapper<OrderBase>().lambda().eq(OrderBase::getOrderNo, out_trade_no));
////            orderBase.setPayTime(LocalDateTime.now());
////            orderBase.setState(OrderState.WITHSEND.getState());
////            iOrderBaseService.updateById(orderBase);
////            //更新订单流水状态
////            PayFlowLog payFlowLog = iPayFlowLogService.getOne(new QueryWrapper<PayFlowLog>().lambda().eq(PayFlowLog::getOrderNo, out_trade_no));
////            payFlowLog.setIsPay(Constant.ENABLE);
////            payFlowLog.setPayTime(LocalDateTime.now());
////            payFlowLog.setPayFlow(trade_no);
////            iPayFlowLogService.updateById(payFlowLog);
////            out.println("success");
//
//        } else {//验证失败
////            out.println("fail");
//
//            //调试用，写文本函数记录程序运行情况是否正常
//            String sWord = AlipaySignature.getSignCheckContentV1(params);
//            AlipayConfig.logResult(sWord);
//        }
//
//        //——请在这里编写您的程序（以上代码仅作参考）——
//    }
//
//    //注意同步返回结果是以get请求形式返回的
//    @RequestMapping("returnUrl")
//    public String return_url(HttpServletRequest request) throws AlipayApiException, UnsupportedEncodingException {
//        //获取支付宝GET过来反馈信息
//        Map<String, String> params = convertRequestParamsToMap(request);
//
//        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名
//
//        //——请在这里编写您的程序（以下代码仅作参考）——
//        if (signVerified) {
//            //商户订单号
//            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
//
//            //支付宝交易号
//            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");
//
//            //付款金额
//            String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");
//
////            out.println("trade_no:"+trade_no+"<br/>out_trade_no:"+out_trade_no+"<br/>total_amount:"+total_amount);
//        } else {
////            out.println("验签失败");
//        }
//        //——请在这里编写您的程序（以上代码仅作参考）——
//
//        return "error";
//    }
//
//    //将请求中的参数转换为Map
//    public static Map<String, String> convertRequestParamsToMap(HttpServletRequest request) throws UnsupportedEncodingException {
//        Map<String, String> params = new HashMap<String, String>();
//        Map<String, String[]> requestParams = request.getParameterMap();
//        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
//            String name = (String) iter.next();
//            String[] values = (String[]) requestParams.get(name);
//            String valueStr = "";
//            for (int i = 0; i < values.length; i++) {
//                valueStr = (i == values.length - 1) ? valueStr + values[i]
//                        : valueStr + values[i] + ",";
//            }
//            //乱码解决，这段代码在出现乱码时使用
//            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
//            params.put(name, valueStr);
//        }
//        return params;
//    }
//
//    //将字符串转换为UTF-8编码以防出现乱码错误
//    public static String getUTF8XMLString(String xml) {
//        StringBuffer sb = new StringBuffer();
//        sb.append(xml);
//        String xmString = "";
//        String xmlUTF8 = "";
//        try {
//            xmString = new String(sb.toString().getBytes("UTF-8"));
//            xmlUTF8 = URLEncoder.encode(xmString, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        return xmlUTF8;
//    }
//
//
//}
//
