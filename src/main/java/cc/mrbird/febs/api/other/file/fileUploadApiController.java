package cc.mrbird.febs.api.other.file;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.ReportUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/12/15:47
 * @ Description:
 */
@RestController
@RequestMapping("/api/file")
public class fileUploadApiController extends BaseController {

    @PostMapping("/kindEditorUpload")
    public FebsResponse kindEditorUpload(MultipartFile imgFile){
        String basePath = "editor";
        String url ;
        FebsResponse response = new FebsResponse();
        try {
            url = aliOSSUpload(imgFile, basePath);
            response.put("error",0);
            response.put("url",url);
        } catch (IOException e) {
            e.printStackTrace();
            response.put("error",1);
            response.message("上传失败！");
        }
        return response;
    }

    @RequestMapping("/getPdfUrl")
    public FebsResponse getPicUrl(MultipartFile file){
        String basePath = "pdf";
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
            System.out.println(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FebsResponse().success().data(url);
    }

    @RequestMapping("/getFileUrl")
    public FebsResponse getFileUrl(MultipartFile file,String basePath){
        if (StringUtils.isBlank(basePath)){
            basePath="file";
        }
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
            System.out.println(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FebsResponse().success().data(url);
    }

    @RequestMapping("/getPicUrl")
    public Map getPicUrl(MultipartFile file, String basePath){
        if (StringUtils.isBlank(basePath)){
            basePath="pic";
        }
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
            System.out.println(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map map = new HashMap();
        map.put("location",url);
        return map;
    }

    @RequestMapping("/getLayuiPicUrl")
    public FebsResponse getLayuiPicUrl(MultipartFile file, String basePath){
        if (StringUtils.isBlank(basePath)){
            basePath="pic";
        }
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
            System.out.println(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FebsResponse().success().data(url).put("code",0).put("msg","success");
    }


    // 通过文件流的方式预览 PDF 文件
    @RequestMapping(value = "pdfStreamHandeler")
    public void pdfStreamHandeler(HttpServletResponse response, HttpSession session) {
        try {
            // 编辑请求头部信息
            // 解决请求头跨域问题（IE兼容性 也可使用该方法）
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setContentType("application/pdf");
            byte[] data =ReportUtils.openFile("http://file.huistone.com/pdf/2020/08/17/3d8eea2367184d5fb1a49346ea5a99a7/【改版】惠思通-在线课堂-用户使用手册.pdf");
            response.getOutputStream().write(data);
        } catch (Exception e) {
        }
    }


}
