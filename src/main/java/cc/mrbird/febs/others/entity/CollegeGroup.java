package cc.mrbird.febs.others.entity;

import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuwenze.poi.config.Options;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class CollegeGroup implements Options {

    @Resource
    private IDeptService deptService;

    @Override
    public String[] get() {
        List<Dept> list = deptService.list(new QueryWrapper<Dept>().lambda().eq(Dept::getParentId, 0));
        return (String[]) list.toArray();
    }

}
