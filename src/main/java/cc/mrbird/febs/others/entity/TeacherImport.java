package cc.mrbird.febs.others.entity;

import cc.mrbird.febs.common.annotation.IsMobile;
import cc.mrbird.febs.common.excelkit.validator.StudentNameValidator;
import cc.mrbird.febs.common.excelkit.validator.StudentPhoneValidator;
import cc.mrbird.febs.common.excelkit.validator.TeacherNumberValidator;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * @ClassName TeacherImport
 * @ Description TODO
 * @Author 王珂
 * @Data 2020/7/14 8:56
 * @Version 1.0
 */
@Data
@TableName("t_user")
@Excel("导入教师数据")
public class TeacherImport {

    /**
     * 真实姓名
     */
    @TableField("TRUE_NAME")
    @Size(min = 2, max = 10, message = "{range}")
    @ExcelField(value = "真实姓名",validator = StudentNameValidator.class)
    private String trueName;

    /**
     * 学号/工号
     */
    @TableField("id_number")
    @ExcelField(value = "工号",validator = TeacherNumberValidator.class)
    private String idNumber;

    /**
     * 联系电话
     */
    @TableField("MOBILE")
    @IsMobile(message = "{mobile}")
    @ExcelField(value = "联系电话",validator = StudentPhoneValidator.class)
    private String mobile;

    /**
     * 性别 0男 1女 2 保密
     */
    @TableField("ssex")
    @ExcelField(value = "性别",  readConverterExp = "未知=2,男=0,女=1", writeConverterExp = "0=男,1=女,2=保密")
    private String sex;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    @Size(max = 100, message = "{noMoreThan}")
    @ExcelField(value = "个人描述")
    private String description;
}
