package cc.mrbird.febs.others.entity;

import cc.mrbird.febs.common.annotation.IsMobile;
import cc.mrbird.febs.common.excelkit.validator.StudentDeptValidator;
import cc.mrbird.febs.common.excelkit.validator.StudentNameValidator;
import cc.mrbird.febs.common.excelkit.validator.StudentNumberValidator;
import cc.mrbird.febs.common.excelkit.validator.StudentPhoneValidator;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
@TableName("t_user")
@Excel("导入用户数据")
public class UserImport   {

//    /**
//     * 用户名
//     */
//    @TableField("USERNAME")
//    @Size(min = 4, max = 10, message = "{range}")
//    @ExcelField(value = "用户名")
//    private String username;

    /**
     * 真实姓名
     */
    @TableField("TRUE_NAME")
    @Size(min = 2, max = 10, message = "{range}")
    @ExcelField(value = "真实姓名",validator = StudentNameValidator.class)
    private String trueName;

    /**
     * 用户昵称
     */
//    @TableField("NICKNAME")
//    @Size(min = 2, max = 10, message = "{range}")
//    @ExcelField(value = "用户昵称")
//    private String nickname;

    /**
     * 学号/工号
     */
    @ExcelField(value = "学号/工号",validator = StudentNumberValidator.class)
    @Size(min = 3, max = 12, message = "{range}")
    private String idNumber;

//    @ExcelField(
//            value = "学院",//
//            required = true,//
//            comment = "请填写学院名称"
//    )
//    private String college;
//
//    @ExcelField(//
//            value = "年级",//
//            required = true,//
//            comment = "请填写学院名称"
//    )
//    private String years;

    @ExcelField(//
            value = "专业班级",//
            required = true,//
            comment = "请填写专业班级",
            validator = StudentDeptValidator.class
    )
    private String course;

    /**
     * 邮箱
     */
//    @Size(max = 50, message = "{noMoreThan}")
//    @Email(message = "{email}")
//    @ExcelField(value = "邮箱")
//    private String email;

    /**
     * 联系电话
     */
    @TableField("MOBILE")
    @IsMobile(message = "{mobile}")
    @ExcelField(value = "联系电话",validator = StudentPhoneValidator.class)
    private String mobile;

    /**
     * 性别 0男 1女 2 保密
     */
    @ExcelField(value = "性别",  readConverterExp = "未知=2,男=0,女=1", writeConverterExp = "0=男,1=女,2=保密")
    private String sex;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    @Size(max = 100, message = "{noMoreThan}")
    @ExcelField(value = "个人描述")
    private String description;

}
