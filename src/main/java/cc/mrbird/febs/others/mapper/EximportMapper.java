package cc.mrbird.febs.others.mapper;

import cc.mrbird.febs.others.entity.Eximport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 马超伟
 */
public interface EximportMapper extends BaseMapper<Eximport> {

}
