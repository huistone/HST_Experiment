package cc.mrbird.febs.job.mapper;


import cc.mrbird.febs.job.entity.Job;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author 马超伟
 */
public interface JobMapper extends BaseMapper<Job> {
	
	List<Job> queryList();
}