package cc.mrbird.febs.job.mapper;


import cc.mrbird.febs.job.entity.JobLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 马超伟
 */
public interface JobLogMapper extends BaseMapper<JobLog> {
}