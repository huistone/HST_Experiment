package cc.mrbird.febs.job.task;

import cc.mrbird.febs.external.mqtt.MQTTPublish;
import cc.mrbird.febs.system.entity.AnalogRecord;
import cc.mrbird.febs.system.entity.DetRecord;
import cc.mrbird.febs.system.service.IAnalogBaseService;
import cc.mrbird.febs.system.service.IAnalogRecordService;
import cc.mrbird.febs.system.service.IDetRecordService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

/**
 * @ClassName freedBaseTask
 * @ Description TODO
 * @Author admin
 * @Date 2020/8/21 16:54
 * @Version 1.0
 */
@Component
@Slf4j
public class ReleaseBaseTask {

    @Autowired
    private IDetRecordService detRecordService;

    @Autowired
    private IAnalogRecordService analogRecordService;

    @Autowired
    private IAnalogBaseService analogBaseService;

    @Resource
    private MQTTPublish mqttPublish;

    public void releaseBase(){
        log.info("开始查询所有主板");
        List<DetRecord> list = detRecordService.list(new QueryWrapper<DetRecord>()
                .lambda()
                .eq(DetRecord::getIsUse,1)
                .select(DetRecord::getIsUse
                ,DetRecord::getEndTime
                ,DetRecord::getStartTime
                ,DetRecord::getDetRecordId
                ,DetRecord::getEndTime));
        for (DetRecord detRecord : list) {
            if(detRecord.getEndTime()==null || detRecord.getIsUse() == 1){
                LocalDateTime startTime = detRecord.getStartTime();
                //获取当前时间
                LocalDateTime now = LocalDateTime.now();
                long start = startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
                long end = now.toInstant(ZoneOffset.of("+8")).toEpochMilli();
                //占用超过一小时，解除占用
                if (end - start >60*60*1000){
                    detRecord.setIsUse(0);
                    detRecord.setEndTime(now);
                }
            }
            detRecordService.updateById(detRecord);
        }
    }


    public void releaseAceBase(){
        log.info("开始查询所有模电主板");
        List<AnalogRecord> records = analogRecordService.list(new QueryWrapper<AnalogRecord>()
                .lambda()
                .eq(AnalogRecord::getIsUse, 1)
                .select(AnalogRecord::getAnalogRecordId
                        , AnalogRecord::getAnalogId
                        , AnalogRecord::getBeginTime
                        , AnalogRecord::getEndTime));

        for (AnalogRecord record : records) {
            if (record.getEndTime()==null||record.getIsUse()==1){
                LocalDateTime beginTime = record.getBeginTime();
                LocalDateTime now = LocalDateTime.now();
                long start = beginTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
                long end = now.toInstant(ZoneOffset.of("+8")).toEpochMilli();
                if (end - start > 60*60*1000){
                    record.setIsUse(0);
                    record.setEndTime(now);
                    Long analogId = record.getAnalogId();
                    analogBaseService.updateById(analogBaseService.getById(analogId).setAnalogUsestatus(0).setOperator("暂无使用"));
                }
            }
            analogRecordService.updateById(record);
        }

    }


    public void getIbIc(String topic){
        log.info("开始采集1主板的IB和IC数值: +"+topic);
//        String topic = "AET/RXACG_BSP00001";
        JSONObject jsonObject = JSONObject.parseObject("{\n" +
                " \"ID\": 100,\n" +
                " \"POT1\": 1000,\n" +
                " \"POT2\": 500,\n" +
                " \"FREQ\": 1000,\n" +
                " \"AMP\": 255,\n" +
                " \"PHA\": 120,\n" +
                " \"WAVE\": 0,\n" +
                " \"TASK\": 4,\n" +
                " \"PID\": 1\n" +
                "}");
        for (int i = 1; i <= 1024; i++) {
            jsonObject.put("POT1",i);
            try {
                Thread.sleep(2000);  System.out.println(jsonObject);
                boolean b = mqttPublish.sendMQTTMessage(topic, jsonObject.toString());
                System.out.println(b);
            } catch (InterruptedException e) {
            }
        }
    }


}
