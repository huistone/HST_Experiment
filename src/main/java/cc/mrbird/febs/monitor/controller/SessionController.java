package cc.mrbird.febs.monitor.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.monitor.entity.ActiveUser;
import cc.mrbird.febs.monitor.service.ISessionService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserRoleService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 马超伟
 */
@RestController
@RequestMapping("session")
public class SessionController extends BaseController {

    @Resource
    private ISessionService sessionService;

    @GetMapping("list")
    @RequiresPermissions("online:view")
    public FebsResponse list(String username) {
        List<ActiveUser> list = sessionService.list(username);
        Map<String, Object> data = new HashMap<>(16);
        data.put("rows", list);
        data.put("total", CollectionUtils.size(list));
        return new FebsResponse().success().data(data);
    }

    @GetMapping("delete/{id}")
    @RequiresPermissions("user:kickout")
    public FebsResponse forceLogout(@PathVariable String id) {
        sessionService.forceLogout(id);
        return new FebsResponse().success();
    }
}
/***
 *  list.forEach(i ->{
 *             User u = userService.getById(i.getUserId());
 *             if(u.getDeptId() != null && !u.getUsername().equals("admin")){
 *                 Dept dept = deptService.getById(u.getDeptId());
 *                 i.setDeptName(dept.getDeptName());
 *             }
 *         });
 *
 *         list.contains(new ActiveUser());
 *
 *         User currentUser = getCurrentUser();
 *
 *         UserRole userRole = userRoleService.getOne(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId, currentUser.getUserId(), false));
 *         if (userRole.getRoleId() == Role.ROLE_TEACHER){
 *             List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, currentUser.getUserId()));
 *             userDeptList.
 *
 *         }
 */
