package cc.mrbird.febs.monitor.service;

import cc.mrbird.febs.monitor.entity.ActiveUser;

import java.util.List;

/**
 * @author 马超伟
 */
public interface ISessionService {

    /**
     * 获取在线用户列表
     *
     * @ Param username 用户名
     * @return List<ActiveUser>
     */
    List<ActiveUser> list(String username);

    /**
     * 踢出用户
     *
     * @ Param sessionId sessionId
     */
    void forceLogout(String sessionId);
}
