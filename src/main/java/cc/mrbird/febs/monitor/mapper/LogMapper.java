package cc.mrbird.febs.monitor.mapper;

import cc.mrbird.febs.monitor.entity.SystemLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 马超伟
 */
public interface LogMapper extends BaseMapper<SystemLog> {

}
