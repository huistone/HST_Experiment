package cc.mrbird.febs.im.mapper;

import cc.mrbird.febs.im.entity.ImMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-09-15
 */
public interface ImMsgMapper extends BaseMapper<ImMsg> {

}
