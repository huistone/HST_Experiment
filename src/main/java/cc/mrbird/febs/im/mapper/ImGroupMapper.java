package cc.mrbird.febs.im.mapper;

import cc.mrbird.febs.im.entity.ImGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户帐号 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
public interface ImGroupMapper extends BaseMapper<ImGroup> {

}
