package cc.mrbird.febs.im.mapper;

import cc.mrbird.febs.im.entity.ImGroupUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户-群 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
public interface ImGroupUserMapper extends BaseMapper<ImGroupUser> {

}
