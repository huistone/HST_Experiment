package cc.mrbird.febs.im.constant;

/**
 * @ Author 马超伟
 * @ Date 2020-09-11 16:14
 * @ Description: Im常量
 * @ Version:
 */
public interface ImConstant {

    String OTHER_INPUT = "<span style=\"color:#FF5722;\">对方正在输入...</span>";

    /**
     * 状态
     */
    interface Status {
        /**
         * 冻结
         */
        Integer FREEZE = 0;
        /**
         * 正常
         */
        Integer OK = 1;
    }

    /**
     * 在线状态
     */
    interface LineStatus {
        /**
         * 在线
         */
        String ONLINE = "online";
        /**
         * 离线
         */
        String OFFLINE = "offline";

        /**
         * 隐身
         */
        String HIDE = "hide";
    }

    /**
     * 在线状态
     */
    interface Type {
        /**
         * 群消息
         */
        String GROUP = "group";
        /**
         * 朋友消息
         */
        String FRIEND = "friend";
    }

}
