package cc.mrbird.febs.im.constant;
/**
 * @ Author: 马超伟
 * @ Date: 2020/9/11 17:54
 * @ Params:  * @ Param null
 * @ return:
 * @ Description: 消息常量
 */
public interface MsgConstant {

    /**
     * 未读
     */
    Byte NO_READ = 0;
    /**
     * 已读
     */
    Byte IS_READ = 1;
    /**
     * 图片正则
     */
    String LAYIM_IMG_REGEX = "^img\\[(.*)\\]$";
    /**
     * 文件正则
     */
    String LAYIM_FILE_REGEX = "^file\\((.*)\\)\\[.*\\]$";
}
