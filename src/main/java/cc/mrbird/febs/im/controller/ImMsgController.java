package cc.mrbird.febs.im.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.im.entity.ImMsg;
import cc.mrbird.febs.im.service.IImMsgService;
import cc.mrbird.febs.system.entity.User;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 消息表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-09-15
 */
@RestController
@Slf4j
@RequestMapping("/api/imMsg")
public class ImMsgController extends BaseController {

    @Resource
    private IImMsgService iImMsgService;

    /**
     * @Author wangke
     * @ Description 查询聊天记录
     * @Date 18:00 2020/9/15
     * @ Param id 群id或好友id  type 消息类型
     * @return
     */
    @RequestMapping("getChatRecord")
    @ControllerEndpoint(operation = "查询聊天记录",exceptionMessage = "查询聊天记录成功")
    public FebsResponse getChatRecord(Integer id, String type, QueryRequest queryRequest){
        User currentUser = getCurrentUser();
        if (currentUser == null ){
            return new FebsResponse().no_login();
        }
        if (IntegerUtils.isBlank(id)|| StringUtils.isBlank(type)){
            return new FebsResponse().no_param();
        }


        try {
            Page<ImMsg> page = new Page<>(queryRequest.getPageNum(),queryRequest.getPageNum());
            SortUtil.handlePageSort(queryRequest, page, "create_time", FebsConstant.ORDER_ASC, false);
            //如果是群聊
            if("group".equals(type)){
                IPage<ImMsg> msgPage = iImMsgService.page(page, new QueryWrapper<ImMsg>()
                        .lambda()
                        .eq(ImMsg::getId, id)
                        .eq(ImMsg::getToId,currentUser.getUserId())
                        .eq(ImMsg::getType,type)
                        .select(ImMsg::getUsername,ImMsg::getAvatar,ImMsg::getCreateTime,ImMsg::getContent,ImMsg::getId)
                        .orderByDesc(ImMsg::getCreateTime));
                List<ImMsg> records = msgPage.getRecords();
                List list = reverseMsgRecord(records);
                msgPage.setRecords(list);
                return new FebsResponse().code(0).msg("查找聊天记录成功").data(getDataTable(msgPage));
            }else if ("friend".equals(type)){
                //两人的聊天记录
                IPage<ImMsg> msgPage =  iImMsgService.page(page,new QueryWrapper<ImMsg>()
                        .lambda()
                        .eq(ImMsg::getToId,id)
                        .eq(ImMsg::getId,currentUser.getUserId())
                        .eq(ImMsg::getType,type)
                        .or()
                        .eq(ImMsg::getToId,currentUser.getUserId())
                        .eq(ImMsg::getId,id)
                        .eq(ImMsg::getType,type)
                        .select(ImMsg::getUsername,ImMsg::getAvatar,ImMsg::getCreateTime,ImMsg::getContent,ImMsg::getId)
                        .orderByDesc(ImMsg::getCreateTime));
                List<ImMsg> records = msgPage.getRecords();
                List list = reverseMsgRecord(records);
                msgPage.setRecords(list);
                return new FebsResponse().code(0).msg("查找聊天记录成功").data(getDataTable(msgPage));
            }else{
                return new FebsResponse().no_param().msg("type不对");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/imMsg/getChatRecord"+e.getMessage());
            return new FebsResponse().code(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Author wangke
     * @ Description 转换聊天记录的内容
     * @Date 17:19 2020/9/24
     * @ Param
     * @return 
     */
    public List<ImMsg> reverseMsgRecord(List<ImMsg> list){
        for (ImMsg record : list) {
            String content = record.getContent();
            if(content.contains("img[")){
                int i = content.indexOf("img[");
                String substring = content.substring(i + 4, content.length() - 1);
                record.setContent("<img class='layui-layim-photos'  src='"+substring+"'>");
            }else if(content.contains("file(") ){
                int i = content.indexOf("file(");
                int i2 = content.lastIndexOf('[');
                String substring = content.substring(i + 5, i2-1);
                int i1 = content.lastIndexOf('/');
                String substring1 = content.substring(i1 + 1, i2-1);
                record.setContent("<a class='layui-layim-file'  href='"+substring+"' download='' target='_blank'>'<i class=\"layui-icon\">\uE61E</i><cite>"+substring1+"</cite></a>");
            }
        }
        return list;
    }



    /**
     * @Author wangke
     * @ Description 查询文件聊天记录内容
     * @Date 17:25 2020/9/24
     * @ Param
     * @return 
     */
    @RequestMapping("getFileRecord")
    @ControllerEndpoint(operation = "查询图片分类记录",exceptionMessage = "查询图片分类记录失败")
    public FebsResponse getFileRecord(Integer id,String type,QueryRequest queryRequest){
        User currentUser = getCurrentUser();
        if(currentUser == null){
            return  new FebsResponse().no_login();
        }
        if(IntegerUtils.isBlank(id)||StringUtils.isBlank(type)){
            return  new FebsResponse().no_param();
        }
        try {
            Page<ImMsg> page = new Page<>(queryRequest.getPageNum(),queryRequest.getPageNum());
            SortUtil.handlePageSort(queryRequest, page, "create_time", FebsConstant.ORDER_ASC, false);
            //如果是群聊
            if("group".equals(type)){
                IPage<ImMsg> msgPage = iImMsgService.page(page, new QueryWrapper<ImMsg>()
                        .lambda()
                        .eq(ImMsg::getId, id)
                        .eq(ImMsg::getToId,currentUser.getUserId())
                        .eq(ImMsg::getType,type)
                        .like(ImMsg::getContent,"file(")
                        .select(ImMsg::getUsername,ImMsg::getAvatar,ImMsg::getCreateTime,ImMsg::getContent,ImMsg::getId)
                        .orderByDesc(ImMsg::getCreateTime));
                List<ImMsg> records = msgPage.getRecords();
                List<ImMsg> imMsgs = reverseFileRecord(records);
                msgPage.setRecords(imMsgs);
                return new FebsResponse().code(0).msg("查找聊天记录成功").data(getDataTable(msgPage));
            }else if ("friend".equals(type)){
                //两人的聊天记录
                IPage<ImMsg> msgPage =  iImMsgService.page(page,new QueryWrapper<ImMsg>()
                        .lambda()
                        .eq(ImMsg::getToId,id)
                        .eq(ImMsg::getId,currentUser.getUserId())
                        .eq(ImMsg::getType,type)
                        .like(ImMsg::getContent,"file(")
                        .or()
                        .eq(ImMsg::getToId,currentUser.getUserId())
                        .eq(ImMsg::getId,id)
                        .eq(ImMsg::getType,type)
                        .select(ImMsg::getUsername,ImMsg::getAvatar,ImMsg::getCreateTime,ImMsg::getContent,ImMsg::getId)
                        .like(ImMsg::getContent,"file(")
                        .orderByDesc(ImMsg::getCreateTime));
                List<ImMsg> records = msgPage.getRecords();
                List<ImMsg> imMsgs = reverseFileRecord(records);
                msgPage.setRecords(imMsgs);
                return new FebsResponse().code(0).msg("查找聊天记录成功").data(getDataTable(msgPage));
            }else{
                return new FebsResponse().no_param().msg("type不对");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/imMsg/getFileRecord"+e.getMessage());
            return new FebsResponse().code(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @Author wangke
     * @ Description 转换文件聊天记录
     * @Date 17:21 2020/9/24
     * @ Param
     * @return
     */
    public List<ImMsg> reverseFileRecord(List<ImMsg> list){
        for (ImMsg record : list) {
            String content = record.getContent();
            if(content.contains("file(") || content.contains("audio[")||content.contains("video[")){
                int i = content.indexOf("file(");
                int i2 = content.lastIndexOf('[');
                String substring = content.substring(i + 5, i2-1);
                int i1 = content.lastIndexOf('/');
                String substring1 = content.substring(i1 + 1, i2-1);
                record.setContent("<a class='layui-layim-file'  href='"+substring+"' download='' target='_blank'><i class=\"layui-icon\">\uE61E</i><cite>"+substring1+"</cite></a>");
            }
        }
        return list;
    }

    /**
     * @Author wangke
     * @ Description 查询图片聊天记录内容
     * @Date 17:25 2020/9/24
     * @ Param
     * @return
     */
    @RequestMapping("getImageRecord")
    @ControllerEndpoint(operation = "查询文件聊天记录",exceptionMessage = "查询文件聊天记录失败")
    public FebsResponse getImageRecord(Integer id,String type,QueryRequest queryRequest){
        User currentUser = getCurrentUser();
        if(currentUser == null){
            return  new FebsResponse().no_login();
        }
        if(IntegerUtils.isBlank(id)||StringUtils.isBlank(type)){
            return  new FebsResponse().no_param();
        }
        try {
            Page<ImMsg> page = new Page<>(queryRequest.getPageNum(),queryRequest.getPageNum());
            SortUtil.handlePageSort(queryRequest, page, "create_time", FebsConstant.ORDER_ASC, false);
            //如果是群聊
            if("group".equals(type)){
                IPage<ImMsg> msgPage = iImMsgService.page(page, new QueryWrapper<ImMsg>()
                        .lambda()
                        .eq(ImMsg::getId, id)
                        .eq(ImMsg::getToId,currentUser.getUserId())
                        .eq(ImMsg::getType,type)
                        .like(ImMsg::getContent,"img[")
                        .select(ImMsg::getUsername,ImMsg::getAvatar,ImMsg::getCreateTime,ImMsg::getContent,ImMsg::getId)
                        .orderByDesc(ImMsg::getCreateTime));
                List<ImMsg> records = msgPage.getRecords();
                List<ImMsg> imMsgs = reverseImageRecord(records);
                msgPage.setRecords(imMsgs);
                return new FebsResponse().code(0).msg("查找聊天记录成功").data(getDataTable(msgPage));
            }else if ("friend".equals(type)){
                //两人的聊天记录
                IPage<ImMsg> msgPage =  iImMsgService.page(page,new QueryWrapper<ImMsg>()
                        .lambda()
                        .eq(ImMsg::getToId,id)
                        .eq(ImMsg::getId,currentUser.getUserId())
                        .eq(ImMsg::getType,type)
                        .like(ImMsg::getContent,"img[")
                        .or()
                        .eq(ImMsg::getToId,currentUser.getUserId())
                        .eq(ImMsg::getId,id)
                        .eq(ImMsg::getType,type)
                        .select(ImMsg::getUsername,ImMsg::getAvatar,ImMsg::getCreateTime,ImMsg::getContent,ImMsg::getId)
                        .like(ImMsg::getContent,"img[")
                        .orderByDesc(ImMsg::getCreateTime));
                List<ImMsg> records = msgPage.getRecords();
                List<ImMsg> imMsgs = reverseImageRecord(records);
                msgPage.setRecords(imMsgs);
                return new FebsResponse().code(0).msg("查找聊天记录成功").data(getDataTable(msgPage));
            }else{
                return new FebsResponse().no_param().msg("type不对");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("api/imMsg/getImageRecord"+e.getMessage());
            return new FebsResponse().code(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Author wangke
     * @ Description 转换图片聊天记录
     * @Date 17:21 2020/9/24
     * @ Param
     * @return
     */
    public List<ImMsg> reverseImageRecord(List<ImMsg> list){
        for (ImMsg record : list) {
            String content = record.getContent();
            if(content.contains("img[")){
                int i = content.indexOf("img[");
                String substring = content.substring(i + 4, content.length() - 1);
                record.setContent("<img class='layui-layim-photos'  src='"+substring+"'>");
            }
        }
        return list;
    }

    /**
     * @Author wangke
     * @ Description 获取消息盒子信息
     * @Date 11:35 2020/9/18
     * @ Param
     * @return
     */
    @RequestMapping("getMsgBox")
    public FebsResponse getMsgBox(QueryRequest queryRequest){
        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }
        Page<ImMsg> page = new Page<>(queryRequest.getPageNum(),queryRequest.getPageNum());
        SortUtil.handlePageSort(queryRequest, page, "create_time", FebsConstant.ORDER_ASC, false);

        IPage<ImMsg> page1 = iImMsgService.page(page, new QueryWrapper<ImMsg>()
                .lambda()
                .eq(ImMsg::getToId, currentUser.getUserId())
                .eq(ImMsg::getVersion, 0));
        List<ImMsg> records = page1.getRecords();
        return new FebsResponse().data(records).code(0).put("pages",1);
    }

    /**
     * @Author wangke
     * @ Description 修改消息为已读
     * @Date 16:08 2020/9/18
     * @ Param
     * @return
     */
    @RequestMapping("updateMsgStatus")
    public FebsResponse updateMsgStatus(Integer fromId,String type){

        User currentUser = getCurrentUser();

        if(currentUser == null){
            return new FebsResponse().no_login();
        }
        boolean update = false;
        if(type.equals("friend")){
            update =  iImMsgService.update(new UpdateWrapper<ImMsg>()
                    .lambda()
                    .set(ImMsg::getVersion,1)
                    .eq(ImMsg::getFromid, fromId)
                    .eq(ImMsg::getToId, currentUser.getUserId())
                    .eq(ImMsg::getVersion, 0)
                    .eq(ImMsg::getType,type));
        }
        if(type.equals("group")){
            update =  iImMsgService.update(new UpdateWrapper<ImMsg>()
                    .lambda()
                    .set(ImMsg::getVersion,1)
                    .eq(ImMsg::getId,fromId)
                    .eq(ImMsg::getToId,currentUser.getUserId())
                    .eq(ImMsg::getVersion,0)
                    .eq(ImMsg::getType,type));
        }
        if (update){
            return new FebsResponse().code(0).msg("已读消息");
        }else{
            return new FebsResponse().code(500).msg("系统繁忙请稍后再试");
        }

    }
}
