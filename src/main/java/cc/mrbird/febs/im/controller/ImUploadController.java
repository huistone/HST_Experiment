package cc.mrbird.febs.im.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName UploadController
 * @ Description 文件上传
 * @Author wangke
 * @Date 2020/9/15 9:37
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("api/imUpload")
public class ImUploadController extends BaseController {

    
    /**
     * @Author wangke
     * @ Description 上传图片
     * @Date 18:00 2020/9/15
     * @ Param
     * @return 
     */
    @RequestMapping("uploadImImage")
    public FebsResponse uploadImImage(MultipartFile file){
        String basePath = "imImage";
        String imagePath = null;
        Map<Object, Object> map = new HashMap<>();
        if(file.isEmpty()){
           return new FebsResponse()
                   .code(HttpStatus.INTERNAL_SERVER_ERROR)
                   .msg("图片不能为空")
                   .data(null);
        }
        try{
            imagePath = aliOSSUpload(file, basePath);
            map.put("src",imagePath);
        }catch (Exception e){
            log.error("api/imUpload/uploadImImage"+e.getMessage());
            return new FebsResponse().data(null).code(500).msg("请检查图片格式或图片大小");
        }
        return new FebsResponse().data(map).code(0).msg("上传图片成功");
    }

    /**
     * @Author wangke
     * @ Description 上传文件
     * @Date 17:07 2020/9/15
     * @ Param
     * @return 
     */
    @RequestMapping("uploadImFile")
    public FebsResponse uploadImFile(MultipartFile file){
        String basePath = "imFile";
        String filePath = null;
        String originName = null;
        Map<String, Object> map = new HashMap<>();
        if(file.isEmpty()){
            return new FebsResponse().code(HttpStatus.INTERNAL_SERVER_ERROR).data(null).msg("文件不能为空");
        }
        try{
            filePath = aliOSSUpload(file, basePath);
            originName = file.getOriginalFilename();
            map.put("src",filePath);
            map.put("name",originName);
        }catch(Exception e){
            log.error("api/imUpload/uploadImFile"+e.getMessage());
            return new FebsResponse().data(null).code(500).msg("请检查文件类型或文件大小");
        }
        return new FebsResponse().data(map).code(0).msg("上传文件成功");
    }



}
