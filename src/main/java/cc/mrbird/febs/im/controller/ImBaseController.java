package cc.mrbird.febs.im.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.utils.imageByFontsUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.im.constant.ImConstant;
import cc.mrbird.febs.im.entity.model.LayimModel;
import cc.mrbird.febs.im.entity.model.LayimModel.Friend;
import cc.mrbird.febs.im.entity.model.LayimModel.Group;
import cc.mrbird.febs.im.entity.model.LayimModel.imUser;
import cc.mrbird.febs.im.imWebSocket.LayimChatWebSocket;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserService;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ Author 马超伟
 * @ Date 2020-09-11 16:03
 * @ Description: Im初始化配置控制器
 * @ Version:     1.0.0
 */
@RestController
@RequestMapping("api/imBase")
public class ImBaseController extends BaseController {

    @Resource
    private LayimChatWebSocket singleChatWebSocket;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserService userService;

    @GetMapping("initIm")
    public FebsResponse initIm() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return new FebsResponse().no_login();
        }
        // 获取自己-好友列表-群组列表
        LayimModel model = new LayimModel();
        //我的信息
        imUser mine = LayimModel.userConvert(currentUser);
        String imStatus = currentUser.getImStatus();
        //如果im状态为空手动设置
        if (imStatus == null) {
            mine.setStatus(ImConstant.LineStatus.ONLINE);// 在线
        } else {
            mine.setStatus(imStatus);
        }
        model.setMine(mine);

        /**
         *  好友列表，分组即为班级名称
         * @ Author: 马超伟
         * @ Date: 2020/9/12 9:35
         * @ Params: [session]
         * @ return: cc.mrbird.febs.common.entity.FebsResponse
         * @ Description: 如果当前登录的是老师用户
         * 老师有多个班级，所有老师的用户列表有多个分组
         * 学生的话只有一个分组，就是班级名称
         * 同样，老师应该有多个群，而学生只有一个群
         */
        Set<Integer> onlineKey = singleChatWebSocket.onlineKey();
        List<Friend> friends = CollUtil.newArrayList();
        List<Group> groups = CollUtil.newArrayList();
        if (FebsConstant.TEACHER_ID.equals(currentUser.getRoleId())) {
            List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                    .eq(UserDept::getUserId, currentUser.getUserId())
                    .eq(UserDept::getIsDel, 1)
            );
            //去除相同的班级id
            ArrayList<UserDept> userDepts = removeDuplicateUser(userDeptList);
            for (UserDept userDept : userDepts) {
                //分组
                Friend friend = new Friend();
                //好友分组名
                String deptName = deptService.getById(userDept.getDeptId()).getDeptName();
                friend.setGroupname(deptName);
                //分组ID
                friend.setId(userDept.getDeptId());
                //分组下的好友列表
                List<imUser> imUserList = getDeptList(onlineKey, userDept.getDeptId());
                friend.setList(imUserList);
                friends.add(friend);
                //群聊
                Group group = new Group();
                group.setGroupname(deptName);
                group.setId(userDept.getDeptId());
                group.setAvatar(setAvatar(userDept.getDeptId()));
                groups.add(group);
            }

            //否则，如果当前登录的用户是学生的话，只有一个分组，就是学生所在的班级
        } else if (FebsConstant.STUDENT_ID.equals(currentUser.getRoleId())) {
            Friend friend = new Friend();
            friend.setGroupname(currentUser.getDeptName());
            friend.setId(currentUser.getDeptId());
            //分组下的好友列表
            List<imUser> imUserList = getDeptList(onlineKey, currentUser.getDeptId());
            friend.setList(imUserList);
            friends.add(friend);
            //群聊
            Group group = new Group();
            group.setGroupname(currentUser.getDeptName());
            group.setId(currentUser.getDeptId());
            group.setAvatar(setAvatar(currentUser.getDeptId()));
            groups.add(group);
        }
        model.setFriend(friends);
        model.setGroup(groups);
        return new FebsResponse().put_code(0, "初始化数据成功！").data(model);
    }

    /**
     * 设置群聊头像，如果没有则自动生成
     *
     * @ Param deptId
     * @return
     */
    private String setAvatar(Long deptId) {
        Dept dept = deptService.getById(deptId);
        if (dept.getAvator() == null) {
            String img = imageByFontsUtil.generateImg(dept.getDeptName());
            dept.setAvator(img);
            deptService.updateById(dept);
        }
        return dept.getAvator();
    }


    /**
     * @ Author: 马超伟
     * @ Date: 2020/9/12 9:54
     * @ Params: []
     * @ return: java.util.List<cc.mrbird.febs.im.entity.model.LayimModel.imUser>
     * @ Description: 获取分组下的好友列表
     */
    private List<imUser> getDeptList(Set<Integer> onlineKey, Long deptId) {
        //查询班级的学生信息
        List<User> userList = userService.list(new QueryWrapper<User>().lambda()
                .eq(User::getDeptId, deptId)
                .eq(User::getStatus, 1).eq(User::getIsDel, 1));
        //查询班级的老师列表
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                .select(UserDept::getUserId)
                .eq(UserDept::getDeptId, deptId).eq(UserDept::getIsDel, 1));
        //获取该班级的老师列表id
        Set<Long> userDeptUserIdList = userDeptList.stream().map(m -> m.getUserId()).collect(Collectors.toSet());
        //将老师用户加入到学生列表信息下
        userList.addAll(userService.listByIds(userDeptUserIdList));
        List<imUser> collect = userList.stream().map(user -> {
            imUser imUsers = LayimModel.userConvert(user);
            //设置用户的在线状态
            if (user.getImStatus() == null) {
                imUsers.setStatus(onlineKey.contains(imUsers.getId())
                        ? ImConstant.LineStatus.ONLINE
                        : ImConstant.LineStatus.OFFLINE);
            } else {
                //如果不在线且没有状态，则设置为offline
                if (!onlineKey.contains(imUsers.getId())) {
                    imUsers.setStatus(ImConstant.LineStatus.OFFLINE);
                } else {
                    imUsers.setStatus(user.getImStatus());
                }
            }
            return imUsers;
        }).collect(Collectors.toList());
        collect.sort(Comparator.comparing(imUser::getStatus).reversed());
        return collect;
    }

    /**
     * @ Author: 马超伟
     * @ Date: 2020/9/11 16:57
     * @ Params: [userDepts]
     * @ return: java.util.ArrayList<cc.mrbird.febs.system.entity.UserDept>
     * @ Description: 根据对象中某一属性进行去重，后重新返回该对象集合
     */
    private ArrayList<UserDept> removeDuplicateUser(List<UserDept> userDepts) {
        Set<UserDept> set = new TreeSet<UserDept>(new Comparator<UserDept>() {
            @Override
            public int compare(UserDept o1, UserDept o2) {
                //字符串,则按照asicc码升序排列
                return o1.getDeptId().compareTo(o2.getDeptId());
            }
        });
        set.addAll(userDepts);
        return new ArrayList<>(set);
    }

}
