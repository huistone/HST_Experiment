package cc.mrbird.febs.im.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 用户-群 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
@RestController
@RequestMapping("/imGroupUser")
public class ImGroupUserController extends BaseController {

}
