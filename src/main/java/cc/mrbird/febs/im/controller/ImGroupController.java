package cc.mrbird.febs.im.controller;


import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.im.constant.ImConstant;
import cc.mrbird.febs.im.entity.model.LayimModel;
import cc.mrbird.febs.im.imWebSocket.LayimChatWebSocket;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户帐号 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
@RestController
@RequestMapping("api/imGroup")
public class ImGroupController extends BaseController {


    @Autowired
    private IUserService userService;

    @Resource
    private LayimChatWebSocket singleChatWebSocket;

    @Resource
    private IUserDeptService userDeptService;


    /**
     * @Author wangke
     * @ Description 获取群员
     * @Date 9:10 2020/9/15
     * @ Param
     * @return
     */
    @RequestMapping("getGroupList")
    public FebsResponse getGroupList(Integer id){

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().no_login();
        }

        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>()
                .lambda()
                .eq(UserDept::getDeptId, id));

        Set<Long> collect = userDeptList.stream().map(UserDept::getUserId).collect(Collectors.toSet());


        Set<Integer> onlineKey = singleChatWebSocket.onlineKey();
        List<User> list = userService.list(new QueryWrapper<User>()
                .lambda()
                .eq(User::getDeptId, id)
                .eq(User::getIsDel, 1)
                .or()
                .in(collect.size()>0,User::getUserId,collect)
                .select(User::getUserId,
                        User::getTrueName,
                        User::getAvatar,
                        User::getDescription));
        List<LayimModel.imUser> imUsers = new ArrayList<>();
        for (User user : list) {
            LayimModel.imUser imUser = LayimModel.userConvert(user);
            if(StringUtils.isBlank(user.getImStatus())){
                imUser.setStatus(onlineKey.contains(user.getUserId())?ImConstant.LineStatus.OFFLINE:ImConstant.LineStatus.OFFLINE);
            }else{
                if(!onlineKey.contains(user.getUserId())){
                    imUser.setStatus(ImConstant.LineStatus.OFFLINE);
                }else{
                    imUser.setStatus(user.getImStatus());
                }
            }
            imUsers.add(imUser);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("list",imUsers);
        return new FebsResponse().success().data(map).code(0).msg("获取群成员成功");
    }
}
