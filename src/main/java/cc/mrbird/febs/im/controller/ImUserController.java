package cc.mrbird.febs.im.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.im.constant.ImConstant;
import cc.mrbird.febs.im.entity.ImGroup;
import cc.mrbird.febs.im.entity.model.LayimModel;
import cc.mrbird.febs.im.imWebSocket.LayimChatWebSocket;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @ClassName ImUserController
 * @ Description TODO
 * @Author admin
 * @Date 2020/9/17 16:00
 * @Version 1.0
 */
@RestController
@RequestMapping("api/imUser")
public class ImUserController extends BaseController {

    @Resource
    private IUserService userService;

    @Resource
    private LayimChatWebSocket singleChatWebSocket;

    @Resource
    private IDeptService deptService;

    /**
     * @Author wangke
     * @ Description 修改个性签名方法
     * @Date 17:40 2020/9/18
     * @ Param
     * @return 
     */
    @RequestMapping("updateSign")
    public FebsResponse updateSign(String sign){
        User currentUser = getCurrentUser();
        if (currentUser == null ){
            return new FebsResponse().no_login();
        }

        if(StringUtils.isBlank(sign)){
            return new FebsResponse().no_param();
        }

        currentUser.setDescription(sign);
        userService.updateById(currentUser);
        return new FebsResponse().code(0).data(null).msg("修改个签成功");

    }


    /**
     * @Author wangke
     * @ Description 修改用户IM状态
     * @Date 18:56 2020/9/17
     * @ Param
     * @return
     */
    @RequestMapping("updateStatus")
    public FebsResponse updateStatus(String status){

        User currentUser = getCurrentUser();
        if (currentUser == null ){
            return new FebsResponse().no_login();
        }

        if(StringUtils.isBlank(status)){
            return new FebsResponse().no_param();
        }

        currentUser.setImStatus(status);
        userService.updateById(currentUser);
        return new FebsResponse().data(null).code(0).msg("修改状态成功");
    }
    
    /**
     * @Author wangke
     * @ Description 查询用户个人信息接口
     * @Date 10:06 2020/9/25
     * @ Param
     * @return 
     */
    @GetMapping("getImUserInfo")
    @ControllerEndpoint(operation = "查找个人信息接口",exceptionMessage = "查找个人信息接口失败")
    public FebsResponse getImUserInfo(Integer uid){
        if(getCurrentUser() == null){
            return new FebsResponse().no_login();
        }
        Set<Integer> onlineKey = singleChatWebSocket.onlineKey();
        User user = userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getUserId, uid)
                .select(User::getTrueName,
                        User::getAvatar,
                        User::getImStatus,
                        User::getDescription,
                        User::getUserId));

        LayimModel.imUser mine = LayimModel.userConvert(user);
        if(user.getImStatus() == null){
            mine.setStatus(onlineKey.contains(user.getId())
                    ? ImConstant.LineStatus.ONLINE
                    : ImConstant.LineStatus.OFFLINE);
        } else {
            //如果不在线且没有状态，则设置为offline
            if (!onlineKey.contains(user.getId())) {
                mine.setStatus(ImConstant.LineStatus.OFFLINE);
            } else {
                mine.setStatus(user.getImStatus());
            }
        }
        return new FebsResponse().code(0).data(mine);
    }

    @GetMapping("getImDeptInfo")
    @ControllerEndpoint(operation = "查找班级信息接口",exceptionMessage = "查找班级信息接口失败")
    public FebsResponse getImDeptInfo(Integer uid){
        try {
            if(getCurrentUser() == null){
                return new FebsResponse().no_login();
            }

            Dept one = deptService.getOne(new QueryWrapper<Dept>()
                    .lambda()
                    .eq(Dept::getDeptId, uid)
                    .select(Dept::getDeptName, Dept::getAvator, Dept::getDeptId));
            ImGroup imGroup = new ImGroup();
            imGroup.setNickName(one.getDeptName());
            imGroup.setImGroupId(one.getDeptId().intValue());
            imGroup.setHeadImg(one.getAvator());
            return new FebsResponse().data(imGroup).code(0);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().code(500).data(null);
        }

    }


}
