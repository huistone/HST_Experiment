package cc.mrbird.febs.im.service;

import cc.mrbird.febs.im.entity.ImGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户帐号 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
public interface IImGroupService extends IService<ImGroup> {

}
