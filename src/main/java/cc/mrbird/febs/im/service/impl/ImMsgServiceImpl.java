package cc.mrbird.febs.im.service.impl;

import cc.mrbird.febs.im.entity.ImMsg;
import cc.mrbird.febs.im.imWebSocket.LayimChatWebSocket;
import cc.mrbird.febs.im.mapper.ImMsgMapper;
import cc.mrbird.febs.im.service.IImMsgService;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 消息表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-09-15
 */
@Service
public class ImMsgServiceImpl extends ServiceImpl<ImMsgMapper, ImMsg> implements IImMsgService {

    @Resource
    private LayimChatWebSocket layimChatWebSocket;

    /**
     * 异步消息推送
     */
    @Async
    @Override
    @Transactional
    public void saveOffLineMsg(ImMsg vo, Collection<Integer> receiveIds) {
        // 利用list中的元素创建HashSet集合，此时set中进行了去重操作
        // 遍历接收者->改变to的值
        List<ImMsg> list = new ArrayList<>();
        for (Integer k : receiveIds) {
//            if (!key.contains(k)) {
                ImMsg imMsg1 = new ImMsg();
                BeanUtil.copyProperties(vo, imMsg1);
                imMsg1.setToId(k);
                list.add(imMsg1);
//            }
        }
        // 批量添加
        if (CollUtil.isNotEmpty(list)) {
            this.saveBatch(list);
        }
//            this.save(vo);
        // 增加历史记录
//            ImMsgHis msgHis = BeanUtil.toBean(vo, ImMsgHis.class)
//                    .setFromTo(ImMsgHisService.fromTo(vo.getFromid(), vo.getToid()));
//            imMsgHisService.save(msgHis);

    }

}
