package cc.mrbird.febs.im.service;

import cc.mrbird.febs.im.entity.ImMsg;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;

/**
 * <p>
 * 消息表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-09-15
 */
public interface IImMsgService extends IService<ImMsg> {

    /**
     * 消息入库
     * @ Param vo 消息
     */
    void saveOffLineMsg(ImMsg vo, Collection<Integer> receiveIds);


}
