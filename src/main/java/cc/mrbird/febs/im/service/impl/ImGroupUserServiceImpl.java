package cc.mrbird.febs.im.service.impl;

import cc.mrbird.febs.im.entity.ImGroupUser;
import cc.mrbird.febs.im.mapper.ImGroupUserMapper;
import cc.mrbird.febs.im.service.IImGroupUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-群 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
@Service
public class ImGroupUserServiceImpl extends ServiceImpl<ImGroupUserMapper, ImGroupUser> implements IImGroupUserService {

}
