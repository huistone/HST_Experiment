package cc.mrbird.febs.im.service;

import cc.mrbird.febs.im.entity.ImGroupUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户-群 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
public interface IImGroupUserService extends IService<ImGroupUser> {

}
