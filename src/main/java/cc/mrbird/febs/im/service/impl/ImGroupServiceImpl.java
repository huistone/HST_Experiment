package cc.mrbird.febs.im.service.impl;

import cc.mrbird.febs.im.entity.ImGroup;
import cc.mrbird.febs.im.mapper.ImGroupMapper;
import cc.mrbird.febs.im.service.IImGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户帐号 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
@Service
public class ImGroupServiceImpl extends ServiceImpl<ImGroupMapper, ImGroup> implements IImGroupService {

}
