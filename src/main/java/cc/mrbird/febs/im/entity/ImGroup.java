package cc.mrbird.febs.im.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户帐号
 * </p>
 *
 * @author Macw
 * @since 2020-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_im_group")
public class ImGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 群id
     */
    @TableId(value = "im_group_id", type = IdType.AUTO)
    private Integer imGroupId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 是否删除（0未删除1已删除）
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;


}
