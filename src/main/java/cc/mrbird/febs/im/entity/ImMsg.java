package cc.mrbird.febs.im.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author Macw
 * @since 2020-09-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_im_msg")
public class ImMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "msg_id", type = IdType.AUTO)
    private Integer msgId;

    /**
     * 发送人
     */
    @JsonInclude(value= JsonInclude.Include.NON_NULL)
    private Integer fromid;

    /**
     * 接收人
     */
    @JsonInclude(value= JsonInclude.Include.NON_NULL)
    private Integer toId;

    /**
     * group=群id    friend=发送者id
     */
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 类型   group friend
     */
    @JsonInclude(value= JsonInclude.Include.NON_NULL)
    private String type;

    /**
     * 乐观锁
     */
    @JsonInclude(value= JsonInclude.Include.NON_NULL)
    private Integer version;

    /**
     * 创建日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 是否系统消息
     */
    @TableField(exist = false)
    private Boolean system;


}
