package cc.mrbird.febs.im.entity.model;

import java.util.List;

import cc.mrbird.febs.system.entity.User;
import cn.hutool.core.util.ReflectUtil;
import lombok.Builder;
import lombok.Data;

@Data
public class LayimModel {

	private imUser mine;// 个人信息
	private List<Friend> friend;// 好友列表
	private List<Group> group;// 群

	public static imUser userConvert(User obj) {
		imUser imuser = new imUser();
		ReflectUtil.setFieldValue(imuser, "id", ReflectUtil.getFieldValue(obj, "userId"));
		ReflectUtil.setFieldValue(imuser, "username", ReflectUtil.getFieldValue(obj, "trueName"));
		ReflectUtil.setFieldValue(imuser, "sign", ReflectUtil.getFieldValue(obj, "description"));
		ReflectUtil.setFieldValue(imuser, "avatar", ReflectUtil.getFieldValue(obj, "avatar"));
		return imuser;
	}

	@Data
//	@JsonInclude(value= JsonInclude.Include.NON_NULL)
	public static class imUser {// 用户
		private Integer id;
		private String username;// 昵称
		private String status;// 状态 若值为offline代表离线，online或者不填为在线
		private String sign;// 签名
		private String avatar;// 头像
	}

	@Data
	public static class Friend {// 好友
		private Long id;
		private String groupname;// 群组昵称
		private List<imUser> list;// 好友列表
	}

	@Data
	public static class Group {// 群组
		private Long id;
		private String groupname;// 昵称
		private String avatar;// 头像
	}

	@Data
	@Builder
	public static class Member {// 群成员
		private List<imUser> list;// 列表
	}

}
