package cc.mrbird.febs.im.entity.model;

import cc.mrbird.febs.im.constant.MsgEnum;
import lombok.Data;

/**
 * @ author：马超伟
 * @ version 1.0.0
 */
@Data
public class WsMsgDTO {

	/**
	 * 数据类型
	 */
	private Integer type;

	/**
	 * 数据内容
	 */
	private Object msg;

	/**
	 * 提示消息
	 * @ Param msg
	 * @return
	 */
	public static WsMsgDTO buildMsg(MsgEnum type, Object msg) {
		WsMsgDTO resp = new WsMsgDTO();
		resp.setType(type.type);
		resp.setMsg(msg);
		return resp;
	}

}
