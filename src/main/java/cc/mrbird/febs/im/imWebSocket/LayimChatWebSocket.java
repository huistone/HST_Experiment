package cc.mrbird.febs.im.imWebSocket;

import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.im.constant.ImConstant;
import cc.mrbird.febs.im.constant.MsgEnum;
import cc.mrbird.febs.im.entity.ImMsg;
import cc.mrbird.febs.im.entity.model.ImMsgDTO;
import cc.mrbird.febs.im.entity.model.WsMsgDTO;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.StaticLog;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 单聊 @功能：
 * @author： 马超伟
 * @version 1.0.0
 */
@Slf4j
@Component
@ServerEndpoint("/api/im/chat/{sid}")
public class LayimChatWebSocket extends BaseWsSocket<Integer> {

	/**
	 * 连接建立成功调用的方法
	 * 
	 */
	@OnOpen
	public void onOpen(Session session, @PathParam("sid") Integer sid) {
		boolean exist = super.exist(sid);
		if (exist) {// 关闭连接
			this.alreadyLogin(session);
			return;
		}
		super.open(session, sid);
		// 发送上线消息
		this.sendAll(WsMsgDTO.buildMsg(MsgEnum.ON_LINE, sid));
		// 推送离线消息
		this.pushOffMsg();
	}

	/**
	 * 收到客户端消息后调用的方法
	 *
	 * @ Param msg 客户端发送过来的消息
	 * @throws IOException
	 */
	@OnMessage
	public void onMessage(String msg, Session session) {
		super.onMessage(msg, session);
		ImMsg vo = JSONUtil.toBean(msg, ImMsg.class);
		// 分类发送消息
		if (!BeanUtil.isEmpty(vo)) {
			// 获取接收人
			Collection<Integer> receiveIds = CollUtil.toList(vo.getToId());
			if (ImConstant.Type.GROUP.equals(vo.getType())) {// 是否为group
				// 查询,该群组下面的所有接收人ID
				// vo.getToId()就是班级id
				List<User> userList = userService.list(new QueryWrapper<User>().lambda()
						.select(User::getUserId)
						.eq(User::getDeptId, vo.getToId())
						.eq(User::getStatus, 1).eq(User::getIsDel, 1));
				List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda()
						.select(UserDept::getUserId)
						.eq(UserDept::getDeptId, vo.getToId()).eq(UserDept::getIsDel, 1));
				//学生id
				Set<Integer> collect = userList.stream().map(m -> m.getUserId().intValue()).collect(Collectors.toSet());
				//任课老师id
				Set<Integer> longs = userDeptList.stream().map(m -> m.getUserId().intValue()).collect(Collectors.toSet());
				//汇总
				collect.addAll(longs);
				receiveIds = collect;
			}
			// 添加数据库
			if (ImConstant.Type.GROUP.equals(vo.getType())) {
				imMsgService.saveOffLineMsg(vo, receiveIds);
			}else {
				imMsgService.save(vo);
			}
//			 发送
			List<Integer> remove = new ArrayList<>();
			remove.addAll(receiveIds);
			remove.remove(vo.getFromid());
			sendMsg(WsMsgDTO.buildMsg(MsgEnum.CHAT, vo), remove);
		}
	}

	@OnClose
	public void onClose() {
		super.onClose();
		// 发送下线消息
		this.sendAll(WsMsgDTO.buildMsg(MsgEnum.OFF_LINE, sid));
	}

	/**
	 * 给所有人发送消息
	 * 
	 * @ Param msg
	 */
	public void sendAll(WsMsgDTO msg) {
		webSocketSet.forEach((sid, item) -> {
			try {
				item.sendMessage(msg);
			} catch (Exception e) {
				log.error("[消息发送失败: user={},msg={},error={}]", sid, msg, e.getMessage());
			}
		});

	}

	/**
	 * 群发自定义消息
	 */
	public void sendMsg(WsMsgDTO msg, Collection<Integer> sids) {
		StaticLog.info("[push-msg-user={},msg={}.]", sids, msg);
		webSocketSet.forEach((sid, item) -> {
			try {
				boolean pass = CollUtil.isNotEmpty(sids) && sids.contains(sid);
				// 这里可以设定只推送给这个sid的，为null则全部推送
				if (pass) {
					item.sendMessage(msg);
				}
			} catch (Exception e) {
				log.error("[消息发送失败: user={},msg={},error={}]", sid, msg, e.getMessage());
			}
		});

	}

	@OnError
	public void onError(Session session, Throwable error) {
		super.onError(session, error);
	}

	public Map<Integer, Long> onlineList() {
		return webSocketSet.keySet().stream().collect(Collectors.groupingBy(k -> k, Collectors.counting()));

	}

	public Set<Integer> onlineKey() {
		return webSocketSet.keySet();
	}

	// 已经登录
	private void alreadyLogin(Session session) {
		try {
			session.getBasicRemote().sendText(JSONUtil.toJsonStr(WsMsgDTO.buildMsg(MsgEnum.CLOSE, "您已经登录")));
			if (session.isOpen()) {
				session.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new FebsException(e.getMessage());
		}
	}

	/**
	 * 推送离线消息
	 * 
	 */
	private void pushOffMsg() {
		// 查询消息
		List<ImMsg> list = imMsgService.list(Wrappers.<ImMsg>lambdaQuery()
				.eq(ImMsg::getToId, sid)
		.eq(ImMsg::getVersion,0));
		if (CollUtil.isNotEmpty(list)) {
			super.sendMessage(WsMsgDTO.buildMsg(MsgEnum.OFF_MSG, EntityUtil.listConver(list, ImMsgDTO.class)));
			// 删除离线消息
			list.stream().forEach(m->m.setVersion(1));
			imMsgService.updateBatchById(list);
		}
	}


}