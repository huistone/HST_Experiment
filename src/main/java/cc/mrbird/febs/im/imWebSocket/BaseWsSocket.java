package cc.mrbird.febs.im.imWebSocket;

import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.im.constant.MsgEnum;
import cc.mrbird.febs.im.entity.model.WsMsgDTO;
import cc.mrbird.febs.im.service.IImMsgService;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserService;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.StaticLog;
import org.springframework.stereotype.Service;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public abstract class BaseWsSocket<T> {

    private static Map<Class<? extends BaseWsSocket>, Map> pool = new ConcurrentHashMap<>();
    // concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    protected Map<Integer, BaseWsSocket<T>> webSocketSet = getWsSocketMap(this.getClass());
    // 与某个客户端的连接会话，需要通过它来给客户端发送数据
    protected Session session;
    // 接收sid
    protected Integer sid = 0 ;

    public static IImMsgService imMsgService;

    public static IDeptService deptService;

    public static IUserDeptService userDeptService;

    public static IUserService userService;

    /**
     * 连接建立成功调用的方法
     */
    public final void open(Session session,Integer sid) {
        this.session = session;
        webSocketSet.put(sid, this); // 加入set中
        this.sid = sid;
        try {
            StaticLog.info("[{}:有新用户开始连接:user={},当前在线人数为:{}]", serverName(), sid, webSocketSet.size());
            sendMessage(WsMsgDTO.buildMsg(MsgEnum.START, "连接成功!"));
        } catch (Exception e) {
            StaticLog.error("[{}:user={},websocket IO异常:{}]", serverName(), sid, e.getMessage());
        }
    }

    /**
     * 连接关闭调用的方法
     */
    protected void onClose() {

        webSocketSet.remove(this.sid); // 从set中删除
        StaticLog.info("{}:有一连接关闭！当前在线人数为:{}", serverName(), webSocketSet.size());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @ Param msg 客户端发送过来的消息
     * @throws IOException
     */
    protected void onMessage(String msg, Session session) {
        StaticLog.info("{}:收到来自用户[{}]的信息:{}", serverName(), sid, msg);
    }

    protected void onError(Session session, Throwable error) {
        StaticLog.error("{}:发生错误--->{}", serverName(), error);
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(Object msg) {
        try {
            this.session.getBasicRemote().sendText(JSONUtil.toJsonStr(msg));
        } catch (Exception e) {
            throw new FebsException(e.getMessage());
        }
    }

    protected String serverName() {
        return StrUtil.lowerFirst(this.getClass().getSimpleName());
    }

    ;

    /**
     * 是否存在
     *
     * @ Param sid
     * @return
     */
    protected boolean exist(Integer sid) {
        return webSocketSet.containsKey(sid);
    }

    private static Map getWsSocketMap(Class<? extends BaseWsSocket> clazz) {
        Map map = pool.get(clazz);
        if (map == null) {
            synchronized (BaseWsSocket.class) {
                map = pool.get(clazz);
                if (null == map) {
                    map = new ConcurrentHashMap<>();
                    pool.put(clazz, map);
                }
            }
        }
        return map;
    }

}