package cc.mrbird.febs.im.imWebSocket;

import cc.mrbird.febs.common.exception.FebsException;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ Author 马超伟
 * @ Date 2020-09-11 18:13
 * @ Description:
 * @ Version:
 */
public final class EntityUtil {

    public static <T, S> List<T> listConver(List<S> source, Class<T> clazz) {
        if (ObjectUtil.isNull(clazz)) {
            throw new FebsException("class is null");
        } else {
            return !CollUtil.isEmpty(source) ? (List)source.stream().map((s) -> {
                return BeanUtil.toBean(s, clazz);
            }).collect(Collectors.toList()) : Collections.emptyList();
        }
    }

}
