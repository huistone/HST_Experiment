package cc.mrbird.febs.common.controller;

import cc.mrbird.febs.app.entity.vo.UserLoginVo;
import cc.mrbird.febs.app.jwt.constant.Constant;
import cc.mrbird.febs.app.jwt.util.JwtUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.service.RedisService;
import cc.mrbird.febs.common.service.SendSmsService;
import cc.mrbird.febs.common.utils.MemberRulesUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static cc.mrbird.febs.external.oss.OssConfig.download;
import static cc.mrbird.febs.external.oss.OssConfig.upload;

/**
 * @ author 马超伟
 */
public class BaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IUserService userService;
    @Resource
    private IUserRoleService userRoleService;
    @Resource
    private IRoleService roleService;
    @Resource
    private SendSmsService sendSmsService;
    @Resource
    private RedisService redisService;
    @Resource
    private IDeptService deptService;
    @Resource
    private IUserDeptService userDeptService;

    public static final int USE_INFO = 1;
    public static final int FAIL = 0;

    /**
     * @ Description: 获取授权实体
     * @ Param: []
     * @ return: org.apache.shiro.subject.Subject
     * @ Author: 马超伟
     * @ Date: 2020/5/14
     */
    private Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    /**
     * @ Description: 获取当前登录用户
     * @ Param: []
     * @ return: cc.mrbird.febs.system.entity.User
     * @ Author: 马超伟
     * @ Date: 2020/5/14
     */
    public User getCurrentUser() {
        return (User) getSubject().getPrincipal();
    }

    /**
     * @ Author wangke
     * @ Description
     * @ Date 9:06 2020/12/29
     * @ Param
     * @ return
     */
    public User getCurrentUserByToken(HttpServletRequest request){
        String token = request.getHeader("AccessToken");

        if (StringUtils.isBlank(token)){
            return null;
        }

        String username = JwtUtil.getClaim(token, Constant.ACCOUNT);

        if (username == null){
            return null;
        }

        UserLoginVo user = (UserLoginVo)redisService.get(Constant.PREFIX_LOGIN_CUSTOMER + username);

        if (user != null){
            User user1 = new User();
            BeanUtils.copyProperties(user,user1);
            return user1;
        }

        return userService.findByName(username);
    }

    /**
     * @ Description: 根据传入的用户，获取该用户的角色信息
     * @ Param: [user]
     * @ return: java.util.List
     * @ Author: 马超伟
     * @ Date: 2020/4/16
     * @ return
     */
    protected Role getCurrentRole(User user) {
        UserRole userRole = userRoleService.getOne(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId, user.getUserId()));
        return roleService.getById(userRole.getRoleId());
    }

    /**
     * @ Description: 获取session对象
     * @ Param: []
     * @ return: org.apache.shiro.session.Session
     * @ Author: 马超伟
     * @ Date: 2020/4/16
     */
    protected Session getSession() {
        return getSubject().getSession();
    }

    protected Session getSession(Boolean flag) {
        return getSubject().getSession(flag);
    }

    protected void login(AuthenticationToken token) {
        getSubject().login(token);
    }

    protected Map<String, Object> getDataTable(IPage<?> pageInfo) {
        return getDataTable(pageInfo, FebsConstant.DATA_MAP_INITIAL_CAPACITY);
    }


    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
    }

    public HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getResponse();
    }


    /**
     * 转换初始大小为常见单位
     * @ Param size
     * @ return
     */
    public static String getSize(long size) {
        final long unit = 1024;
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");

        String[] units = {"B", "KB", "MB", "GB", "TB"};
        for (int i = 0; i < units.length; i++) {
            double d = getNumSize(size, i);
            if (d < unit) {
                return df.format(d) + units[i];
            }
        }
        return null;
    }

    //单位换算
    private static double getNumSize(long size, int n) {
        return size / Math.pow(1024, n);
    }

    protected Map<String, Object> getDataTable(IPage<?> pageInfo, int dataMapInitialCapacity) {
        Map<String, Object> data = new HashMap<>(dataMapInitialCapacity);
        data.put("rows", pageInfo.getRecords());
        data.put("total", pageInfo.getTotal());
        return data;
    }




    /**
     * @ Description: 阿里云文件上传
     * @ Param: [file：文件名, basePath：基础一级目录路径]
     * @ return: java.lang.String
     * @ Author: 马超伟
     * @ Date: 2020/5/14
     */
    public String aliOSSUpload(MultipartFile file, String basePath) throws IOException {
        return upload(file, basePath);
    }

    /**
     * @ Description: 阿里云文件下载
     * @ Param: [objectName：下载的文件名称路径]
     * @ return: void
     * @ Author: 马超伟
     * @ Date: 2020/5/14
     */
    public void downloadOss(String objectName) {
        download(objectName, getResponse());
    }


    /**
     * @ Description: 发送短信验证码方法封装
     * @ Param: [mobile, templateCode]
     * @ return: boolean
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     */
    public boolean sendSms(String mobile, String templateCode) {
        String smsCode = MemberRulesUtil.createRandom(true, 4);
        String data = sendSmsService.sendSms(mobile, templateCode, smsCode);
        if (data != null) {
            JSONObject jsonObject = JSONObject.parseObject(data);
            String code = (String) jsonObject.get("Code");
            Boolean set = redisService.set(mobile, smsCode, 300L);
            return "OK".equals(code) && set;
        }
        return false;
    }


    /**
     * @ Description: 通过deptId 找该班级对应的班级名称全路径
     * @ Param: [deptId]
     * @ return: java.lang.String
     * @ Author: 马超伟
     * @ Date: 2020/6/24
     */
    public String getDeptName(Long deptId) {
        List<String> deptNames = getDeptNames(deptId, new ArrayList<>());
        Collections.reverse(deptNames);
        return deptNames.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(",", "/")
                .replace(" ", "");
    }
    private List<String> getDeptNames(Long deptId, List<String> deptName) {
        Dept dept = deptService.getById(deptId);
        if (dept == null) {
            return deptName;
        }
        if (dept.getParentId() == 0) {
            deptName.add(dept.getDeptName());
        } else {
            deptName.add(dept.getDeptName());
            getDeptNames(dept.getParentId(), deptName);
        }
        return deptName;
    }

    /**
     * @ Description: 验证当前传入的老师是否是该班级的主课老师
     * @ Param: [deptId, courseId, userId]
     * @ return: cc.mrbird.febs.system.entity.UserDept
     * @ Author: 马超伟
     * @ Date: 2020/6/29
     */
    public UserDept validTeacherByCourse(Long deptId, Long courseId, Long userId) {
        UserDept userDept = null;
        try {
            userDept = userDeptService.getOne(new QueryWrapper<UserDept>().lambda()
                    .eq(UserDept::getDeptId, deptId)
                    .eq(UserDept::getCourseId, courseId)
                    .eq(UserDept::getUserId, userId)
                    .eq(UserDept::getType, 1));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/api/teacherProject/validTeacherByCourse,老师主副课查询失败：" + e.getMessage());
        }
        return userDept;
    }



    /**
     * @ Description: 通过流接收前端传来的json数据
     * @ Param: [request]
     * @ return: com.alibaba.fastjson.JSONArray
     * @ Author: 马超伟
     * @ Date: 2020/5/15
     */
    public JSONArray getJSONParam(HttpServletRequest request){
        JSONArray jsonParam = null;
        try {
            // 获取输入流
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));
            // 写入数据到Stringbuilder
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = streamReader.readLine()) != null) {
                sb.append(line);
            }
            String toString = sb.toString();
            //String 转 JSONArray 数组
            jsonParam = JSONArray.parseArray(toString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonParam;
    }

    public String getUrl(MultipartFile file){
        String basePath = "customerCommit";
        String url = "";
        try {
            //调用阿里云OSS的上传方法
            url = aliOSSUpload(file, basePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }





}
