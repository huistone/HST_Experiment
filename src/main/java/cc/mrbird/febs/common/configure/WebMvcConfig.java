package cc.mrbird.febs.common.configure;

import cc.mrbird.febs.app.interceptor.JwtInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA.
 *
 * @author admin
 * @ Auther: 马超伟
 * @ Date: 2020/04/24/17:30
 * @ Description:
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {



    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //添加映射路径
        registry.addMapping("/api/**")
                //放行哪些原始域
                .allowedOrigins("*")
                //是否发送Cookie信息
                .allowCredentials(true)
                //放行哪些原始域(请求方式)
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
                //放行哪些原始域(头部信息)
                .allowedHeaders("*","Content-Type,authorization","AccessToken")
                //暴露哪些头部信息（因为跨域访问默认不能获取全部头部信息）
                .exposedHeaders("Header1", "Header2")
                //跨域允许时间
                .maxAge(3600);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new JwtInterceptor())
                .addPathPatterns("/app/**")
                .excludePathPatterns(
                "/app/mobile/login",
                "/app/home/getOpenCourse",
                "/app/back/**",
                "/app/project/getSingleProjectInfo",
                "/app/mobile/isLogin",
                "/app/mobile/regist",
                "/app/mobile/validSms",
                "/app/mobile/getMobileUserInfo",
                "/app/mobile/sendSmsMsg",
                "/app/mobile/updateProfile",
                "/app/mobile/MobileLogin",
                "/app/mobile/sendSmsMsgByForgetPassword",
                "/app/mobile/sendSmsMsgByMobileLogin",
                "/app/mobile/resetPassword",
                "/app/home/getHostProject",
                "/app/home/getCourseAndProject");
    }

}
