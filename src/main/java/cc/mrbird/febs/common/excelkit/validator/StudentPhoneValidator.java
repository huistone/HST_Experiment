package cc.mrbird.febs.common.excelkit.validator;

import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuwenze.poi.util.ValidatorUtil;
import com.wuwenze.poi.validator.Validator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName StudentPhoneValidator
 * @ Description TODO
 * @Author wangke
 * @Data 2020-7-28 10:11
 * @Version 1.0
 */
@Component
public class StudentPhoneValidator implements Validator {

    private static StudentPhoneValidator studentPhoneValidator;

    @Resource
    private IUserService userService;


    @PostConstruct
    public void init(){
        studentPhoneValidator = this;
        studentPhoneValidator.userService = this.userService;
    }

    @Override
    public String valid(Object value) {
        String phone = (String) value;
        if(phone==null){
            return "手机号不能为空";
        }
        if(!ValidatorUtil.isMobile(phone)){
            return "请输入正确的手机号码";
        }
        if(studentPhoneValidator.userService.getOne(new QueryWrapper<User>()
                .lambda().eq(User::getIsDel,1).eq(User::getMobile,phone))!=null){
            return "手机号已存在";
        }

        return null;
    }
}
