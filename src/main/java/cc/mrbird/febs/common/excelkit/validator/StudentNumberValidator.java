package cc.mrbird.febs.common.excelkit.validator;

import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuwenze.poi.validator.Validator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName StudentNumberValidator
 * @ Description TODO
 * @Author wangke
 * @Data 2020-7-28 9:26
 * @Version 1.0
 */
@Component
public class StudentNumberValidator implements Validator {

    private static StudentNumberValidator studentNumberValidator;

    @Resource
    private IUserService userService;


    @PostConstruct
    public void init(){
        studentNumberValidator = this;
        studentNumberValidator.userService = this.userService;
    }

    @Override
    public String valid(Object value) {
        String idNumber = (String) value;
        if(value==null){
            return "学号不能为空";
        }
        String idnumber="";
        for(int i = 0;i<idNumber.length();i++){
            if(idNumber.charAt(i)>=48&&idNumber.charAt(i)<=57){
                idnumber+=idNumber.charAt(i);
            }
        }
        if(idnumber.length()<3||idnumber.length()>12){
            return "学号/工号的长度应是3-12位";
        }
        if(studentNumberValidator.userService.getOne(new QueryWrapper<User>()
                .lambda().eq(User::getIsDel,1).eq(User::getIdNumber,idNumber))!=null){
            return "学号已重复";
        }
        return null;
    }
}
