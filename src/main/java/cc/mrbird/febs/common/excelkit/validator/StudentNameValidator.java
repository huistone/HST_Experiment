package cc.mrbird.febs.common.excelkit.validator;

import cc.mrbird.febs.system.service.IUserService;
import com.wuwenze.poi.validator.Validator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName StudentNameValidator
 * @ Description TODO
 * @Author wangke
 * @Data 2020-7-28 10:51
 * @Version 1.0
 */

@Component
public class StudentNameValidator implements Validator {

    private static StudentNameValidator studentNameValidator;

    @Resource
    private IUserService userService;

    @PostConstruct
    public void init(){
        studentNameValidator = this;
        studentNameValidator.userService = this.userService;
    }


    @Override
    public String valid(Object value) {
        String trueName = (String) value;
        if(trueName == null){
            return "真实姓名不能为空";
        }
        return null;
    }
}
