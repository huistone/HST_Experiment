package cc.mrbird.febs.common.excelkit.util;

import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName ExcelUtil
 * @ Description TODO
 * @Author wangke
 * @Data 2020-7-28 10:31
 * @Version 1.0
 */
@Component
public class ExcelKitUtil {

    @Resource
   private IUserService userService;

    @Resource
   private IDeptService deptService;

    private static ExcelKitUtil excelKitUtil;

    @PostConstruct
    public void init(){
        excelKitUtil = this;
        excelKitUtil.userService = this.userService;
        excelKitUtil.deptService = this.deptService;
    }
}
