package cc.mrbird.febs.common.excelkit.validator;

import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuwenze.poi.validator.Validator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName TeacherNumberValidator
 * @ Description TODO
 * @Author wangke
 * @Data 2020-8-13 15:10
 * @Version 1.0
 */
@Component
public class TeacherNumberValidator implements Validator {
    private static TeacherNumberValidator teacherNumberValidator;

    @Resource
    private IUserService userService;


    @PostConstruct
    public void init(){
        teacherNumberValidator = this;
        teacherNumberValidator.userService = this.userService;
    }

    @Override
    public String valid(Object value) {
        String idNumber = (String) value;
        if(value==null){
            return "工号不能为空";
        }
        String idnumber="";
        for(int i = 0;i<idNumber.length();i++){
            if(idNumber.charAt(i)>=48&&idNumber.charAt(i)<=57){
                idnumber+=idNumber.charAt(i);
            }
        }
        if(idnumber.length()<3||idnumber.length()>12){
            return "工号的长度应是3-12位";
        }
        if(teacherNumberValidator.userService.getOne(new QueryWrapper<User>()
                .lambda().eq(User::getIsDel,1).eq(User::getIdNumber,idNumber))!=null){
            return "工号已重复";
        }
        return null;
    }
}
