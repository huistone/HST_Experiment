package cc.mrbird.febs.common.excelkit.validator;

import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuwenze.poi.validator.Validator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName StudentDeptValidator
 * @ Description TODO
 * @Author wangke
 * @Data 2020-7-28 10:16
 * @Version 1.0
 */
@Component
public class StudentDeptValidator implements Validator {

    private static StudentDeptValidator studentDeptValidator;

    @Resource
    private IDeptService deptService;

    @PostConstruct
    public void init(){
        studentDeptValidator = this;
        studentDeptValidator.deptService = this.deptService;
    }

    @Override
    public String valid(Object value) {
        String course = (String) value;
        if(course == null){
            return "班级不能为空";
        }

        Dept dept = studentDeptValidator.deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptName, course));

        if(dept==null){
            return "暂无该班级,请核实班级名称";
        }
        return null;
    }
}
