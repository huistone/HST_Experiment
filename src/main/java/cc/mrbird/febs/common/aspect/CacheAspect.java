package cc.mrbird.febs.common.aspect;

import cc.mrbird.febs.common.annotation.AddCache;
import cc.mrbird.febs.common.annotation.DeleteCache;
import cc.mrbird.febs.common.service.RedisService;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.google.gson.JsonObject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * @ ClassName CacheAspect
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/2/26 10:03
 * @ Version 1.0
 */
@Aspect
@Configuration
@Slf4j
public class CacheAspect extends AspectSupport {

    @Resource
    private RedisService redisService;

    @Before("@annotation(cc.mrbird.febs.common.annotation.DeleteCache)")
    public void deleteCache(JoinPoint joinPoint) throws Throwable {
        Method method = resolveMethod((ProceedingJoinPoint) joinPoint);
        DeleteCache annotation = method.getAnnotation(DeleteCache.class);
        String[] key = annotation.key();
        log.info("缓存key======>"+key);
        redisService.del(key);
    }

    @SneakyThrows
    @Around("@annotation(cc.mrbird.febs.common.annotation.AddCache)")
    public Object addCache(ProceedingJoinPoint point){
        Method method = resolveMethod(point);
        AddCache annotation = method.getAnnotation(AddCache.class);
        String key = annotation.key();
        log.info("添加缓存注解=====>"+key);
        Object result=  redisService.get(key);
        if (result == null){
            log.info("缓存未命中,查询数据库");
             result = point.proceed();
            redisService.set(key,result,604800L);
        }else{
            log.info("缓存击中!!!");
        }
        return result;
    }

}
