package cc.mrbird.febs.common.aspect;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
/**
 * @ Author 马超伟
 * @ Date 2020-10-09 15:41
 * @ Description:spring 在加载controller 的时候，发现这些类里面的方法名称有重复的，
 * 所以给你自动生成了一些名称，用以区分这些方法
 * 通过aop 切面切入改方法，不进行日志打印，或者以警告的方式进行日志打印，看到警告则进行规避，代码如下
 * @ Version:  https://www.codeleading.com/article/62262541279/
 */
@Slf4j
@Component
@Aspect
public class UserCachingOperationNameGenerator {

    private Map<String, Integer> generated = newHashMap();


    @Pointcut("execution(* springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator.startingWith(String))")
    public void c() {
    }


    @Around("c()")
    public Object a(ProceedingJoinPoint point) {
        Object[] args = point.getArgs();
        return startingWith(String.valueOf(args[0]));
    }

    private String startingWith(String prefix) {
        if (generated.containsKey(prefix)) {
            generated.put(prefix, generated.get(prefix) + 1);
            String nextUniqueOperationName = String.format("%s_%s", prefix, generated.get(prefix));
//            log.warn("组件中存在相同的方法名称，自动生成组件方法唯一名称进行替换: {}", nextUniqueOperationName);
            return nextUniqueOperationName;
        } else {
            generated.put(prefix, 0);
            return prefix;
        }
    }
}
