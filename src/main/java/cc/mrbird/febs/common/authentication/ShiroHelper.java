package cc.mrbird.febs.common.authentication;

import cc.mrbird.febs.common.annotation.Helper;
import org.apache.shiro.authz.AuthorizationInfo;

/**
 * @author 马超伟
 */
@Helper
public class ShiroHelper extends ShiroRealm {

    /**
     * 获取当前用户的角色和权限集合
     *
     * @return AuthorizationInfo
     */
    public AuthorizationInfo getCurrentuserAuthorizationInfo() {
        return super.doGetAuthorizationInfo(null);
    }
}
