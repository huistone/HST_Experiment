package cc.mrbird.febs.common.utils;

import cc.mrbird.febs.external.oss.OSSConstant;
import cc.mrbird.febs.external.oss.OssConfig;
import com.aliyun.oss.OSS;

import java.net.URL;
import java.util.Date;

/**
 * @ Author 马超伟
 * @ Date 2020-09-12 11:06
 * @ Description:
 * @ Version:
 */
public class OSSUtil {


    /**
     * 获得url链接
     *
     * @ Param key
     * @return
     */
    public String getUrl(String key) {
        // 设置URL过期时间为10年 3600l* 1000*24*365*10
        OSS ossClient = OssConfig.getOssClient();
        Date expiration = new Date(System.currentTimeMillis() + 3600L * 1000 * 24 * 365 * 10);
        // 生成URL
        URL url = ossClient.generatePresignedUrl(OSSConstant.BUCKET_NAME, key, expiration);
        if (url != null) {
            String host ="https://"+url.getHost();
            return host;
        }
        return  "";
    }


}
