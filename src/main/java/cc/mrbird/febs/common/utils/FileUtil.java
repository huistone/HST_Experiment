package cc.mrbird.febs.common.utils;

import cc.mrbird.febs.common.constant.FebsConstant;
import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ws.schild.jave.MultimediaInfo;
import ws.schild.jave.MultimediaObject;
//import ws.schild.jave.MultimediaInfo;
//import ws.schild.jave.MultimediaObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author 马超伟
 */
@Slf4j
public class FileUtil {

    private static final int BUFFER = 1024 * 8;

    /**
     * 压缩文件或目录
     *
     * @ Param fromPath 待压缩文件或路径
     * @ Param toPath   压缩文件，如 xx.zip
     */
    public static void compress(String fromPath, String toPath) throws IOException {
        File fromFile = new File(fromPath);
        File toFile = new File(toPath);
        if (!fromFile.exists()) {
            throw new FileNotFoundException(fromPath + "不存在！");
        }
        try (
                FileOutputStream outputStream = new FileOutputStream(toFile);
                CheckedOutputStream checkedOutputStream = new CheckedOutputStream(outputStream, new CRC32());
                ZipOutputStream zipOutputStream = new ZipOutputStream(checkedOutputStream)
        ) {
            String baseDir = "";
            compress(fromFile, zipOutputStream, baseDir);
        }
    }

    /***
     * @Author wangke
     * @ Description 预览pdf
     * @Date 14:48 2020/9/7
     * @ Param [response, fileName, fileUrl]
     * @return void
     */
    public static void previewPdf(HttpServletResponse response, String fileName, String fileUrl) {
        URL file = null;
        InputStream inputStream = null;
        String codedFileName = null;
        try {
            codedFileName = URLEncoder.encode(fileName + ".pdf", "UTF-8");
            file = new URL(fileUrl);
            inputStream = new BufferedInputStream(file.openStream());
            BufferedInputStream br = new BufferedInputStream(inputStream);
            byte[] bs = new byte[1024];
            int len = 0;
            response.reset();
            URL u = new URL("file:///" + fileUrl);
            String contentType = u.openConnection().getContentType();
            response.setContentType(contentType);
            response.setHeader("Content-Disposition", "inline;filename="
                    + codedFileName);
            // 文件名应该编码成utf-8，注意：使用时，我们可忽略这句
            OutputStream out = response.getOutputStream();
            while ((len = br.read(bs)) > 0) {
                out.write(bs, 0, len);
            }
            out.flush();
            out.close();
            br.close();
        } catch (Exception e) {
            log.info("预览pdf失败");
        }

    }


    /**
     * 文件下载
     *
     * @throws Exception Exception
     * @ Param filePath 待下载文件路径
     * @ Param fileName 下载文件名称
     * @ Param delete   下载后是否删除源文件
     * @ Param response HttpServletResponse
     */
    public static void download(String filePath, String fileName, Boolean delete, HttpServletResponse response) throws Exception {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new Exception("文件未找到");
        }

        String fileType = getFileType(file);
        if (!fileTypeIsValid(fileType)) {
            throw new Exception("暂不支持该类型文件下载");
        }
        response.setHeader("Content-Disposition", "attachment;fileName=" + java.net.URLEncoder.encode(fileName, "utf-8"));
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        try (InputStream inputStream = new FileInputStream(file); OutputStream os = response.getOutputStream()) {
            byte[] b = new byte[2048];
            int length;
            while ((length = inputStream.read(b)) > 0) {
                os.write(b, 0, length);
            }
        } finally {
            if (delete) {
                delete(filePath);
            }
        }
    }

    /**
     * 递归删除文件或目录
     *
     * @ Param filePath 文件或目录
     */
    public static void delete(String filePath) {
        File file = new File(filePath);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                Arrays.stream(files).forEach(f -> delete(f.getPath()));
            }
        }
        file.delete();
    }

    /**
     * 获取文件类型
     *
     * @return 文件类型
     * @throws Exception Exception
     * @ Param file 文件
     */
    private static String getFileType(File file) throws Exception {
        Preconditions.checkNotNull(file);
        if (file.isDirectory()) {
            throw new Exception("file不是文件");
        }
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }


    /**
     * 校验文件类型是否是允许下载的类型
     * （出于安全考虑：https://github.com/wuyouzhuguli/FEBS-Shiro/issues/40）
     *
     * @return Boolean
     * @ Param fileType fileType
     */
    private static Boolean fileTypeIsValid(String fileType) {
        Preconditions.checkNotNull(fileType);
        fileType = StringUtils.lowerCase(fileType);
        return ArrayUtils.contains(FebsConstant.VALID_FILE_TYPE, fileType);
    }

    private static void compress(File file, ZipOutputStream zipOut, String baseDir) throws IOException {
        if (file.isDirectory()) {
            compressDirectory(file, zipOut, baseDir);
        } else {
            compressFile(file, zipOut, baseDir);
        }
    }

    private static void compressDirectory(File dir, ZipOutputStream zipOut, String baseDir) throws IOException {
        File[] files = dir.listFiles();
        if (files != null && ArrayUtils.isNotEmpty(files)) {
            for (File file : files) {
                compress(file, zipOut, baseDir + dir.getName() + "/");
            }
        }
    }

    private static void compressFile(File file, ZipOutputStream zipOut, String baseDir) throws IOException {
        if (!file.exists()) {
            return;
        }
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            ZipEntry entry = new ZipEntry(baseDir + file.getName());
            zipOut.putNextEntry(entry);
            int count;
            byte[] data = new byte[BUFFER];
            while ((count = bis.read(data, 0, BUFFER)) != -1) {
                zipOut.write(data, 0, count);
            }
        }
    }

    /**
     * MultipartFile file转File
     *
     * @return
     * @throws IOException
     * @ Param file
     */
    public static File MultipartFileConverFile(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        String prefix = fileName.substring(fileName.lastIndexOf("."));
        // 用uuid作为文件名，防止生成的临时文件重复
        File excelFile = File.createTempFile(RandomUtil.getUUID(), prefix);
        // MultipartFile to File
        file.transferTo(excelFile);
        return excelFile;
    }


    /**
     * 视频时长
     *
     * @param FileUrl
     * @return
     */
    public static String ReadVideoTime(String FileUrl, HttpServletRequest request) {
        String length = "";
        try {
            URL url = new URL(FileUrl);
            InputStream inputStream = new BufferedInputStream(url.openStream());
            // 模板临时目录
            String rootPath = request.getServletContext().getRealPath("template_temp2/");
            //临时文件名
            String[] split = FileUrl.split("/");
//            String filePath = rootPath + "_" + "thinkScore/"+ split[split.length-1];
            // 模板临时目录
//            String rootPath = request.getServletContext().getRealPath("template_temp/");
            //临时文件名
            String filePath = rootPath +  "_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "prepareScore";
//            String filePath = "C:\\Users\\ma\\temp\\"+ split[split.length-1];
            File source = new File(filePath);
            if(!source.getParentFile().exists()){ //如果文件的目录不存在
                source.getParentFile().mkdirs(); //创建目录
            }
            inputStream2File(inputStream,filePath);
            MultimediaObject instance = new MultimediaObject(source);
            MultimediaInfo result = instance.getInfo();
            long ls = result.getDuration() / 1000;
            Integer hour = (int) (ls / 3600);
            Integer minute = (int) (ls % 3600) / 60;
            Integer second = (int) (ls - hour * 3600 - minute * 60);
            String hr = hour.toString();
            String mi = minute.toString();
            String se = second.toString();
            if (hr.length() < 2) {
                hr = "0" + hr;
            }
            if (mi.length() < 2) {
                mi = "0" + mi;
            }
            if (se.length() < 2) {
                se = "0" + se;
            }
            length = hr + ":" + mi + ":" + se;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length;
    }

    /**
     * 将inputStream转化为file
     * @param destination 写入本地目录
     * @param input 输入流
     */
    public static void inputStream2File(InputStream input,String destination) throws IOException {
        int index;
        byte[] bytes = new byte[1024];
        FileOutputStream downloadFile = new FileOutputStream(destination);
        while ((index = input.read(bytes)) != -1) {
            downloadFile.write(bytes, 0, index);
            downloadFile.flush();
        }
        input.close();
        downloadFile.close();
    }

}
