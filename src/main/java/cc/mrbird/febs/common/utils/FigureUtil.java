package cc.mrbird.febs.common.utils;

import java.math.BigDecimal;

/**
 * @ClassName FigureUtil
 * @ Description 数字转换工具类
 * @Author wangke
 * @Data 2020-8-5 14:58
 * @Version 1.0
 */
public class FigureUtil {

    //截取两位小数
    public static Double getDouble(double number){
        return  new BigDecimal(number).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
