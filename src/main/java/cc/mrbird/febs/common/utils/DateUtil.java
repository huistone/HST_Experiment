package cc.mrbird.febs.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * 时间工具类
 *
 * @author 马超伟
 */
public class DateUtil {

    public static final String FULL_TIME_PATTERN = "yyyyMMddHHmmss";

    public static final String FULL_TIME_PATTERN_ = "yyyy年MM月dd日";

    public static final String FULL_TIME_SPLIT_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String CST_TIME_PATTERN = "EEE MMM dd HH:mm:ss zzz yyyy";

    public static String formatFullTime(LocalDateTime localDateTime) {
        return formatFullTime(localDateTime, FULL_TIME_PATTERN);
    }

    /**
     * @Author wangke
     * @ Description // LocalDateTime转Date
     * @Date 17:55 2020/8/21
     * @ Param [LocalTime]
     * @return java.util.Date
     */
    public static Date localDateTimeToDate(LocalDateTime LocalTime) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = LocalTime.atZone(zone).toInstant();
        Date date = Date.from(instant);
        return date;
    }

    /**
     * @Author wangke
     * @ Description //LocalDateTime转 String
     * @Date 17:55 2020/8/21
     * @ Param [localDateTime, pattern]
     * @return java.lang.String
     */
    public static String formatFullTime(LocalDateTime localDateTime, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(dateTimeFormatter);
    }

    /**
     * @Author wangke
     * @ Description //Date转Sting
     * @Date 17:55 2020/8/21
     * @ Param [date, dateFormatType]
     * @return java.lang.String
     */
    public static String getDateFormat(Date date, String dateFormatType) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatType, Locale.CHINA);
        return simpleDateFormat.format(date);
    }
    
    /**
     * @Author wangke
     * @ Description //国外日期
     * @Date 17:55 2020/8/21
     * @ Param [date, format]
     * @return java.lang.String
     */
    public static String formatCSTTime(String date, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CST_TIME_PATTERN, Locale.US);
        Date usDate = simpleDateFormat.parse(date);
        return DateUtil.getDateFormat(usDate, format);
    }
    /**
     * @Author wangke
     * @ Description //TODO
     * @Date 18:04 2020/8/21
     * @ Param [instant, format]
     * @return java.lang.String
     */
    public static String formatInstant(Instant instant, String format) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    public static LocalDateTime StringToLocalDateTime(String time){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();
        String localTime = df.format(localDateTime);
        LocalDateTime ldt = LocalDateTime.parse(time,df);
        return ldt;
    }

    public static LocalDateTime date2LocalDateTime(Date date) {
        if(null == date) {
            return null;
        }
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zoneId);
    }

}
