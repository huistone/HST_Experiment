package cc.mrbird.febs.common.constant;

import cc.mrbird.febs.common.exception.FebsException;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 常量
 *
 * @author 马超伟
 */
public class FebsConstant {

    // 注册用户角色ID
    public static final Long REGISTER_ROLE_ID = 2L;

    //公共启用
    public static final int USE = 1;

    //公共禁用
    public static final int DIS_USE = 0;

    //校园版用户
    public static final Integer SCHOOL_TYPE = 1;

    //企业版用户
    public static final Integer SOCIAL_TYPE = 0;


    //注册用户为学生角色
    public static final Long STUDENT_ID = 82L;

    //注册用户为顾客角色
    public static final Long CUSTOMER_ID = 86L;

    //注册用户为课程管理员角色
    public static final Long COURSE_MANAGER_ID = 87L;

    //注册用户为老师角色
    public static final Long TEACHER_ID = 81L;

    // 排序规则：降序
    public static final String ORDER_DESC = "desc";
    // 排序规则：升序
    public static final String ORDER_ASC = "asc";

    // 前端页面路径前缀
    public static final String VIEW_PREFIX = "febs/views/";

    // 验证码 Session Key
    public static final String CODE_PREFIX = "febs_captcha_";

    // 允许下载的文件类型，根据需求自己添加（小写）
    public static final String[] VALID_FILE_TYPE = {"xlsx", "zip"};

    /**
     * {@link cc.mrbird.febs.common.controller.BaseController}
     * getDataTable 中 HashMap 默认的初始化容量
     */
    public static final int DATA_MAP_INITIAL_CAPACITY = 4;

    public static final String USER_UNLOGIN_MESSAGE = "您还没有登录，请登录后再操作！";

    /**
     * 异步线程池名称
     */
    public static final String ASYNC_POOL = "febsAsyncThreadPool";

    /**
     * socket服务端口号
     */
    public static final Integer SOCKET_PORT = 6666;

    /**
     * socket服务IP地址
     */
//    public static final String  SOCKET_IP = "192.168.2.138";


    public static final String  SOCKET_IP = getIpAddress();

    private static  String getIpAddress(){
        String address = "127.0.0.1";
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            address = localHost.getHostAddress();
            return address;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            throw new FebsException("IP地址获取异常！");
        }
    }

}
