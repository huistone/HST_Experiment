package cc.mrbird.febs.common.properties;

import lombok.Data;

@Data

public class OssProperties {

    public String bucketName;

    private String endpoint;

    private String accessKeyId;

    private String accessKeySecret;


}
