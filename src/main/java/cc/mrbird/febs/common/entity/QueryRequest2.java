package cc.mrbird.febs.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * @ClassName QueryRequest2
 * @ Description TODO
 * @Author wangke
 * @Data 2020-8-17 15:51
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class QueryRequest2 extends QueryRequest implements Serializable {

    private static final long serialVersionUID = 3306429084140790873L;

    // 当前页面数据量
    private int pageSize = 20;
    // 当前页码
    private int pageNum = 1;
    // 排序字段
    private String field;
    // 排序规则，asc升序，desc降序
    private String order;
}
