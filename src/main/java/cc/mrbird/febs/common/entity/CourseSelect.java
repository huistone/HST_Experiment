package cc.mrbird.febs.common.entity;

import lombok.Data;

/**
 * @ClassName CourseSelect
 * @Description TODO
 * @Author admin
 * @Date 2020/12/25 10:58
 * @Version 1.0
 */
@Data
public class CourseSelect {

    private String name;

    private Long value;

    private Boolean selected;
}
