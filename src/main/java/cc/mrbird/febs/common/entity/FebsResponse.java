package cc.mrbird.febs.common.entity;

import org.springframework.http.HttpStatus;

import java.util.HashMap;

/**
 * @author 马超伟
 */
public class FebsResponse extends HashMap<String, Object> {

    private static final long serialVersionUID = -8713837118340960775L;

    public FebsResponse code(HttpStatus status) {
        status.value();
        this.put("code", status.value());
        return this;
    }

    public FebsResponse code(Integer status) {
        this.put("code", status);
        return this;
    }

    public FebsResponse msg(String message) {
        this.put("msg", message);
        return this;
    }

    public FebsResponse message(String message) {
        this.put("message", message);
        return this;
    }

    public FebsResponse data(Object data) {
        this.put("data", data);
        return this;
    }

    public FebsResponse success() {
        this.code(HttpStatus.OK);
        return this;
    }

    public FebsResponse complete(Boolean b) {
        if (b){
            return success().message("成功！");
        }else {
            return fail().message("失败！");
        }
    }

    public FebsResponse fail() {
        this.code(HttpStatus.INTERNAL_SERVER_ERROR);
        return this;
    }

    public FebsResponse no_login() {
        super.put("code", 400);
        this.message("您还没有登录，请登录后再操作！");
        return this;
    }

    public FebsResponse no_param() {
        super.put("code", 600);
        this.message("参数不足，请验证后再操作！");
        return this;
    }

    public FebsResponse put_code(Integer code,String message) {
        super.put("code", code);
        this.message(message);
        return this;
    }

    @Override
    public FebsResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
