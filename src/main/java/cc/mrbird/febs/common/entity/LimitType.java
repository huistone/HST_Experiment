package cc.mrbird.febs.common.entity;

/**
 * @author 马超伟
 */
public enum LimitType {
    /**
     * 传统类型
     */
    CUSTOMER,
    /**
     *  根据 IP地址限制
     */
    IP
}
