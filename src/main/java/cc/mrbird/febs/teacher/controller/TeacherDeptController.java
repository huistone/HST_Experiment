package cc.mrbird.febs.teacher.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/05/18/11:02
 * @ Description:
 */
@RestController("teacherDept")
@RequestMapping("/teacherDept")
public class TeacherDeptController extends BaseController {

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserService userService;

    /**
     * @ Description:  根据当前登录用户（老师）
     * 去查询该老师拥有的班级，学生，课程信息
     * @ Param: [dept, request]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @GetMapping("teacher/list")
    @RequiresPermissions("teacherDept:view")
    public FebsResponse teacherDeptList(Dept dept, QueryRequest request) {
        Page<Dept> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "dept_id", FebsConstant.ORDER_ASC, false);
        User user = getCurrentUser();
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, user.getUserId()));
        List<Long> collect = userDeptList.stream().map(UserDept::getDeptId).collect(Collectors.toList());
        IPage<Map<String, Object>> iPage = deptService.pageMaps(page, new QueryWrapper<Dept>().lambda().in(Dept::getDeptId, collect));
        List<Map<String, Object>> records = iPage.getRecords();
        for (Map<String, Object> record : records) {
            List<User> userList = userService.list(new QueryWrapper<User>().lambda().eq(User::getDeptId, record.get("DEPT_ID")));
            record.put("count", userList.size());
        }
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }
}
