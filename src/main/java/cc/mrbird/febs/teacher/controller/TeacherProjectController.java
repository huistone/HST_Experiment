package cc.mrbird.febs.teacher.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@RestController
@RequestMapping("/teacherProject")
public class TeacherProjectController extends BaseController {

    @Autowired
    private ITeacherProjectService teacherProjectService;

    @Autowired
    private IDeptService deptService;

    @Autowired
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ICourseService courseService;


    /**
     * @ Description: 查询老师所管理的所有试验项目
     * @ Param: [teacherProject]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @GetMapping("/getAllTeacherProject")
    public FebsResponse getAllTeacherProjects(TeacherProject teacherProject) {
        User currentUser = getCurrentUser();
        List<ExperimentProject> projectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getCourseId, currentUser.getCourseId()));
        return new FebsResponse().success().data(projectList);
    }

    /**
     * @ Description: 查询老师所管理的所有试验项目
     * @ Param: [request, teacherProject]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @RequestMapping("newList")
    @RequiresPermissions("teacherProject:view")
    public FebsResponse getTeacherProject(QueryRequest request, TeacherProject teacherProject) {
        User user = getCurrentUser();
        return new FebsResponse().success().data(this.teacherProjectService.findProjectDetailList(teacherProject, request, user));
    }

    @RequestMapping("/list")
    @RequiresPermissions("teacherProject:view")
    public FebsResponse getTeacherProjectList(QueryRequest request, TeacherProject teacherProject) {
        User currentUser = getCurrentUser();
        //获取当前登录用户信息
        Page<TeacherProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "project_id", FebsConstant.ORDER_ASC, false);
        IPage<TeacherProject> iPage = this.teacherProjectService.page(page, new QueryWrapper<TeacherProject>().lambda()
                .like(StringUtils.isNotBlank(teacherProject.getClassName()), TeacherProject::getClassName, teacherProject.getClassName())
                .like(StringUtils.isNotBlank(teacherProject.getProjectName()), TeacherProject::getProjectName, teacherProject.getProjectName())
                .like(TeacherProject::getTeacherId, currentUser.getUserId())
                .eq(IntegerUtils.isLongNotBlank(teacherProject.getDeptId()), TeacherProject::getDeptId, teacherProject.getDeptId())
        );
        List<TeacherProject> records = iPage.getRecords();
        for (TeacherProject r : records) {
            Dept dept = deptService.getById(r.getDeptId());
            if (dept != null) {
                r.setClassName(dept.getDeptName());
            }
            ExperimentProject eProject = experimentProjectService.getById(r.getProjectId());
            if (eProject != null) {
                r.setProjectName(eProject.getProjectName());
                if (IntegerUtils.isLongNotBlank(eProject.getCourseId())) {
                    r.setCourseName(courseService.getById(eProject.getCourseId()).getCourseName());
                }

            }


        }
        iPage.setRecords(records);
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * @ Description: 通过课程id查询老师名下已经开启的项目id列表
     * @ Param: [deptId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @RequestMapping("/getProjectIdByTeacher")
    public FebsResponse getProjectIdByTeacher(Integer deptId) {
        List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>()
                .lambda()
                .eq(TeacherProject::getDeptId, deptId)
                .eq(TeacherProject::getIsDel, FebsConstant.USE));
        List<Long> collect = teacherProjectList.stream().map(TeacherProject::getProjectId).collect(Collectors.toList());
        return new FebsResponse().success().data(collect);
    }

    /**
     * @ Description: 添加实验项目
     * @ Param: [deptId, projectId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @RequestMapping("/add")
    @RequiresPermissions("teacherProject:add")
    @ControllerEndpoint(operation = "新增实验项目", exceptionMessage = "新增实验项目失败")
    public FebsResponse teacherProjectAdd(Long deptId, String projectId) {
        String projectIds[] = projectId.split(",");
        List<String> projectIdsList = Arrays.asList(projectIds);
        User currentUser = getCurrentUser();
        //先删除老师下面已经开启的项目
        teacherProjectService.remove(new QueryWrapper<TeacherProject>().lambda().eq(TeacherProject::getDeptId, deptId).eq(TeacherProject::getTeacherId, currentUser.getUserId()));
        //然后重新添加
        for (String id : projectIds) {
            TeacherProject teacherProject = new TeacherProject();
            teacherProject.setTeacherId(currentUser.getUserId());
            teacherProject.setProjectId(Long.parseLong(id));
            teacherProject.setDeptId(deptId);
            teacherProject.setCourseId(experimentProjectService.getById(Integer.parseInt(id)).getCourseId());
            teacherProjectService.save(teacherProject);
        }
        return new FebsResponse().success();
    }

    /**
    * @ Description: 删除实验项目
    * @ Param: [teacherProjectId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @RequestMapping("/delete/{teacherProjectId}")
    @RequiresPermissions("teacherProject:delete")
    @ControllerEndpoint(operation = "删除实验项目", exceptionMessage = "删除实验项目失败")
    public FebsResponse deleteTeacherProject(@NotBlank(message = "{required}") @PathVariable String teacherProjectId) {
        String[] ids = teacherProjectId.split(StringPool.COMMA);

        List<String> list = Arrays.asList(ids);
        teacherProjectService.removeByIds(list);
        return new FebsResponse().success();
    }


}
