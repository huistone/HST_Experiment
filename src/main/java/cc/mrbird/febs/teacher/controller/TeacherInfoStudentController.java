package cc.mrbird.febs.teacher.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/05/14/17:08
 * @ Description:
 */
@RestController("teacherInfoStudent")
@RequestMapping("/teacherInfoStudent")
public class TeacherInfoStudentController extends BaseController {

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IUserService userService;

    @Resource
    private IDeptService deptService;

    /**
     * @ Description: 查询学生信息
     * @ Param: [request, deptId]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/5
     */
    @GetMapping("/studentInfo")
    @RequiresPermissions("deptStudent:view")
    public FebsResponse selectUserStudent(QueryRequest request, Integer deptId) {
        //查询，用户表的学生角色用户，状态有效，且学生所在班级为传入的班级ID
        User currentUser = getCurrentUser();
        List<UserDept> userDepts = userDeptService.list(new QueryWrapper<UserDept>()
                .lambda()
                .eq(UserDept::getUserId, currentUser.getUserId()));
        if (userDepts.size() != 0 && userDepts != null) {
            List<Long> deptsList = new ArrayList<>();
            userDepts.forEach(
                    i ->
                            deptsList.add(i.getDeptId())
            );
            Page<User> page = new Page(request.getPageNum(), request.getPageSize());
            IPage<User> users = userService.page(page, new QueryWrapper<User>().eq(IntegerUtils.isNotBlank(deptId), "dept_id", deptId).in("DEPT_ID", deptsList));

            List<User> userRecords = users.getRecords();
            userRecords.forEach(i -> {
                if (i.getDeptId() != null) {
                    Dept dept = deptService.getById(i.getDeptId());
                    i.setDeptName(dept.getDeptName());
                }
            });
            Map<String, Object> dataTable = getDataTable(users);
            return new FebsResponse().success().data(dataTable);
        }
        return new FebsResponse().success().message("数据为空");
    }

}
