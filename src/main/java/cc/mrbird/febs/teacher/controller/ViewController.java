package cc.mrbird.febs.teacher.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping(FebsConstant.VIEW_PREFIX)
@Controller("teacherView")
public class ViewController extends BaseController {

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IExperimentAnswerService answerService;

    @Resource
    private IExperimentQuestionService questionService;

    /**
     * Teacher，老师管理
     */
    @GetMapping( "system/teacher/dept")
    @RequiresPermissions("teacherDept:view")
    public String systemTeacherDept() {
        return FebsUtil.view("teacher/dept/dept");
    }

    @GetMapping("teacherView/student")
    @RequiresPermissions("deptStudent:view")
    public String selectStudentDept(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/dept/deptStudent");
    }

    /**
     *GuiYongkang
     * 2020/4/02 10:36am
     * 控制老师账号的查询课程项目
     */
    @GetMapping("teacher/select/project")
    @RequiresPermissions("teacherProject:view")
    public String selectTeacherProject(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/teacherProject/teacherProject");
    }

    /**
     * GuiYongkang
     * 2020/4/02 2:35pm
     * 控制老师账号课程项目的增加页面跳转
     */
    @GetMapping("teacherView/teacherProject/add")
    @RequiresPermissions("teacherProject:view")
    public String systemTeacherProjectAdd(Model model) {
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/teacherProject/teacherProjectAdd");
    }

    /**
     * 跳转到老师账号下的简答题页面
     * @ Param model Model
     */
    @RequestMapping("teacher/questionAnswer")
    public String teacherQuestionAnswer(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/questionAnswer/teacherQuestionAnswer");
    }

    /**
     * 跳转到评分页面
     * @ Param model Model
     */
    @RequestMapping("/getStudentShortAnswer")
    public String turnToGrade(Model model,Integer studentId){
        List<ExperimentAnswer> answers = answerService.list(new QueryWrapper<ExperimentAnswer>().lambda().eq(ExperimentAnswer::getCreateId, studentId));
        model.addAttribute(answers);
        return "teacher/questionAnswer/studentShortAnswer";
    }

    /**
     * 查询提交情况
     * @ Param model Model
     */
    @GetMapping("teacher/getCommit")
    @RequiresPermissions("teacher:commit")
    public String getCommit(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/commit/commit");
    }

    /**
     * 查询提交统计
     * @ Param model Model
     */
    @GetMapping("teacher/getCommitCount")
    @RequiresPermissions("teacher:commitCount")
    public String getCommitCount(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/commit/commitCount");
    }


    /**
     * 批阅报告，打分
     * @ Param model Model
     */
    @GetMapping("teacher/getScore")
    @RequiresPermissions("teacher:score")
    public String getScore(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/score/score");
    }

    @GetMapping("teacherView/getOneClassScorc/{deptId}")
    @RequiresPermissions("teacher:score")
    public String getOneDeptScore(Model model,@PathVariable Integer deptId){
        model.addAttribute("deptId",deptId);
        return FebsUtil.view("teacher/testReport/studentTestReport");
    }
    /**
     * 查询当前登录老师所拥有的班级列表
     */
    private  Collection<Dept> getDepts(){
        User currentUser = getCurrentUser();
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, currentUser.getUserId()));
        List<Long> collect = userDeptList.stream().map(UserDept::getDeptId).collect(Collectors.toList());
        return deptService.listByIds(collect);
    }

    /**
     * GuiZI_cheng
     * 跳转到老师的批阅简答题页面
     */
    @RequestMapping("/teacher/shortQuestion/{projectId}/{deptId}/{studentId}")
    public String shortQuestion(Model model,@PathVariable Integer projectId,@PathVariable Integer deptId,@PathVariable Integer studentId){
        System.out.println(projectId+","+deptId+","+studentId);
        List<ExperimentAnswer> answers = answerService.list(new QueryWrapper<ExperimentAnswer>().lambda().eq(ExperimentAnswer::getProjectId, projectId)
                .eq(ExperimentAnswer::getDeptId, deptId)
                .eq(ExperimentAnswer::getCreateId, studentId)
                .eq(ExperimentAnswer::getQuestionType,2));
        answers.forEach(i->{
            ExperimentQuestion experimentQuestion = questionService.getById(i.getQuestionId());

            if (experimentQuestion!=null) {
                i.setTitle(questionService.getById(i.getQuestionId()).getQuestionName());
            }
        });
        model.addAttribute("data",answers);
        return FebsUtil.view("teacher/questionAnswer/studentShortAnswer");
    }
}
