package cc.mrbird.febs.teacher.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController("teacherScore")
@RequestMapping("/teacherScore")
public class TeacherScoreController extends BaseController {

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private IExperimentAnswerService answerService;
    @Resource
    private IExperimentProjectService projectService;

    @Resource
    private IUserService userService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IDeptService deptService;

    
    /**
    * @ Description: 老师批阅报告
    * @ Param: [userCommit, request]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @GetMapping("/scoreList")
    @RequiresPermissions("teacher:score")
    @ControllerEndpoint(operation = "批阅报告", exceptionMessage = "批阅报告失败")
    public FebsResponse scoreList(UserCommit userCommit, QueryRequest request) {
        User currentUser = getCurrentUser();
            Page<UserCommit> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "commit_id", FebsConstant.ORDER_ASC, false);
        IPage<UserCommit> iPage = this.userCommitService.page(page, new QueryWrapper<UserCommit>().lambda()
                .eq(UserCommit::getTeacherId, currentUser.getUserId())
                .eq(UserCommit::getIsDel,FebsConstant.USE)
                .eq(IntegerUtils.isLongNotBlank(userCommit.getDeptId()), UserCommit::getDeptId, userCommit.getDeptId())
        );
        List<UserCommit> userCommitList = iPage.getRecords();
        for (UserCommit commit : userCommitList) {
            Long courseId = commit.getCourseId();
            Course course = courseService.getById(courseId);
            if (course!=null) {
                commit.setCourseName(course.getCourseName());
                commit.setDeptName(deptService.getById(commit.getDeptId()).getDeptName());
            }
            if(IntegerUtils.isLongNotBlank(commit.getProjectId())){
                commit.setProjectName(projectService.getById(commit.getProjectId()).getProjectName());
            }
            commit.setStuName(userService.getById(commit.getStuId()).getUsername());
            commit.setIdNumber(userService.getById(commit.getStuId()).getIdNumber());
            commit.setTeacherName(currentUser.getUsername());
            commit.setCreateTimes(DateUtil.formatFullTime(commit.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }


    /**
    * @ Description: 批阅报告的搜索方法
    * @ Param: [userCommit, request, deptId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @GetMapping("/deptScoreList/{deptId}")
    @RequiresPermissions("teacher:score")
    @ControllerEndpoint(operation = "批阅报告", exceptionMessage = "批阅报告失败")
    public FebsResponse deptScoreList(UserCommit userCommit, QueryRequest request,@PathVariable Integer deptId) {
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        Page<UserCommit> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "commit_id", FebsConstant.ORDER_ASC, false);
        IPage<UserCommit> iPage = this.userCommitService.page(page, new QueryWrapper<UserCommit>().lambda()
                .eq(UserCommit::getTeacherId,currentUser.getUserId())
                .eq(IntegerUtils.isLongNotBlank(userCommit.getDeptId()), UserCommit::getDeptId, userCommit.getDeptId())
                .eq(UserCommit::getDeptId,deptId)
        );
        List<UserCommit> userCommitList = iPage.getRecords();
        for (UserCommit commit : userCommitList) {
            commit.setCourseName(courseService.getById(commit.getCourseId()).getCourseName());
            commit.setDeptName(deptService.getById(commit.getDeptId()).getDeptName());
            commit.setProjectName(projectService.getById(commit.getProjectId()).getProjectName());
            commit.setStuName(userService.getById(commit.getStuId()).getUsername());
            commit.setIdNumber(userService.getById(commit.getStuId()).getIdNumber());
            commit.setTeacherName(currentUser.getUsername());
            commit.setCreateTimes(DateUtil.formatFullTime(commit.getCreateTime(),DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
    * @ Description: 更新成绩分数
    * @ Param: [userCommit]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @RequestMapping("/updateScore")
    public FebsResponse update(UserCommit userCommit) {
        boolean b = userCommitService.updateById(userCommit);
        if (b) {
            return new FebsResponse().success();
        } else {
            return new FebsResponse().fail();
        }
    }

    /**
    * @ Description: 修改题目分数
    * @ Param: [answer]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @RequestMapping("/updateShortQuestionScore")
    public FebsResponse updateShortQuestionScore(ExperimentAnswer answer) {
        boolean b = answerService.updateById(answer);
        if (b) {
            return new FebsResponse().success();
        } else {
            return new FebsResponse().fail();
        }
    }







}
