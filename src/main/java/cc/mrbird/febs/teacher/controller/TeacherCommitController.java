package cc.mrbird.febs.teacher.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TableCols;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author admin
 */
@RestController("teacherCommit")
@RequestMapping("/teacherCommit")
public class TeacherCommitController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IUserService userService;


    /**
    * @ Description: 查看提交情况
    * @ Param: [deptId, userCommit, request]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @GetMapping("/commitList")
    @RequiresPermissions("teacher:commit")
    @ControllerEndpoint(operation = "查看提交情况", exceptionMessage = "学生提交情况查询失败")
    public FebsResponse commitList(Integer deptId, UserCommit userCommit, QueryRequest request) {
        User currentUser = getCurrentUser();
        Page<UserCommit> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "commit_id", FebsConstant.ORDER_ASC, false);
        IPage<Map<String, Object>> iPage = this.userCommitService.pageMaps(page, new QueryWrapper<UserCommit>().lambda()
                .eq(IntegerUtils.isNotBlank(deptId), UserCommit::getDeptId, deptId)
        );
        List<Map<String, Object>> mapList = iPage.getRecords();
        for (Map<String, Object> map : mapList) {
            //获取已开通的所有项目列表
            Collection<ExperimentProject> experimentProjects = getProjectsId(deptId, currentUser);
            for (ExperimentProject experimentProject : experimentProjects) {
                //从开通的项目列表中，找该项目对应的提交情况
                List<UserCommit> userCommitList = userCommitService.list(new QueryWrapper<UserCommit>()
                        .lambda()
                        .eq(UserCommit::getProjectId, experimentProject.getProjectId()));

                User stu = userService.getById((Serializable) map.get("stu_id"));
                String status = "<div style=\"color: red;font-style:oblique\">未提交</div>";
                if (userCommitList != null && userCommitList.size() > 0) {
                    for (UserCommit commit : userCommitList) {
                        if (commit.getStuId() == stu.getUserId().intValue()) {
                            if (commit != null) {
                                if (IntegerUtils.isNotBlank(commit.getIsCommit())) {
                                    status = "<div style=\"color: orange;\">未批阅</div>";
                                }
                                if (commit.getScore() != null) {
                                    status = "" + commit.getScore();
                                }
                            }
                        }
                    }
                }
                stu.setStatus(status);
                map.put(experimentProject.getProjectId().toString(), stu.getStatus());
                map.put("idNumber", stu.getIdNumber());
                map.put("name", stu.getUsername());
            }
        }
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }


    /**
    * @ Description: 创建所有的表头
    * @ Param: [deptId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @RequestMapping("/getCols")
    public FebsResponse getCols(Integer deptId) {
        User currentUser = getCurrentUser();
        Collection<ExperimentProject> experimentProjects = getProjectsId(deptId, currentUser);
        List<TableCols> colsList = new ArrayList<>();
        colsList.add(new TableCols("idNumber", "学号", "center", true));
        colsList.add(new TableCols("name", "姓名", "center", false));
        for (ExperimentProject experimentProject : experimentProjects) {
            TableCols tableCols = new TableCols();
            tableCols.setTitle(experimentProject.getProjectName());
            tableCols.setField(experimentProject.getProjectId().toString());
            tableCols.setAlign("center");
            tableCols.setSort(false);
            colsList.add(tableCols);
        }
        return new FebsResponse().success().data(colsList);
    }


   /**
   * @ Description: 根据部门ID和当前登录老师，查询已开通的所有项目列表
   * @ Param: [deptId, user]
   * @ return: java.util.Collection<cc.mrbird.febs.system.entity.ExperimentProject>
   * @ Author: 马超伟
   * @ Date: 2020/6/5
   */
    private Collection<ExperimentProject> getProjectsId(Integer deptId, User user) {
        List<TeacherProject> teacherProjects = teacherProjectService.list(new QueryWrapper<TeacherProject>()
                .lambda()
                .eq(IntegerUtils.isNotBlank(deptId), TeacherProject::getDeptId, deptId)
                .eq(TeacherProject::getTeacherId, user.getUserId()));
        Collection<ExperimentProject> experimentProjects = new ArrayList<>();

        for (TeacherProject teacherProject : teacherProjects) {
            ExperimentProject experimentProject = experimentProjectService
                    .getOne(new QueryWrapper<ExperimentProject>().lambda()
                    .eq(ExperimentProject::getProjectId, teacherProject.getProjectId()));
            experimentProjects.add(experimentProject);
        }
        return experimentProjects;
    }

    @GetMapping("/getSubmittedSituations/{deptId}")
    public FebsResponse getSubmittedSituations(@PathVariable Integer deptId, Model model) {
        User currentUser = getCurrentUser();

        List<UserCommit> isConmit = userCommitService.list(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getTeacherId, currentUser.getUserId()).eq(IntegerUtils.isNotBlank(deptId), UserCommit::getDeptId, deptId).eq(UserCommit::getIsCommit, BaseController.USE_INFO));
        List<UserCommit> notConmit = userCommitService.list(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getTeacherId, currentUser.getUserId()).eq(IntegerUtils.isNotBlank(deptId), UserCommit::getDeptId, deptId).eq(UserCommit::getIsCommit, BaseController.FAIL));
        //还正在做实验
        model.addAttribute("isConmit", isConmit.size());
        model.addAttribute("notConmit", notConmit.size());
        return new FebsResponse().success().data(model);
    }


    /**
    * @ Description: 查询项目列表
    * @ Param: [deptId]
    * @ return: java.util.Collection<cc.mrbird.febs.system.entity.ExperimentProject>
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @RequestMapping("/getProjects")
    public Collection<ExperimentProject> getProjects(Integer deptId) {
        User currentUser = getCurrentUser();
        return getProjectsId(deptId, currentUser);
    }

   /**
   * @ Description: 获取试验提交统计的表头
   * @ Param: [projectId]
   * @ return: cc.mrbird.febs.common.entity.FebsResponse
   * @ Author: 马超伟
   * @ Date: 2020/6/5
   */
    @RequestMapping("/getColsProject")
    public FebsResponse getColsProject(Integer projectId) {
        User currentUser = getCurrentUser();
        if(currentUser==null){
            return new FebsResponse().no_login();
        }
        List<TableCols> colsList = new ArrayList<>();
        colsList.add(new TableCols("idNumber", "学号", "center", true));
        colsList.add(new TableCols("name", "姓名", "center", false));
        ExperimentProject experimentProject = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda()
                .eq(ExperimentProject::getProjectId, projectId));
        TableCols tableCols = new TableCols();
        tableCols.setTitle(experimentProject.getProjectName());
        tableCols.setField(experimentProject.getProjectId().toString());
        tableCols.setAlign("center");
        tableCols.setSort(false);
        colsList.add(tableCols);
        return new FebsResponse().success().data(colsList);
    }

    /**
    * @ Description: 通过班级，和项目，查询提交统计
    * @ Param: [deptId, projectId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    @RequestMapping("/getStudentCount")
    public FebsResponse getStudentCount(Integer deptId, Integer projectId) {
        User currentUser = getCurrentUser();
        //查询该班级下的所有学生数量
        int allStudentSize = userService.list(new QueryWrapper<User>().lambda()
                .eq(User::getDeptId, deptId)).size();

        LambdaQueryWrapper<UserCommit> qw = new QueryWrapper<UserCommit>().lambda()
                .eq(UserCommit::getDeptId, deptId)
                .eq(UserCommit::getProjectId, projectId)
                .eq(UserCommit::getTeacherId, currentUser.getUserId())
                .eq(UserCommit::getIsCommit, 1);
        List<UserCommit> userCommitList = userCommitService.list(qw);
        //查询该班级下的学生项目提交了几份
        int commitSize = userCommitList.size();
        //查询分数大于等于1的说明是已经批阅过的
        qw.ge(UserCommit::getScore, 1);

        List<UserCommit> commitList = userCommitService.list(qw);
        //已经批阅过的数量
        int scoreSize = commitList.size();

        Map<String,Object> map = new HashMap<>(16);
        map.put("all_student", allStudentSize);
        map.put("commit_size", commitSize);
        map.put("score_size", scoreSize);
        map.put("complete", percnet(Double.valueOf(scoreSize), Double.valueOf(commitSize)));
        return new FebsResponse().success().data(map);
    }

    /**
    * @ Description: 计算百分比
    * @ Param: [d, e]
    * @ return: java.lang.String
    * @ Author: 马超伟
    * @ Date: 2020/6/5
    */
    public static String percnet(double d, double e) {
        if (d == 0.0) {
            return "0";
        }
        double p = d / e;
        DecimalFormat nf = (DecimalFormat) NumberFormat.getPercentInstance();
        nf.applyPattern("00%"); //00表示小数点2位
        nf.setMaximumFractionDigits(2); //2表示精确到小数点后2位
        return nf.format(p).toString().replace("%", "").toString();
    }

}
