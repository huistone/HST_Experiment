package cc.mrbird.febs.teacher.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_teacher_project")
public class TeacherProject implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 老师和项目表主键ID
     */
    @TableId(value = "teacher_project_id", type = IdType.AUTO)
    private Integer teacherProjectId;

    /**
     * 老师用户表主键ID
     */
    private Long teacherId;

    /**
     * 项目主键ID
     */
    private Long projectId;

    /**
     * 项目所属课程id
     */
    private Long courseId;

    /**
     * 课程名称
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 项目名称
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String className;


    /**
     * 班级表ID
     */
    private Long deptId;

    /**
     * 是否删除标记
     */
    @TableLogic
    private Integer isDel;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 项目关闭时间
     */
    private LocalDateTime endTime;

    /**
     * 项目开启时间
     */
    private LocalDateTime startTime;

    /**
     * 1进行中，0关闭
     */
    private Integer status;

    /**
     * type,1主课，0辅导
     */
    @TableField(exist = false)
    private Integer type;

    @TableField(exist = false)
    private String projectIds;



}
