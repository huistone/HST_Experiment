package cc.mrbird.febs.teacher.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-08
 */
public interface ITeacherProjectService extends IService<TeacherProject> {

    Map findProjectDetailList(TeacherProject teacherProject, QueryRequest request, User user);

}
