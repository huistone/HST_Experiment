package cc.mrbird.febs.teacher.service.impl;

import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.entity.vo.ProjectDTO;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.mapper.TeacherProjectMapper;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-08
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class TeacherProjectServiceImpl extends ServiceImpl<TeacherProjectMapper, TeacherProject> implements ITeacherProjectService {

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Override
    public Map findProjectDetailList(TeacherProject teacherProject, QueryRequest request, User user) {
        log.debug("--------------------" + teacherProject);
        Page<TeacherProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "teacher_project_id", FebsConstant.ORDER_ASC, false);
        //老师已开启的实验列表  老师当前已开启的所有项目
        IPage<TeacherProject> iPage = teacherProjectService.page(page, new QueryWrapper<TeacherProject>()
                .lambda()
                .eq(TeacherProject::getTeacherId, user.getUserId())
                .eq(IntegerUtils.isLongNotBlank(teacherProject.getDeptId()), TeacherProject::getDeptId, teacherProject.getDeptId()));

        //老师开启的项目的集合 （所有的）
        List<TeacherProject> teacherPorjects = iPage.getRecords();

        //设置班级名字和项目名字
        teacherPorjects.forEach(i -> {
            i.setDeptName(deptService.getById(i.getDeptId()).getDeptName());
            i.setProjectName(experimentProjectService.getById(i.getProjectId()).getProjectName());
        });

        //通过用户ID查用户班级
        List<UserDept> userDeptlist = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                .eq(IntegerUtils.isNotBlank(user.getUserId().intValue()), UserDept::getUserId, user.getUserId()));

        Map<String,Object> map = new HashMap<>();
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        userDeptlist.forEach(i -> {
            Dept dept = deptService.getById(i.getDeptId());
            List<String> projectNames = teacherPorjects.stream().filter(x -> x.getDeptId().equals(i.getDeptId())).map(TeacherProject::getProjectName).collect(Collectors.toList());
            ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setDeptName(dept.getDeptName());
            projectDTO.setProjectList(projectNames);
            projectDTOList.add(projectDTO);
        });
        //进行分类
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("count", userDeptlist.size());
        map.put("data", projectDTOList);
        return map;
    }
}
