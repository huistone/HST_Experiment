package cc.mrbird.febs;

import cc.mrbird.febs.external.mqtt.MQTTSubscribe;
import cc.mrbird.febs.external.netty.server.NettyServer;
import cn.shuibo.annotation.EnableSecurity;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author 马超伟
 */
@EnableSecurity //开启RSA的接口加解密
@EnableAsync // 异步调用
@SpringBootApplication
@EnableTransactionManagement
@EnableCaching
@MapperScan("cc.mrbird.febs.*.mapper")
public class FebsApplication {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private NettyServer nettyServer;

    public static void main(String[] args) {
        new SpringApplicationBuilder(FebsApplication.class).run(args);
    }

    /**
     * 类要实现该方法以开启Netty的调用：implements CommandLineRunner
     * Netty
     */
//    @Override
//    public void run(String... strings) {
//        InetSocketAddress address = new InetSocketAddress(FebsConstant.SOCKET_IP, FebsConstant.SOCKET_PORT);
//        logger.info("netty服务器已经启动.......");
//        logger.info("netty服务器启动地址为： " + FebsConstant.SOCKET_IP);
//        nettyServer.start(address);
//    }

//    @Bean
//    public MessageConverter jsonMessageConverter(ObjectMapper objectMapper) {
//        return new Jackson2JsonMessageConverter(objectMapper);
//    }

//    //最后在springboot的启动类中添加
    @Autowired
    private MQTTSubscribe mqttSubscribe;

    @PostConstruct
    public void consumeMqttClient()  {
        mqttSubscribe.init();           // 项目启动，自动 订阅 消息
    }



}
