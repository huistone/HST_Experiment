package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.AnalogBase;
import cc.mrbird.febs.system.entity.ElectricControl;
import cc.mrbird.febs.system.service.IElectricControlService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-12-05
 */
@RestController
@RequestMapping("/electricControl")
public class ElectricControlController extends BaseController {

    @Resource
    public IElectricControlService electricControlService;


    @RequestMapping("/list")
    @RequiresPermissions("electricControl:view")
    @ControllerEndpoint(operation = "查询电气主板信息", exceptionMessage = "查询电气主板信息失败")
    public FebsResponse list(ElectricControl electricControl, QueryRequest request){
        IPage<ElectricControl> page = new Page<>(request.getPageNum(),request.getPageSize());
        IPage iPage = electricControlService.page(page,
                new QueryWrapper<ElectricControl>()
                        .lambda()
                        .like(StringUtils.isNotBlank(electricControl.getElecControlName()), ElectricControl::getElecControlName, electricControl.getElecControlName())
                        .eq(electricControl.getStatus()!=null,ElectricControl::getStatus,electricControl.getStatus())
                        .eq(electricControl.getUseStatus()!=null,ElectricControl::getUseStatus,electricControl.getUseStatus()));
        return new FebsResponse().data(getDataTable(iPage)).success();
    }
    @RequestMapping("/add")
    @RequiresPermissions("electricControl:add")
    @ControllerEndpoint(operation = "添加主板",exceptionMessage = "添加主板失败")
    public FebsResponse add(ElectricControl electricControl){
        try {
            electricControl.setCreateTime(LocalDateTime.now());
            electricControl.setUseStatus(0);
            electricControl.setOperator("暂无使用");
            electricControlService.save(electricControl);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

    @PostMapping("/update")
    @RequiresPermissions("electricControl:update")
    @ControllerEndpoint(exceptionMessage = "主板更新失败", operation = "主板更新成功")
    public FebsResponse update(ElectricControl electricControl){
        try {
            electricControl.setUpdateTime(LocalDateTime.now());
            electricControlService.updateById(electricControl);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("/delete/{analogIds}")
    @RequiresPermissions("electricControl:delete")
    @ControllerEndpoint(exceptionMessage = "主板删除失败", operation = "主板删除成功")
    public FebsResponse delete(@PathVariable String analogIds){
        try {
            String[] split = analogIds.split(StringPool.COMMA);
            List<String> list = Arrays.asList(split);
            for (String s : list) {
                electricControlService.remove(new QueryWrapper<ElectricControl>().lambda().eq(ElectricControl::getElecControlId,s));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }
}
