package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.Feedback;
import cc.mrbird.febs.system.service.IFeedbackService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 意见反馈表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-05-08
 */
@RestController
@RequestMapping("/feedback")
public class FeedbackController extends BaseController {

    @Resource
    private IFeedbackService feedbackService;

    @RequestMapping("/getFeedBackList")
    @RequiresPermissions("feedback:view")
    @ControllerEndpoint(exceptionMessage = "查询意见反馈失败")
    public FebsResponse getFeedBackList(String title, QueryRequest request) {

        IPage<Feedback> page = new Page<>(request.getPageNum(),request.getPageSize());
        SortUtil.handlePageSort(request, (Page<Feedback>) page, "feedback_id", FebsConstant.ORDER_DESC, false);
        IPage  iPage = feedbackService.page(page,new QueryWrapper<Feedback>().lambda().like(StringUtils.isNotBlank(title), Feedback::getTitle, title));
        return new FebsResponse().data(getDataTable(iPage)).success().message("查询数据成功");
    }

    @GetMapping("delete/{feedbackIds}")
    @ControllerEndpoint(operation = "删除意见成功", exceptionMessage = "删除意见失败")
    public FebsResponse deleteFeedBack(@NotBlank(message = "{required}") @PathVariable String feedbackIds){
        String[] feedIds = feedbackIds.split(StringPool.COMMA);
        feedbackService.deleteFeedBacks(feedIds);
        return new FebsResponse().success();
    }


}
