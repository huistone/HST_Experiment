package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 收藏表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@RestController
@RequestMapping("/collect")
public class CollectController extends BaseController {

}
