package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.external.mqtt.MQTTSubscribe;
import cc.mrbird.febs.system.entity.DetBase;
import cc.mrbird.febs.system.service.IDetBaseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 数电主板信息表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
@RestController
@RequestMapping("/detBase")
public class DetBaseController extends BaseController {

    @Resource
    private IDetBaseService detBaseService;

    @Resource
    private MQTTSubscribe mqttSubscribe;

    @RequestMapping("/list")
    @RequiresPermissions("det:view")
    @ControllerEndpoint(operation = "查询数电主板信息", exceptionMessage = "查询数电主板信息失败")
    public FebsResponse list(DetBase detBase, QueryRequest request){
        IPage<DetBase> page = new Page<>(request.getPageNum(),request.getPageSize());
        IPage  iPage = detBaseService.page(page, new QueryWrapper<DetBase>()
                        .lambda()
                        .like(StringUtils.isNotBlank(detBase.getDetName()), DetBase::getDetName, detBase.getDetName())
                        .eq(detBase.getStatus()!=null,DetBase::getStatus,detBase.getStatus()));
        return new FebsResponse().data(getDataTable(iPage)).success();
    }

    @RequestMapping("/add")
    @RequiresPermissions("det:add")
    @ControllerEndpoint(operation = "添加主板",exceptionMessage = "添加主板失败")
    public FebsResponse add(DetBase detBase){
        try {
            detBase.setCreateTime(LocalDateTime.now());
            detBaseService.save(detBase);
            if (detBase.getStatus()!=null && detBase.getStatus()==1){
                String subChannel = detBase.getSubChannel();
                String[] split = subChannel.split("@@");
                for (String s : split) {
                    mqttSubscribe.subscribe(s,2);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

    @PostMapping("/update")
    @RequiresPermissions("det:update")
    @ControllerEndpoint(exceptionMessage = "主板更新失败", operation = "主板更新成功")
    public FebsResponse update(DetBase detBase){
        try {
            detBase.setUpdateTime(LocalDateTime.now());

            if (detBase.getStatus()!=null){
                DetBase detBaseChannel = detBaseService.getOne(new QueryWrapper<DetBase>()
                        .lambda()
                        .eq(DetBase::getDetId, detBase.getDetId())
                        .select(DetBase::getSubChannel));
                String subChannel = detBaseChannel.getSubChannel();
                String[] split = subChannel.split("@@");
                //修改主板时,需要判断status是否为1 如果为1 则订阅主题 为0则取消订阅
                if (detBase.getStatus() == 1){
                    for (String s : split) {
                        mqttSubscribe.subscribe(s,2);
                    }
                }else if (detBase.getStatus() == 0){
                    mqttSubscribe.unsubscribe(split);
                }
            }
            detBaseService.updateById(detBase);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("/delete/{detIds}")
    @RequiresPermissions("det:delete")
    @ControllerEndpoint(exceptionMessage = "主板删除失败", operation = "主板删除成功")
    public FebsResponse delete(@PathVariable String detIds){
        try {
            String[] split = detIds.split(StringPool.COMMA);
            List<String> list = Arrays.asList(split);
            for (String s : list) {
                DetBase detBaseChannel = detBaseService.getOne(new QueryWrapper<DetBase>()
                        .lambda()
                        .eq(DetBase::getDetId, s)
                        .select(DetBase::getSubChannel));
                String subChannel = detBaseChannel.getSubChannel();
                String[] split1 = subChannel.split("@@");
                mqttSubscribe.unsubscribe(split1);
                detBaseService.remove(new QueryWrapper<DetBase>().lambda().eq(DetBase::getDetId,s));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

}
