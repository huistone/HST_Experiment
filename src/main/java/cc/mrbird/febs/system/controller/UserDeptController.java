package cc.mrbird.febs.system.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @ Description: 老师角色的班级和课程设置表
 * @ Param:
 * @ return:
 * @ Author: 马超伟
 * @ Date: 2020/6/22
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/userDept")
public class UserDeptController extends BaseController {

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IDeptService deptService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "userDept")
    public String userDeptIndex() {
        return FebsUtil.view("userDept/userDept");
    }

    @GetMapping("userDept")
    @RequiresPermissions("userDept:list")
    @ControllerEndpoint(operation = "查询老师班级列表成功",exceptionMessage = "查询老师班级列表失败")
    public FebsResponse getAllUserDepts(UserDept userDept) {
        return new FebsResponse().success().data(userDeptService.findUserDepts(userDept));
    }

    @GetMapping("userDept/list")
    @RequiresPermissions("userDept:list")
    @ControllerEndpoint(operation = "查询老师班级列表成功",exceptionMessage = "查询老师班级列表失败")
    public FebsResponse userDeptList(QueryRequest request, UserDept userDept) {
        Map<String, Object> dataTable = getDataTable(this.userDeptService.findUserDepts(request, userDept));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增UserDept", exceptionMessage = "新增UserDept失败")
    @PostMapping("userDept/add")
    @RequiresPermissions("userDept:add")
    public FebsResponse addUserDept( UserDept userDept) {
        try {
            if (userDept.getDeptId()==null){
                return new FebsResponse().put_code(501,"所选班级为空，请重新选择！");
            }
            if (deptService.getById(userDept.getDeptId()).getLevel()!=3){
                return new FebsResponse().put_code(502,"参数错误！");
            }
            Integer code = this.userDeptService.createUserDept(userDept);
            if(code==201){
                return new FebsResponse().put_code(201,"该课已经有主课老师");
            }
            if (code==503){
                return new FebsResponse().put_code(503,"该老师已经是这门课的主课老师");
            }
            if(code==504){
                return new FebsResponse().put_code(504,"该老师已经是这门课的辅课老师");
            }
            return new FebsResponse().success().message("分配成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().code(HttpStatus.INTERNAL_SERVER_ERROR).message("参数异常！！！");
        }
    }

    @ControllerEndpoint(operation = "删除UserDept", exceptionMessage = "删除UserDept失败")
    @GetMapping("userDept/delete/{userDeptId}")
    @RequiresPermissions("userDept:delete")
    public FebsResponse deleteUserDept(@PathVariable Integer userDeptId) {
        try {
            UserDept byId = userDeptService.getById(userDeptId);
            //删除老师所开启的项目，关闭项目
            boolean update = teacherProjectService.update(new UpdateWrapper<TeacherProject>().lambda()
                    .set(TeacherProject::getStatus, 0)
                    .set(TeacherProject::getEndTime, LocalDateTime.now())
                    .set(TeacherProject::getIsDel, 0)
                    .eq(TeacherProject::getTeacherId,byId.getUserId()));
            this.userDeptService.removeById(userDeptId);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改UserDept", exceptionMessage = "修改UserDept失败")
    @PostMapping("userDept/update")
    @RequiresPermissions("userDept:update")
    public FebsResponse updateUserDept(UserDept userDept) {
        this.userDeptService.updateUserDept(userDept);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改UserDept", exceptionMessage = "导出Excel失败")
    @PostMapping("userDept/excel")
    @RequiresPermissions("userDept:export")
    public void export(QueryRequest queryRequest, UserDept userDept, HttpServletResponse response) {
        List<UserDept> userDepts = this.userDeptService.findUserDepts(queryRequest, userDept).getRecords();
        ExcelKit.$Export(UserDept.class, response).downXlsx(userDepts, false);
    }

    /**
     * @ Description: 查询所选老师的班级课程列表
     * @ Param: [userId, request]
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/6/22
     */
    @GetMapping("/selectTeacherDept")
    @ControllerEndpoint(operation = "查询所选老师的班级课程列表",exceptionMessage ="查询所选老师的班级课程失败" )
    public FebsResponse selectTeacherDept(Integer userId, QueryRequest request) {
        Page<UserDept> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<UserDept> iPage = userDeptService.page(page, new QueryWrapper<UserDept>().lambda()
                .eq(UserDept::getUserId, userId));
        List<UserDept> userDeptList = iPage.getRecords();
        //设置班级和课程名
        for (UserDept userDept : userDeptList) {
            if (IntegerUtils.isLongNotBlank(userDept.getDeptId())) {
                userDept.setDeptName(deptService.getOne(new QueryWrapper<Dept>()
                        .lambda()
                        .eq(Dept::getDeptId, userDept.getDeptId())
                        .select(Dept::getDeptName))
                        .getDeptName());
            }
            if (IntegerUtils.isLongNotBlank(userDept.getCourseId())) {
                userDept.setCourseName(courseService.getOne(new QueryWrapper<Course>()
                        .lambda()
                        .eq(Course::getCourseId, userDept.getCourseId())
                        .select(Course::getCourseName))
                        .getCourseName());
            }
        }
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }
}
