package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.entity.FebsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 课程学生表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@RestController
@RequestMapping("/courseStudent")
public class CourseStudentController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());
    
    @RequestMapping("/list")
    public FebsResponse listCourseStudent(){

        return null;
    }


}
