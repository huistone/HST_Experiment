package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Article;
import cc.mrbird.febs.system.entity.ArticleCategory;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IArticleCategoryService;
import cc.mrbird.febs.system.service.IArticleService;
import cc.mrbird.febs.system.service.ICategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import cc.mrbird.febs.common.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 资讯表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
@RestController
@RequestMapping("/article")
public class ArticleController extends BaseController {

    @Resource
    private IArticleService articleService;


    @Resource
    private IArticleCategoryService articleCategoryService;

    @RequestMapping("/list")
    @RequiresPermissions("article:view")
    @ControllerEndpoint(operation = "查询文章", exceptionMessage = "查询文章失败")
    public FebsResponse getArticleList(Article article, QueryRequest request){
        Page<Article> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Article> page1 = articleService.page(page, new QueryWrapper<Article>()
                .lambda()
                .like(StringUtils.isNotBlank(article.getArticleTitle()), Article::getArticleTitle, article.getArticleTitle())
                .eq(article.getArticleStatus() != null, Article::getArticleStatus, article.getArticleStatus())
                .eq(article.getCategoryId() != null, Article::getCategoryId, article.getCategoryId())
                .orderByDesc(Article::getArticleUpdatetime)
                .orderByDesc(Article::getArticleNewstime));
        return new FebsResponse().success().data(getDataTable(page1));
    }


    @PostMapping("/add")
    @RequiresPermissions("article:add")
    @ControllerEndpoint(operation = "新增文章", exceptionMessage = "新增项目失败")
    public FebsResponse addArticle(@Valid Article article, @RequestParam("image") MultipartFile image) {

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().fail().no_login();
        }
        if (image != null && image.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "article";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(image, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setArticleThumbnail(url);
        }
        article.setArticleNewstime(LocalDateTime.now());
        article.setUserId(currentUser.getUserId());
        articleService.save(article);
        ArticleCategory articleCategory = new ArticleCategory();
        articleCategory.setArticleId(article.getArticleId().intValue());
        articleCategory.setCategoryId(article.getCategoryId());
        articleCategoryService.save(articleCategory);
        return new FebsResponse().success();
    }

    @PostMapping("/update")
    @RequiresPermissions("article:update")
    @ControllerEndpoint(operation = "修改文章", exceptionMessage = "修改文章失败")
    public FebsResponse updateArticle( Article article, @RequestParam("image") MultipartFile image) {

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().fail().no_login();
        }
        if (image != null && image.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "article";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(image, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setArticleThumbnail(url);
        }
        article.setArticleUpdatetime(LocalDateTime.now());
        article.setUserId(currentUser.getUserId());
        try{
            articleService.updateById(article);
            if(article.getCategoryId()!=null){
                ArticleCategory one = articleCategoryService.getOne(new QueryWrapper<ArticleCategory>()
                        .lambda()
                        .eq(ArticleCategory::getArticleId, article.getArticleId()));
                one.setCategoryId(article.getCategoryId());
                articleCategoryService.updateById(one);
            }
        } catch (Exception e){
            return new FebsResponse().fail().message("系统繁忙请稍后再试");
        }
        return new FebsResponse().success();
    }

    @GetMapping("delete/{articleIds}")
    @RequiresPermissions("article:delete")
    @ControllerEndpoint(operation = "删除文章", exceptionMessage = "删除文章失败")
    public FebsResponse deleteCourses(@NotBlank(message = "{required}") @PathVariable String articleIds) {
        String[] ids = articleIds.split(StringPool.COMMA);
        //获取所有项目id列表
        List<String> list = Arrays.asList(ids);
        //删除关联表
        for (String s : list) {
            articleCategoryService.remove(new QueryWrapper<ArticleCategory>()
                    .lambda()
                    .eq(ArticleCategory::getArticleId,s));
        }
        //删除文章
        articleService.removeByIds(list);
        return new FebsResponse().success();
    }

    @RequestMapping("/articlePicUrl")
    public Map<String,Object> productPicUrl(MultipartFile file) {
        String basePath = "articlePic";
        String url = null;
        Map<String,Object> map = new HashMap<>();
        try {
            url = aliOSSUpload(file, basePath);
            map.put("errno", 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> list = new ArrayList<>();
        list.add(url);
        map.put("data", list);
        return map;
    }


}
