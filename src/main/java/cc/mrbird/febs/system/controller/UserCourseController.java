package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.UserCourse;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IUserCourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 管理员分配课程表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@RestController
@RequestMapping("/userCourse")
public class UserCourseController extends BaseController {

    @Resource
    private IUserCourseService userCourseService;

    @Resource
    private ICourseService courseService;

    @RequestMapping("getUserCourseList/{userId}")
    @ControllerEndpoint(operation = "查询课程分配",exceptionMessage = "查询课程分配失败")
    @RequiresPermissions("userCourse:list")
    public FebsResponse getUserCourseList(@PathVariable Integer userId, QueryRequest request){
        Page<UserCourse> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<UserCourse> page1 = userCourseService.page(page, new QueryWrapper<UserCourse>()
                .lambda()
                .eq(UserCourse::getUserId,userId));
        List<UserCourse> records = page1.getRecords();
        for (UserCourse record : records) {
            Course byId = courseService.getById(record.getCourseId());
            record.setCourseName(byId.getCourseName());
        }
        return new FebsResponse().success().data(getDataTable(page1));
    }


    /**
     * @Author wangke
     * @ Description 添加管理员与课程关系
     * @Date 15:59 2020/11/16
     * @ Param
     * @return
     */
    @RequestMapping("addUserCourse")
    @ControllerEndpoint(operation = "分配课程与老师",exceptionMessage = "分配课程与老师失败")
    @RequiresPermissions("userCourse:add")
    public FebsResponse addUserCourse(UserCourse userCourse){
        //先查询该管理员是否已经管理该课程
        int count = userCourseService.count(new QueryWrapper<UserCourse>()
                .lambda()
                .eq(UserCourse::getUserId, userCourse.getUserId())
                .eq(UserCourse::getCourseId, userCourse.getCourseId()));
        if (count > 0){
            return new FebsResponse().fail().message("不可重复分配");
        }
        try{
            userCourseService.save(userCourse);
            return new FebsResponse().success();
        }catch (Exception e){
            return  new FebsResponse().fail().message("系统忙，请稍后再试");
        }
    }

    /**
     * @Author wangke
     * @ Description 取消管理员课程分配
     * @Date 16:26 2020/11/16
     * @ Param
     * @return
     */
    @ControllerEndpoint(operation = "取消管理员课程绑定", exceptionMessage = "取消管理员课程绑定失败")
    @GetMapping("delete/{userCourseId}")
    @RequiresPermissions("userCourse:delete")
    public FebsResponse deleteUserCourse(@PathVariable Integer userCourseId ){
        try{
            userCourseService.removeById(userCourseId);
            return new FebsResponse().success();
        } catch (Exception e){
            return new FebsResponse().fail();
        }
    }

}
