package cc.mrbird.febs.system.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.student.entity.ExperimentAnswer;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.service.IExperimentAnswerService;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jsoup.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/question")
@ResponseBody
public class QuestionController extends BaseController {


    @Autowired
    private IExperimentQuestionService questionService;

    @Autowired
    private IExperimentProjectService experimentProjectService;

    @Autowired
    private ICourseService courseService;

    @Autowired
    private IExperimentAnswerService answerService;


    @RequestMapping("/questionList")
    @RequiresPermissions("question:view")
    @ControllerEndpoint(exceptionMessage = "获取习题列表失败", operation = "获取习题列表成功")
    public FebsResponse questionList(QueryRequest queryRequest,ExperimentQuestion question) {
        User currentUser = getCurrentUser();
        if (currentUser != null) {
            Page<ExperimentQuestion> pages = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
//            SortUtil.handlePageSort(queryRequest, page, "create_time", FebsConstant.ORDER_DESC, false);
            IPage<ExperimentQuestion> iPage = this.questionService.page(pages, new QueryWrapper<ExperimentQuestion>().lambda()
                    .eq(IntegerUtils.isNotBlank(question.getType()), ExperimentQuestion::getType, question.getType())
                    .eq(IntegerUtils.isNotBlank(question.getProjectId()), ExperimentQuestion::getProjectId, question.getProjectId())
                    .eq(IntegerUtils.isNotBlank(question.getQuestionType()), ExperimentQuestion::getQuestionType, question.getQuestionType())
                    .like(StringUtils.isNotBlank(question.getQuestionName()), ExperimentQuestion::getQuestionName, question.getQuestionName())
                    .orderByDesc(ExperimentQuestion::getCreateTime)
                    .orderByDesc(ExperimentQuestion::getUpdateTime)
            );
            if (iPage.getRecords() != null) {
                iPage.getRecords().forEach(i -> {
                    i.setOptional(Arrays.asList(i.getContext().split("@@")));
                    i.setProjectName(experimentProjectService.getById(i.getProjectId()).getProjectName());
                });
            }
            Map<String, Object> dataTable = getDataTable(iPage);
            return new FebsResponse().success().data(dataTable).message("查询数据成功");
        }
        return new FebsResponse().fail().message("数据参数异常");

    }

    @RequestMapping("/questionOne")
    @RequiresPermissions("question:query")
    @ControllerEndpoint(exceptionMessage = "习题查询失败", operation = "习题查询成功")
    public FebsResponse queryOne(Integer questionId) {
        ExperimentQuestion question = questionService.getOne(new QueryWrapper<ExperimentQuestion>().lambda().eq(ExperimentQuestion::getQuestionId, questionId));


        return new FebsResponse().success().data(question).message("查询数据成功");
    }

    @PostMapping("/updateOne")
    @RequiresPermissions("question:update")
    @ControllerEndpoint(exceptionMessage = "习题更新失败", operation = "习题更新成功")
    public FebsResponse updateOne(ExperimentQuestion question) {
        question.setUpdateTime(LocalDateTime.now());
        List<String> contextList = question.getContextList();
        if (contextList.size()==0 || contextList==null){
            return new FebsResponse().fail().message("选项不能为空");
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0;i<contextList.size();i++){
            String s = contextList.get(i);
            stringBuilder.append(s+"@@");
        }
        String s = stringBuilder.toString();
        question.setContext(s.substring(0,s.length()-2));
        boolean b = questionService.updateById(question);
        if (b) {
            return new FebsResponse().success().message("修改成功");
        } else {
            return new FebsResponse().message("修改失败").fail();
        }
    }


    @RequestMapping("/deleteOne/{questionIds}")
    @RequiresPermissions("question:delete")
    @ControllerEndpoint(exceptionMessage = "习题删除失败", operation = "习题删除成功")
    public FebsResponse updateOne(@PathVariable String questionIds) {
        String[] ids = questionIds.split(StringPool.COMMA);
        List<String> list = Arrays.asList(ids);
        list.forEach(i -> {
            answerService.remove(new QueryWrapper<ExperimentAnswer>().lambda().eq(ExperimentAnswer::getQuestionId, i));
        });
        boolean b = questionService.removeByIds(list);
        if (b) {
            return new FebsResponse().success().message("删除成功");
        } else {
            return new FebsResponse().fail().message("查询数据失败");
        }
    }


    @RequestMapping("/insertOne")
    @RequiresPermissions("question:insert")
    @ControllerEndpoint(exceptionMessage = "习题新增失败", operation = "习题新增成功")
    public FebsResponse insertOne(ExperimentQuestion question) {
        List<String> contextList = question.getContextList();
        if(contextList==null || contextList.size()==0){
            return new FebsResponse().fail().message("选项不能为空");
        }
        StringBuilder stringBuilder = new StringBuilder();
        int start = 0,end = 4;
        start = question.getQuestionType()==2?4:0;
        end = question.getQuestionType()==2?6:4;
        for (int i = start;i<end;i++){
            String s = contextList.get(i);
            if (!StringUtils.isEmpty(s)){
                stringBuilder.append(s+"@@");
            }
        }

        String s = stringBuilder.toString();
        question.setContext(s.substring(0,s.length()-2));
        String answer = question.getAnswer();
        String replace = answer.replace(",", "");
        question.setAnswer(replace);
        ExperimentProject experimentProject = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getProjectName, question.getProjectName()));
//        //检测该题号是否有题目
//        int count = questionService.count(new QueryWrapper<ExperimentQuestion>()
//                .lambda()
//                .eq(ExperimentQuestion::getIsDel, 1)
//                .eq(ExperimentQuestion::getType, question.getType())
//                .eq(ExperimentQuestion::getNumber, question.getNumber())
//                .eq(ExperimentQuestion::getProjectId,experimentProject.getProjectId()));
//        if(count>0){
//            return new FebsResponse().put_code(201,"同实验同题型下题号不可重复");
//        }
        question.setProjectId(experimentProject.getProjectId().intValue());
        question.setCreateTime(LocalDateTime.now());
        boolean b = questionService.save(question);
        if (b) {
            return new FebsResponse().success().data(question).message("添加数据成功");
        } else {
            return new FebsResponse().fail().data(question).message("添加数据失败");
        }
    }

    @GetMapping("select/projectName")
    @ControllerEndpoint(exceptionMessage = "获取课程列表失败")
    public FebsResponse getProjects(String projectName,Integer courseId, Integer page,Integer limit,QueryRequest request) throws FebsException {
        request.setPageNum(page);
        request.setPageSize(limit);
        if(courseId==null){
            Page<ExperimentProject> page1 = new Page<ExperimentProject>(request.getPageNum(), request.getPageSize());
            IPage<ExperimentProject> iPage = this.experimentProjectService.page(page1,
                    new QueryWrapper<ExperimentProject>()
                            .lambda()
                            .like(StringUtils.isNotBlank(projectName), ExperimentProject::getProjectName, projectName)
                            .eq(ExperimentProject::getIsDel,1)
                            .eq(ExperimentProject::getState,1)
                            .select(ExperimentProject::getProjectId, ExperimentProject::getProjectName));
            FebsResponse febsResponse = new FebsResponse();
            febsResponse.put("code",0);
            febsResponse.put("count", iPage.getTotal());
            febsResponse.put("data", iPage.getRecords());
            return febsResponse;
        }
        Page<ExperimentProject> page1 = new Page<ExperimentProject>(request.getPageNum(), request.getPageSize());
        IPage<ExperimentProject> iPage = this.experimentProjectService.page(page1,
                new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .like(StringUtils.isNotBlank(projectName), ExperimentProject::getProjectName, projectName)
                        .eq(ExperimentProject::getCourseId,courseId)
                        .eq(ExperimentProject::getIsDel,1)
                        .eq(ExperimentProject::getState,1)
                        .select(ExperimentProject::getProjectId, ExperimentProject::getProjectName));
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code", 0);
        febsResponse.put("count", iPage.getTotal());
        febsResponse.put("data", iPage.getRecords());
        return febsResponse;
    }

    @GetMapping("select/courseName")
    @ControllerEndpoint(exceptionMessage = "获取课程列表失败")
    public FebsResponse getCourses(String courseName, QueryRequest request) throws FebsException {
        Page<Course> page = new Page<Course>(request.getPageNum(), request.getPageSize());
        IPage<Course> iPage = this.courseService.page(page, new QueryWrapper<Course>()
                .lambda()
                .like(StringUtils.isNotBlank(courseName), Course::getCourseName, courseName));
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code", 0);
        febsResponse.put("count", iPage.getTotal());
        febsResponse.put("data", iPage.getRecords());
        return febsResponse;
    }

}
