package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.utils.FileUtil;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.Video;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IVideoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
@RestController
@RequestMapping("/video")
public class VideoController extends BaseController {

    @Resource
    private IVideoService videoService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    /**
     * @ Author wangke
     * @ Description 查询视频分页
     * @ Date 11:13 2021/2/3
     * @ Param 
     * @ return 
     */
    @RequestMapping("list")
    @ControllerEndpoint(operation = "查询视频列表",exceptionMessage = "查询视频列表异常")
    public FebsResponse selectVideoList(Video video, QueryRequest queryRequest){
        Page<Video> objectPage = new Page<>(queryRequest.getPageNum(), queryRequest.getPageSize());
        IPage<Video> page = videoService.page(objectPage, new QueryWrapper<Video>()
                .lambda()
                .like(StringUtils.isNotBlank(video.getProjectName()),Video::getProjectName, video.getProjectName())
                .like(StringUtils.isNotBlank(video.getVideoName()),Video::getVideoName, video.getVideoName())
        );
        return new FebsResponse().success().data(getDataTable(page));
    }

    /**
     * @ Author wangke
     * @ Description 添加实验视频接口
     * @ Date 9:52 2021/2/4
     * @ Param 
     * @ return 
     */
    @RequestMapping("add")
    @ControllerEndpoint(operation = "添加实验视频",exceptionMessage = "添加实验视频失败")
    public FebsResponse addProjectVideo(Video video, @RequestParam("videoFile")MultipartFile videoFile, HttpServletRequest request){
        String projectName = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, video.getProjectId())
                .select(ExperimentProject::getProjectName)).getProjectName();
        video.setProjectName(projectName);
        if (videoFile.getSize()>0 && videoFile!=null){
            //获取上传文件的路径
            String basePath = "projectVideo";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(videoFile, basePath);

                String videoSize = FileUtil.ReadVideoTime(url, request);
                video.setVideoSize(videoSize);
                video.setVideoUrl(url);
            } catch (IOException e) {
                e.printStackTrace();
            }

            video.setIsDel(1);

            video.setVideoUrl(url);
        }
        videoService.save(video);

        return new FebsResponse().success();
    }

    @RequestMapping("update")
    @ControllerEndpoint(operation = "修改实验视频",exceptionMessage = "修改实验视频失败")
    public FebsResponse updateProjectVideo(Video video, @RequestParam("videoFile")MultipartFile videoFile, HttpServletRequest request){
        String projectName = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getProjectId, video.getProjectId())
                .select(ExperimentProject::getProjectName)).getProjectName();
        video.setProjectName(projectName);
        if (videoFile.getSize()>0 && videoFile!=null){
            //获取上传文件的路径
            String basePath = "projectVideo";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(videoFile, basePath);

                String videoSize = FileUtil.ReadVideoTime(url, request);
                video.setVideoSize(videoSize);
                video.setVideoUrl(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            video.setIsDel(1);
            video.setCreateTime(LocalDateTime.now());
            video.setVideoUrl(url);
        }
        videoService.updateById(video);

        return new FebsResponse().success();
    }

//    @RequestMapping("deleteFile")
//    public FebsResponse deleteFile(String filePath){
//        try {
//            aliOSSDelete(filePath);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return new FebsResponse().fail();
//        }
//        return new FebsResponse().success();
//    }

    @RequestMapping("delete/{videoIds}")
    @ControllerEndpoint(operation = "删除实验视频",exceptionMessage = "删除实验视频失败")
    public FebsResponse deleteProjectVideo(@PathVariable String videoIds){
        try {
            String[] split = videoIds.split(StringPool.COMMA);
            for (String s : split) {

                videoService.removeById(s);
            }
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }


}
