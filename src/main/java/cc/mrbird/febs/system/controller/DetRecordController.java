package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.DetRecord;
import cc.mrbird.febs.system.service.IDetRecordService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 数电主板使用记录表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
@RestController
@RequestMapping("/detRecord")
public class DetRecordController extends BaseController {

    @Resource
    private IDetRecordService detRecordService;
   

//    @RequiresPermissions("det:view")
    @RequestMapping("list")
    @ControllerEndpoint(exceptionMessage = "获取主板操作记录失败",operation = "获取主板操作记录成功")
    public FebsResponse list(DetRecord detRecord, QueryRequest request){
        IPage<DetRecord> list = detRecordService.selectDetRecordByPage(detRecord, request);
        return new FebsResponse().data(getDataTable(list)).success();
    }

    @RequestMapping("/delete/{detRecordIds}")
    @RequiresPermissions("det:delete")
    @ControllerEndpoint(exceptionMessage = "主板删除失败", operation = "主板删除成功")
    public FebsResponse delete(@PathVariable String detRecordIds){
        try {
                String[] split = detRecordIds.split(StringPool.COMMA);
            List<String> ids = Arrays.asList(split);
            for (String id : ids) {
                detRecordService.removeById(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().data("系统繁忙");
        }
        return new FebsResponse().success();
    }

}
