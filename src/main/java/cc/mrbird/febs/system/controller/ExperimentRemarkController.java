package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 实验题目 评语表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-07-01
 */
@RestController
@RequestMapping("/experiment-remark")
public class ExperimentRemarkController extends BaseController {

}
