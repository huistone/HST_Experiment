package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 用户报告提交记录表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@RestController
@RequestMapping("/customer-commit")
public class CustomerCommitController extends BaseController {

}
