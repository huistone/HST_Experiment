package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.system.entity.ArticleCategory;
import cc.mrbird.febs.system.entity.Category;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IArticleCategoryService;
import cc.mrbird.febs.system.service.ICategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 分类表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
@RestController
@RequestMapping("/category")
public class CategoryController extends BaseController {

    @Resource
    private ICategoryService categoryService;

    @Resource
    private IArticleCategoryService articleCategoryService;

    @RequestMapping("/list")
    @RequiresPermissions("category:view")
    @ControllerEndpoint(operation = "查询分类", exceptionMessage = "查询文章失败")
    public FebsResponse getArticleList(Category category, QueryRequest request){
        Page<Category> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Category> page1 = categoryService.page(page, new QueryWrapper<Category>()
                .lambda()
                .eq(IntegerUtils.isNotBlank(category.getCategoryId()), Category::getCategoryId, category.getCategoryId()));
        return new FebsResponse().success().data(getDataTable(page1));
    }

    @PostMapping("/add")
    @RequiresPermissions("category:add")
    @ControllerEndpoint(operation = "新增分类", exceptionMessage = "新增分类失败")
    public FebsResponse addCategory(@Valid Category category) {

        User currentUser = getCurrentUser();
        if (currentUser == null){
            return new FebsResponse().fail().no_login();
        }
        categoryService.save(category);

        return new FebsResponse().success();
    }

    @PostMapping("/update")
    @RequiresPermissions("category:update")
    @ControllerEndpoint(operation = "修改分类", exceptionMessage = "修改分类失败")
    public FebsResponse updateCategory(@Valid Category category) {
        categoryService.updateById(category);
        return new FebsResponse().success();
    }

    @GetMapping("delete/{categoryIds}")
    @RequiresPermissions("article:delete")
    @ControllerEndpoint(operation = "删除分类", exceptionMessage = "删除分类失败")
    public FebsResponse deleteCourses(@NotBlank(message = "{required}") @PathVariable String categoryIds) {
        String[] ids = categoryIds.split(StringPool.COMMA);
        //获取所有项目id列表
        List<String> list = Arrays.asList(ids);
        //删除关联表
        for (String s : list) {
            ArticleCategory one = articleCategoryService.getOne(new QueryWrapper<ArticleCategory>()
                    .lambda()
                    .eq(ArticleCategory::getCategoryId, s));
            if (one !=null){
                return new FebsResponse().fail().message("该分类下有文章，请先删除文章");
            }
        }
        categoryService.removeByIds(list);
        //删除文章
        return new FebsResponse().success();
    }

}
