package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.CourseDept;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 课程班级表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-26
 */
@RestController
@RequestMapping("/courseDept")
public class CourseDeptController extends BaseController {

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IExperimentProjectService experimentProjectService;
    
    /**
    * @ Description: 通过课程查询班级
    * @ Param: [courseId]
    * @ return: java.util.List
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @GetMapping("/getDeptIdListByCourseId")
    public List getDeptIdListByCourseId(Integer courseId){
        List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>().lambda()
                .eq(CourseDept::getCourseId, courseId));
        return courseDeptList.stream().map(m -> m.getDeptId()).collect(Collectors.toList());
    }

    /**
    * @ Description: 通过课程ID查班级集合
    * @ Param: [courseId]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/7/8
    */
    @GetMapping("/getDeptByCourseId")
    public FebsResponse getDeptByCourseId(Integer courseId){
        List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>().lambda()
                .eq(CourseDept::getCourseId, courseId));
        List<Map> mapList = new ArrayList<>();
        for (CourseDept courseDept : courseDeptList) {
            Map<String,Object> map = new HashMap<>();
            map.put("id",courseDept.getDeptId());
            map.put("name",deptService.getById(courseDept.getDeptId()).getDeptName());
            mapList.add(map);
        }
        return new FebsResponse().success().data(mapList).message("查询成功！");
    }

    @GetMapping("/getProjectByCourseId")
    public FebsResponse getProjectByCourseId(Integer courseId){
        List<ExperimentProject> courseProjectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getCourseId, courseId).eq(ExperimentProject::getState, 1).eq(ExperimentProject::getIsDel, 1));
        List<Map> listMap = new ArrayList<>();
        for (ExperimentProject experimentProject : courseProjectList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id",experimentProject.getProjectId());
            map.put("name",experimentProject.getProjectName());
            listMap.add(map);
        }
        return new FebsResponse().success().data(listMap).message("查询成功");
    }


    /**
    * @ Description: 添加课程班级关联
    * @ Param: [courseDept, deptIds]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @GetMapping("/courseDeptAdd")
    @ControllerEndpoint(operation = "添加课程班级关联成功",exceptionMessage = "添加课程班级关联失败")
    public FebsResponse courseDeptAdd(CourseDept courseDept,@RequestParam("deptIds[]") Integer deptIds[]){
        //先删除
        if (deptIds==null || courseDept.getCourseId()==null){
            return new FebsResponse().fail().message("参数不正确！");
        }
        courseDeptService.remove(new QueryWrapper<CourseDept>().lambda().eq(CourseDept::getCourseId,courseDept.getCourseId()));
        //再添加
        for (Integer deptId : deptIds) {
            courseDept.setDeptId(deptId);
            courseDeptService.save(courseDept);
        }
        return new FebsResponse().success();
    }

}
