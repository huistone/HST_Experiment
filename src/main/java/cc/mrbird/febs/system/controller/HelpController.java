package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Help;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IHelpService;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import cc.mrbird.febs.common.controller.BaseController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-30
 */
@RestController
@RequestMapping("/help")
public class HelpController extends BaseController {

    @Autowired
    private IHelpService helpService;

    @GetMapping("list")
    @RequiresPermissions("help:list")
    @ControllerEndpoint(operation = "查询帮助", exceptionMessage = "查询帮助失败")
    public FebsResponse helpList(Help help, QueryRequest queryRequest){
        Map<String, Object> dataTable = getDataTable(helpService.findAll(help, queryRequest,getCurrentUser()));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("add")
    @RequiresPermissions("help:add")
    @ControllerEndpoint(operation = "添加帮助", exceptionMessage = "添加帮助失败")
    public FebsResponse helpAdd(Help help){
        try {
            helpService.saveHelpInfo(help,getCurrentUser());
            return new FebsResponse().success().message("添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("添加失败");
        }
    }

    @PostMapping("update")
    @RequiresPermissions("help:update")
    @ControllerEndpoint(operation = "修改帮助", exceptionMessage = "修改帮助失败")
    public FebsResponse helpUpdate(@Valid Help help){
        try {
            helpService.updateHelpInfo(help,getCurrentUser());
            return new FebsResponse().success().message("修改帮助信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("修改帮助信息失败");
        }
    }

    @GetMapping("delete/{helpIds}")
    @RequiresPermissions("help:delete")
    @ControllerEndpoint(operation = "删除帮助", exceptionMessage = "删除帮助败")
    public FebsResponse helpDelete(@PathVariable String helpIds){
        try {
            helpService.deleteByIds(helpIds);
            return new FebsResponse().success().message("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().message("删除失败");
        }
    }


}
