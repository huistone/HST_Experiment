package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.Pot;
import cc.mrbird.febs.system.service.IPotService;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * POT1 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/pot")
public class PotController extends BaseController {



}
