package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
@RestController
@RequestMapping("/det-report")
public class DetReportController extends BaseController {

    @RequestMapping("/ssss")
    public FebsResponse ssss(){
        User currentUser = getCurrentUser();
        return new FebsResponse().success().data(currentUser);
    }

}
