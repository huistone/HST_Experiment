package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 实验项目表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-25
 */
@RestController
@RequestMapping("/experimentProject")
public class ExperimentProjectController extends BaseController {

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ICourseService courseService;

    @GetMapping("list")
    @RequiresPermissions("experimentProject:view")
    @ControllerEndpoint(operation = "查询实验项目", exceptionMessage = "查询实验项目失败")
    public FebsResponse courseList(ExperimentProject experimentProject, QueryRequest request) {
        Page<ExperimentProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "project_sort", FebsConstant.ORDER_ASC, false);
        IPage<ExperimentProject> iPage = this.experimentProjectService.page(page, new QueryWrapper<ExperimentProject>().lambda()
                .like(StringUtils.isNotBlank(experimentProject.getProjectName()), ExperimentProject::getProjectName, experimentProject.getProjectName())
                .like(experimentProject.getState() != null, ExperimentProject::getState, experimentProject.getState())
                .eq(experimentProject.getCourseId()!=null ,ExperimentProject::getCourseId,experimentProject.getCourseId())
                .select(ExperimentProject.class,m -> !"project_content".equals(m.getColumn()))
        );
        List<ExperimentProject> projectList = iPage.getRecords();
        for (ExperimentProject project : projectList) {
            Course course = courseService.getById(project.getCourseId());

            if (project.getCourseId()!=null){
                project.setCourseName(courseService.getById(project.getCourseId()).getCourseName());
            }
        }

        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("/getContent")
    public FebsResponse getContent(Long projectId){
        ExperimentProject experimentProject = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda()
                .eq(ExperimentProject::getProjectId, projectId)
                .select(ExperimentProject::getProjectContent));
        return new FebsResponse().success().data(experimentProject.getProjectContent());
    }

    @PostMapping("/add")
    @RequiresPermissions("experimentProject:add")
    @ControllerEndpoint(operation = "新增项目", exceptionMessage = "新增项目失败")
    public FebsResponse addExperiment(@Valid ExperimentProject experimentProject, @RequestParam("doc_file") MultipartFile doc_file,@RequestParam("pdf_file") MultipartFile pdf_file,  @RequestParam("image") MultipartFile image) {
        if (doc_file != null && doc_file.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(doc_file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //获取文件大小
            long size = doc_file.getSize();
            //转换文件大小格式
            String size1 = getSize(size);
            //将文件大小，和文件上传后返回的路径，入库
            experimentProject.setReportSize(size1);
            experimentProject.setReportUrl(url);
        }
        if(pdf_file != null && pdf_file.getSize() > 0){
            String basePath = "projectPdf";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(pdf_file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            experimentProject.setProjectContentUrl(url);
        }

        if (image != null && image.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(image, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            experimentProject.setProjectPictureUrl(url);
        }
        experimentProject.setCreateTime(LocalDateTime.now());
//        if (StringUtils.isNotBlank(experimentProject.getCourseName())) {
//            Course course = courseService.getOne(new QueryWrapper<Course>().lambda().eq(Course::getCourseName, experimentProject.getCourseName()));
//            experimentProject.setCourseId(course.getCourseId());
//        }
        experimentProjectService.save(experimentProject);
        return new FebsResponse().success();
    }

    @RequestMapping("downloadFile")
    public void downloadFile(String objectName) {
        downloadOss(objectName);
    }


    /**
     * GuiYongkang
     * 2020/4/01 14:44pm
     * 修改
     */
    @PostMapping("/update")
    @RequiresPermissions("experimentProject:update")
    @ControllerEndpoint(operation = "修改项目", exceptionMessage = "修改项目失败")
    public FebsResponse updateExperiment(@Valid ExperimentProject experimentProject,String cnt, @RequestParam(value = "doc_file",required = false) MultipartFile doc_file, @RequestParam(value = "pdf_file",required = false) MultipartFile pdf_file, @RequestParam(value = "image",required = false) MultipartFile image) {
        if (experimentProject.getProjectId() == null) {
            throw new FebsException("实验ID为空");
        }
        String imgUrl = "";
        String reportUrl = "";
        if (image != null && image.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            try {
                //调用阿里云OSS的上传方法
                imgUrl = aliOSSUpload(image, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            experimentProject.setProjectPictureUrl(imgUrl);
        }
        if(pdf_file != null && pdf_file.getSize() > 0){
            String basePath = "projectPdf";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(pdf_file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            experimentProject.setProjectContentUrl(url);
        }

        if (doc_file != null && doc_file.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            try {
                //调用阿里云OSS的上传方法
                reportUrl = aliOSSUpload(doc_file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            experimentProject.setReportUrl(reportUrl);
        }
        experimentProject.setUpdateTime(LocalDateTime.now());
        this.experimentProjectService.updateById(experimentProject);
        return new FebsResponse().success();
    }

    /**
     * GuiYongkang;
     * 2020/4/01 17:16pm
     * 删除实验项目
     */
    @GetMapping("delete/{projectIds}")
    @RequiresPermissions("experimentProject:delete")
    @ControllerEndpoint(operation = "删除实验项目", exceptionMessage = "删除实验项目失败")
    public FebsResponse deleteCourses(@NotBlank(message = "{required}") @PathVariable String projectIds) {
        String[] ids = projectIds.split(StringPool.COMMA);
        //获取所有项目id列表
        List<String> list = Arrays.asList(ids);
        //删除实验项目
        experimentProjectService.removeByIds(list);
        for (String projectId : list) {
            experimentProjectService.deleteProject(projectId);
        }
        return new FebsResponse().success();
    }


    @RequestMapping("/productPicUrl")
    public Map<String,Object> productPicUrl(MultipartFile file) {
        String basePath = "productPic";
        String url = null;
        Map<String,Object> map = new HashMap<>();
        try {
            url = aliOSSUpload(file, basePath);
            map.put("errno", 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> list = new ArrayList<>();
        list.add(url);
        map.put("data", list);
        return map;
    }

    @RequestMapping("/uploadPicUrl")
    public FebsResponse uploadPicUrl(MultipartFile file) {
        String basePath = "productPic";
        String url = null;
        Map<String,Object> map = new HashMap<>();
        FebsResponse response = new FebsResponse().success();
        try {
            url = aliOSSUpload(file, basePath);
            response.put("code",0);
        } catch (IOException e) {
            e.printStackTrace();
            response.put("code",1);
            response.put("msg","上传失败");
        }
        map.put("src", url);
        map.put("title", file.getOriginalFilename());
        response.data(map);
        return response;
    }

}
