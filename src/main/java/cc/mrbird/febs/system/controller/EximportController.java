package cc.mrbird.febs.system.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.common.utils.imageByFontsUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.others.entity.Eximport;
import cc.mrbird.febs.others.entity.TeacherImport;
import cc.mrbird.febs.others.entity.UserImport;
import cc.mrbird.febs.others.service.IEximportService;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserRole;
import cc.mrbird.febs.system.mapper.UserMapper;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserRoleService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ Description: Excel 导入导出工具类
 * @ Author: 马超伟
 * @ Date: 2020/6/9
 */
@Slf4j
@RestController
@RequestMapping("eximport")
public class EximportController extends BaseController {



    @Autowired
    private IEximportService eximportService;

    @Resource
    private IUserService userService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserRoleService userRoleService;

    @Resource
    private UserMapper userMapper;



    @GetMapping
    @RequiresPermissions("others:eximport:view")
    public FebsResponse findEximports(QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(eximportService.findEximports(request, null));
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 生成 Excel导入模板
     */
    @GetMapping("template")
    @RequiresPermissions("user:template")
    public void generateImportTemplate(HttpServletResponse response) {
        List<UserImport> userList = new ArrayList<>();
        UserImport userImport = new UserImport();
        //userImport.setUsername("student001");
        userImport.setTrueName("韩苏峰");
//        userImport.setCollege("机电学院");
//        userImport.setYears("2015");
        userImport.setCourse("电子信息工程1301");
        userImport.setIdNumber("20152222456");
        userImport.setMobile("13512341234");
        userImport.setSex("男");
        userImport.setDescription("导入学生测试，正式导入请删除这一行");
        userList.add(userImport);
        // 构建模板
        ExcelKit.$Export(UserImport.class, response).downXlsx(userList, true);
    }



    /**
     * 生成学生Excel导入模板
     */
    @GetMapping("templateWithStudent")
    @RequiresPermissions("user:template")
    @ControllerEndpoint(operation = "下载学生导入模板成功",exceptionMessage = "下载学生导入模板失败")
    public void generateImportStudentTemplate(HttpServletResponse response){
        try {
//            downloadTemplate("学生信息导入模板","http://file.huistone.com/excel/2020/07/14/a27758c09f404b7ca1bc2a10688e8350/学生Excel导入模板1.xlsx",response);
            downloadOss("http://file.huistone.com/excel/2020/07/29/91ed232119584cc7b1cbc1f7dd048aec/学生信息导入模板.xlsx");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 生成教师Excel导入模板
     */
    @GetMapping("templateWithTeacher")
    @RequiresPermissions("user:template")
    @ControllerEndpoint(operation = "下载教师导入模板成功",exceptionMessage = "下载教师导入模板失败")
    public void generateImportTeacherTemplate(HttpServletResponse response){
        try {
//            downloadTemplate("教师信息导入模板","http://file.huistone.com/excel/2020/07/14/dc8af47b453745e98aa6992cd36fd40c/教师Excel导入模板.xlsx",response);
            downloadOss("http://file.huistone.com/excel/2020/07/14/dc8af47b453745e98aa6992cd36fd40c/教师Excel导入模板.xlsx");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //下载用户信息导入模板
    public void downloadTemplate(String fileName,String excelUrl,HttpServletResponse response){
        OutputStream os = null;
        InputStream inputStream = null;
        Workbook wb = null;
        try {
            URL url = new URL(excelUrl);
            inputStream = new BufferedInputStream(url.openStream());
            wb = new XSSFWorkbook(inputStream);
            inputStream.close();
            // 清空response
            response.reset();
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename="+new String(fileName.getBytes("UTF-8"),"iso8859-1"));
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            os = response.getOutputStream();
        } catch (Exception e) {
        } finally {
            try {
                wb.write(os);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * 导入Excel数据，并批量插入 T_EXIMPORT表
     */
    @PostMapping("importStudent")
    @RequiresPermissions("user:import")
    @ControllerEndpoint(exceptionMessage = "导入Excel数据失败")
    public FebsResponse importExcels(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new FebsException("导入数据为空");
        }
        String filename = file.getOriginalFilename();
        if (!StringUtils.endsWith(filename, ".xlsx")) {
            throw new FebsException("只支持.xlsx类型文件导入");
        }
        // 开始导入操作
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<User> data = new ArrayList<>();
        final List<Map<String, Object>> error = Lists.newArrayList();
        ExcelKit.$Import(UserImport.class).readXlsx(file.getInputStream(), new ExcelReadHandler<UserImport>() {
            @Override
            public void onSuccess(int sheet, int row, UserImport userImport) {
                // 数据校验成功时，加入集合
                User user = new User();
                user.setCreateTime(new Date());
                String idnumber = userImport.getIdNumber();
                String iNumber="";
                for(int i = 0;i<idnumber.length();i++){
                    if(idnumber.charAt(i)>=48&&idnumber.charAt(i)<=57){
                        iNumber+=idnumber.charAt(i);
                    }
                }
//                if(iNumber.length()<3||idnumber.length()>12){
//                    throw new FebsException("学号过长");
//                }
                String idNumber = StringTrim(iNumber);
                idNumber = idNumber.replaceAll(" ","");
                String username = "stu"+idNumber;
                String str = StringTrim(username);
                user.setUsername(str);
                user.setPassword(MD5Util.encrypt(str, User.DEFAULT_PASSWORD));
                if(userImport.getSex()==null){
                    userImport.setSex("2");
                }
                user.setSex(userImport.getSex());
                user.setTrueName(userImport.getTrueName());
                //这下面就是设置基础信息
                user.setStatus(User.STATUS_VALID);
                user.setCreateTime(new Date());
                String image = imageByFontsUtil.generateImg(userImport.getTrueName());
                user.setAvatar(image);
                user.setTheme(User.THEME_BLACK);
                user.setIsTab(User.TAB_OPEN);
                user.setMobile(userImport.getMobile());
                user.setIdNumber(idNumber);
                user.setDeptName(userImport.getCourse());
                //设置描述内容
                if(userImport.getDescription()==null){
                    user.setDescription("我是"+user.getTrueName());
                }else{
                    user.setDescription(userImport.getDescription());
                }
                //设置昵称
                user.setNickname(userImport.getTrueName());
                user.setIsDel(1);
                data.add(user);
            }

            @Override
            public void onError(int sheet, int row, List<ExcelErrorField> errorFields) {
                // 数据校验失败时，记录到 error集合
                error.add(ImmutableMap.of("row", row, "errorFields", errorFields));
            }
        });

        if (CollectionUtils.isNotEmpty(data)) {
            for (User user : data) {
                Dept dept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptName, user.getDeptName()));
                if (dept!=null){
                    user.setDeptId(dept.getDeptId());
                }
                userMapper.insert(user);
                UserRole ur = new UserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(FebsConstant.STUDENT_ID);
                userRoleService.save(ur);
            }

        }
        ImmutableMap<String, Object> result = ImmutableMap.of(
                "time", stopwatch.stop().toString(),
                "data", data,
                "error", error
        );
        return new FebsResponse().success().data(result);
    }

    /**
     * 去空格
     * @ Param str
     * @return
     */
    private  String StringTrim(String str){
        return str.replaceAll("[\\s\\u00A0]+","").trim();
    }


    /**
     * 导入Excel数据，并批量插入 T_EXIMPORT表
     */
    @PostMapping("importTeacher")
    @RequiresPermissions("user:import")
    @ControllerEndpoint(exceptionMessage = "导入Excel数据失败")
    public FebsResponse importExcelsTeacher(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new FebsException("导入数据为空");
        }
        String filename = file.getOriginalFilename();
        if (!StringUtils.endsWith(filename, ".xlsx")) {
            throw new FebsException("只支持.xlsx类型文件导入");
        }
        // 开始导入操作
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<User> data = new ArrayList<>();
        final List<Map<String, Object>> error = Lists.newArrayList();
        ExcelKit.$Import(TeacherImport.class).readXlsx(file.getInputStream(), new ExcelReadHandler<TeacherImport>() {
            @Override
            public void onSuccess(int sheet, int row, TeacherImport userImport) {

                // 数据校验成功时，加入集合
                User user = new User();
                user.setCreateTime(new Date());
                String idnumber = userImport.getIdNumber();
                String iNumber="";
                for(int i = 0;i<idnumber.length();i++){
                    if(idnumber.charAt(i)>=48&&idnumber.charAt(i)<=57){
                        iNumber+=idnumber.charAt(i);
                    }
                }
                if(iNumber.length()<3||idnumber.length()>12){
                    throw new FebsException("学号过长或过短");
                }
                String idNumber = StringTrim(iNumber);
                idNumber = idNumber.replaceAll(" ","");
                String username = "tch"+idNumber;
                String str = StringTrim(username);
                user.setUsername(str);
                user.setPassword(MD5Util.encrypt(str, User.DEFAULT_PASSWORD));
                if(userImport.getSex()==null){
                    userImport.setSex("2");
                }
                user.setSex(userImport.getSex());
                user.setTrueName(userImport.getTrueName());
                //设置基础信息
                user.setStatus(User.STATUS_VALID);
                user.setCreateTime(new Date());
                String image = imageByFontsUtil.generateImg(userImport.getTrueName());
                user.setAvatar(image);
                user.setTheme(User.THEME_BLACK);
                user.setIsTab(User.TAB_OPEN);
                user.setMobile(userImport.getMobile());
                user.setIdNumber(idNumber);
                if(userImport.getDescription()==null){
                    user.setDescription("我是"+user.getTrueName());
                }else{
                    user.setDescription(userImport.getDescription());
                }
                //设置昵称
                user.setNickname(userImport.getTrueName());
                user.setIsDel(1);
                data.add(user);
            }
            @Override
            public void onError(int sheet, int row, List<ExcelErrorField> errorFields) {
                // 数据校验失败时，记录到 error集合
                error.add(ImmutableMap.of("row", row, "errorFields", errorFields));
            }
        });
        if (CollectionUtils.isNotEmpty(data)) {
            // 将合法的记录批量入库
//            this.eximportService.batchInsert();
            for (User user : data) {
                userMapper.insert(user);
                UserRole ur = new UserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(FebsConstant.TEACHER_ID);
                userRoleService.save(ur);
            }
        }
        ImmutableMap<String, Object> result = ImmutableMap.of(
                "time", stopwatch.stop().toString(),
                "data", data,
                "error", error
        );
        return new FebsResponse().success().data(result);
    }




    @GetMapping("excel")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, Eximport eximport, HttpServletResponse response) {
        List<Eximport> eximports = this.eximportService.findEximports(queryRequest, eximport).getRecords();
        ExcelKit.$Export(Eximport.class, response).downXlsx(eximports, false);
    }


}
