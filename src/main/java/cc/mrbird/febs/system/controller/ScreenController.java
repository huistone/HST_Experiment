package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.vo.ScreenVo;
import cc.mrbird.febs.system.service.IScreenService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;

/**
 * <p>
 * 截屏记录保持表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-12-30
 */
@RestController
@RequestMapping("/screen")
public class ScreenController extends BaseController {

    @Resource
    private IScreenService screenService;


    /**
     * @Author wangke
     * @Description 查询实验截图list
     * @Date 15:09 2020/12/30
     * @Param
     * @return
     */
    @RequestMapping("list")
    @ControllerEndpoint(operation = "查询实验截图",exceptionMessage = "查询实验截图失败")
    public FebsResponse selectScreenList(QueryRequest request, ScreenVo screenVo){
        IPage<ScreenVo> screenPage = screenService.selectListByCondition(request,screenVo);
        return new FebsResponse().success().data(getDataTable(screenPage));
    }
}
