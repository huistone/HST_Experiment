package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.CourseSelect;
import cc.mrbird.febs.common.entity.DeptTree;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.system.entity.CourseDept;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import cc.mrbird.febs.system.mapper.DeptMapper;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author 马超伟
 */
@Slf4j
@RestController
@RequestMapping("dept")
public class DeptController extends BaseController {

    @Autowired
    private IDeptService deptService;

    @Resource
    private DeptMapper deptMapper;

    @Resource
    private ICourseDeptService courseDeptService;

    @GetMapping("select/tree")
    @ControllerEndpoint(exceptionMessage = "获取部门树失败")
    public List<DeptTree<Dept>> getDeptTree() throws FebsException {
        return this.deptService.findDepts();
    }

    @GetMapping("/getCascader")
    public FebsResponse getCascader(){
        List<ElementCascader> cascader = deptMapper.getCascader();
        return new FebsResponse().put("Code",0).data(cascader);
    }

    @GetMapping("tree")
    @ControllerEndpoint(exceptionMessage = "获取部门树失败")
    public FebsResponse getDeptTree(Dept dept) throws FebsException {
        List<DeptTree<Dept>> depts = this.deptService.findDepts(dept);
        for (DeptTree<Dept> deptDeptTree : depts) {
            if (deptDeptTree.getChildren()!=null){
                deptDeptTree.setDisabled(true);
                List<DeptTree<Dept>> children = deptDeptTree.getChildren();
                for (DeptTree<Dept> child : children) {
                    child.setDisabled(true);
                }
            }
        }
        return new FebsResponse().success().data(depts);
    }

    @RequestMapping("setDeptCourse")
    @RequiresPermissions("dept:update")
    @ControllerEndpoint(operation = "新增班级课程关系", exceptionMessage = "新增班级课程关系失败")
    public FebsResponse setDeptCourse( String select, String deptId) {
        //先删除
        courseDeptService.remove(new QueryWrapper<CourseDept>()
                .lambda().eq(CourseDept::getDeptId,deptId));
        //再添加
        String[] split = select.split(",");
        for (String s : split) {
            CourseDept courseDept = new CourseDept();
            courseDept.setDeptId(Integer.parseInt(deptId));
            courseDept.setCourseId(Integer.parseInt(s));
            courseDeptService.save(courseDept);
        }
        return new FebsResponse().success();
    }


    @PostMapping
    @RequiresPermissions("dept:add")
    @ControllerEndpoint(operation = "新增部门", exceptionMessage = "新增部门失败")
    public FebsResponse addDept(@Valid Dept dept) {
        this.deptService.createDept(dept);
        return new FebsResponse().success();
    }

    @PostMapping("/newUpdate")
    @RequiresPermissions("dept:update")
    @ControllerEndpoint(operation = "修改", exceptionMessage = "修改部门失败")
    public FebsResponse updateNewDept(@Valid Dept dept) {
        this.deptService.updateDept(dept);
        return new FebsResponse().success();
    }


    @GetMapping("delete/{deptIds}")
    @RequiresPermissions("dept:delete")
    @ControllerEndpoint(operation = "删除部门", exceptionMessage = "删除部门失败")
    public FebsResponse deleteDepts(@NotBlank(message = "{required}") @PathVariable String deptIds) throws FebsException {
        String[] ids = deptIds.split(StringPool.COMMA);
        this.deptService.deleteDepts(ids);
        return new FebsResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("dept:update")
    @ControllerEndpoint(operation = "修改部门", exceptionMessage = "修改部门失败")
    public FebsResponse updateDept(@Valid Dept dept) throws FebsException {
        this.deptService.updateDept(dept);
        return new FebsResponse().success();
    }

    @GetMapping("excel")
    @RequiresPermissions("dept:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(Dept dept, QueryRequest request, HttpServletResponse response) throws FebsException {
        List<Dept> depts = this.deptService.findDepts(dept, request);
        ExcelKit.$Export(Dept.class, response).downXlsx(depts, false);
    }

    @GetMapping("getTreeTable")
    @ControllerEndpoint(exceptionMessage = "获取部门树失败")
    @RequiresPermissions("newDept:view")
    public FebsResponse getTreeTable(){
        List<Dept> deptList = this.deptMapper.selectList(new QueryWrapper<Dept>().lambda().orderByAsc(Dept::getOrderNum));
        FebsResponse response = new FebsResponse().success().data(deptList);
        response.put("count",deptList.size());
        return response;
    }

}
