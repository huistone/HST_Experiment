package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.SysConfig;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.vo.WebInformation;
import cc.mrbird.febs.system.service.ISysConfigService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import cc.mrbird.febs.common.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-05-25
 */
@RestController
@RequestMapping("/sysConfig")
public class SysConfigController extends BaseController {

    @Resource
    private ISysConfigService configService;

    /**
     *
     * 作者:GuiZi_cheng
     * 时间：2020/5/25
     * 系统配置表的增删改查
     */
    @RequestMapping("selectConfigList")
    @ControllerEndpoint(operation = "查询系统配置成功",exceptionMessage = "查询系统配置失败")
    public FebsResponse selectConfigList(QueryRequest queryRequest,String configName){
        Page page = new Page(queryRequest.getPageNum(),queryRequest.getPageSize());
        IPage iPage = configService.page(page, new QueryWrapper<SysConfig>().lambda().like(StringUtils.isNotBlank(configName), SysConfig::getConfigName, configName));
        List<SysConfig> sysConfigs = iPage.getRecords();
        sysConfigs.forEach(i->{
            i.setConfigType(i.getConfigType().toUpperCase());
        });
        return new FebsResponse().success().data(getDataTable(iPage)).message("查询数据成功");
    }

    @RequestMapping("auSysConfig")
    public FebsResponse addOneSysConfig(SysConfig sysConfig){
        User user = getCurrentUser();
        if(sysConfig.getConfigId()!=null){
            sysConfig.setUpdateUser(user.getUsername());
            LocalDateTime dateTime = LocalDateTime.now();
            sysConfig.setUpdateTime(dateTime);
        }else {
            LocalDateTime time = LocalDateTime.now();
            sysConfig.setCreateTime(time);
            sysConfig.setCreateUser(user.getUsername());
        }

        configService.saveOrUpdate(sysConfig);
        return new FebsResponse().success();
    }

    @RequestMapping("deleteSysConfig/{systemConfigIds}")
    @RequiresPermissions("systemConfig:delete")
    @ControllerEndpoint(operation = "删除系统配置成功",exceptionMessage = "删除系统配置失败")
    public FebsResponse deleteSysConfig(@PathVariable String systemConfigIds){
        String[] newIds = systemConfigIds.split(",");
        configService.removeByIds(Arrays.asList(newIds));
        return new FebsResponse().success();
    }

    @RequestMapping("selectOneSysConfig")
    public FebsResponse selectOneSysConfig(Integer configId){
        return new FebsResponse().success().data(configService.getById(configId)).success();
    }

    private List<String> getUrl(String configValue){
        String img = "";
        Pattern p_image;
        Matcher m_image;
        List<String> pics = new ArrayList<String>();

        String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
        p_image = Pattern.compile(regEx_img, Pattern.CASE_INSENSITIVE);
        m_image = p_image.matcher(configValue);
        while (m_image.find()) {
            img = img + "," + m_image.group();
            // Matcher m =
            // Pattern.compile("src=\"?(.*?)(\"|>|\\s+)").matcher(img); //匹配src
            Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);

            while (m.find()) {
                pics.add(m.group(1));
            }
        }
         return pics;
    }

    private String tagfilter(String configValue){
        // 过滤所有以<开头以>结尾的标签
        final String regxpForHtml = "<([^>]*)>";
        Pattern pattern = Pattern.compile(regxpForHtml);
        Matcher matcher = pattern.matcher(configValue);
        StringBuffer sb = new StringBuffer();
        boolean result1 = matcher.find();
        while (result1) {
            matcher.appendReplacement(sb, "");
            result1 = matcher.find();
        }
        matcher.appendTail(sb);
        return sb.toString();
    }


    @RequestMapping("updateSystemConfig")
    @ResponseBody
    @ControllerEndpoint(operation = "更新系统配置成功",exceptionMessage = "更新系统配置失败")
    public FebsResponse updateSystemConfig(@RequestParam Map<String,String> map,WebInformation webInformation){

        if(map.get("companyName") != null && !map.get("companyName").trim().equals("")){
            configService.updateByConfigName("companyName",map.get("companyName"));
        }
        if(webInformation.getLogo() != null){
            String imageUrl = getImageUrl(webInformation.getLogo());
            configService.updateByConfigName("logo",imageUrl);
        }
        if(webInformation.getIco() != null){
            String imageUrl = getImageUrl(webInformation.getIco());
            configService.updateByConfigName("ico",imageUrl);
        }
        if(webInformation.getWeChat() != null ){
            String imageUrl = getImageUrl(webInformation.getWeChat());
            configService.updateByConfigName("Wechat",imageUrl);
        }

        if(map.get("pic1addr") != null && !map.get("pic1addr").trim().equals("")){
            configService.updateByConfigName("pic1addr",map.get("pic1addr"));
        }

        if(map.get("pic2addr") != null && !map.get("pic2addr").trim().equals("")){
            configService.updateByConfigName("pic2addr",map.get("pic2addr"));
        }

        if(map.get("pic3addr") != null && !map.get("pic3addr").trim().equals("")){
            configService.updateByConfigName("pic3addr",map.get("pic3addr"));
        }
        if(webInformation.getPic1() != null){
            String imageUrl = getImageUrl(webInformation.getPic1());
            configService.updateByConfigName("pic1",imageUrl);
        }
        if(webInformation.getPic2() != null){
            String imageUrl = getImageUrl(webInformation.getPic2());
            configService.updateByConfigName("pic2",imageUrl);
        }
        if(webInformation.getPic3() != null){
            String imageUrl = getImageUrl(webInformation.getPic3());
            configService.updateByConfigName("pic3",imageUrl);
        }

        if(map.get("telephoneNumber") != null && !map.get("telephoneNumber").trim().equals("")){
            configService.updateByConfigName("telephoneNumber",map.get("telephoneNumber"));
        }

        if(map.get("QQ1") != null && !map.get("QQ1").trim().equals("")){
            configService.updateByConfigName("QQ1",map.get("QQ1"));
        }
        if(map.get("QQ2") != null && !map.get("QQ2").trim().equals("")){
            configService.updateByConfigName("QQ2",map.get("QQ2"));
        }
        if(map.get("QQ3") != null && !map.get("QQ3").trim().equals("")){
            configService.updateByConfigName("QQ3",map.get("QQ3"));
        }
        if(map.get("email") != null && !map.get("email").trim().equals("")){
            configService.updateByConfigName("email",map.get("email"));
        }
        if(map.get("officialWebsite") != null && !map.get("officialWebsite").trim().equals("")){
            configService.updateByConfigName("officialWebsite",map.get("officialWebsite"));
        }
        if(map.get("address") != null && !map.get("address").trim().equals("")){
            configService.updateByConfigName("address",map.get("address"));
        }
        if(map.get("copyRight") != null && !map.get("copyRight").trim().equals("")){
            configService.updateByConfigName("copyRight",map.get("copyRight"));
        }
        if(map.get("not_duty") != null && !map.get("not_duty").trim().equals("")){
            configService.updateByConfigName("not_duty",map.get("not_duty"));
        }
        if(map.get("doc_help") != null && !map.get("doc_help").trim().equals("")){
            configService.updateByConfigName("doc_help",map.get("doc_help"));
        }
        if(map.get("project_info") != null && !map.get("project_info").trim().equals("")){
            configService.updateByConfigName("project_info",map.get("project_info"));
        }
        if(map.get("privacy_policy") != null && !map.get("privacy_policy").trim().equals("")){
            configService.updateByConfigName("privacy_policy",map.get("privacy_policy"));
        }
        return new FebsResponse().success().data("更改数据成功");
    }


    String getImageUrl(MultipartFile file){
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return url;
        }
        return null;
    }

}
