package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.external.mqtt.MQTTSubscribe;
import cc.mrbird.febs.system.entity.AnalogBase;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.service.IAnalogBaseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-10-13
 */
@RestController
@RequestMapping("/analogBase")
public class AnalogBaseController extends BaseController {

    @Resource
    private IAnalogBaseService analogBaseService;

    @Resource
    private MQTTSubscribe mqttSubscribe;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @RequestMapping("/list")
    @RequiresPermissions("analog:view")
    @ControllerEndpoint(operation = "查询模电主板信息", exceptionMessage = "查询模电主板信息失败")
    public FebsResponse list(AnalogBase analogBase, QueryRequest request){
        IPage<AnalogBase> page = new Page<>(request.getPageNum(),request.getPageSize());
        IPage iPage = analogBaseService.page(page,
                new QueryWrapper<AnalogBase>()
                        .lambda()
                        .like(StringUtils.isNotBlank(analogBase.getAnalogName()), AnalogBase::getAnalogName, analogBase.getAnalogName())
                        .eq(analogBase.getAnalogStatus()!=null,AnalogBase::getAnalogStatus,analogBase.getAnalogStatus())
                        .eq(analogBase.getAnalogUsestatus()!=null,AnalogBase::getAnalogUsestatus,analogBase.getAnalogUsestatus()));
        return new FebsResponse().data(getDataTable(iPage)).success();
    }


    @RequestMapping("/add")
    @RequiresPermissions("analog:add")
    @ControllerEndpoint(operation = "添加主板",exceptionMessage = "添加主板失败")
    public FebsResponse add(AnalogBase analogBase){
        try {
            analogBase.setCreateTime(LocalDateTime.now());
            analogBase.setAnalogUsestatus(0);
            if (analogBase.getAnalogStatus() == 1){
                String[] split = analogBase.getSubChannel().split("@@");
                for (String s : split) {
                    mqttSubscribe.subscribe(s,2);
                }
            }
            String projectId = analogBase.getProjectId();
            String projectIds = analogBase.getProjectId();
            String[] split = projectIds.split(",");
            StringBuilder stringBuilder = new StringBuilder();
            for (String s : split) {
                ExperimentProject one = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq(ExperimentProject::getProjectId, s)
                        .select(ExperimentProject::getProjectName));
                stringBuilder.append(one.getProjectName()+" ");
            }
            analogBase.setOperator("暂无使用");
            analogBase.setProjectName(stringBuilder.toString());
            analogBaseService.save(analogBase);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

    @PostMapping("/update")
    @RequiresPermissions("analog:update")
    @ControllerEndpoint(exceptionMessage = "主板更新失败", operation = "主板更新成功")
    public FebsResponse update(AnalogBase analogBase){
        try {
            analogBase.setUpdateTime(LocalDateTime.now());
            String projectId = analogBase.getProjectId();
            String[] split1 = projectId.split(",");
            StringBuilder stringBuilder = new StringBuilder();
            for (String s : split1) {
                stringBuilder.append(experimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
                        .lambda()
                        .eq(ExperimentProject::getProjectId, s)
                        .select(ExperimentProject::getProjectName)).getProjectName()+" ");
            }
            analogBase.setProjectName(stringBuilder.toString());
            if (analogBase.getAnalogStatus() != null){
                AnalogBase one = analogBaseService.getOne(new QueryWrapper<AnalogBase>()
                        .lambda()
                        .eq(AnalogBase::getAnalogId, analogBase.getAnalogId())
                        .select(AnalogBase::getSubChannel));
                String[] split = one.getSubChannel().split("@@");
                if (analogBase.getAnalogStatus() == 1){
                    for (String s : split) {
                        mqttSubscribe.subscribe(s,2);
                    }
                }else{
                    mqttSubscribe.unsubscribe(split);
                }
            }
            analogBaseService.updateById(analogBase);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("/delete/{analogIds}")
    @RequiresPermissions("analog:delete")
    @ControllerEndpoint(exceptionMessage = "主板删除失败", operation = "主板删除成功")
    public FebsResponse delete(@PathVariable String analogIds){
        try {
            String[] split = analogIds.split(StringPool.COMMA);
            List<String> list = Arrays.asList(split);
            for (String s : list) {
                AnalogBase one = analogBaseService.getOne(new QueryWrapper<AnalogBase>()
                        .lambda()
                        .eq(AnalogBase::getAnalogId, s)
                        .select(AnalogBase::getSubChannel));
                String[] split1 = one.getSubChannel().split("@@");
                mqttSubscribe.unsubscribe(split1);
                analogBaseService.remove(new QueryWrapper<AnalogBase>().lambda().eq(AnalogBase::getAnalogId,s));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }



}
