package cc.mrbird.febs.system.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.others.entity.CourseManagerImport;
import cc.mrbird.febs.others.entity.CustomerImport;
import cc.mrbird.febs.others.entity.TeacherImport;
import cc.mrbird.febs.others.entity.UserImport;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.*;

/**
 * @author 马超伟
 */
@Slf4j
@Validated
@RestController
@RequestMapping("user")
public class UserController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserService userService;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IUserRoleService userRoleService;

    @Resource
    private IDeptService deptService;


    @GetMapping("{username}")
    @ControllerEndpoint(operation = "查询用户信息成功", exceptionMessage = "查询用户信息失败")
    public User getUser(@NotBlank(message = "{required}") @PathVariable String username) {
        return this.userService.findUserDetailList(username);
    }

    @GetMapping("check/{username}")
    @ControllerEndpoint(operation = "校验用户成功", exceptionMessage = "校验用户失败")
    public boolean checkUserName(@NotBlank(message = "{required}") @PathVariable String username, String userId) {
        return this.userService.findByName(username) == null || StringUtils.isNotBlank(userId);
    }

    @GetMapping("list")
    @RequiresPermissions("user:view")
    @ControllerEndpoint(operation = "查询不包括老师和学生用户，查询成功", exceptionMessage = "查询不包括老师用户失败")
    public FebsResponse userList(User user, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.userService.findUserDetailListExcludeTeacher(user, request));
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("listTeacher")
    @RequiresPermissions("user:view")
    @ControllerEndpoint(operation = "查询老师用户成功", exceptionMessage = "查询老师用户失败")
    public FebsResponse userTeacherList(User user, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.userService.findUserTeacherDetailList(user, request));
        return new FebsResponse().success().data(dataTable);
    }


    @GetMapping("courseManagerList")
    @RequiresPermissions("user:view")
    @ControllerEndpoint(operation = "查询课程管理员成功", exceptionMessage = "查询课程管理员失败")
    public FebsResponse courseManagerList(User user, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.userService.findCourseManagerList(user, request));
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("listStudent")
    @RequiresPermissions("user:view")
    @ControllerEndpoint(operation = "查询学生用户成功", exceptionMessage = "查询学生用户失败")
    public FebsResponse userStudentList(User user, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.userService.findUserStudentDetailList(user, request));
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("customerList")
    @RequiresPermissions("user:view")
    @ControllerEndpoint(operation = "查询企业用户成功", exceptionMessage = "查询企业用户失败")
    public FebsResponse customerList(User user, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.userService.findUserCustomerList(user, request));
        return new FebsResponse().success().data(dataTable);
    }


    private void setUserRoles(User user, Long roleId) {
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getUserId());
        userRole.setRoleId(Long.valueOf(roleId));
        userRoleService.save(userRole);
    }

    @PostMapping
    @RequiresPermissions("user:add")
    @ControllerEndpoint(operation = "新增用户", exceptionMessage = "新增用户失败")
    public FebsResponse addUser(@Valid User user) {
        if (user.getUsername() == null ||
                user.getIdNumber() == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("添加失败！请检查输入信息是否正确！");
        }
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda().
                eq(User::getIdNumber, user.getIdNumber())) != null) {
            return new FebsResponse().fail().message("该学号已存在");
        }
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setUserType(FebsConstant.SCHOOL_TYPE);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        userService.save(user);
        // 保存用户角色
        setUserRoles(user, user.getRoleId());
        return new FebsResponse().success();
    }

    @PostMapping("addCourseManager")
    @RequiresPermissions("user:add")
    @ControllerEndpoint(operation = "新增课程管理员", exceptionMessage = "新增课程管理员失败")
    public FebsResponse addCourseManager(@Valid User user) {
        if (user.getUsername() == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("添加失败！请检查输入信息是否正确！");
        }
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        userService.save(user);
        // 保存用户角色
        setUserRoles(user, 87L);
        return new FebsResponse().success();
    }

    @PostMapping("addUserTeacher")
    @RequiresPermissions("user:add")
    @ControllerEndpoint(operation = "新增老师", exceptionMessage = "新增老师失败")
    public FebsResponse addUserTeacher(@Valid User user, @RequestParam(value = "dept[]", required = false) Integer[] dept) {
        if (user.getUsername() == null ||
                user.getIdNumber() == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("添加失败！请检查输入信息是否正确！");
        }
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda().
                eq(User::getIdNumber, user.getIdNumber())) != null) {
            return new FebsResponse().fail().message("该学号已存在");
        }
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setUserType(FebsConstant.SCHOOL_TYPE);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        userService.save(user);
        // 保存用户角色
        setUserRoles(user, user.getRoleId());
        addUserDept(dept, user);
        return new FebsResponse().success();
    }

    @PostMapping("addUserStudent")
    @RequiresPermissions("user:add")
    @ControllerEndpoint(operation = "新增学生", exceptionMessage = "新增学生失败")
    public FebsResponse addUserStudent(@Valid User user, @RequestParam(value = "dept[]", required = false) Integer[] dept) {
        Integer deptId = dept[0];
        if (user.getUsername() == null ||
                user.getIdNumber() == null ||
                deptId == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("添加失败！请检查输入信息是否正确！");
        }
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda().
                eq(User::getIdNumber, user.getIdNumber())) != null) {
            return new FebsResponse().fail().message("该学号已存在");
        }
        user.setDeptId(deptId.longValue());
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setUserType(FebsConstant.SCHOOL_TYPE);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        userService.save(user);
        // 保存用户角色
        setUserRoles(user, user.getRoleId());
//        this.userService.createUser(user);
        return new FebsResponse().success();
    }

    /**
     * @Author wangke
     * @ Description 添加客户
     * @Date 16:51 2020/11/27
     * @ Param
     * @return 
     */
    @PostMapping("addCustomer")
    @RequiresPermissions("customer:add")
    @ControllerEndpoint(operation = "新增客户", exceptionMessage = "新增客户失败")
    public FebsResponse addCustomer(@Valid User user) {
        if (user.getUsername() == null ||
                user.getTrueName() == null ||
                user.getMobile() == null
        ) {
            return new FebsResponse().fail().message("添加失败！请检查输入信息是否正确！");
        }
        if (userService.findByName(user.getUsername()) != null) {
            return new FebsResponse().fail().message("该用户名已存在");
        }
        if (userService.getOne(new QueryWrapper<User>().lambda()
                .eq(User::getMobile, user.getMobile())) != null) {
            return new FebsResponse().fail().message("手机号已存在");
        }
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        userService.save(user);
        // 保存用户角色
        setUserRoles(user, 86L);
        return new FebsResponse().success();
    }


    @GetMapping("delete/{userIds}")
    @RequiresPermissions("user:delete")
    @ControllerEndpoint(operation = "删除用户", exceptionMessage = "删除用户失败")
    public FebsResponse deleteUsers(@NotBlank(message = "{required}") @PathVariable String userIds) {
        String[] ids = userIds.split(StringPool.COMMA);
        this.userService.deleteUsers(ids);
        return new FebsResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("user:update")
    @ControllerEndpoint(operation = "修改用户", exceptionMessage = "修改用户失败")
    public FebsResponse updateUser(@Valid User user, @RequestParam(value = "dept[]", required = false) Integer[] dept) {
        if (user.getUserId() == null)
            throw new FebsException("用户ID为空");
        addUserDept(dept, user);
        this.userService.updateUser(user);
        return new FebsResponse().success();
    }


    /**
     * 添加用户和部门关系
     *
     * @ Param dept 部门id数组
     * @ Param user 用户
     */
    private void addUserDept(Integer[] dept, User user) {
        if (dept != null) {
            //先删
            userDeptService.remove(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, user.getUserId()));
            //再添加
            ArrayList<UserDept> userDeptArrayList = new ArrayList<>();
            for (Integer integer : dept) {
                UserDept userDept = new UserDept();
                userDept.setDeptId(integer.longValue());
                userDept.setUserId(user.getUserId());
                userDeptArrayList.add(userDept);
            }
            userDeptService.saveBatch(userDeptArrayList);
        }
    }

    @PostMapping("password/reset/{usernames}")
    @RequiresPermissions("user:password:reset")
    @ControllerEndpoint(exceptionMessage = "重置用户密码失败")
    public FebsResponse resetPassword(@NotBlank(message = "{required}") @PathVariable String usernames) {
        String[] usernameArr = usernames.split(StringPool.COMMA);
        this.userService.resetPassword(usernameArr);
        return new FebsResponse().success();
    }

    @PostMapping("password/update")
    @ControllerEndpoint(exceptionMessage = "修改密码失败")
    public FebsResponse updatePassword(
            @NotBlank(message = "{required}") String oldPassword,
            @NotBlank(message = "{required}") String newPassword) {
        User user = getCurrentUser();
        if (!StringUtils.equals(user.getPassword(), MD5Util.encrypt(user.getUsername(), oldPassword))) {
            throw new FebsException("原密码不正确");
        }
        userService.updatePassword(user.getUsername(), newPassword);
        return new FebsResponse().success();
    }

    @GetMapping("avatar")
    @ControllerEndpoint(exceptionMessage = "修改头像失败")
    public FebsResponse updateAvatar(@NotBlank(message = "{required}") String image) {
        User user = getCurrentUser();
        this.userService.updateAvatar(user.getUsername(), image);
        return new FebsResponse().success();
    }

    @PostMapping("theme/update")
    @ControllerEndpoint(exceptionMessage = "修改系统配置失败")
    public FebsResponse updateTheme(String theme, String isTab) {
        User user = getCurrentUser();
        this.userService.updateTheme(user.getUsername(), theme, isTab);
        return new FebsResponse().success();
    }

    @PostMapping("profile/update")
    @ControllerEndpoint(exceptionMessage = "修改个人信息失败")
    public FebsResponse updateProfile(User user) throws FebsException {
        User currentUser = getCurrentUser();
        user.setUserId(currentUser.getUserId());
        this.userService.updateProfile(user);
        return new FebsResponse().success();
    }

    @GetMapping("excel")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出管理员Excel失败")
    public void export(HttpServletResponse response) {
        List<User> users = this.userService.findUserManagerList();
        ExcelKit.$Export(UserImport.class, response).downXlsx(users, false);
    }
    @GetMapping("exportWithCustomer")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出企业版用户Excel失败")
    public void exportCustomer(HttpServletResponse response){
        List<User> users =  this.userService.findUserCustomerExcelInfo();
        ExcelKit.$Export(CustomerImport.class,response).downXlsx(users,false);
    }

    @GetMapping("excelWithTeacher")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出老师Excel失败")
    public void exportTeacher(HttpServletResponse response) {
        List<User> list = userService.findUserTeacherList();
        ExcelKit.$Export(TeacherImport.class, response).downXlsx(list, false);
    }

    @GetMapping("excelWithCourseManager")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出课程管理员Excel失败")
    public void exportCourseManager(HttpServletResponse response) {
        List<User> list = userService.findCourseManagerExcelInfo();
        ExcelKit.$Export(CourseManagerImport.class, response).downXlsx(list, false);
    }

    @GetMapping("excelWithStudent")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出学生Excel失败")
    public void exportStudent(HttpServletResponse response) {
        List<User> list = userService.list(new QueryWrapper<User>()
                .lambda().isNotNull(User::getDeptId).eq(User::getIsDel, 1));

        list.stream().forEach(s -> {
            s.setDeptName(deptService.getById(s.getDeptId()).getDeptName());
        });

        ExcelKit.$Export(User.class, response).downXlsx(list, false);
    }


    @GetMapping("select/teacher")
    @ControllerEndpoint(exceptionMessage = "获取老师列表失败")
    public FebsResponse getTeachers(User user, QueryRequest request) throws FebsException {
        IPage<User> userIPage = this.userService.findTeacherList(user, request);
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code", 0);
        febsResponse.put("count", userIPage.getTotal());
        febsResponse.put("data", userIPage.getRecords());
        return febsResponse;
    }


    @PostMapping("/getExcelUrl")
    public FebsResponse getExcelUrl(MultipartFile file) {
        String basePath = "excel";
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FebsResponse().success().data(url);
    }

    /**
     * 上传实验步骤文档pdf
     *
     * @ Param file
     * @return
     */
    @PostMapping("/getProjectPdfUrl")
    public FebsResponse getProjectPdfUrl(MultipartFile file) {
        String basePath = "projectPdf";
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FebsResponse().success().data(url);
    }

    @PostMapping("/getPictureUrl")
    public FebsResponse getPicUrl(MultipartFile file) {
        String basePath = "picture";
        String url = null;
        try {
            url = aliOSSUpload(file, basePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(url);
        return new FebsResponse().success().data(url);
    }

    /**
     * @return java.util.List<java.lang.String>
     * @Author wangke
     * @ Description //批量上传图片接口
     * @Date 9:36 2020/8/25
     * @ Param [file]
     */
//    @PostMapping("/MultiUploadPic")
//    public List<String> multiUploadPic(MultipartFile[] file) {
//        String basePath = "picture";
//        String url = null;
//        List list = new ArrayList();
//        try {
//            for (MultipartFile multipartFiles : file) {
//                url = aliOSSUpload(multipartFiles, basePath);
//                list.add(url);
//                url = null;
//            }
//        } catch (Exception e) {
//            return null;
//        }
//        return list;
//    }

}
