package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Order;
import cc.mrbird.febs.system.service.IOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-19
 */
@RestController
@RequestMapping("/order")
public class OrderController extends BaseController {

    @Resource
    private IOrderService orderService;

    @RequestMapping("/list")
    @RequiresPermissions("article:view")
    @ControllerEndpoint(operation = "查询订单", exceptionMessage = "查询订单失败")
    public FebsResponse getArticleList(Order order, QueryRequest request){
        Page<Order> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Order> page1 = orderService.page(page, new QueryWrapper<Order>()
                .lambda()
                .like(StringUtils.isNotBlank(order.getOrderSn()), Order::getOrderSn, order.getOrderSn())
                .eq(order.getPayStatus() != null, Order::getPayStatus, order.getPayStatus())
                .eq(order.getPaymentMethod() != null, Order::getPaymentMethod, order.getPaymentMethod())
                .eq(StringUtils.isNotBlank(order.getGoodsName()), Order::getGoodsName,order.getGoodsName())
                .orderByDesc(Order::getPayTime)
                .orderByDesc(Order::getCreateTime));
        return new FebsResponse().success().data(getDataTable(page1));

    }

}
