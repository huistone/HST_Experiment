package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 波形数据集，自动学习 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
@RestController
@RequestMapping("/wave")
public class WaveController extends BaseController {

}
