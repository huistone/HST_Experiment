package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 资讯分类关联表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
@RestController
@RequestMapping("/article-category")
public class ArticleCategoryController extends BaseController {

}
