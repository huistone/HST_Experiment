package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-11-25
 */
@RestController
@RequestMapping("/ec-score")
public class EcScoreController extends BaseController {

}
