package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.annotation.DeleteCache;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.CourseDept;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/course")
public class CourseController extends BaseController {

    @Resource
    private ICourseService courseService;

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IUserService userService;


    /**
    * @ Description: 查询所有的课程列表
    * @ Param: [course, request]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @GetMapping("list")
    @RequiresPermissions("course:view")
    @ControllerEndpoint(operation = "查询课程", exceptionMessage = "查询课程失败")
    public FebsResponse courseList(Course course, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.courseService.findUserDetailList(course, request));
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * @ return
     * @ RequestParam("deptIds[]")
     * @ RequestParam("deptNameLists[]")
     */
    @PostMapping("/add")
    @RequiresPermissions("course:add")
    @ControllerEndpoint(operation = "新增课程", exceptionMessage = "新增课程失败")
    public FebsResponse addExperiment(@Valid Course course, Integer[] deptIds,
                                      @RequestParam(name = "file") MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            course.setCoursePictureUrl(url);
        }

        course.setCreateTime(LocalDateTime.now());
        courseService.save(course);
        List<CourseDept> courseDeptList = new ArrayList<>();
        for (Integer deptId : deptIds) {
            CourseDept courseDept = new CourseDept();
            courseDept.setDeptId(deptId);
            courseDept.setCourseId(course.getCourseId().intValue());
            courseDeptList.add(courseDept);
        }
        courseDeptService.saveBatch(courseDeptList);
        return new FebsResponse().success();
    }

    /**
     * @ Description: 删除课程信息
     * @ Param: [courseIds] 课程ID列表
     * @ return: cc.mrbird.febs.common.entity.FebsResponse
     * @ Author: 马超伟
     * @ Date: 2020/5/12
     * 删除课程的同时将courseDept关联表一块删除
     */
    @GetMapping("delete/{courseIds}")
    @RequiresPermissions("course:delete")
    @ControllerEndpoint(operation = "删除课程", exceptionMessage = "删除课程失败")
    public FebsResponse deleteCourses(@NotBlank(message = "{required}") @PathVariable String courseIds) {
        String[] ids = courseIds.split(StringPool.COMMA);
        List<String> list = Arrays.asList(ids);
        for (String courseId : list) {
            //通过课程id去查询该课程下的所有实验项目
            List<ExperimentProject> experimentProjectList = experimentProjectService.list(new QueryWrapper<ExperimentProject>()
                    .lambda().eq(ExperimentProject::getCourseId, courseId));
            for (ExperimentProject experimentProject : experimentProjectList) {
                //删除课程下面的实验项目；
                if (experimentProject!=null) {
                    experimentProjectService.deleteProject(experimentProject.getProjectId().toString());
                }
            }
            //删除课程对应的班级关联
            courseDeptService.remove(new QueryWrapper<CourseDept>().lambda().eq(CourseDept::getCourseId, courseId));
            //删除课程和老师的关联,既将教这个课程的老师改为空
            userService.update(new UpdateWrapper<User>()
                    .lambda().set(User::getCourseId, null)
                    .eq(User::getCourseId, courseId));
        }
        //删除课程
        courseService.removeByIds(list);
        return new FebsResponse().success();
    }

    /**
     * @ Auther:贵永康
     * @ Date: Create in 9:21 2020/3/31
     * @ Description: 修改课程信息
     */
    @PostMapping("update")
    @RequiresPermissions("course:update")
    @ControllerEndpoint(operation = "修改课程", exceptionMessage = "修改课程失败")
    public FebsResponse updateCourses(@Valid Course course, @RequestParam(name = "file") MultipartFile file) {
        if (course.getCourseId() == null) {
            throw new FebsException("用户ID为空");
        }
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            String url = "";
            try {
                //调用阿里云OSS的上传方法
                url = aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            course.setCoursePictureUrl(url);
        }
        course.setUpdateTime(LocalDateTime.now());
        this.courseService.updateById(course);
        return new FebsResponse().success();
    }

    /*
     *搜索框查询
     */
    @GetMapping("select/course")
    @ControllerEndpoint(exceptionMessage = "获取课程列表失败")
    public FebsResponse getTeachers(QueryRequest request) throws FebsException {
        Page<Course> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Course> iPage = this.courseService.page(page, new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code", 0);
        febsResponse.put("count", iPage.getTotal());
        febsResponse.put("data", iPage.getRecords());
        return febsResponse;
    }

    /**
    * @ Description: 获取课程表格列表
    * @ Param: [courseName, request]
    * @ return: cc.mrbird.febs.common.entity.FebsResponse
    * @ Author: 马超伟
    * @ Date: 2020/5/20
    */
    @GetMapping("/select/courseName")
    @ControllerEndpoint(exceptionMessage = "获取课程列表失败")
    public FebsResponse getTeachers(String courseName, QueryRequest request) throws FebsException {
        Page<Course> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Course> iPage = this.courseService.page(page, new QueryWrapper<Course>().lambda().like(StringUtils.isNotBlank(courseName), Course::getCourseName, courseName));
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code", 0);
        febsResponse.put("count", iPage.getTotal());
        febsResponse.put("data", iPage.getRecords());
        return febsResponse;
    }

}
