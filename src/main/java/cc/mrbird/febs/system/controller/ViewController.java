package cc.mrbird.febs.system.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.authentication.ShiroHelper;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.CourseSelect;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.entity.vo.ProjectSelect;
import cc.mrbird.febs.system.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.ExpiredSessionException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 马超伟
 */
@Controller("systemView")
@Slf4j
public class ViewController extends BaseController {

    @Resource
    private IDetBaseService detBaseService;

    @Resource
    private IUserService userService;

    @Resource
    private ShiroHelper shiroHelper;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IDeptService deptService;

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private IExperimentProjectService iExperimentProjectService;

    @Resource
    private IExperimentQuestionService questionService;

    @Resource
    private ISysConfigService sysConfigService;

    @Resource
    private IHelpService helpService;

    @Resource
    private IRoleService roleService;

    @Resource
    private ITopicService topicService;

    @Resource
    private IAnalogBaseService analogBaseService;

    @Resource
    private ICategoryService categoryService;

    @Resource
    private IArticleService articleService;

    @Resource
    private IVideoService videoService;


    @Resource
    private IElectricControlService electricControlService;

    @GetMapping("login")
    @ResponseBody
    public Object login(HttpServletRequest request) {
        if (FebsUtil.isAjaxRequest(request)) {
            throw new ExpiredSessionException();
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName(FebsUtil.view("login"));
            return mav;
        }
    }

    @GetMapping("unauthorized")
    public String unauthorized() {
        return FebsUtil.view("error/403");
    }

    @GetMapping("/")
    public String redirectIndex() {
        return "redirect:/index";
    }

    @GetMapping("index")
    public String index(Model model) {
        try {
            AuthorizationInfo authorizationInfo = shiroHelper.getCurrentuserAuthorizationInfo();
            User user = super.getCurrentUser();
            User currentUserDetail = userService.findByName(user.getUsername());
            currentUserDetail.setPassword("It's a secret");
            model.addAttribute("user", currentUserDetail);
            model.addAttribute("permissions", authorizationInfo.getStringPermissions());
            model.addAttribute("roles", authorizationInfo.getRoles());
            return "index";
        } catch (Exception e) {
            return FebsConstant.VIEW_PREFIX + "login";
        }
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "layout")
    public String layout() {
        return FebsUtil.view("layout");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "password/update")
    public String passwordUpdate() {
        return FebsUtil.view("system/user/passwordUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "user/profile")
    public String userProfile() {
        return FebsUtil.view("system/user/userProfile");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "user/avatar")
    public String userAvatar() {
        return FebsUtil.view("system/user/avatar");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "user/profile/update")
    public String profileUpdate() {
        return FebsUtil.view("system/user/profileUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/user")
    @RequiresPermissions("user:view")
    public String systemUser() {
        return FebsUtil.view("system/user/user");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/userTeacher")
    @RequiresPermissions("user:view")
    public String systemUserTeacher() {
        return FebsUtil.view("system/user/userTeacher/userTeacher");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toUserStudent")
    public String systemUserStudent(){
        return FebsUtil.view("system/user/userStudent/userStudent");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toUserCustomer")
    public String systemUserCustomer(){
        return FebsUtil.view("system/user/customer/customer");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "system/user/add")
    @RequiresPermissions("user:add")
    public String systemUserAdd(Model model) {
//        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
//        model.addAttribute("courseList",courseList);
        User currentUser = getCurrentUser();
        Role role = getCurrentRole(currentUser);
        List<Role> roleList = roleService.list(new QueryWrapper<Role>().lambda()
                .le(Role::getLevel,role.getLevel())
                .ne(Role::getRoleId,81)
                .ne(Role::getRoleId,82));
        model.addAttribute("roleList",roleList);
        return FebsUtil.view("system/user/userAdd");
    }

    /**
     * @Author wangke
     * @ Description 添加课程管理员
     * @Date 15:52 2020/11/30
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/courseManager/add")
    public String addCourseManager(){
        return FebsUtil.view("system/user/courseManager/userCourseAdd");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/user/addUserStudent")
    @RequiresPermissions("user:add")
    public String systemUserStudentAdd(Model model) {
//        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
//        model.addAttribute("courseList",courseList);

        return FebsUtil.view("system/user/userStudent/userStudentAdd");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/user/addCustomer")
    @RequiresPermissions("user:add")
    public String systemCustomerAdd(Model model) {
//        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
//        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/user/customer/customerAdd");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "system/user/detail/{username}")
    @RequiresPermissions("user:view")
    public String systemUserDetail(@PathVariable String username, Model model) {
        resolveUserModel(username, model, true);
        return FebsUtil.view("system/user/userDetail");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/user/update/{username}")
    @RequiresPermissions("user:update")
    public String systemUserUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);
        User currentUser = getCurrentUser();
        Role role = getCurrentRole(currentUser);
        List<Role> roleList = roleService.list(new QueryWrapper<Role>()
                .lambda()
                .le(Role::getLevel,role.getLevel())
                .ne(Role::getRoleId,81)
                .ne(Role::getRoleId,82));
        model.addAttribute("roleList",roleList);
        return FebsUtil.view("system/user/userUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/userTeacher/update/{username}")
    @RequiresPermissions("user:update")
    public String systemUserTeacherUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        return FebsUtil.view("system/user/userTeacher/userTeacherUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/courseManager/update/{username}")
    @RequiresPermissions("user:update")
    public String systemUserManagerUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        return FebsUtil.view("system/user/courseManager/userCourseUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/userStudent/update/{username}")
    @RequiresPermissions("user:update")
    public String systemUserStudentUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        return FebsUtil.view("system/user/userStudent/userStudentUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/userCustomer/update/{username}")
    @RequiresPermissions("user:update")
    public String systemUserCustomerUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        return FebsUtil.view("system/user/customer/customerUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/role")
    @RequiresPermissions("role:view")
    public String systemRole() {
        return FebsUtil.view("system/role/role");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/menu")
    @RequiresPermissions("menu:view")
    public String systemMenu() {
        return FebsUtil.view("system/menu/menu");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/dept")
    @RequiresPermissions("dept:view")
    public String systemDept() {
        return FebsUtil.view("system/dept/dept");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/deptNew")
    @RequiresPermissions("newDept:view")
    public String systemDeptNew() {
        return FebsUtil.view("system/dept_new/dept");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/deptNewAdd")
    @RequiresPermissions("newDept:add")
    public String deptNewAdd(Integer deptId,Model model) {
        if (IntegerUtils.isNotBlank(deptId)){
            model.addAttribute("deptId",deptId);
        }
        return FebsUtil.view("system/dept_new/deptAdd");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/deptNewUpdate/{deptId}")
    @RequiresPermissions("newDept:create")
    public String deptNewUpdate(@PathVariable Integer deptId,Model model) {
        if (IntegerUtils.isNotBlank(deptId)){
            model.addAttribute("dId",deptId);
            model.addAttribute("dName",deptService.getById(deptId).getDeptName());
        }
        return FebsUtil.view("system/dept_new/deptUpdate");
    }


    @RequestMapping(FebsConstant.VIEW_PREFIX + "index")
    public String pageIndex() {
        return FebsUtil.view("index");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "404")
    public String error404() {
        return FebsUtil.view("error/404");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "403")
    public String error403() {
        return FebsUtil.view("error/403");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "500")
    public String error500() {
        return FebsUtil.view("error/500");
    }

    /**
    * @ Description: 查询用户的详情信息
    * @ Param: [username, model, transform]
    * @ return: void
    * @ Author: 马超伟
    * @ Date: 2020/6/8
    */
    private void resolveUserModel(String username, Model model, Boolean transform) {
        User user = userService.findByName(username);
        List<Long> deptIdList = new ArrayList<>();
        List<String> deptList = new ArrayList<>();
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, user.getUserId()));
        if("81".equals(user.getRoleId())){
            List<Long> deptIds = userDeptList.stream().map(UserDept::getDeptId).distinct().collect(Collectors.toList());
//            for (UserDept userDept : userDeptList) {
//                deptIdList.add(userDept.getDeptId());
//                Long deptId = userDept.getDeptId();
//                Dept dept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptId, deptId));
//                deptList.add(dept.getDeptName());
//            }
            if (deptIds.size()>0){
                for (Long deptId : deptIds) {
                    deptList.add(deptService.getOne(new QueryWrapper<Dept>()
                            .lambda()
                            .eq(Dept::getDeptId,deptId)
                            .select(Dept::getDeptName)).getDeptName());
                }
            }
            user.setDeptName(deptList.toString().replace("[","").replace("]",""));
        }
        if ("82".equals(user.getRoleId())){
            deptIdList.add(user.getDeptId());
            user.setDeptName(deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptId,user.getDeptId())).getDeptName());
        }
        user.setDeptIds(deptIdList);
        model.addAttribute("user", user);
        if (transform) {
            String ssex = user.getSex();
            if (User.SEX_MALE.equals(ssex)) {
                user.setSex("男");
            } else if (User.SEX_FEMALE.equals(ssex)) {
                user.setSex("女");
            } else {
                user.setSex("保密");
            }
        }
        if (user.getLastLoginTime() != null) {
            model.addAttribute("lastLoginTime", DateUtil.getDateFormat(user.getLastLoginTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/course")
    @RequiresPermissions("course:view")
    public String systemCourse() {
        return FebsUtil.view("system/course/course");
    }

    /**
     * 去添加课程
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/course/add")
    @RequiresPermissions("course:add")
    public String systemCourseAdd() {
        return FebsUtil.view("system/course/courseAdd");
    }

    /**
     * 去添加 课程班级关联
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/systemCourseDept/add")
    @RequiresPermissions(value = {"system:course:add","system:course:update"})
    @ControllerEndpoint(exceptionMessage = "跳转新增/修改分配班级页面失败")
    public String systemCourseDeptAdd(Model model) {
        List<Course> courseList = courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, FebsConstant.USE));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/courseDept/courseDeptAdd");
    }

    /**
     * 去修改 课程班级关联
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/systemCourseDept/update/{courseId}")
    @RequiresPermissions(value = {"system:course:update"})
    @ControllerEndpoint(exceptionMessage = "跳转修改分配班级页面失败")
    public String systemCourseDeptUpdate(Model model ,@PathVariable Integer courseId) {
        List<Course> courseList = courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, FebsConstant.USE));
        List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>()
                .lambda()
                .eq(CourseDept::getCourseId, courseId));
        List<Integer> collect = courseDeptList.stream().map(m -> m.getDeptId()).collect(Collectors.toList());
        Course course = courseService.getOne(new QueryWrapper<Course>()
                .lambda()
                .eq(Course::getCourseId,courseId)
                .select(Course::getCourseId,Course::getCourseName));
        model.addAttribute("courseId",courseId);
        model.addAttribute("courseName",course.getCourseName());
        model.addAttribute("courseList",courseList);
        model.addAttribute("defaultDept",collect);
        return FebsUtil.view("system/courseDept/courseDeptUpdate");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "system/course/update/{courseId}")
    @RequiresPermissions("course:update")
    public String systemCourseUpdate(@PathVariable Integer courseId,Model model) {
        Course course = courseService.getById(courseId);
        model.addAttribute("course",course);
        return FebsUtil.view("system/course/courseUpdate");
    }

    /**
     * courseDept的编辑页面
     * @ Param courseId 课程id
     * @ Param model Model
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/courseDept/update/{courseId}")
    public String systemCourseDeptUpdate(@PathVariable Integer courseId,Model model) {
        model.addAttribute("courseId",courseId);
        List<CourseDept> courseDeptList = courseDeptService.list(new QueryWrapper<CourseDept>().lambda()
                .eq(CourseDept::getCourseId, courseId));
        List<Integer> deptIdList = courseDeptList.stream().map(m -> m.getDeptId()).collect(Collectors.toList());
        model.addAttribute("deptIdList",deptIdList);
        List<Course> courseList = courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, FebsConstant.USE));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/courseDept/courseDeptAdd");
    }

    @RequestMapping(FebsConstant.VIEW_PREFIX+"turnToQuestionUpdate/{questionId}")
    @RequiresPermissions("question:update")
    public String turnToUpdate(@PathVariable Integer questionId,Model model){
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);

        ExperimentQuestion question = questionService.getById(questionId);
        String answer = question.getAnswer();
        if (answer.equals("对")){
            answer = "A";
        }else if(answer.equals("错")){
            answer = "B";
        }
        question.setAnswer(answer);
        ExperimentProject experimentProject = iExperimentProjectService.getById(question.getProjectId());
        question.setProjectName(experimentProject.getProjectName());
        String context = question.getContext();
        String[] split = context.split("@@");
        List<String> contextList = new ArrayList<String>();
        if (question.getQuestionType()!=null){
            for (String s : split) {
                contextList.add(s);
            }
        }

        question.setContextList(contextList);
        model.addAttribute("question",question);
        return FebsUtil.view("system/question/updateQuestion");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/course/view/{courseId}")
    @RequiresPermissions("course:update")
    public String systemCourseView(@PathVariable Integer courseId,Model model) {
        Course course = courseService.getById(courseId);
        String s = DateUtil.formatFullTime(course.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN);
        course.setCreateTimes(s);
        model.addAttribute("course",course);
        return FebsUtil.view("system/course/courseView");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/experiment")
    @RequiresPermissions("experiment:view")
    public String systemExperiment() {
        return FebsUtil.view("system/experiment/experiment");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/experiment/add")
    @RequiresPermissions("experiment:add")
    public String systemExperimentAdd() {
        return FebsUtil.view("system/experiment/experimentAdd");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/experiment/project")
    @RequiresPermissions("experimentProject:view")
    public String systemExperimentProject(Model model) {
        List<Course> list = courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, 1).select(Course::getCourseId, Course::getCourseName));
        model.addAttribute("courseList",list);
        return FebsUtil.view("system/experiment/project/project");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/experiment/project/add")
    @RequiresPermissions("experimentProject:add")
    public String systemExperimentProjectAdd(Model model) {
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/experiment/project/projectAdd");
    }

    /**
     * GuiYongkang
     * 2020/4/01 14:19pm
     * 项目管理 实验项目模块中  修改单个实验项目详情页面的跳转
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/experiment/project/update/{projectId}")
    @RequiresPermissions("experimentProject:update")
    public String systemExperimentProjectUpdate(@PathVariable Long projectId,Model model) {
        ExperimentProject project = iExperimentProjectService.getById(projectId);
        model.addAttribute("project",project);
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/experiment/project/projectUpdate");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/experiment/project/reportSize/{projectId}")
    public String systemExperimentProjectReportSize(@PathVariable Long projectId,Model model) {
        ExperimentProject project = iExperimentProjectService.getOne(new QueryWrapper<ExperimentProject>()
        .lambda()
        .eq(ExperimentProject::getProjectId,projectId)
        .select(ExperimentProject::getProjectId,ExperimentProject::getReportSize));
        model.addAttribute("project",project);
        return FebsUtil.view("system/experiment/project/reportSize");
    }


    /**
     * 实验视频模块
     * @return
     */
    @RequestMapping(FebsConstant.VIEW_PREFIX+"system/video/list")
    @RequiresPermissions("video:list")
    public String toProjectVideoList(){
        return FebsUtil.view("system/experiment/video/video");
    }
    
    /**
     * @ Author wangke
     * @ Description 跳转到添加视频模块
     * @ Date 15:55 2021/2/3
     * @ Param 
     * @ return 
     */
    @RequestMapping(FebsConstant.VIEW_PREFIX+"system/video/add")
    @RequiresPermissions("video:add")
    public String toProjectVideoAdd(Model model){
        List<ExperimentProject> list = iExperimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getCourseId, 100002)
                .select(ExperimentProject::getProjectId
                        , ExperimentProject::getProjectName
                        , ExperimentProject::getProjectSort));
        List<ProjectSelect> collect = list.stream().sorted(Comparator.comparing(ExperimentProject::getProjectSort)).map(s -> {
            ProjectSelect projectSelect = new ProjectSelect();
            projectSelect.setValue(s.getProjectId());
            projectSelect.setName(s.getProjectName());
            return projectSelect;
        }).collect(Collectors.toList());
        model.addAttribute("data",collect);
        return FebsUtil.view("system/experiment/video/videoAdd");
    }

    /**
     * @ Author wangke
     * @ Description 跳转到修改视频模块
     * @ Date 15:59 2021/2/3
     * @ Param
     * @ return
     */
    @RequestMapping(FebsConstant.VIEW_PREFIX+"system/video/update/{videoId}")
    @RequiresPermissions("video:update")
    public String toProjectUpdate(@PathVariable Integer videoId,Model model){
        Video byId = videoService.getById(videoId);
        List<ExperimentProject> list = iExperimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getCourseId, 100002)
                .select(ExperimentProject::getProjectId
                        , ExperimentProject::getProjectName
                        , ExperimentProject::getProjectSort));
        List<ProjectSelect> collect = list.stream().sorted(Comparator.comparing(ExperimentProject::getProjectSort)).map(s -> {
            ProjectSelect projectSelect = new ProjectSelect();
            projectSelect.setValue(s.getProjectId());
            projectSelect.setName(s.getProjectName());
            if (s.getProjectId() == byId.getProjectId().longValue()) {
                projectSelect.setSelected(true);
            }
            return projectSelect;
        }).collect(Collectors.toList());
        model.addAttribute("data",collect);
        model.addAttribute("video",byId);
        return FebsUtil.view("system/experiment/video/videoUpdate");
    }
    
    

    @GetMapping(FebsConstant.VIEW_PREFIX+"hstView")
    public String hstView(){
        return FebsUtil.view("hst/view");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/think/question")
    public String exercises(){
        return FebsUtil.view("system/question/question");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/think/addQuestion")
    public String addQuestion(Model model){
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/question/addQuestion");
    }

    @RequiresPermissions("feedback:view")
    @RequestMapping(FebsConstant.VIEW_PREFIX+"system/feedback")
    public String turnToFeedBack(){
        return FebsUtil.view("system/feedback/feedback");
    }

    /**
     * 去分配班级页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toCourseDept")
    @RequiresPermissions("system:coure:dept")
    public String toCourseDept(){
        return FebsUtil.view("system/courseDept/course_dept");
    }

    /**
     * 去班级的修改页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toUpdateCourseDept/{deptId}")
    @RequiresPermissions("newDept:update")
    @ControllerEndpoint(exceptionMessage = "跳转到new_dept修改页面失败")
    public String toUpdateCourseDept(Model model,@PathVariable Integer deptId){
        Dept dept = deptService.getById(deptId);
        model.addAttribute("deptId",dept.getDeptId());
        model.addAttribute("deptName",dept.getDeptName());
        model.addAttribute("order",dept.getOrderNum());
        return FebsUtil.view("system/dept_new/deptUpdate2");
    }

    /**
     * 去分配班级课程页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toSetDeptCourse/{deptId}")
    @RequiresPermissions("newDept:update")
    @ControllerEndpoint(exceptionMessage = "跳转到分配班级课程页面失败")
    public String toSetDeptCourse(Model model,@PathVariable Integer deptId){
        List<CourseDept> list = courseDeptService.list(new QueryWrapper<CourseDept>()
                .lambda()
                .eq(CourseDept::getDeptId, deptId));
        List<Course> courseList = courseService.list(new QueryWrapper<Course>()
                .lambda()
                .select(Course::getCourseName, Course::getCourseId));
        List<CourseSelect> collect = courseList.stream().map(course -> {
            CourseSelect courseSelect = new CourseSelect();
            courseSelect.setName(course.getCourseName());
            courseSelect.setValue(course.getCourseId().longValue());
            courseSelect.setSelected(false);
            return courseSelect;
        }).collect(Collectors.toList());

        for (CourseSelect courseSelect : collect) {
            for (CourseDept courseDept : list) {
                if (courseSelect.getValue().intValue()==courseDept.getCourseId()){
                    courseSelect.setSelected(true);
                }
            }
        }

        model.addAttribute("data",collect);
        model.addAttribute("deptId",deptId);
        return FebsUtil.view("system/dept_new/setDeptCourse");
    }

    @RequestMapping(FebsConstant.VIEW_PREFIX+"toSystemConfigView")
    @RequiresPermissions("systemConfig:view")
    @ControllerEndpoint(exceptionMessage = "获取系统配置列表失败",operation = "获取系统配置列表成功")
    public String toSystemConfigView(){
        return FebsUtil.view("system/sysconfig/sysconfig");
    }

    @RequestMapping(FebsConstant.VIEW_PREFIX+"system/sysconfig/addSysConfig")
    @RequiresPermissions("systemConfig:add")
    @ControllerEndpoint(exceptionMessage = "跳转新增系统配置页面失败",operation = "跳转新增系统配置页面成功")
    public String toAddSysConfig(Model model){
        List<SysConfig> list = sysConfigService.list();
        list.forEach(i->{
            model.addAttribute(i.getConfigKey(),i.getConfigValue());
        });
        return FebsUtil.view("system/sysconfig/addSystemConfig");
    }

    @RequestMapping(FebsConstant.VIEW_PREFIX+"turnToSystemConfigUpdate/{systemConfigId}")
    @RequiresPermissions("systemConfig:update")
    @ControllerEndpoint(exceptionMessage = "跳转修改系统配置页面失败",operation = "跳转修改系统配置页面成功")
    public String toAddSysConfig(@PathVariable Integer systemConfigId,Model model){
        SysConfig sysConfig = sysConfigService.getById(systemConfigId);
        model.addAttribute("sysConfig",sysConfig);
        return FebsUtil.view("system/sysconfig/updateSysconfig");
    }

    /**
    * @ Description: Excel 的导出返回结果
    * @ Param: []
    * @ return: java.lang.String
    * @ Author: 马超伟
    * @ Date: 2020/6/9
    */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/eximport/result")
    public String eximportResult() {
        return FebsUtil.view("system/user/eximportResult");
    }

    /**
     * 去老师界面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toUserTeacher")
    public String toUserTeacher() {
        return FebsUtil.view("system/user/userTeacher/userTeacher");
    }

    /**
     * 去课程管理员界面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toCourseManager")
    public String toCourseManager() {
        return FebsUtil.view("system/user/courseManager/userCourse");
    }




    /**
     * 去管理员设置课程界面
     * @ Param userId 用户id
     * @ Param model Model
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toManagerSetCourse/{userId}")
    public String toManagerSetCourse(@PathVariable Integer userId,Model model){
        model.addAttribute("userId",userId);
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/user/courseManager/setCourse");
    }

    /**
     * 去设置课程界面
     * @ Param userId 用户id
     * @ Param model Model
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/toUserTeacherSetCourse/{userId}")
    public String toUserTeacherSetCourse(@PathVariable Integer userId,Model model){
        model.addAttribute("userId",userId);
        List<Course> courseList = this.courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        model.addAttribute("courseList",courseList);
        return FebsUtil.view("system/user/userTeacher/setCourse");
    }



    /**
     * 去帮助文档添加页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/help/add")
    public String toHelpListAdd(Model model){
        List<Course> list = courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, 1).select(Course::getCourseId, Course::getCourseName));
        model.addAttribute("courseList",list);
        return FebsUtil.view("system/help/helpAdd");
    }

    /**
     * 去帮助文档编辑页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/help/update/{helpId}")
    public String toHelpListUpdate(@PathVariable Integer helpId,Model model){
        System.out.println("helpID"+helpId);
        Help byId = helpService.getById(helpId);
        model.addAttribute("help",byId);
        return FebsUtil.view("system/help/helpUpdate");
    }

    /**
     * 查看帮助内容
     * @ Param helpId 帮助id
     * @ Param model Model
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/help/view/{helpId}")
    public String toHelpView(@PathVariable Integer helpId,Model model){
        Help byId = helpService.getById(helpId);
        model.addAttribute("help",byId);
        return FebsUtil.view("system/help/helpView");
    }

    /**
     * 去帮助页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/help/help")
    public String toHelpList(Model model){
        List<Course> list = courseService.list(new QueryWrapper<Course>().lambda().eq(Course::getState, 1).select(Course::getCourseId, Course::getCourseName));
        model.addAttribute("courseList",list);
        return FebsUtil.view("system/help/help");
    }


    /**
     * 查看班级下的详细信息
     * @ Param deptId 班级id
     * @ Param model Model
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/deptDetailView/{deptId}")
    @RequiresPermissions("newDept:create")
    public String deptDetailView(@PathVariable Integer deptId,Model model) {
        List<UserDept> deptDetailInfo = deptService.findDeptDetailInfo(deptId);
        model.addAttribute("deptDetailInfo",deptDetailInfo);
        return FebsUtil.view("system/dept_new/deptDetailView");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/userTeacher/add")
    public String addUserTeacher(){
        return FebsUtil.view("system/user/userTeacher/userTeacherAdd");
    }

    /**
     * 去数电管理界面
     * @ Param model Model
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/detMainboard")
    @RequiresPermissions("det:view")
    public String toDetMainboardList(Model model){
        return FebsUtil.view("system/mainboard/det/det");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/updateDet/{detId}")
    @RequiresPermissions("det:update")
    public String toUpdateDetMainboard(@PathVariable Integer detId,Model model){
        DetBase det = detBaseService.getById(detId);
        model.addAttribute("det",det);
        return FebsUtil.view("system/mainboard/det/updateDet");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/addDet")
    @RequiresPermissions("det:add")
    public String toAddDetMainboard(){
        return FebsUtil.view("system/mainboard/det/addDet");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/detRecord")
    @RequiresPermissions("det:view")
    public String toDetRecord(){
        return FebsUtil.view("system/mainboard/record/record");
    }

    /**
     * @Author wangke
     * @ Description 去模电管理界面
     * @Date 17:04 2020/10/13
     * @ Param
     * @return 
     */

    @GetMapping(FebsConstant.VIEW_PREFIX + "system/analogList")
    @RequiresPermissions("analog:view")
    public String toAnalogList(Model model){
        return FebsUtil.view("system/mainboard/analog/analog");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/updateAnalog/{analogId}")
    @RequiresPermissions("analog:update")
    public String toUpdateAnalog(@PathVariable Integer analogId,Model model){
        AnalogBase byId = analogBaseService.getById(analogId);
        List<ExperimentProject> list = iExperimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getCourseId, 100002)
                .select(ExperimentProject::getProjectId,
                        ExperimentProject::getProjectName,
                        ExperimentProject::getProjectSort));
        List<ProjectSelect> collect = list.stream().sorted(Comparator.comparing(ExperimentProject::getProjectSort)).map(s -> {
            ProjectSelect projectSelect = new ProjectSelect();
            projectSelect.setValue(s.getProjectId());
            projectSelect.setName(s.getProjectName());
            if (byId.getProjectId().contains(s.getProjectId().toString())){
                projectSelect.setSelected(true);
            }
            return projectSelect;
        }).collect(Collectors.toList());


        model.addAttribute("analog",byId);
        model.addAttribute("data",collect);
        return FebsUtil.view("system/mainboard/analog/updateAnalog");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/addAnalog")
    @RequiresPermissions("analog:add")
    public String toAddAnalog(Model model){
        List<ExperimentProject> list = iExperimentProjectService.list(new QueryWrapper<ExperimentProject>()
                .lambda()
                .eq(ExperimentProject::getCourseId, 100002)
                .select(ExperimentProject::getProjectId,
                        ExperimentProject::getProjectName,
                        ExperimentProject::getProjectSort));
        List<ProjectSelect> collect = list.stream().sorted(Comparator.comparing(ExperimentProject::getProjectSort)).map(s -> {
            ProjectSelect projectSelect = new ProjectSelect();
            projectSelect.setValue(s.getProjectId());
            projectSelect.setName(s.getProjectName());
            return projectSelect;
        }).collect(Collectors.toList());
        model.addAttribute("data",collect);

        return FebsUtil.view("system/mainboard/analog/addAnalog");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/analogRecord")
    @RequiresPermissions("analog:delete")
    public String toDetAnalog(){
        return FebsUtil.view("system/mainboard/analogRecord/analogRecord");
    }
    /**
     * @Author wangke
     * @ Description 跳转到电气控制列表
     * @Date 10:32 2020/12/5
     * @ Param
     * @return 
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/electricControlList")
    @RequiresPermissions("electricControl:view")
    public String toElectricControlList(){
        return FebsUtil.view("system/mainboard/electricControl/eleControl");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/updateElectricControl/{ElectricControlId}")
    @RequiresPermissions("ElectricControl:update")
    public String toUpdateElectricControl(@PathVariable Integer ElectricControlId,Model model){
        ElectricControl byId = electricControlService.getById(ElectricControlId);
        model.addAttribute("electricControl",byId);
        return FebsUtil.view("system/mainboard/electricControl/updateElectricControl");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX+"system/addElectricControl")
    @RequiresPermissions("ElectricControl:add")
    public String toAddElectricControl(){
        return FebsUtil.view("system/mainboard/electricControl/addElectricControl");
    }
    
    /**
     * @Author wangke
     * @ Description 跳转到电气操作记录界面
     * @Date 11:55 2020/12/5
     * @ Param
     * @return 
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/electricRecord")
    @RequiresPermissions("electricRecord:view")
    public String toElectricRecord(){
        return FebsUtil.view("system/mainboard/electricRecord/electricRecord");
    }


    /**
     * @Author wangke
     * @ Description 跳转主题列表页面
     * @Date 10:17 2020/10/23
     * @ Param
     * @return 
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "system/topic")
    @RequiresPermissions("topic:view")
    public String toTopicList(Model model){
        return FebsUtil.view("system/topic/topic");
    }
    
    /**
     * @Author wangke
     * @ Description 跳转至主题修改页面
     * @Date 10:17 2020/10/23
     * @ Param
     * @return 
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/updateTopic/{tid}")
    @RequiresPermissions("topic:update")
    public String toUpdateTopic(@PathVariable Integer tid,Model model){
        Topic byId = topicService.getById(tid);
        List<Course> list = courseService.list(new QueryWrapper<Course>()
                .lambda()
                .select(Course::getCourseId, Course::getCourseName));
        model.addAttribute("topic",byId);
        model.addAttribute("courseList",list);
        return FebsUtil.view("system/topic/updateTopic");
    }

    /**
     * @Author wangke
     * @ Description 跳转至添加主题页面
     * @Date 10:18 2020/10/23
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/addTopic")
    @RequiresPermissions("topic:add")
    public String toAddTopic(Model model){
        List<Course> list = courseService.list(new QueryWrapper<Course>()
                .lambda()
                .eq(Course::getState,1)
                .select(Course::getCourseId, Course::getCourseName));
        model.addAttribute("courseList",list);
        return FebsUtil.view("system/topic/addTopic");
    }
    
    /**
     * @Author wangke
     * @ Description 跳转到订单界面
     * @Date 14:19 2020/11/30
     * @ Param
     * @return 
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/order")
    @RequiresPermissions("order:view")
    public String toOrderList(){
        return FebsUtil.view("system/order/orderList");
    }
    /**
     * @Author wangke
     * @ Description 去分类列表界面
     * @Date 10:50 2020/11/17
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/category")
    @RequiresPermissions("category:view")
    public String toCategory(Model model)
    {
        List<Category> list = categoryService.list(new QueryWrapper<Category>()
                .lambda());
        model.addAttribute("categoryList",list);
        return  FebsUtil.view("system/category/categoryList");
    }

    /**
     * @Author wangke
     * @ Description 添加分类
     * @Date 10:50 2020/11/17
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/addCategory")
    @RequiresPermissions("category:add")
    public String addCategory(){
        return FebsUtil.view("system/category/addCategory");
    }

    /**
     * @Author wangke
     * @ Description 修改分类
     * @Date 10:55 2020/11/17
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/updateCategory/{categoryId}")
    @RequiresPermissions("article:update")
    public String updateCategory(@PathVariable Integer categoryId,Model model){
        Category byId = categoryService.getById(categoryId);
        model.addAttribute("category",byId);
        return FebsUtil.view("system/category/updateCategory");
    }

    /**
     * @Author wangke
     * @ Description 去文章列表界面
     * @Date 10:50 2020/11/17
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/article")
    @RequiresPermissions("article:view")
    public String toArticle(Model model){
        List<Category> list = categoryService.list();
        model.addAttribute("categoryList",list);
        return  FebsUtil.view("system/article/articleList");
    }

    /**
     * @Author wangke
     * @ Description 添加文章资讯
     * @Date 10:50 2020/11/17
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/addArticle")
    @RequiresPermissions("article:add")
    public String addArticle(Model model){
        List<Category> list = categoryService.list(new QueryWrapper<Category>()
                .lambda());
        model.addAttribute("categoryList",list);
        return FebsUtil.view("system/article/addArticle");
    }

    /**
     * @Author wangke
     * @ Description 修改文章
     * @Date 10:55 2020/11/17
     * @ Param
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/updateArticle/{articleId}")
    @RequiresPermissions("article:update")
    public String updateArticle(@PathVariable Integer articleId,Model model){
        Article byId = articleService.getById(articleId);
        List<Category> list = categoryService.list(new QueryWrapper<Category>()
                .lambda());
        model.addAttribute("article",byId);
        model.addAttribute("categoryList",list);
        return FebsUtil.view("system/article/updateArticle");
    }
    
    /**
     * @Author wangke
     * @Description 跳转至图像管理界面 
     * @Date 15:38 2020/12/30
     * @Param 
     * @return 
     */
    @GetMapping(FebsConstant.VIEW_PREFIX+"system/screenList")
    @RequiresPermissions("screen:view")
    public String toScreen(){
        return FebsUtil.view("system/screen/screenList");
    }



}
