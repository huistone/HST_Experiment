package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.AnalogRecord;
import cc.mrbird.febs.system.entity.DetRecord;
import cc.mrbird.febs.system.service.IAnalogRecordService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-10-13
 */
@RestController
@RequestMapping("/analogRecord")
public class AnalogRecordController extends BaseController {

    @Resource
    private IAnalogRecordService analogRecordService;

    @RequestMapping("list")
    @ControllerEndpoint(exceptionMessage = "获取主板操作记录失败",operation = "获取主板操作记录成功")
    public FebsResponse list(AnalogRecord analogRecord, QueryRequest request){
        IPage<AnalogRecord> list = analogRecordService.selectAnalogRecordByPage(analogRecord, request);
        return new FebsResponse().data(getDataTable(list)).success();
    }

    @RequestMapping("/delete/{analogIds}")
    @RequiresPermissions("analog:delete")
    @ControllerEndpoint(exceptionMessage = "主板记录删除失败", operation = "主板记录删除成功")
    public FebsResponse delete(@PathVariable String analogIds){
        try {
            String[] split = analogIds.split(StringPool.COMMA);
            List<String> ids = Arrays.asList(split);
            for (String id : ids) {
                analogRecordService.removeById(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail().data("系统繁忙");
        }
        return new FebsResponse().success();
    }
}
