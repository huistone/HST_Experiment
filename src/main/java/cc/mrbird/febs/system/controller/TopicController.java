package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.external.mqtt.MQTTSubscribe;
import cc.mrbird.febs.system.entity.AnalogBase;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.Topic;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.ITopicService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * MQTT主题记录表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-10-23
 */
@RestController
@RequestMapping("/topic")
public class TopicController extends BaseController {

    @Resource
    private ITopicService topicService;

    @Resource
    private ICourseService courseService;

    @Resource
    private MQTTSubscribe mqttSubscribe;

    @RequestMapping("list")
    @RequiresPermissions("topic:view")
    @ControllerEndpoint(operation = "查询主题列表",exceptionMessage = "查询主题列表失败")
    public FebsResponse selectTopicList(Topic topic,QueryRequest request){
        IPage<Topic> page = new Page<>(request.getPageNum(),request.getPageSize());
        IPage<Topic> page1 = topicService.page(page, new QueryWrapper<Topic>()
                .lambda()
                .like(topic.getName()!=null,Topic::getName,topic.getName()));
        List<Topic> records = page1.getRecords();
        records.forEach(item->{
            String courseName = courseService.getOne(new QueryWrapper<Course>()
                    .lambda()
                    .eq(Course::getCourseId, item.getCourseId())
                    .select(Course::getCourseName)).getCourseName();
            item.setCourseName(courseName);
        });
        return new FebsResponse().data(getDataTable(page1)).success();
    }

    @RequestMapping("/add")
    @RequiresPermissions("topic:add")
    @ControllerEndpoint(operation = "添加主题",exceptionMessage = "添加主题失败")
    public FebsResponse add(Topic topic){
        try {
            //订阅主题
            if (1==topic.getStatus() && 1==topic.getType()){
                mqttSubscribe.subscribe(topic.getName(),2);
            }
            topicService.save(topic);
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

    @PostMapping("/update")
    @RequiresPermissions("topic:update")
    @ControllerEndpoint(exceptionMessage = "主题更新失败", operation = "主题更新成功")
    public FebsResponse update(Topic topic){
        try {
            //订阅主题
            if (1==topic.getStatus() && 1==topic.getType()){
                mqttSubscribe.subscribe(topic.getName(),2);
            }
            topicService.updateById(topic);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @PostMapping("/updateByStatus")
    @RequiresPermissions("topic:update")
    @ControllerEndpoint(exceptionMessage = "主题更新失败", operation = "主题更新成功")
    public FebsResponse updateByStatus(Topic topic){
        try {
            //订阅主题
            Topic currentTopic = topicService.getById(topic.getTid());
            if (1==topic.getStatus() && 1==currentTopic.getType()){
                mqttSubscribe.subscribe(currentTopic.getName(),2);
            }
            topicService.updateById(topic);
            return new FebsResponse().success();
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
    }

    @RequestMapping("/delete/{tids}")
    @RequiresPermissions("topic:delete")
    @ControllerEndpoint(exceptionMessage = "主题删除失败", operation = "主题删除成功")
    public FebsResponse delete(@PathVariable String tids){
        try {
            String[] split = tids.split(StringPool.COMMA);
            List<String> list = Arrays.asList(split);
            for (String s : list) {
                topicService.remove(new QueryWrapper<Topic>().lambda().eq(Topic::getTid,s));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new FebsResponse().fail();
        }
        return new FebsResponse().success();
    }

}
