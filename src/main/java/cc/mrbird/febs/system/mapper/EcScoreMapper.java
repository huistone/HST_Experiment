package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.EcScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-25
 */
public interface EcScoreMapper extends BaseMapper<EcScore> {

}
