package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分类表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
