package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 马超伟
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
