package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-05-25
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {


    void updateByConfigName(@ Param("key") String keyName,@ Param("value") String value);
}
