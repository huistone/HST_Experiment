package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 意见反馈表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-05-08
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

}
