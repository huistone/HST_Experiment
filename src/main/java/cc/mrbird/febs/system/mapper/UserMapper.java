package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.vo.CascaderVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 马超伟
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 通过用户名查找用户
     *
     * @ Param username 用户名
     * @return 用户
     */
    User findByName(String username);

    /**
     * 查找用户详细信息
     *
     * @ Param page 分页对象
     * @ Param user 用户对象，用于传递查询条件
     * @return Ipage
     */
    IPage<User> findUserDetailPage(Page<User> page, @ Param("user") User user);

    /**
     * 查找用户详细信息,排除老师角色
     *
     * @ Param page 分页对象
     * @ Param user 用户对象，用于传递查询条件
     * @return Ipage
     */
    IPage<User> findUserDetailPageExcludeTeacher(Page<User> page, @ Param("user") User user, @ Param("levels")Integer level);

    /**
     * 查找所有老师详细信息
     *
     * @ Param page 分页对象
     * @ Param user 用户对象，用于传递查询条件
     * @return Ipage
     */
    IPage<User> findTeacherPage(Page page, @ Param("user") User user);

    /**
     * 查找用户详细信息
     *
     * @ Param user 用户对象，用于传递查询条件
     * @return List<User>
     */
    List<User> findUserDetail(@ Param("user") User user);

    /**
     * 根据部门ID，查询对应的学生信息
     * @ Param deptId
     * @ Param user
     * @return
     */
    IPage<User> findStudentByDeptIdList(Page<User> page, @ Param("deptId") Integer deptId,@ Param("user") User user);

    /**
     * 查找学生信息
     * @ Param page
     * @ Param user
     * @return
     */
    IPage<User> findStudentPage(Page<User> page, @ Param("user") User user);

    /**
     * 根据班级ID查询班级学生集合
     * @ Param deptId
     * @return
     */
    List<User> getStudentByDeptId(Long deptId);

    /**
     * 导出老师信息
     * @return
     */
    List<User> findUserTeacher();

    /**
     * 导出管理员信息
     * @return
     */
    List<User> findUserManager();


//    List<User> selectStudentScoreInfo(Integer pageNum, Integer pageSize, int deptId, int projectId);

    /**
     * 学生信息成绩复杂分页查询
     * @ Param deptId
     * @ Param projectId
     * @ Param pageNum
     * @ Param pageSize
     * @return
     */
    List<User> selectStudentScoreInfoByPage(@ Param("deptId") Long deptId,
                                            @ Param("projectId") Long projectId,
                                            @ Param("pageNum") Integer pageNum,
                                            @ Param("pageSize") Integer pageSize);

    /**
     * 学生信息成绩分页查询数量
     * @ Param deptId
     * @ Param projectId
     * @return
     */
    Integer selectStudentScoreCount(@ Param("deptId") Long deptId,
                                    @ Param("projectId") Long projectId);

    IPage<User> findCustomerPage(Page<User> page, User user);

    List<User> findUserCustomerExcelInfo();

    IPage<User> findCourseManagerPage(Page<User> page, User user);

    List<User> findCourseManagerExcelInfo();
}
