package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 实验题目 评语表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-07-01
 */
public interface ExperimentRemarkMapper extends BaseMapper<ExperimentRemark> {

    /**
    * @ Description: 查询该班级，和项目下的所有学生的提交情况
    * @ Param: [queryRequest, experimentRemark]
    * @ return: java.util.List<cc.mrbird.febs.system.entity.ExperimentRemark>
    * @ Author: 马超伟
    * @ Date: 2020/7/3
    */
    List<ExperimentRemark> listCommit(@ Param("pageNum") Integer pageNum,
                                      @ Param("pageSize") Integer pageSize,
                                      @ Param("type")Integer type,
                                      @ Param("projectId")Long projectId,
                                      @ Param("deptId")Long deptId);

    Integer listCommitCount(@ Param("pageNum") Integer pageNum,
                                      @ Param("pageSize") Integer pageSize,
                                      @ Param("type")Integer type,
                                      @ Param("projectId")Long projectId,
                                      @ Param("deptId")Long deptId);

    List<Double> selectRemarkScore(@ Param("userId") Long userId,@ Param("deptId") Long deptId,@ Param("ids") List list);

    ExperimentRemark selectRemark(@ Param("stuId") Long stuId,@ Param("projectId") Long projectId,@ Param("type") Integer type);
}
