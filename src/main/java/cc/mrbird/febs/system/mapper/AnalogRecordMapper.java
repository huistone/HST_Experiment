package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.AnalogRecord;
import cc.mrbird.febs.system.entity.DetRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-10-13
 */
public interface AnalogRecordMapper extends BaseMapper<AnalogRecord> {

    /**
     * @Author wangke
     * @ Description 查询模电操作记录分页条件查询
     * @Date 18:16 2020/10/13
     * @ Param
     * @return 
     */
    IPage<AnalogRecord> selectAnalogRecordByPage(Page<AnalogRecord> page, AnalogRecord analogRecord);

    /**
     * @Author wangke
     * @ Description 查询模电实验操作记录个数
     * @Date 18:30 2020/10/13
     * @ Param
     * @return 
     */
    Integer selectAnalogRecordCount(AnalogRecord analogRecord);
}
