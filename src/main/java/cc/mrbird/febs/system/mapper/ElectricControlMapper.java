package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.ElectricControl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-12-05
 */
public interface ElectricControlMapper extends BaseMapper<ElectricControl> {

}
