package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Collect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收藏表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface CollectMapper extends BaseMapper<Collect> {

}
