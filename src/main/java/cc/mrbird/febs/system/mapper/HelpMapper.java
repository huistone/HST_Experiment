package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Help;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-30
 */
public interface HelpMapper extends BaseMapper<Help> {

    /**
     * 获取帮助列表
     */
    List<Help> getHelpList();
}
