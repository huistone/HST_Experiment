package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Screen;
import cc.mrbird.febs.system.entity.vo.ScreenVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 截屏记录保持表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-12-30
 */
public interface ScreenMapper extends BaseMapper<Screen> {

    IPage<ScreenVo> selectListByCondition(@Param("page") Page page, @Param("screenVo") ScreenVo screenVo);
}
