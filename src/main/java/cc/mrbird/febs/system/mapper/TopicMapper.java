package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Topic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * MQTT主题记录表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-10-23
 */
public interface TopicMapper extends BaseMapper<Topic> {

}
