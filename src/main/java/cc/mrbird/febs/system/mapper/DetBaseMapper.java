package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.DetBase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 数电主板信息表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
public interface DetBaseMapper extends BaseMapper<DetBase> {

}
