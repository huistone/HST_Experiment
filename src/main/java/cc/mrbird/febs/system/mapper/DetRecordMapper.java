package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.DetRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 数电主板使用记录表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
public interface DetRecordMapper extends BaseMapper<DetRecord> {
//    ,
//   @ Param("pageNum") int pageNum,
//                                 @ Param("pageSize") int pageSize ,
    IPage<DetRecord> selectDetRecordByPage( Page<DetRecord> page,@ Param("detRecord") DetRecord detRecord);

    Integer selectDeRecordCount(@ Param("detRecord") DetRecord detRecord);
}
