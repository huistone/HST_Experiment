package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author 马超伟
 */
public interface DeptMapper extends BaseMapper<Dept> {

    /**
     * @ Description: 查询element的 班级 级联选择器
     * @ Param: []
     * @ return: java.util.List<cc.mrbird.febs.api.user.deptApiController.ElementCascader>
     * @ Author: 马超伟
     * @ Date: 2020/5/18
     */
    List<ElementCascader> getCascader();

}
