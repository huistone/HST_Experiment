package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.ExperimentProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 实验项目表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-25
 */
public interface ExperimentProjectMapper extends BaseMapper<ExperimentProject> {


}
