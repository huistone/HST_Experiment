package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.DetReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
public interface DetReportMapper extends BaseMapper<DetReport> {

}
