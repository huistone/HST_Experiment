package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.UserDetreport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
public interface UserDetreportMapper extends BaseMapper<UserDetreport> {

}
