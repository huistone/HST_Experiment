package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.ArticleCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资讯分类关联表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
public interface ArticleCategoryMapper extends BaseMapper<ArticleCategory> {

}
