package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.UserDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
public interface UserDeptMapper extends BaseMapper<UserDept> {

}
