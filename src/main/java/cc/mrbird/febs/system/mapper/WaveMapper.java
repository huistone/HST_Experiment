package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Wave;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 波形数据集，自动学习 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
public interface WaveMapper extends BaseMapper<Wave> {

}
