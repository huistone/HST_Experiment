package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.UserCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员分配课程表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface UserCourseMapper extends BaseMapper<UserCourse> {

}
