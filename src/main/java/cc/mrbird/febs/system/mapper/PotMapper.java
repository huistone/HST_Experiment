package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Pot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * POT1 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-12-29
 */
public interface PotMapper extends BaseMapper<Pot> {

}
