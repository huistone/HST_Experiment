package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 马超伟
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
