package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.CourseStudent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程学生表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
public interface CourseStudentMapper extends BaseMapper<CourseStudent> {

}
