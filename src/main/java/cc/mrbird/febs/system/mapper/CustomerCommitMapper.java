package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.CustomerCommit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户报告提交记录表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface CustomerCommitMapper extends BaseMapper<CustomerCommit> {

}
