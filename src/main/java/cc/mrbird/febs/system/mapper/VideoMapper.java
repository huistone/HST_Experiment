package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
public interface VideoMapper extends BaseMapper<Video> {

}
