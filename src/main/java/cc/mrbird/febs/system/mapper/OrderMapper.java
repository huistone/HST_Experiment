package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-11-19
 */
public interface OrderMapper extends BaseMapper<Order> {

}
