package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.AnalogBase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-10-14
 */
public interface AnalogBaseMapper extends BaseMapper<AnalogBase> {

}
