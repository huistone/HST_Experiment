package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.Feedback;
import cc.mrbird.febs.system.mapper.FeedbackMapper;
import cc.mrbird.febs.system.service.IFeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 意见反馈表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-05-08
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {

    @Override
    @Transactional
    public void deleteFeedBacks(String[] feedbackIds) {
        List<String> strings = Arrays.asList(feedbackIds);
        this.removeByIds(strings);
    }
}
