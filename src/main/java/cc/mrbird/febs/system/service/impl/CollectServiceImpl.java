package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.Collect;
import cc.mrbird.febs.system.mapper.CollectMapper;
import cc.mrbird.febs.system.service.ICollectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 收藏表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class CollectServiceImpl extends ServiceImpl<CollectMapper, Collect> implements ICollectService {

}
