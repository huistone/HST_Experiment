package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.DetBase;
import cc.mrbird.febs.system.entity.Help;
import cc.mrbird.febs.system.mapper.DetBaseMapper;
import cc.mrbird.febs.system.service.IDetBaseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 数电主板信息表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class DetBaseServiceImpl extends ServiceImpl<DetBaseMapper, DetBase> implements IDetBaseService {

    @Override
    public IPage<DetBase> selectList(DetBase detBase, QueryRequest request) {
        Page<DetBase> detPage = new Page<>(request.getPageNum(),request.getPageSize());
       return this.baseMapper.selectPage(detPage,new QueryWrapper<DetBase>().lambda().like(StringUtils.isNotBlank(detBase.getDetName()),DetBase::getDetName,detBase.getDetName()));
    }
}
