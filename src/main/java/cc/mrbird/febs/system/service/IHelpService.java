package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Help;
import cc.mrbird.febs.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-30
 */
public interface IHelpService extends IService<Help> {

    List<Help> getHelpList();

    IPage<Help> findAll(Help help, QueryRequest queryRequest, User currentUser);

    void saveHelpInfo(Help help, User currentUser);

    void updateHelpInfo(Help help, User currentUser);

    void deleteByIds(String helpIds);
}
