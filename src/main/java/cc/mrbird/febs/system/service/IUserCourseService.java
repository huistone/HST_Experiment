package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.UserCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员分配课程表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface IUserCourseService extends IService<UserCourse> {

}
