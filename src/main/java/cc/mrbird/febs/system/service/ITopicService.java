package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Topic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * MQTT主题记录表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-10-23
 */
public interface ITopicService extends IService<Topic> {

}
