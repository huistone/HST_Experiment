package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.CourseStudent;
import cc.mrbird.febs.system.mapper.CourseStudentMapper;
import cc.mrbird.febs.system.service.ICourseStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 课程学生表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class CourseStudentServiceImpl extends ServiceImpl<CourseStudentMapper, CourseStudent> implements ICourseStudentService {

}
