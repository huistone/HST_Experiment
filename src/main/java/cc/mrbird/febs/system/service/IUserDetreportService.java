package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.UserDetreport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
public interface IUserDetreportService extends IService<UserDetreport> {

}
