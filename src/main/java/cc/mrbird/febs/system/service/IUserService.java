package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 马超伟
 */
public interface IUserService extends IService<User> {

    /**
     * 通过用户名查找用户
     *
     * @ Param username 用户名
     * @return 用户
     */
    User findByName(String username);

    /**
     * 查找用户详细信息
     *
     * @ Param request request
     * @ Param user    用户对象，用于传递查询条件
     * @return IPage
     */
    IPage<User> findUserDetailList(User user, QueryRequest request);

    /**
     * 查找用户详细信息，排除老师,学生角色
     *
     * @ Param request request
     * @ Param user    用户对象，用于传递查询条件
     * @return IPage
     */
    IPage<User> findUserDetailListExcludeTeacher(User user, QueryRequest request);

    /**
     * 查找老师详细信息
     *
     * @ Param request request
     * @ Param user    用户对象，用于传递查询条件
     * @return IPage
     */
    IPage<User> findUserTeacherDetailList(User user, QueryRequest request);

    /**
     * 导出教师信息excel
     * @return
     */
    List<User> findUserTeacherList();

    /**
     * 导出管理员信息excel
     * @return
     */
    List<User> findUserManagerList();

    /**
     * 查找学生详细信息
     * @ Param user 条件对象
     * @ Param request 分页对象
     * @return
     */
    IPage<User> findUserStudentDetailList(User user, QueryRequest request);

    /**
     * 查找所有老师详细信息
     *
     * @ Param request request
     * @ Param user    用户对象，用于传递查询条件
     * @return IPage
     */
    IPage<User> findTeacherList(User user, QueryRequest request);

    /**
     * 通过用户名查找用户详细信息
     *
     * @ Param username 用户名
     * @return 用户信息
     */
    User findUserDetailList(String username);


    /**
     * 更新用户登录时间
     *
     * @ Param username 用户名
     */
    void updateLoginTime(String username);

    /**
     * 新增用户
     *
     * @ Param user user
     */
    void createUser(User user);

    /**
     * 删除用户
     *
     * @ Param userIds 用户 id数组
     */
    void deleteUsers(String[] userIds);

    /**
     * 修改用户
     *
     * @ Param user user
     */
    void updateUser(User user);

    /**
     * 重置密码
     *
     * @ Param usernames 用户名数组
     */
    void resetPassword(String[] usernames);

    /**
     * 注册用户
     *
     * @ Param username 用户名
     * @ Param password 密码
     */
    void regist(String username, String password);

    /**
     * 修改密码
     *
     * @ Param username 用户名
     * @ Param password 新密码
     */
    void updatePassword(String username, String password);

    /**
     * 更新用户头像
     *
     * @ Param username 用户名
     * @ Param avatar   用户头像
     */
    void updateAvatar(String username, String avatar);

    /**
     * 修改用户系统配置（个性化配置）
     *
     * @ Param username 用户名称
     * @ Param theme    主题风格
     * @ Param isTab    是否开启 TAB
     */
    void updateTheme(String username, String theme, String isTab);

    /**
     * 更新个人信息
     * @ Param user 个人信息
     */
    void updateProfile(User user);


    /**
     * 获取当前登录用户
     */
    User getCurrentUser();

    /**
     * 根据班级ID查询班级学生集合
     * @ Param deptId 班级id
     * @return
     */
    List<User> getStudentByDeptId(Long deptId);

    /**
     * 根据班级ID查询学生信息
     * @ Param request 分页对象
     * @ Param deptId 班级id
     */
    IPage<User> selectStudentInfo(QueryRequest request, Long deptId,User teacher);

    /**
     * 查询学生名称，院系名称，专业名称，班级名称
     * @ Param userId 学生id
     */
    User selectSingleStudentInfo( Long userId);

    /**
     * 查询老师个人信息
     * @ Param currentUser 老师对象
     */
    User selectTeacherInfo(User currentUser);

    /**
     * 学生信息成绩复杂分页查询
     * @ Param deptId
     * @ Param projectId
     * @ Param pageNum
     * @ Param pageSize
     * @return
     */
    List<User> selectStudentScoreInfoByPage( Long deptId,
                                             Long projectId,
                                             Integer pageNum,
                                             Integer pageSize);

    /**
     * 学生信息成绩分页查询数量
     * @ Param deptId
     * @ Param projectId
     * @return
     */
    Integer selectStudentScoreCount( Long deptId,
                                     Long projectId);

    /**
     * 查找所有客户信息
     *
     * @ Param request request
     * @ Param user    用户对象，用于传递查询条件
     * @return IPage
     */
    IPage<?> findUserCustomerList(User user, QueryRequest request);

    /**
     * 查询客户信息用于excel导出
     * @return
     */
    List<User> findUserCustomerExcelInfo();

    IPage<?> findCourseManagerList(User user, QueryRequest request);

    List<User> findCourseManagerExcelInfo();

    void registMobileUser(String username, String password,String telephone);

    void addStudentForTeacher(User user);

    int updateStudentInfo(User user);
}
