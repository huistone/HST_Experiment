package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Role;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author 马超伟
 */
public interface IUserRoleService extends IService<UserRole> {

    /**
     * 通过角色 id 删除
     *
     * @ Param roleIds 角色 id
     */
    void deleteUserRolesByRoleId(List<String> roleIds);

    /**
     * 通过用户 id 删除
     *
     * @ Param userIds 用户 id
     */
    void deleteUserRolesByUserId(List<String> userIds);

    /**
     * 通过用户id删除关联班级
     * @ Param list
     */
    void deleteUserDeptByUserId(List<String> list);

    /**
     * 通过用户id 查询用户的角色信息
     * @ Param userId
     * @return
     */
    Role selectRoleByUser(Long userId);

}
