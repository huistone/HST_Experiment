package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.DeptTree;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.entity.vo.CascaderVO;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @author 马超伟
 */
public interface IDeptService extends IService<Dept> {

    /**
     * 获取部门树（下拉选使用）
     *
     * @return 部门树集合
     */
    List<DeptTree<Dept>> findDepts();

    /**
     * 获取部门列表（树形列表）
     *
     * @ Param dept 部门对象（传递查询参数）
     * @return 部门树
     */
    List<DeptTree<Dept>> findDepts(Dept dept);

    /**
     * 获取部门列表无限级联
     */
    List<ElementCascader> getCascader();

    /**
     * 获取部门树（供Excel导出）
     *
     * @ Param dept    部门对象（传递查询参数）
     * @ Param request QueryRequest
     * @return List<Dept>
     */
    List<Dept> findDepts(Dept dept, QueryRequest request);

    /**
     * 新增部门
     *
     * @ Param dept 部门对象
     */
    void createDept(Dept dept);

    /**
     * 修改部门
     *
     * @ Param dept 部门对象
     */
    void updateDept(Dept dept);

    /**
     * 删除部门
     *
     * @ Param deptIds 部门 ID集合
     */
    void deleteDepts(String[] deptIds);

    /**
     * 根据部门ID，查询对应的学生信息
     * @ Param deptId 班级id
     * @ Param user 条件对象
     * @ Param request 分页对象
     */
    IPage<User> findStudentByDeptIdList(Integer deptId, User user, QueryRequest request);


    /**
     * 获取当前班级下的详细信息
     * @ Param deptId 班级id
     */
    List<UserDept> findDeptDetailInfo(Integer deptId);
}
