package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.mapper.CourseMapper;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 课程表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private IDeptService deptService;

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public IPage<Map<String, Object>> findUserDetailList(Course course, QueryRequest request) {
        Page<Course> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "course_sort", FebsConstant.ORDER_ASC, false);
        IPage<Map<String, Object>> courseIPage = this.baseMapper.selectMapsPage(page, new QueryWrapper<Course>().lambda()
                .like(StringUtils.isNotBlank(course.getCourseName()), Course::getCourseName, course.getCourseName())
                .eq(course.getState() != null, Course::getState, course.getState()));
        List<Map<String, Object>> records = courseIPage.getRecords();
        for (Map<String, Object> record : records) {
            List<CourseDept> deptList = courseDeptService.list(new QueryWrapper<CourseDept>().lambda().eq(CourseDept::getCourseId, record.get("course_id")));
            if (deptList!=null){
                //获取所有班级id
                List<Integer> collect = deptList.stream().map(m -> m.getDeptId()).collect(Collectors.toList());
                if (collect!=null && collect.size()>0){
                    Collection<Dept> depts = deptService.listByIds(collect);
                    List<String> list = depts.stream().map(dept -> dept.getDeptName()).collect(Collectors.toList());
                    record.put("deptNameList",list);
                }
            }
        }
        return courseIPage;
    }

    @Override
    public List<Course> selectCourseProject(Long courseId) {
        return courseMapper.selectCourseProject(courseId);
    }
}
