package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Collect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收藏表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface ICollectService extends IService<Collect> {

}
