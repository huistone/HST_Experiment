package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.SysConfig;
import cc.mrbird.febs.system.mapper.SysConfigMapper;
import cc.mrbird.febs.system.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-05-25
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Resource
    private SysConfigMapper mapper;

    @Override
    public void updateByConfigName(String keyName, String value){
        mapper.updateByConfigName(keyName,value);
    }
}
