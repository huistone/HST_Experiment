package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import cc.mrbird.febs.system.mapper.ExperimentRemarkMapper;
import cc.mrbird.febs.system.service.IExperimentRemarkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 实验题目 评语表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-07-01
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class ExperimentRemarkServiceImpl extends ServiceImpl<ExperimentRemarkMapper, ExperimentRemark> implements IExperimentRemarkService {

    @Resource
    private ExperimentRemarkMapper experimentRemarkMapper;

    @Override
    public List<ExperimentRemark> listCommit(Integer pageNum,Integer pageSize, Long projectId, Long deptId, Integer type) {
        pageNum=((pageNum-1)*pageSize);
        List<ExperimentRemark> experimentRemarks = this.baseMapper.listCommit(pageNum,pageSize, type, projectId, deptId);
        return experimentRemarks;
    }

    @Override
    public int listCommitCount(Integer pageNum, Integer pageSize, Long projectId, Long deptId, Integer type) {
        pageNum=((pageNum-1)*pageSize);
        Integer integer = this.baseMapper.listCommitCount(pageNum, pageSize, type, projectId, deptId);
        return integer;
    }

    @Override
    public List<Double> selectRemarkScore(Long userId, Long deptId, List list) {
        return experimentRemarkMapper.selectRemarkScore(userId,deptId,list);
    }

    @Override
    public ExperimentRemark selectRemark(Long stuId, Long projectId, Integer type) {
        return experimentRemarkMapper.selectRemark(stuId,projectId,type);
    }
}
