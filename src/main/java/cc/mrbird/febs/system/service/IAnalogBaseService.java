package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.AnalogBase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-10-14
 */
public interface IAnalogBaseService extends IService<AnalogBase> {

}
