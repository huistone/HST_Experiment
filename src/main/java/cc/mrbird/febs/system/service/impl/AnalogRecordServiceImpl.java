package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.AnalogRecord;
import cc.mrbird.febs.system.entity.DetRecord;
import cc.mrbird.febs.system.mapper.AnalogRecordMapper;
import cc.mrbird.febs.system.service.IAnalogRecordService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-10-13
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class AnalogRecordServiceImpl extends ServiceImpl<AnalogRecordMapper, AnalogRecord> implements IAnalogRecordService {

    @Override
    public IPage<AnalogRecord> selectAnalogRecordByPage(AnalogRecord analogRecord, QueryRequest request) {
        Page<AnalogRecord> page = new Page<>(request.getPageNum(), request.getPageSize());

        return baseMapper.selectAnalogRecordByPage(page,analogRecord);
    }

    @Override
    public Integer count(AnalogRecord analogRecord) {
        return baseMapper.selectAnalogRecordCount(analogRecord);
    }
}
