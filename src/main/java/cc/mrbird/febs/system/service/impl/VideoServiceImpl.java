package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.Video;
import cc.mrbird.febs.system.mapper.VideoMapper;
import cc.mrbird.febs.system.service.IVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements IVideoService {

}
