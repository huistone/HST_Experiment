package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Feedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 意见反馈表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-05-08
 */
public interface IFeedbackService extends IService<Feedback> {

    void deleteFeedBacks(String[] feedbackIds);
}
