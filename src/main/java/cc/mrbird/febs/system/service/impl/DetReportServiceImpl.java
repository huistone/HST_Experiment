package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.DetReport;
import cc.mrbird.febs.system.mapper.DetReportMapper;
import cc.mrbird.febs.system.service.IDetReportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class DetReportServiceImpl extends ServiceImpl<DetReportMapper, DetReport> implements IDetReportService {

}
