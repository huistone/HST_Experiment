package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.mapper.UserDeptMapper;
import cc.mrbird.febs.system.mapper.UserMapper;
import cc.mrbird.febs.system.service.IUserDeptService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Service实现
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class UserDeptServiceImpl extends ServiceImpl<UserDeptMapper, UserDept> implements IUserDeptService {


    @Resource
    private UserDeptMapper userDeptMapper;

    @Resource
    private UserMapper userMapper;

    @Override
    public IPage<UserDept> findUserDepts(QueryRequest request, UserDept userDept) {
        LambdaQueryWrapper<UserDept> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<UserDept> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<UserDept> findUserDepts(UserDept userDept) {
	    LambdaQueryWrapper<UserDept> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public Integer createUserDept(UserDept userDept) {
        int code = 200;
        List<UserDept> userDeptList = userDeptMapper.selectList(new QueryWrapper<UserDept>().lambda()
                .eq(UserDept::getDeptId, userDept.getDeptId())
                .eq(UserDept::getCourseId, userDept.getCourseId()));
        //如果该老师已经是该课程的主课老师，那么就不可以继续新增
        Integer count = userDeptMapper.selectCount(new QueryWrapper<UserDept>().lambda()
                .eq(UserDept::getUserId, userDept.getUserId())
                .eq(UserDept::getDeptId,userDept.getDeptId())
                .eq(UserDept::getCourseId,userDept.getCourseId())
                .eq(UserDept::getType,1));
        //该老师已经是这门课的辅课老师
        Integer count1 = userDeptMapper.selectCount(new QueryWrapper<UserDept>().lambda()
                .eq(UserDept::getUserId, userDept.getUserId())
                .eq(UserDept::getDeptId,userDept.getDeptId())
                .eq(UserDept::getCourseId,userDept.getCourseId())
                .eq(UserDept::getType,0));
        if(count>0){
            return 503;
        }
        if(count1>0){
            return 504;
        }
        //一个老师一个班级只能带一门课
//        Integer integer = userDeptMapper.selectCount(new QueryWrapper<UserDept>()
//                .lambda()
//                .eq(UserDept::getUserId, userDept.getUserId())
//                .eq(UserDept::getDeptId, userDept.getDeptId()));
//        if(integer>0){
//            return code = 202;
//        }

        //如果要设置为主课老师,需要先判断该课是否有主课老师
        if(userDept.getType()==1){
//

            for (UserDept dept : userDeptList) {
                //只要有主课老师,就设置为1
                if(dept.getType()==1){
                    code = 201;
                    return code;
                }
            }
        }
        this.save(userDept);
        return code;
        //如果该班级和课程下没有老师，那么该老师就是主课老师，否则再次添加的就是辅导老师
//        boolean flag = false;
//        for (UserDept dept : userDeptList) {
//            //只要有主课老师,就设置为1
//            if(dept.getType()==1){
//                flag = true;
//            }
//        }
//        if(flag){
//            userDept.setType(0);
//        }else{
//            //没有主课老师,设置该老师为主课老师
//            userDept.setType(1);
//        }

    }

    @Override
    @Transactional
    public void updateUserDept(UserDept userDept) {
        this.saveOrUpdate(userDept);
    }


}
