package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资讯表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
public interface IArticleService extends IService<Article> {

}
