package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.DeptTree;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.utils.TreeUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.entity.vo.ElementCascader;
import cc.mrbird.febs.system.mapper.*;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author 马超伟
 */
@Service
@Slf4j
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserDeptMapper userDeptMapper;

    @Resource
    private CourseMapper courseMapper;

    @Resource
    private DeptMapper deptMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Override
    public List<DeptTree<Dept>> findDepts() {
        List<Dept> depts = this.baseMapper.selectList(new QueryWrapper<>());
        List<DeptTree<Dept>> trees = this.convertDepts(depts);
        return TreeUtil.buildDeptTree(trees);
    }

    @Override
    public List<DeptTree<Dept>> findDepts(Dept dept) {
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(dept.getDeptName())) {
            queryWrapper.lambda().like(Dept::getDeptName, dept.getDeptName()).orderByAsc(Dept::getOrderNum);
        }

        List<Dept> depts = this.baseMapper.selectList(queryWrapper);
        List<DeptTree<Dept>> trees = this.convertDepts(depts);
        return TreeUtil.buildDeptTree(trees);
    }

    @Override
    public List<ElementCascader> getCascader() {
       return deptMapper.getCascader();
    }

    @Override
    public List<Dept> findDepts(Dept dept, QueryRequest request) {
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(dept.getDeptName())) {
            queryWrapper.lambda().eq(Dept::getDeptName, dept.getDeptName());
        }
        SortUtil.handleWrapperSort(request, queryWrapper, "orderNum", FebsConstant.ORDER_ASC, true);
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Exception.class)
    public void createDept(Dept dept) {
        Long parentId = dept.getParentId();
        //如果是一级分类
        if (parentId == null) {
            dept.setParentId(0L);
            //设置级别为1
            dept.setLevel(1);
        } else {
            Dept dept1 = deptMapper.selectOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptId, parentId));
            //二级分类
            if (dept1.getParentId() == 0) {
                dept.setLevel(2);
            } else {
                //三级分类
                dept.setLevel(3);
            }
        }
        dept.setCreateTime(new Date());
        this.save(dept);
    }

    @Override
    @Transactional
    public void updateDept(Dept dept) {
        dept.setModifyTime(new Date());
        this.baseMapper.updateById(dept);
    }

    @Override
    @Transactional
    public void deleteDepts(String[] deptIds) {
        this.delete(Arrays.asList(deptIds));
    }


    private List<DeptTree<Dept>> convertDepts(List<Dept> depts) {
        List<DeptTree<Dept>> trees = new ArrayList<>();
        depts.forEach(dept -> {
            DeptTree<Dept> tree = new DeptTree<>();
            tree.setId(String.valueOf(dept.getDeptId()));
            tree.setParentId(String.valueOf(dept.getParentId()));
            tree.setName(dept.getDeptName());
            tree.setData(dept);
            trees.add(tree);
        });
        return trees;
    }

    private void delete(List<String> deptIds) {
        removeByIds(deptIds);
        LambdaQueryWrapper<Dept> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Dept::getParentId, deptIds);
        List<Dept> depts = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(depts)) {
            List<String> deptIdList = new ArrayList<>();
            depts.forEach(d -> deptIdList.add(String.valueOf(d.getDeptId())));
            this.delete(deptIdList);
        }
    }


    @Override
    public IPage<User> findStudentByDeptIdList(Integer deptId, User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = userMapper.findStudentByDeptIdList(page, deptId, user);
        return userDetailPage;
    }

    @Override
    public List<UserDept> findDeptDetailInfo(Integer deptId) {
        List<CourseDept> course1 = courseDeptService.list(new QueryWrapper<CourseDept>().lambda().eq(CourseDept::getDeptId, deptId));
        Set<Long> courseIds = new HashSet<>();
       for (CourseDept course : course1) {
            if(course.getCourseId()!=null){
                courseIds.add(course.getCourseId().longValue());
            }
        }
        List<UserDept> list = new ArrayList<>();
        //获取该课程对应的用户ID
        for (Long courseId : courseIds) {
            UserDept uDept = new UserDept();
            StringBuilder stringBuilder = new StringBuilder();
            //为了获取该课程对应的用户id集合
            List<UserDept> userDepts1 = userDeptMapper.selectList(new QueryWrapper<UserDept>().lambda().eq(courseId != null, UserDept::getCourseId, courseId).eq(UserDept::getDeptId,deptId));
            if(userDepts1.size() == 0){
                //获取该课程
                Course course = courseMapper.selectById(courseId);
                //设置该课程名字
                uDept.setCourseName(course.getCourseName());
                uDept.setMainTeacher("");
                uDept.setTeacherName("");
                list.add(uDept);
                continue;
            }
            //获取该课程
            Course course = courseMapper.selectById(courseId);
            //设置该课程名字
            uDept.setCourseName(course.getCourseName());
            //判断每个用户是主老师还是辅助老师
            for (UserDept user : userDepts1) {
                //获取用户
                User user1 = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUserId, user.getUserId()).eq(User::getIsDel,1));
                UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId, user1.getUserId()));
                //如果是主课老师
                if(userRole.getRoleId()==81&&user.getType()==1){
                    uDept.setMainTeacher(user1.getTrueName());
                }
                //如果是辅课老师
                if(userRole.getRoleId()==81&&user.getType()==0){
                    stringBuilder.append(user1.getTrueName()+"");
                }
            }
            uDept.setTeacherName(stringBuilder.toString());
            if(uDept.getMainTeacher()==null){
                uDept.setMainTeacher("");
            }
            list.add(uDept);
        }
        return list;
    }
}


