package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Wave;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 波形数据集，自动学习 服务类
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
public interface IWaveService extends IService<Wave> {

}
