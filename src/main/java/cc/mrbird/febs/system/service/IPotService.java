package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Pot;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * POT1 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-12-29
 */
public interface IPotService extends IService<Pot> {

}
