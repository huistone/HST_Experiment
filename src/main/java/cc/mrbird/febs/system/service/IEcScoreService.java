package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.EcScore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-25
 */
public interface IEcScoreService extends IService<EcScore> {

}
