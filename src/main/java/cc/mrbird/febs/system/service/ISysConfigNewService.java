package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.SysConfigNew;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface ISysConfigNewService extends IService<SysConfigNew> {

}
