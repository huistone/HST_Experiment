package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.SysConfigNew;
import cc.mrbird.febs.system.mapper.SysConfigNewMapper;
import cc.mrbird.febs.system.service.ISysConfigNewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class SysConfigNewServiceImpl extends ServiceImpl<SysConfigNewMapper, SysConfigNew> implements ISysConfigNewService {

}
