package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.AnalogRecord;
import cc.mrbird.febs.system.entity.DetRecord;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-10-13
 */
public interface IAnalogRecordService extends IService<AnalogRecord> {

    IPage<AnalogRecord> selectAnalogRecordByPage(AnalogRecord analogRecord, QueryRequest request);

    Integer count(AnalogRecord analogRecord);
}
