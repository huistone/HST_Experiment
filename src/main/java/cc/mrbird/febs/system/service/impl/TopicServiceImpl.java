package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.Topic;
import cc.mrbird.febs.system.mapper.TopicMapper;
import cc.mrbird.febs.system.service.ITopicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * MQTT主题记录表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-10-23
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class TopicServiceImpl extends ServiceImpl<TopicMapper, Topic> implements ITopicService {

}
