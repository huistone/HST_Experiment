package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
public interface IVideoService extends IService<Video> {

}
