package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.ElectricControl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-12-05
 */
public interface IElectricControlService extends IService<ElectricControl> {

}
