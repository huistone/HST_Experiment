package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.ElectricControl;
import cc.mrbird.febs.system.mapper.ElectricControlMapper;
import cc.mrbird.febs.system.service.IElectricControlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-12-05
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class ElectricControlServiceImpl extends ServiceImpl<ElectricControlMapper, ElectricControl> implements IElectricControlService {

}
