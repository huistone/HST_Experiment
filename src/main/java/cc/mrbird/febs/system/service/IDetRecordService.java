package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.DetRecord;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 数电主板使用记录表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
public interface IDetRecordService extends IService<DetRecord> {

    IPage<DetRecord> selectDetRecordByPage(DetRecord detRecord, QueryRequest request);

    Integer count(DetRecord detRecord);
}
