package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.CourseStudent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程学生表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
public interface ICourseStudentService extends IService<CourseStudent> {

}
