package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.CustomerCommit;
import cc.mrbird.febs.system.mapper.CustomerCommitMapper;
import cc.mrbird.febs.system.service.ICustomerCommitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 用户报告提交记录表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class CustomerCommitServiceImpl extends ServiceImpl<CustomerCommitMapper, CustomerCommit> implements ICustomerCommitService {

}
