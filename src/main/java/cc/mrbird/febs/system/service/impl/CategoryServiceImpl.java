package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.Category;
import cc.mrbird.febs.system.mapper.CategoryMapper;
import cc.mrbird.febs.system.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 分类表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

}
