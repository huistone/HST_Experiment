package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Screen;
import cc.mrbird.febs.system.entity.vo.ScreenVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 截屏记录保持表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-12-30
 */
public interface IScreenService extends IService<Screen> {

    IPage<ScreenVo> selectListByCondition(QueryRequest request, ScreenVo screenVo);
}
