package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.authentication.ShiroRealm;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.mapper.UserMapper;
import cc.mrbird.febs.system.mapper.UserRoleMapper;
import cc.mrbird.febs.system.service.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 马超伟
 */
@Service
@Slf4j
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor = RuntimeException.class)
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private IUserRoleService userRoleService;
    @Autowired
    private ShiroRealm shiroRealm;

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    @Resource
    private UserMapper userMapper;

    @Resource
    private IRoleService roleService;

    @Resource
    private IUserCourseService userCourseService;

    @Resource
    private ICourseService courseService;

    @Resource
    private UserRoleMapper userRoleMapper;


    @Override
    public User findByName(String username) {
        User user = this.baseMapper.findByName(username);
        if (user != null) {
            if (FebsConstant.TEACHER_ID.equals(user.getRoleId())) {
                List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, user.getUserId())
                        .select(UserDept::getUserDeptId, UserDept::getDeptId, UserDept::getCourseId, UserDept::getUserId, UserDept::getType));
                List<String> deptList = new ArrayList<>();
                for (UserDept userDept : userDeptList) {
                    Dept dept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptId, userDept.getDeptId()));
                    deptList.add(dept.getDeptName());
                }
                user.setDeptName(deptList.toString().replace("[", "").replace("]", ""));
            }else if (FebsConstant.STUDENT_ID.equals(user.getRoleId())){
                user.setDeptName(deptService.getById(user.getDeptId()).getDeptName());
            }
        }
        return user;
    }

    @Override
    public IPage<User> findUserDetailList(User user, QueryRequest request) {
//        return findUserInfo(user,request);
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = this.baseMapper.findUserDetailPage(page, user);
        List<User> userList = userDetailPage.getRecords();
        setUserDeptInfo(userList);
        return userDetailPage;
    }

    @Override
    public IPage<User> findUserDetailListExcludeTeacher(User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        User currentUser = getCurrentUser();
        Long roleId = currentUser.getRoleId();
        Role role = roleService.getById(roleId);
        IPage<User> userDetailPage = this.baseMapper.findUserDetailPageExcludeTeacher(page, user, role.getLevel());
        List<User> userList = userDetailPage.getRecords();
        setUserDeptInfo(userList);
        return userDetailPage;
    }

    /**
     * 给用户设置部门名称
     *
     * @ Param list
     */
    public void setUserDeptInfo(List<User> list) {
        for (User user1 : list) {
            List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, user1.getUserId()));
            List<String> deptList = new ArrayList<>();
            List<Long> deptId = userDeptList.stream().map(UserDept::getDeptId).distinct().collect(Collectors.toList());
            for (Long detId : deptId) {
                Dept dept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptId, detId));
                deptList.add(dept.getDeptName());
            }
            user1.setDeptName(deptList.toString().replace("[", "").replace("]", ""));
        }
    }

    @Override
    public IPage<User> findUserTeacherDetailList(User user, QueryRequest request) {
//        return findUserInfo(user,request);
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = this.baseMapper.findTeacherPage(page, user);
        List<User> userLists = userDetailPage.getRecords();
        setUserDeptInfo(userLists);

        return userDetailPage;
        
    }

    @Override
    public List<User> findUserTeacherList() {
        return this.baseMapper.findUserTeacher();
    }

    @Override
    public List<User> findUserManagerList() {
        return this.baseMapper.findUserManager();
    }

    @Override
    public IPage<User> findUserStudentDetailList(User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = this.baseMapper.findStudentPage(page, user);
        return userDetailPage;
    }

    @Override
    public IPage<User> findTeacherList(User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = this.baseMapper.findTeacherPage(page, user);
        return userDetailPage;
    }

    @Override
    public User findUserDetailList(String username) {
        User param = new User();
        param.setUsername(username);
        List<User> users = this.baseMapper.findUserDetail(param);
        return CollectionUtils.isNotEmpty(users) ? users.get(0) : null;
    }

    @Override
    @Transactional
    public void updateLoginTime(String username) {
        User user = new User();
        user.setLastLoginTime(new Date());
        this.baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional
    public void createUser(User user) {
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        save(user);
        // 保存用户角色
        setUserRoles(user, user.getRoleId());
    }

    @Override
    @Transactional
    public void deleteUsers(String[] userIds) {
        List<String> list = Arrays.asList(userIds);
        // 删除用户
        this.removeByIds(list);
        // 删除关联角色
        this.userRoleService.deleteUserRolesByUserId(list);

        //如果有对应的班级信息则删除关联班级
        int count = userDeptService.count(new QueryWrapper<UserDept>().lambda().in(UserDept::getUserId, list));
        if (count > 0) {
            this.userRoleService.deleteUserDeptByUserId(list);
        }
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        String username = user.getUsername();
        // 更新用户
        user.setPassword(null);
        user.setUsername(null);
        user.setModifyTime(new Date());
        updateById(user);
        // 更新关联角色
        this.userRoleService.remove(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, user.getUserId()));
        setUserRoles(user, user.getRoleId());
        User currentUser = FebsUtil.getCurrentUser();
        if (StringUtils.equalsIgnoreCase(currentUser.getUsername(), username)) {
            shiroRealm.clearCache();
        }
    }

    @Override
    @Transactional
    public void resetPassword(String[] usernames) {
        Arrays.stream(usernames).forEach(username -> {
            User user = new User();
            user.setPassword(MD5Util.encrypt(username, User.DEFAULT_PASSWORD));
            this.baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
        });
    }

    @Override
    @Transactional
    public void regist(String username, String password) {
        User user = new User();
        user.setPassword(MD5Util.encrypt(username, password));
        user.setUsername(username);
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setSex(User.SEX_UNKNOW);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setDescription("注册用户");
        this.save(user);

        UserRole ur = new UserRole();
        ur.setUserId(user.getUserId());
        ur.setRoleId(FebsConstant.STUDENT_ID);
        this.userRoleService.save(ur);
    }

    @Override
    @Transactional
    public void updatePassword(String username, String password) {
        User user = new User();
        user.setPassword(MD5Util.encrypt(username, password));
        user.setModifyTime(new Date());
        this.baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional
    public void updateAvatar(String username, String avatar) {
        User user = new User();
        user.setAvatar(avatar);
        user.setModifyTime(new Date());
        this.baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional
    public void updateTheme(String username, String theme, String isTab) {
        User user = new User();
        user.setTheme(theme);
        user.setIsTab(isTab);
        user.setModifyTime(new Date());
        this.baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional
    public void updateProfile(User user) {
        user.setUsername(null);
        user.setRoleId(null);
        user.setPassword(null);
        if (isCurrentUser(user.getId())) {
            updateById(user);
        } else {
            throw new FebsException("您无权修改别人的账号信息！");
        }
    }

    @Override
    public User getCurrentUser() {
        return (User) SecurityUtils.getSubject().getPrincipal();
    }

    @Override
    public List<User> getStudentByDeptId(Long deptId) {
        return this.baseMapper.getStudentByDeptId(deptId);
    }

    @Override
    public IPage<User> selectStudentInfo(QueryRequest request, Long deptId, User teacher) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<User> userIPage = this.baseMapper.selectPage(page, new QueryWrapper<User>()
                .lambda()
                .eq(deptId != null, User::getDeptId, deptId)
                .eq(User::getStatus, 1)
                .eq(User::getIsDel, 1)
                .isNotNull(User::getDeptId));
        List<User> studentList = userIPage.getRecords();
//        UserDept one = userDeptService.getOne(new QueryWrapper<UserDept>().lambda().eq(UserDept::getDeptId, deptId).eq(UserDept::getType, 1));
//        User teacher1 = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUserId, one.getUserId()).eq(User::getIsDel, 1));
        for (User user : studentList) {
            //通过部门id查出院专业以及班级名
            Dept thirdDept = deptService.getById(user.getDeptId());
            user.setThirdDeptName(thirdDept.getDeptName());
            List<UserDept> one = userDeptService.list(new QueryWrapper<UserDept>().lambda()
                    .eq(UserDept::getDeptId, user.getDeptId())
                    .eq(UserDept::getUserId, teacher.getUserId())
                    .eq(UserDept::getIsDel, 1));
            UserDept userDept = one.get(0);
            //TODO 前段不显示老师名字
            if (userDept != null) {
                //查出主课老师的id
                UserDept one1 = userDeptService.getOne(new QueryWrapper<UserDept>().lambda()
                        .eq(UserDept::getCourseId, userDept.getCourseId())
                        .eq(UserDept::getType, 1)
                        .eq(UserDept::getDeptId, user.getDeptId()));
                //查出主课老师
                User mainTeacher = userMapper.selectOne(new QueryWrapper<User>().lambda()
                        .eq(User::getUserId, userDept.getUserId())
                        .eq(User::getIsDel, 1)
                        .eq(User::getStatus, 1));
                user.setTeacherName(mainTeacher.getTrueName());
            } else {
                user.setTeacherName("暂无老师");
            }
            //通过学生班级和当前老师id查出课程
//            UserDept one = userDeptService.getOne(new QueryWrapper<UserDept>().lambda()
//                    .eq(UserDept::getDeptId, user.getDeptId())
//                    .eq(UserDept::getUserId, teacher.getUserId()));
//            if(one!=null){
//                //查出主课老师的id
//                UserDept one1 = userDeptService.getOne(new QueryWrapper<UserDept>().lambda()
//                        .eq(UserDept::getCourseId, one.getCourseId())
//                        .eq(UserDept::getType, 1)
//                        .eq(UserDept::getDeptId,user.getDeptId()));
//                //查出主课老师
//                User mainTeacher = userMapper.selectOne(new QueryWrapper<User>().lambda()
//                        .eq(User::getUserId, one1.getUserId())
//                        .eq(User::getIsDel, 1)
//                        .eq(User::getStatus, 1));
//                user.setTeacherName(mainTeacher.getTrueName());
//            }else{
//                user.setTeacherName("暂无老师");
//            }
        }
        userIPage.setRecords(studentList);
        return userIPage;
    }

    @Override
    public User selectSingleStudentInfo(Long userId) {
        User student = this.baseMapper.selectById(userId);
        //通过部门id查出院专业以及班级名
        Dept thirdDept = deptService.getById(student.getDeptId());
        Dept secondDept = deptService.getById(thirdDept.getParentId());
        Dept firstDept = deptService.getById(secondDept.getParentId());
        User user = userMapper.selectById(userId);
        user.setFirstDeptName(firstDept.getDeptName());
        user.setSecondDeptName(secondDept.getDeptName());
        user.setThirdDeptName(thirdDept.getDeptName());
        return user;
    }

    @Override
    public User selectTeacherInfo(User currentUser) {
        List<UserDept> list = userDeptService.list(new QueryWrapper<UserDept>()
                .lambda()
                .eq(UserDept::getUserId, currentUser.getUserId()));
        List<Long> deptIdList = list.stream().map(s -> s.getDeptId()).distinct().collect(Collectors.toList());
        for (UserDept userDept : list) {
            if (userDept.getType() == 1) {
                currentUser.setRoleName("主课老师");
                break;
            }
        }
        StringBuilder str = new StringBuilder();
        //查询该老师对应的deptName
        for (Long deptId : deptIdList) {
            Dept thirdDept = deptService.getById(deptId);
            Dept secondDept = deptService.getById(thirdDept.getParentId());
            Dept firstDept = deptService.getById(secondDept.getParentId());
            str.append(firstDept.getDeptName() + " " + thirdDept.getDeptName() + "  ");
        }
        currentUser.setDeptName(str.toString());
        return currentUser;
    }

    @Override
    public List<User> selectStudentScoreInfoByPage(Long deptId, Long projectId, Integer pageNum, Integer pageSize) {
        pageNum = (pageNum - 1) * pageSize;
        return userMapper.selectStudentScoreInfoByPage(deptId, projectId, pageNum, pageSize);
    }

    @Override
    public Integer selectStudentScoreCount(Long deptId, Long projectId) {
        return userMapper.selectStudentScoreCount(deptId, projectId);
    }

    @Override
    public IPage<?> findUserCustomerList(User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = this.baseMapper.findCustomerPage(page, user);
        return userDetailPage;
    }

    @Override
    public List<User> findUserCustomerExcelInfo() {
        return this.baseMapper.findUserCustomerExcelInfo();
    }

    @Override
    public IPage<?> findCourseManagerList(User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "userId", FebsConstant.ORDER_ASC, false);
        IPage<User> userDetailPage = this.baseMapper.findCourseManagerPage(page, user);
        List<User> userLists = userDetailPage.getRecords();
        for (User userList : userLists) {
            List<String> userCourses = new ArrayList<>();
            List<UserCourse> list = userCourseService.list(new QueryWrapper<UserCourse>()
                    .lambda()
                    .eq(UserCourse::getUserId, userList.getUserId()));
            for (UserCourse userCourse : list) {
                Course one = courseService.getOne(new QueryWrapper<Course>()
                        .lambda()
                        .eq(Course::getCourseId, userCourse.getCourseId())
                        .select(Course::getCourseName));
                userCourses.add(one.getCourseName());
            }
            userList.setCourseName(userCourses.toString().replace("[", "").replace("]", ""));
        }
        return userDetailPage;
    }

    @Override
    public List<User> findCourseManagerExcelInfo() {
        return this.baseMapper.findCourseManagerExcelInfo();
    }

    @Override
    public void registMobileUser(String username, String password,String telephone) {
        User user = new User();
        user.setPassword(MD5Util.encrypt(username, password));
        user.setUsername(username);
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setSex(User.SEX_UNKNOW);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setUserType(0);
        user.setMobile(telephone);
        user.setDescription("H5注册用户");
        this.save(user);

        UserRole ur = new UserRole();
        ur.setUserId(user.getUserId());
        ur.setRoleId(FebsConstant.CUSTOMER_ID);
        this.userRoleService.save(ur);
    }


    private void setUserRoles(User user, Long roldId) {
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getUserId());
        userRole.setRoleId(roldId);
        userRoleService.save(userRole);
    }

    private boolean isCurrentUser(Long id) {
        User currentUser = FebsUtil.getCurrentUser();
        return currentUser.getUserId().equals(id);
    }

    @Transactional
    @Override
    public void addStudentForTeacher(User user) {
        user.setStatus("1");
        user.setCreateTime(new Date());
        user.setUserType(FebsConstant.SCHOOL_TYPE);
        user.setPassword(MD5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        this.baseMapper.insert(user);
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getUserId());
        userRole.setRoleId(82L);
        userRoleMapper.insert(userRole);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateStudentInfo(User user) {
        try {
            user.setModifyTime(new Date());
            //判断手机号
            if (!StringUtils.isEmpty(user.getMobile())){
                User user1 = this.baseMapper.selectOne(new QueryWrapper<User>()
                        .lambda()
                        .eq(User::getMobile, user.getMobile()));
                if (user1 != null){
                     if(!(user1.getUsername().equals( user.getUsername()))){
                         return 501;
                     }
                }
            }
            //判断学号
            if(!StringUtils.isEmpty(user.getIdNumber())){
                User user1 = this.baseMapper.selectOne(new QueryWrapper<User>()
                        .lambda()
                        .eq(User::getIdNumber, user.getIdNumber()));
                if (user1!=null){
                    if(!(user1.getUsername().equals( user.getUsername()))){
                        return 502;
                    }
                }
            }
            int i = this.baseMapper.updateById(user);
            if(i==1){
                return 200;
            }else{
                return 500;
            }
        } catch (Exception e) {
            throw new FebsException("修改学生信息异常");
        }
    }
}
