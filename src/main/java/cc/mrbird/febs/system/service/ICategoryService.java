package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分类表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
public interface ICategoryService extends IService<Category> {

}
