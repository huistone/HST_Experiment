package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
public interface ICourseService extends IService<Course> {

    /**
     * 查询课程详情对象
     * @ Param course 课程
     * @ Param request 分页对象
     */
    IPage<Map<String, Object>> findUserDetailList(Course course, QueryRequest request);


    /**
     * 查询课程对应的项目
     */
    List<Course> selectCourseProject(Long courseId);
}
