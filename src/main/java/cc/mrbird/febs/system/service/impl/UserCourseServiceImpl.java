package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.UserCourse;
import cc.mrbird.febs.system.mapper.UserCourseMapper;
import cc.mrbird.febs.system.service.IUserCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 管理员分配课程表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class UserCourseServiceImpl extends ServiceImpl<UserCourseMapper, UserCourse> implements IUserCourseService {

}
