package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.UserDetreport;
import cc.mrbird.febs.system.mapper.UserDetreportMapper;
import cc.mrbird.febs.system.service.IUserDetreportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class UserDetreportServiceImpl extends ServiceImpl<UserDetreportMapper, UserDetreport> implements IUserDetreportService {

}
