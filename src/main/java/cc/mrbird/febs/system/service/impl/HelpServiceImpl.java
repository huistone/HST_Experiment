package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.common.constant.FebsConstant;
import cc.mrbird.febs.system.entity.Help;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.mapper.HelpMapper;
import cc.mrbird.febs.system.mapper.UserMapper;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IHelpService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-30
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class HelpServiceImpl extends ServiceImpl<HelpMapper, Help> implements IHelpService {

    @Resource
    private HelpMapper helpMapper;

    @Resource
    private ICourseService courseService;

    @Override
    public List<Help> getHelpList() {
        return helpMapper.getHelpList();
    }

    @Override
    public IPage findAll(Help help, QueryRequest request,User user) {
        Page<Help> helpPage = new Page<>(request.getPageNum(),request.getPageSize());
        SortUtil.handlePageSort(request, helpPage, "number", FebsConstant.ORDER_ASC, false);
        IPage<Help> helpIPage = this.baseMapper.selectPage(helpPage, new QueryWrapper<Help>().lambda()
                .eq(IntegerUtils.isNotBlank(help.getCourseId()),Help::getCourseId,help.getCourseId())
                .like(StringUtils.isNotBlank(help.getHelpName()), Help::getHelpName, help.getHelpName())
                .like(help.getNumber() != null, Help::getNumber, help.getNumber()));
        List<Help> records = helpIPage.getRecords();
        for (Help record : records) {
            record.setCourseName(courseService.getById(record.getCourseId()).getCourseName());
        }
        return helpIPage;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS,  rollbackFor = Exception.class )
    public void saveHelpInfo(Help help,User user) {
        try {
            help.setCreateName(user.getUsername());
            help.setCreateTime(new Date());
            help.setCreateId(user.getUserId().intValue());
            helpMapper.insert(help);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FebsException("系统内部错误");
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS,  rollbackFor = Exception.class )
    public void deleteByIds(String helpIds) {
        try {
            String[] split = helpIds.split(StringPool.COMMA);
            List<String> ids = Arrays.asList(split);
            ids.stream().forEach(s -> this.baseMapper.deleteById(s));
        } catch (Exception e) {
            e.printStackTrace();
            throw new FebsException("系统内部错误");
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS,  rollbackFor = Exception.class )
    public void updateHelpInfo(Help help,User user) {
        try {
            help.setCreateTime(new Date());
            help.setUpdateName(user.getUsername());
            help.setUpdateId(user.getUserId().intValue());
            help.setUpdateTime(new Date());
            this.baseMapper.updateById(help);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FebsException("修改帮助信息失败");
        }
    }

}
