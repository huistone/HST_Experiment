package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.ArticleCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资讯分类关联表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
public interface IArticleCategoryService extends IService<ArticleCategory> {

}
