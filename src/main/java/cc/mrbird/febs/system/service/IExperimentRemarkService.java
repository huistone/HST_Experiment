package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.ExperimentRemark;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 实验题目 评语表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-07-01
 */
public interface IExperimentRemarkService extends IService<ExperimentRemark> {

    List<ExperimentRemark> listCommit(Integer pageNum,Integer pageSize,Long projectId,Long deptId,Integer type);

    int listCommitCount(Integer pageNum,Integer pageSize,Long projectId,Long deptId,Integer type);


    List<Double> selectRemarkScore( Long userId,  Long deptId, List list);

    ExperimentRemark selectRemark( Long stuId, Long projectId,Integer type);
}
