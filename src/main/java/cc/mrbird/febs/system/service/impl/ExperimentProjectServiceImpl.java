package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.student.entity.ExperimentQuestion;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IExperimentQuestionService;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.mapper.ExperimentProjectMapper;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 实验项目表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-25
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class ExperimentProjectServiceImpl extends ServiceImpl<ExperimentProjectMapper, ExperimentProject> implements IExperimentProjectService {

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private IExperimentQuestionService experimentQuestionService;

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private ITeacherProjectService teacherProjectService;


    @Override
    public void deleteProject(String projectId) {
        //删除实验项目下的思考题
        experimentQuestionService.remove(new QueryWrapper<ExperimentQuestion>()
                .lambda().eq(ExperimentQuestion::getProjectId, projectId));
        //删除该实验下学生的提交报告
        userCommitService.remove(new QueryWrapper<UserCommit>()
                .lambda().eq(UserCommit::getProjectId, projectId));
        //删除实验项目
        experimentProjectService.removeById(projectId);
        //删除项目和班级的关联，既老师对班级开启的项目
        teacherProjectService.remove(new QueryWrapper<TeacherProject>()
                .lambda().eq(TeacherProject::getProjectId, projectId));

    }


}
