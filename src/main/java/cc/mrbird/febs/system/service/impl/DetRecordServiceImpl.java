package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.DetRecord;
import cc.mrbird.febs.system.mapper.DetRecordMapper;
import cc.mrbird.febs.system.service.IDetRecordService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 数电主板使用记录表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class DetRecordServiceImpl extends ServiceImpl<DetRecordMapper, DetRecord> implements IDetRecordService {

    @Override
    public IPage<DetRecord> selectDetRecordByPage(DetRecord detRecord, QueryRequest request) {
//        int pageNum = request.getPageNum();
//        int pageSize = request.getPageSize();
//        pageNum = (pageNum-1)*pageSize;
        Page<DetRecord> page = new Page<>(request.getPageNum(), request.getPageSize());
//,pageNum,pageSize
//        return baseMapper.selectList(page,detRecord);
        return baseMapper.selectDetRecordByPage(page,detRecord);
    }


    @Override
    public Integer count(DetRecord detRecord) {
        return baseMapper.selectDeRecordCount(detRecord);
    }
}
