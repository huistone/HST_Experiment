package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.DetBase;
import cc.mrbird.febs.system.entity.Help;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 数电主板信息表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
public interface IDetBaseService extends IService<DetBase> {

    IPage<DetBase> selectList(DetBase detBase, QueryRequest request);
}
