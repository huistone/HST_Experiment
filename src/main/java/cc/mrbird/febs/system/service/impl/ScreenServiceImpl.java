package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Screen;
import cc.mrbird.febs.system.entity.vo.ScreenVo;
import cc.mrbird.febs.system.mapper.ScreenMapper;
import cc.mrbird.febs.system.service.IScreenService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 截屏记录保持表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-12-30
 */
@Service
@Transactional(propagation= Propagation.NESTED,isolation= Isolation.DEFAULT,readOnly = false,rollbackFor=Exception.class)
public class ScreenServiceImpl extends ServiceImpl<ScreenMapper, Screen> implements IScreenService {

    @Override
    public IPage<ScreenVo> selectListByCondition(QueryRequest request, ScreenVo screenVo) {
        Page page = new Page<>(request.getPageNum(),request.getPageSize());
        return this.baseMapper.selectListByCondition(page,screenVo);
    }
}
