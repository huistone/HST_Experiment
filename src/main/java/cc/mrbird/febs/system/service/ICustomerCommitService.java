package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.CustomerCommit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户报告提交记录表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
public interface ICustomerCommitService extends IService<CustomerCommit> {

}
