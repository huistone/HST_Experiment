package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-11-19
 */
public interface IOrderService extends IService<Order> {

}
