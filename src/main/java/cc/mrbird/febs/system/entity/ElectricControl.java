package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-12-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_electric_control")
public class ElectricControl implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 电气控制主键
     */
    @TableId(value = "elec_control_id",type = IdType.AUTO)
    private Long elecControlId;

    /**
     * 电气主板编号
     */
    private String elecControlNumber;

    /**
     * 电气主板名称
     */
    private String elecControlName;

    /**
     * 主板状态 1 可用 0禁用
     */
    private Integer status;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;

    /**
     * 修改日期
     */
    private LocalDateTime updateTime;

    /**
     * 订阅通道
     */
    private String subChannel;

    /**
     * 发布通道
     */
    private String publicChannel;

    /**
     * 操作者名称
     */
    private String operator;

    /**
     * 使用状态  1正在使用 0暂无使用
     */
    private Integer useStatus;


}
