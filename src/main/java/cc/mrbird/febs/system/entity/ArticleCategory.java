package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资讯分类关联表
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_article_category")
public class ArticleCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "article_category_id", type = IdType.AUTO)
    private Long articleCategoryId;

    /**
     * 资讯id
     */
    private Integer articleId;

    /**
     * 分类id
     */
    private Integer categoryId;


}
