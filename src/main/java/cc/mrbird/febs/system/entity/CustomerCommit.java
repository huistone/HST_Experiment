package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户报告提交记录表
 * </p>
 *
 * @author Macw
 * @since 2020-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_customer_commit")
public class CustomerCommit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户报告提交表
     */
    @TableId(value = "commit_id", type = IdType.AUTO)
    private Integer commitId;

    /**
     * 用户ID
     */
    private Integer customerId;

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 课程ID
     */
    private Integer courseId;

    /**
     * 课程管理员ID
     */
    private Integer teacherId;

    /**
     * 用户提交的报告URL
     */
    private String reportUrl;

    /**
     * 用户成绩
     */
    private Double score;

    /**
     * 1提交，0，未提交，2，已批阅
     */
    private Integer isCommit;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;

    /**
     * 批阅时间
     */
    private LocalDateTime updateTime;

    /**
     * 批阅人
     */
    private String updateUser;

    /**
     * 1,正常老师开启，0关闭
     */
    private Integer useType;

    /**
     * 1正常，0删除
     */
    private Integer isDel;

    /**
     * 提交次数
     */
    private Integer time;


}
