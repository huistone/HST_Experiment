package cc.mrbird.febs.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.List;

/**
 *  Entity
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
@Data
@TableName("t_user_dept")
public class UserDept {

    /**
     * 用户班级表
     */
    @TableId(value = "user_dept_id", type = IdType.AUTO)
    private Integer userDeptId;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 部门ID
     */
    @TableField("dept_id")
    private Long deptId;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 课程ID
     */
    @TableField("course_id")
    private Long courseId;

    /**
     * 课程名称
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 1主课老师，0辅导老师
     */
    @TableField("type")
    private Integer type;

    /**
     * 主课老师名字
     */
    @TableField(exist = false)
    private String mainTeacher;

    /**
     * 辅课老师名字
     */
    @TableField(exist = false)
    private String teacherName;


    private Integer isDel;

}
