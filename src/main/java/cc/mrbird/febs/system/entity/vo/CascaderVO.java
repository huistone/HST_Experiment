package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/06/22/17:55
 * @ Description: 基于layui的无限级联选择器 工具映射类
 */
@Data
public class CascaderVO {

    private Integer value;
    private String label;
    private List<CascaderVO> children;

}
