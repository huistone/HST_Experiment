package cc.mrbird.febs.system.entity;

import cc.mrbird.febs.student.entity.UserCommit;
import lombok.Data;

/**
 * @ClassName UserScore
 * @ Description 学生个人成绩实体
 * @Author Administrator
 * @Data 2020/7/15 11:01
 * @Version 1.0
 */
@Data
public class UserScore {
    private ExperimentRemark prepareScore;

    private ExperimentRemark thinkScore;

    private UserCommit projectScore;

    private Double scoreRow;

    private String projectName;

}
