package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资讯表
 * </p>
 *
 * @author Macw
 * @since 2020-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "article_id", type = IdType.AUTO)
    private Long articleId;

    /**
     * 发表用户
     */
    private Long userId;

    /**
     * 文章内容(去除HTML标签)
     */
    private String articleContent;

    /**
     * 文章内容包含HTML标签
     */
    private String articleContentMd;

    /**
     * 发布时间
     */
    private LocalDateTime articleNewstime;

    /**
     * 文章状态 1已发布0未发布
     */
    private Integer articleStatus;

    /**
     * 文章摘要
     */
    private String articleSummary;

    /**
     * 略缩图
     */
    private String articleThumbnail;

    /**
     * 文章标题
     */
    private String articleTitle;

    /**
     * 文章最后修改时间
     */
    private LocalDateTime articleUpdatetime;

    /**
     * 访问量统计
     */
    private Long articleViews;

    /**
     * 类别id
     */
    private Integer categoryId;

    /**
     * 发表人昵称
     */
    private String nickname;


}
