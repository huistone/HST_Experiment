package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_analog_base")
public class AnalogBase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 模电主板主键
     */
    @TableId(value = "analog_id", type = IdType.AUTO)
    private Long analogId;

    /**
     * 实验id
     */
    private String projectId;

    /**
     * 实验名称
     */
    private String projectName;

    /**
     * 模电主板编号
     */
    private String analogNumber;

    /**
     * 模电主板名称
     */
    private String analogName;

    /**
     * 主板状态1可用.0禁用
     */
    private Integer analogStatus;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 使用状态1占用,0空闲
     */
    @TableField("analog_useStatus")
    private Integer analogUsestatus;

    /**
     * 订阅通道
     */
    private String subChannel;

    /**
     * 发布通道
     */
    private String releaseChannel;

    /**
     * 操作人
     */
    private String operator;

    @TableField(exist = false)
    private Integer useNumber;

    private String description;

}
