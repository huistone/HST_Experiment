package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wk
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("u_video")
public class Video implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 视频表主键
     */
    @TableId(type = IdType.AUTO)
    private Long videoId;

    /**
     * 视频名称
     */
    private String videoName;

    /**
     * 视频长度
     */
    private String videoSize;

    private Integer videoSort;


    /**
     * 创建日期
     */
    private LocalDateTime createTime;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 视频链接
     */
    private String videoUrl;

    /**
     * 1未删除 0删除
     */
    @TableLogic
    private Integer isDel;

}
