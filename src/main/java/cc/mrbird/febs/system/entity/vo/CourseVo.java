package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

/**
 * @ClassName CourseVo
 * @ Description 老师所带班级下拉框
 * @Author wangke
 * @Data 2020-8-12 10:45
 * @Version 1.0
 */

@Data
public class CourseVo {

    private Integer courseId;

    private String courseName;
}
