package cc.mrbird.febs.system.entity.vo;

import cc.mrbird.febs.student.entity.ExperimentQuestion;

import java.util.List;

/**
 * @ClassName QuestionDTO
 * @ Description TODO
 * @Author wangke
 * @Data 2020-7-31 8:55
 * @Version 1.0
 */
public class QuestionDTO {
    private List<ExperimentQuestion> questionList;
}
