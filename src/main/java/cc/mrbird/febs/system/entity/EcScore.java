package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-11-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_ec_score")
public class EcScore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 电气控制成绩主键
     */
    @TableId(value = "ecscore_id", type = IdType.AUTO)
    private Long ecscoreId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 成绩
     */
    private Double score;


    /**
     * 实验id
     */
    private Integer projectId;

    /**
     * 1 代表连线成绩  0代表故障排除成绩
     */
    private Integer type;


}
