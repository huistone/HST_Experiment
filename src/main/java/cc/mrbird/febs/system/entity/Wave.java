package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 波形数据集，自动学习
 * </p>
 *
 * @author Macw
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_wave")
public class Wave implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer waveId;

    /**
     * 电位器 1,0 ~ 1024
     */
    @TableField("POT1")
    private Integer pot1;

    /**
     * 电位器 2,0 ~ 1024
     */
    @TableField("POT2")
    private Integer pot2;

    /**
     * 波形的频率，0 ~ 65536
     */
    @TableField("FREQ")
    private Integer freq;

    /**
     * 幅度,0 ~ 255
     */
    @TableField("AMP")
    private Integer amp;

    /**
     * 相位, 0 ~ 180
     */
    @TableField("PHA")
    private Integer pha;

    /**
     * 波形, 正弦波0,方波1, 三角波  2
     */
    @TableField("WAVE")
    private Integer wave;

    /**
     * 项目课程 id
     */
    @TableField("PID")
    private Integer pid;

    /**
     * 测试任务号
     */
    @TableField("TASK")
    private Integer task;

    /**
     * json数据
     */
    @TableField("Json_List")
    private String jsonList;


}
