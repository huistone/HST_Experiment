package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

/**
 * @ ClassName ProejctSelect
 * @ Description TODO
 * @ Author admin
 * @ Date 2021/2/3 17:00
 * @ Version 1.0
 */
@Data
public class ProjectSelect {

    private Long value;

    private String name;

    private Boolean selected = false;

}
