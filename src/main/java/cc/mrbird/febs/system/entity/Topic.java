package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * MQTT主题记录表
 * </p>
 *
 * @author Macw
 * @since 2020-10-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_topic")
public class Topic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主题表ID
     */
    @TableId(value = "tid", type = IdType.AUTO)
    private Integer tid;

    /**
     * 主题名称
     */
    private String name;

    /**
     * 0发布主题，1订阅主题
     */
    private Integer type;

    /**
     * 关联课程id
     */
    private Integer courseId;

    /**
     * 1可用，0失效
     */
    private Integer status;

    @TableField(exist = false)
    private String courseName;


}
