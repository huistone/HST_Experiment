package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 贵永康
 */
@Data
public class ProjectDTO implements Serializable {

    private String deptName;

    private List<String> projectList;

}
