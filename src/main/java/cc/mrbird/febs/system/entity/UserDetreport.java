package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_user_detReport")
public class UserDetreport implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户实验报告记录表
     */
    @TableId(value = "user_detReport_id", type = IdType.AUTO)
    private Long userDetreportId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 数电报告id
     */
    private Long detReportId;

    /**
     * 生成时间
     */
    private LocalDateTime reportCreateTime;

    /**
     * 实验项目id
     */
    private Long projectId;

    /**
     * 班级id
     */
    private Long deptId;

    /**
     * 课程id
     */
    private Long courseId;


}
