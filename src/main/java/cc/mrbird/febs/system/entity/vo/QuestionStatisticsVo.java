package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

/**
 * @ClassName QuestionStatisticsVo
 * @Description TODO
 * @Author admin
 * @Date 2021/1/13 17:25
 * @Version 1.0
 */
@Data
public class QuestionStatisticsVo {

    private Integer questionId;

    private Integer number;

    private String questionName;

    private Integer projectId;

    private Integer type;

    private Integer questionType;

    /**
     * 回答总数
     */
    private Long answerCount;

    /**
     * 正确总数
     */
    private Long correctCount;

    /**
     * 正确率
     */
    private Double correctRate;



}
