package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_analog_record")
public class AnalogRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 模电主板操作记录id
     */
    @TableId(value = "analog_record_id",type = IdType.AUTO)
    private Long analogRecordId;

    /**
     * 模电主板id
     */
    private Long analogId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 实验次数
     */
    private Integer time;

    /**
     * 开始时间
     */
    private LocalDateTime beginTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    @TableField(value = "is_use")
    private Integer isUse;

    @TableField(exist = false)
    private String trueName;

    @TableField(exist = false)
    private String analogName;

}
