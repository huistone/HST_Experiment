package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 实验项目表
 * </p>
 *
 * @author Macw
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_experiment_project")
public class ExperimentProject implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 实验项目id
     */
    @TableId(value = "project_id", type = IdType.AUTO)
    private Long projectId;

    /**
     * 实验项目编号
     */
    private String projectCode;

    /**
     * 实验项目名称
     */
    private String projectName;


    /**
     * 副标题
     */
    private String subTitle;

    /**
     * 实验头图
     */
    private String projectPictureUrl;

    /**
     * 所属实验ID
     */
    private Long courseId;

    /**
     * 所属课程名称
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 实验项目内容
     */
    private String projectContent;

    /**
     * 排序字段，小的靠前
     */
    private Integer projectSort;

    /**
     * 删除状态[0:停用, 1:启用 2：正常]
     */
    private Integer state;

    /**
     * 实验报告路径
     */
    private String reportUrl;

    /**
     * 实验报告大小
     */
    private String reportSize;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 老师的id编号 只在数据传输的时候使用 不映射到数据库
     */
    @TableField(exist = false)
    private Integer teacherId;

    /**
     * 实验指导书url，pdf格式
     */
    private String projectContentUrl;

    /**
     * 实验项目价格
     */
    private BigDecimal projectPrice;

    /**
     * 1正常，0删除
     */
    @TableLogic
    private Integer isDel;
}
