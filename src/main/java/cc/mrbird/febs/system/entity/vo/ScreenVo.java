package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClassName ScreenVo
 * @Description TODO
 * @Author admin
 * @Date 2020/12/30 14:48
 * @Version 1.0
 */
@Data
public class ScreenVo {

    private static final long serialVersionUID = 1L;

    /**
     * 截屏表主键
     */
    private Integer screenId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 实验项目id
     */
    private Integer projectId;

    /**
     * 实验名称
     */
    private String projectName;

    /**
     * 截屏图片
     */
    private String pictureUrl;

    /**
     * 截屏时间
     */
    private LocalDateTime createTime;
}
