package cc.mrbird.febs.system.entity.vo;

import lombok.Data;

/**
 * @ClassName ProjectScreenVo
 * @Description TODO
 * @Author admin
 * @Date 2020/12/31 17:26
 * @Version 1.0
 */
@Data
public class ProjectScreenVo {

    private Integer projectId;

    private String projectName;

    private String projectPictureUrl;
}
