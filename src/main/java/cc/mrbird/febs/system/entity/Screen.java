package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 截屏记录保持表
 * </p>
 *
 * @author Macw
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_screen")
public class Screen implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 截屏表主键
     */
    @TableId(value = "screen_id",type = IdType.AUTO)
    private Integer screenId;

    /**
     * 用户id
     */
    private Integer userId;


    /**
     * 课程id
     */
    private Integer courseId;

    /**
     * 实验项目id
     */
    private Integer projectId;

    /**
     * 截屏图片
     */
    private String pictureUrl;

    /**
     * 截屏时间
     */
    @JsonFormat(pattern = "yyyy:MM:dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField(exist = false)
    private List<String> imgList;

}
