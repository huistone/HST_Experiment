package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 课程班级表
 * </p>
 *
 * @author Macw
 * @since 2020-03-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_course_dept")
public class CourseDept implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程班级表ID
     */
    @TableId(value = "course_dept_id", type = IdType.AUTO)
    private Integer courseDeptId;

    /**
     * 课程ID
     */
    private Integer courseId;

    /**
     * 班级表ID
     */
    private Integer deptId;


}
