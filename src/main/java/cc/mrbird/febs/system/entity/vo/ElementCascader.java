package cc.mrbird.febs.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @ Auther: 马超伟
 * @ Date: 2020/05/18/16:39
 * @ Description: element 级联组件实体类
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElementCascader{
    private Long value;
    private String label;
    private List<ElementCascader> children;
}

