package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数电主板使用记录表
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_det_record")
public class DetRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数电主板使用记录表
     */
    @TableId(value = "det_record_id", type = IdType.AUTO)
    private Integer detRecordId;

    /**
     * 使用人id
     */
    private Long userId;

    /**
     * 主板id
     */
    private Integer detId;

    /**
     * 1占用；0释放
     */
    private Integer isUse;

    /**
     * 实验次数
     */
    private Integer time;

    /**
     * 实验开始时间
     */
    private LocalDateTime startTime;

    /**
     * 实验结束时间
     */
    private LocalDateTime endTime;

    @TableField(exist = false)
    private String trueName;

    @TableField(exist = false)
    private String detName;



}
