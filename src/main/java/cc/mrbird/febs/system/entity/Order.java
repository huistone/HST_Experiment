package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author Macw
 * @since 2020-11-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    @TableId(value = "order_id", type = IdType.AUTO)
    private Long orderId;

    /**
     * 下单人id
     */
    private Long customerId;

    /**
     * 支付宝交易号/微信交易号
     */
    private String tradeNo;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 订单价格
     */
    private BigDecimal orderPrice;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 支付时间
     */
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "GMT+8")
    private LocalDateTime payTime;

    /**
     * 支付方式：1支付宝 0 微信
     */
    private Integer paymentMethod;

    /**
     * 下单人用户名
     */
    private String username;

    /**
     * 课程名称
     */
    private String goodsName;

    /**
     * 1是课程 0是单个项目
     */
    private Integer type;

    /**
     * 1 支付成功 2 待支付 0支付失败 3订单不可用或超时
     */
    private Integer payStatus;

    /**
     * 商品描述
     */
    private String goodsDetail;

    /**
     * 下单手机号
     */
    private String telephone;

    /**
     * 商品id
     */
    private Integer goodsId;


    /**
     * 商品图片
     */
    private String goodsPictureUrl;


}
