package cc.mrbird.febs.system.entity.vo;

import cc.mrbird.febs.system.entity.ExperimentProject;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CourseScreenVo
 * @Description TODO
 * @Author admin
 * @Date 2020/12/31 17:25
 * @Version 1.0
 */
@Data
public class CourseScreenVo {

    private Integer courseId;

    private String courseName;

    private List<ExperimentProject> children;

}
