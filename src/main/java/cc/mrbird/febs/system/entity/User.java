package cc.mrbird.febs.system.entity;

import cc.mrbird.febs.common.annotation.IsMobile;
import cc.mrbird.febs.common.converter.TimeConverter;
import cc.mrbird.febs.student.entity.UserCommit;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 马超伟
 */
@Data
@TableName("t_user")
@Excel("用户信息表")
public class User implements Serializable {

    private static final long serialVersionUID = -4352868070794165001L;

    // 用户状态：有效
    public static final String STATUS_VALID = "1";
    // 用户状态：锁定
    public static final String STATUS_LOCK = "0";
    // 默认头像
    public static final String DEFAULT_AVATAR = "http://file.huistone.com/avatar/2020/04/30/18bdd331447d4855ac2799343fc3cc30/default.jpg";
    // 默认密码
    public static final String DEFAULT_PASSWORD = "123456";
    // 性别男
    public static final String SEX_MALE = "0";
    // 性别女
    public static final String SEX_FEMALE = "1";
    // 性别保密
    public static final String SEX_UNKNOW = "2";
    // 黑色主题
    public static final String THEME_BLACK = "black";
    // 白色主题
    public static final String THEME_WHITE = "white";
    // TAB开启
    public static final String TAB_OPEN = "1";
    // TAB关闭
    public static final String TAB_CLOSE = "0";

    /**
     * 用户 ID
     */
    @TableId(value = "USER_ID", type = IdType.AUTO)
    private Long userId;

    /**
     * 用户名
     */
    @TableField("USERNAME")
    @Size(min = 4, max = 18, message = "{range}")
    private String username;

    /**
     * 密码
     */
    @TableField("PASSWORD")
    private String password;

    /**
     * 学号/工号
     */
    @TableField("ID_NUMBER")
    @Size(min = 3, max = 12, message = "{range}")
    @ExcelField(value = "学号/工号")
    private String idNumber;

    /**
     * 部门 ID
     */
    @TableField("DEPT_ID")
    private Long deptId;

    /**
     * 课程 ID
     */
    @TableField("COURSE_ID")
    private Long courseId;

    /**
     * 真实姓名
     */
    @TableField("TRUE_NAME")
    @ExcelField(value = "真实姓名")
    private String trueName;

    /**
     * 用户昵称
     */
    @TableField("NICKNAME")
    private String nickname;

    /**
     * 邮箱
     */
    @TableField("EMAIL")
    @Size(max = 50, message = "{noMoreThan}")
    @Email(message = "{email}")
//    @ExcelField(value = "邮箱")
    private String email;

    /**
     * 联系电话
     */
    @TableField("MOBILE")
    @IsMobile(message = "{mobile}")
    @ExcelField(value = "联系电话")
    private String mobile;

    /**
     * 状态 0锁定 1有效
     */
    @TableField("STATUS")
    @NotBlank(message = "{required}")
    @ExcelField(value = "状态", writeConverterExp = "0=锁定,1=有效")
    private String status;

    @TableField("im_status")
    private String imStatus;


    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    //@ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("MODIFY_TIME")
    //@ExcelField(value = "修改时间", writeConverter = TimeConverter.class)
    private Date modifyTime;

    /**
     * 最近访问时间
     */
    @TableField("LAST_LOGIN_TIME")
    //@ExcelField(value = "最近访问时间", writeConverter = TimeConverter.class)
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "GMT+8")
    private Date lastLoginTime;

    /**
     * 性别 0男 1女 2 保密
     */
    @TableField("SSEX")
    @NotBlank(message = "{required}")
    @ExcelField(value = "性别", writeConverterExp = "0=男,1=女,2=保密")
    private String sex;

    /**
     * 头像
     */
    @TableField("AVATAR")
    private String avatar;

    /**
     * 主题
     */
    @TableField("THEME")
    private String theme;

    /**
     * 是否开启 tab 0开启，1关闭
     */
    @TableField("IS_TAB")
    private String isTab;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    @Size(max = 100, message = "{noMoreThan}")
    private String description;

    /**
     * 部门名称
     */
    @TableField(exist = false)
    @ExcelField(value = "班级")
    private String deptName;

    /**
     * 部门ID数组列表
     */

    @TableField(exist = false)
    private List deptIds;

    @TableField(exist = false)
    private String createTimeFrom;


    @TableField(exist = false)
    private String createTimeTo;

    /**
     * 角色 ID
     */
//    @NotBlank(message = "{required}")
    @TableField(exist = false)
    private Long roleId;

    /**
     * 角色
     */
//    @ExcelField(value = "角色")
    @TableField(exist = false)
    private String roleName;


    /**
     * 普通用户的标识，对当前开发者帐号唯一
     */
    private String openid;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 国家
     */
    private String country;

    /**
     * 用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。
     */
    private String unionid;

    /**
     * 1代表企业版 0代表校园版
     */
    private Integer userType;

    /**
     * 1正常，0删除
     */
//    @TableLogic
    private Integer isDel;

    /**
     * 院级名称
     */
    @TableField(exist = false)
    private String firstDeptName;

    /**
     * 专业名称
     */
    @TableField(exist = false)
    private String secondDeptName;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String thirdDeptName;

    public Long getId() {
        return userId;
    }

    @TableField(exist = false)
    private String teacherName;

    @TableField(exist = false)
    private ExperimentRemark prepareScore;

    /**
     * 思考题成绩实体
     */
    @TableField(exist = false)
    private ExperimentRemark thinkScore;

    /**
     * 实验成绩实体
     */
    @TableField(exist = false)
    private UserCommit projectScore;

    /**
     * 总分
     */
    @TableField(exist = false)
    private Double scoreSum;

    /**
     * 每个实验的总分
     */
    @TableField(exist = false)
    private Double scoreRow;

    /**
     * 课程名称
     *
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 实验名称
     */
    @TableField(exist = false)
    private String projectName;

    @TableField(exist = false)
    private Double preScore;

    @TableField(exist = false)
    private Double thScore;

    @TableField(exist = false)
    private Double proScore;

    @TableField(exist = false)
    private Integer preCommit;

    @TableField(exist = false)
    private Integer thCommit;

    @TableField(exist = false)
    private Integer proCommit;

    @TableField(exist = false)
    private Integer proCommitId;

    @TableField(exist = false)
    private Integer preRemarkId;

    @TableField(exist = false)
    private Integer thRemarkId;


}
