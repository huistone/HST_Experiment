package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-04-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_help")
public class Help implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "help_id", type = IdType.AUTO)
    private Integer helpId;

    /**
     * 帮助 题号
     */
    private Integer number;

    /**
     * 帮助名称
     */
    private String helpName;

    /**
     * 帮助详情
     */
    private String helpContext;

    /**
     * 帮助父ID
     */
    private Integer parentId;

    /**
     * 创建人ID
     */
    private Integer createId;

    /**
     * 所属课程ID
     */
    private Integer courseId;

    /**
     * 所属课程ID
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 创建人name
     */
    private String createName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人ID
     */
    private Integer updateId;

    /**
     * 更新人name
     */
    private String updateName;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 二级菜单
     */
    @TableField(exist = false)
    private List<Help> helps;
}
