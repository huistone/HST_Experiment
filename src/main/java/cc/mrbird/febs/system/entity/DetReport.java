package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-08-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_det_report")
public class DetReport implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数电报告记录表主键
     */
    @TableId(value = "det_report_id", type = IdType.AUTO)
    private Long detReportId;

    /**
     * 分数
     */
    private Double detReportSum;

    /**
     * json串
     */
    private String detReportJson;


}
