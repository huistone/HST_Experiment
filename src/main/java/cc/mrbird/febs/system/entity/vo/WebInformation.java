package cc.mrbird.febs.system.entity.vo;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class WebInformation {

        private String companyName;
        private MultipartFile logo;
        private MultipartFile ico;
        /**
         * 微信二维码
         */
        private MultipartFile WeChat;
        private MultipartFile pic1;
        private MultipartFile pic2;
        private MultipartFile pic3;
        private String pic1addr;
        private String pic2addr;
        private String pic3addr;
        private String telephoneNumber;
        private String QQ1;
        private String QQ2;
        private String QQ3;
        private String email;
        private String officialWebsite;
        private String address;
        private String copyright;
}
