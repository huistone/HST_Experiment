package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 实验题目 评语表
 * </p>
 *
 * @author Macw
 * @since 2020-07-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_experiment_remark")
public class ExperimentRemark implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "remark_id", type = IdType.AUTO)
    private Long remarkId;

    /**
     * 评语
     */
    private String remark;

    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 提交人ID
     */
    private Long createId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后得分
     */
    private Double score;

    /**
     * 课程所属班级ID
     */
    private Long deptId;

    /**
     * 评分时间
     */
    private LocalDateTime scoreTime;

    /**
     * 评分人
     */
    private String scoreUser;

    /**
     * 1预习题，2思考题
     */
    private Integer type;


    /**
     * 0未提交 1已提交，2已批阅
     */
    private Integer isCommit;

    /**
     * 习题的题型 1判断 2简答 3单选  4多选
     */
    private Integer questionType;

    /**
     * 学生姓名
     */
    @TableField(exist = false)
    private String stuName;


    /**
     * 学号
     */
    @TableField(exist = false)
    private String idNumber;

    /**
     * 性别 0男 1女 2 保密
     */
    @TableField(exist = false)
    private String sex;

    /**
     * 习题所属的项目名称
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 1代表校园 0代表企业
     */
    private Integer userType;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    private Integer isDel;


}
