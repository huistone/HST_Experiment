package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 课程表
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_course")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    @TableId(value = "course_id", type = IdType.AUTO)
    private Long courseId;

    /**
     * 课程名称
     */
    private String courseName;


    /**
     * 英文名称
     */
    @TableField(value = "english_name")
    private String englishName;

    /**
     * 课程内容
     */
    private String courseContent;

    /**
     * 课程头图
     */
    private String coursePictureUrl;

    /**
     * 排序字段，小的靠前
     */
    private String courseSort;

    /**
     * 删除状态[0:停用, 1:有效]
     */
    private Integer state;

    /**
     * 操作人
     */
    private String createUser;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy:MM:dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 整理后的时间格式
     *  @TableField(exist = false) ：意思为不和数据库中的字段进行映射
     */
    @TableField(exist = false)
    private String createTimes;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy:MM:dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

//    /**
//     * 课程价格
//     */
//    private BigDecimal coursePrice;

    @TableField(exist = false)
    private Integer isCollect;

    /**
     * 1正常，0删除
     */
    @TableLogic
    private Integer isDel;

    @TableField(exist = false)
    private List<ExperimentProject> children;

    @TableField(exist = false)
    private String teacherName;

}
