package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数电主板信息表
 * </p>
 *
 * @author Macw
 * @since 2020-08-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_det_base")
public class DetBase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数字电子技术
     */
    @TableId(value = "det_id", type = IdType.AUTO)
    private Integer detId;

    /**
     * 主板名称
     */
    private String detName;

    /**
     * 串口号
     */
    private String detUsb;

    /**
     *  USB-Blaster
     */
    private String detBlaster;

    /**
     * 订阅通道
     */
    private String subChannel;

    /**
     * 发布通道
     */
    private String pubChannel;

    /**
     * 1可用，0无效
     */
    private Integer status;

    /**
     * 使用状态 1占用 0空闲
     */
    private Integer useStatus;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime  createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
