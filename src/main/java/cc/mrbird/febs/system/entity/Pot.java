package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * POT1
 * </p>
 *
 * @author Macw
 * @since 2020-12-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_pot")
public class Pot implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "pot_id", type = IdType.INPUT)
    private Integer potId;


    @TableField("V1E")
    private Double v1e;

    @TableField("V1B")
    private Double v1b;

    @TableField("V1C")
    private Double v1c;

    @TableField("IB")
    private Double ib;

    @TableField("IC")
    private Double ic;

    private LocalDateTime updateTime;


}
